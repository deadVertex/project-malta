#!/bin/bash -e
CC=clang++
CFLAGS="-g -O0 -DPLATFORM_LINUX"

mkdir -p build

# Create game library
$CC $CFLAGS src/game.cpp -o build/game_temp.so -shared -rdynamic -fPIC
mv build/game_temp.so build/game.so
rm -f build/game_temp.so


# Create game executable
$CC $CFLAGS src/main.cpp -o build/project-malta -lglfw -lGLEW -lGL -ldl
