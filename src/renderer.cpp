#include "renderer.h"

struct RendererState
{
    Shader shaders[MAX_SHADERS];
    u32 textures[MAX_TEXTURES];
    VertexBuffer vertexBuffers[MAX_VERTEX_BUFFERS];

    Framebuffer framebuffers[MAX_FRAMEBUFFERS];
    UniformBuffer uniformBuffers[MAX_UNIFORM_BUFFERS];
};

internal void InitializeRenderCommandQueue(RenderCommandQueue *queue,
        void *buffer, u32 capacity)
{
    queue->data = buffer;
    queue->length = 0;
    queue->capacity = capacity;
}

internal void ClearRenderCommandQueue(RenderCommandQueue *queue)
{
    ClearToZero(queue->data, queue->length);
    queue->length = 0;
}

inline void *ReadDataFromQueue_(
    RenderCommandQueue queue, u32 *cursor, u32 length)
{
    void *result = (u8 *)queue.data + *cursor;
    Assert(*cursor + length <= queue.length);
    *cursor += length;

    return result;
}

#define ReadFromRenderCommandQueue(QUEUE, CURSOR, TYPE) \
    (TYPE *)ReadDataFromQueue_(QUEUE, CURSOR, sizeof(TYPE))

#define GLSL_VERSION_STRING "#version 330 core\n"

internal b32 CompileShader(
    GLuint shader, const char *source, u32 type, const char **defines, u32 defineCount)
{
    const char *typeNames[MAX_SHADER_TYPES];
    typeNames[ShaderType_Vertex] = "Vertex";
    typeNames[ShaderType_Fragment] = "Fragment";
    typeNames[ShaderType_Geometry] = "Geometry";

    const char *typeDefines[MAX_SHADER_TYPES];
    typeDefines[ShaderType_Vertex]   = "#define VERTEX_SHADER\n";
    typeDefines[ShaderType_Fragment] = "#define FRAGMENT_SHADER\n";
    typeDefines[ShaderType_Geometry] = "#define GEOMETRY_SHADER\n";

    Assert(type < MAX_SHADER_TYPES);

    const char *sources[64] = {};
    sources[0] = GLSL_VERSION_STRING;
    sources[1] = typeDefines[type];
    sources[2] = "#line 1\n";
    u32 sourceCount = 3;

    for (u32 i = 0; i < defineCount; ++i)
    {
        Assert(sourceCount < ArrayCount(sources));
        sources[sourceCount++] = defines[i];
    }
    sources[sourceCount++] = source;

    glShaderSource(shader, sourceCount, sources, NULL);
    glCompileShader(shader);
    i32 status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    i32 length;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
    char buffer[4096];
    if (length > 0)
    {
        Assert(length < sizeof(buffer));
        glGetShaderInfoLog(shader, sizeof(buffer), NULL, buffer);
        LogMessage("%s shader compiler log:\n%s", typeNames[type], buffer);
    }

    return (status == GL_TRUE);
}

internal b32 LinkShaderProgram(GLuint program, GLuint *shaders, u32 count)
{
    for (u32 shaderIndex = 0; shaderIndex < count; shaderIndex++)
    {
        glAttachShader(program, shaders[shaderIndex]);
    }

    glLinkProgram(program);
    i32 status;
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    i32 length;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
    char buffer[4096];
    if (length > 0)
    {
        Assert(length < sizeof(buffer));
        glGetProgramInfoLog(program, sizeof(buffer), NULL, buffer);
        LogMessage("Shader program linker log:\n%s", buffer);
    }

    return (status == GL_TRUE);
}

internal void DeleteShader(GLuint program)
{
    Assert(program != 0);
    glUseProgram(0);
    GLsizei count = 0;
    GLuint shaders[MAX_SHADER_TYPES];
    // Need to retrieve the vertex and fragment shaders and delete them
    // explicitly, deleting the shader program only detaches the two
    // shaders, it does not free the resources for them.
    glGetAttachedShaders(program, ArrayCount(shaders), &count, shaders);
    glDeleteProgram(program);
    for (i32 i = 0; i < count; ++i)
    {
        glDeleteShader(shaders[i]);
    }
}

internal u32 CreateShader(const char *source, u32 stages, const char **defines, u32 defineCount)
{
    u32 shaderProgram = 0;

    u32 shaders[MAX_SHADER_TYPES] = {};
    u32 count = 0;

    u32 openglShaderEnums[MAX_SHADER_TYPES];
    openglShaderEnums[ShaderType_Vertex]   = GL_VERTEX_SHADER;
    openglShaderEnums[ShaderType_Fragment] = GL_FRAGMENT_SHADER;
    openglShaderEnums[ShaderType_Geometry] = GL_GEOMETRY_SHADER;


    b32 success = true;
    for (u32 i = ShaderType_Vertex; i < MAX_SHADER_TYPES; ++i)
    {
        if (stages & Bit(i))
        {
            u32 shader = glCreateShader(openglShaderEnums[i]);
            if (CompileShader(shader, source, i, defines, defineCount))
            {
                shaders[count++] = shader;
            }
            else
            {
                success = false;
                break;
            }
        }
    }

    if (success)
    {
        // Create shader program
        shaderProgram = glCreateProgram();
        b32 linkSuccess = LinkShaderProgram(shaderProgram, shaders, count);
    }
    else
    {
        for (u32 i = 0; i < count; ++i)
        {
            glDeleteShader(shaders[i]);
        }
    }

    return shaderProgram;
}

internal b32 InitializeShader(Shader *shader, const char *source, u32 stages,
    const char **defines, u32 defineCount)
{
    b32 result = false;
    shader->program = CreateShader(source, stages, defines, defineCount);
    if (shader->program != 0)
    {
        glUseProgram(shader->program);

        u32 uboCount = 0;
        b32 allFound = true;
        for (u32 uniformIndex = 0; uniformIndex < shader->uniformCount;
                ++uniformIndex)
        {
            ShaderUniform *uniform = shader->uniforms + uniformIndex;
            if (uniform->type == UniformType_Buffer)
            {
                i32 uboIdx = glGetUniformBlockIndex(shader->program, uniform->name);
                if (uboIdx >= 0)
                {
                    u32 uniformLocation = uboCount++;
                    glUniformBlockBinding(shader->program, uboIdx, uniformLocation);
                    uniform->location = uniformLocation;
                }
                else
                {
                    uniform->location = -1;
                }
            }
            else
            {
                uniform->location =
                    glGetUniformLocation(shader->program, uniform->name);
            }
            if (uniform->location == -1)
            {
                LogMessage("No uniform with the name %s found in shader %s",
                    uniform->name, shader->name);
                allFound = false;
            }
        }

        result = allFound;
    }

    return result;
}

internal b32 BindShader(RendererState *state, Shader *shader, UniformValue *values, u32 count)
{
    b32 result = true;
    glUseProgram(shader->program);

    if (shader->depthWriteEnabled)
    {
        glDepthMask(GL_TRUE);
    }
    else
    {
        glDepthMask(GL_FALSE);
    }

    if (shader->depthTestEnabled)
    {
        glEnable(GL_DEPTH_TEST);
    }
    else
    {
        glDisable(GL_DEPTH_TEST);
    }

    if (shader->alphaBlendingEnabled)
    {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }
    else
    {
        glDisable(GL_BLEND);
    }

    if (shader->faceCullingMode != FaceCulling_None)
    {
        glEnable(GL_CULL_FACE);
        switch (shader->faceCullingMode)
        {
            case FaceCulling_BackFace:
                glCullFace(GL_BACK);
                break;
            case FaceCulling_FrontFace:
                glCullFace(GL_FRONT);
                break;
            default:
                InvalidCodePath();
                break;
        }
    }
    else
    {
        glDisable(GL_CULL_FACE);
    }

    if (shader->program != 0)
    {
        u32 textureBindingCount = 0;
        for (u32 uniformIndex = 0; uniformIndex < shader->uniformCount;
             ++uniformIndex)
        {
            ShaderUniform *uniform = shader->uniforms + uniformIndex;

            b32 found = false;
            for (u32 valueIndex = 0; valueIndex < count; ++valueIndex)
            {
                UniformValue *value = values + valueIndex;
                if (uniform->id == value->id)
                {
                    if (uniform->type == value->type)
                    {
                        switch (uniform->type)
                        {
                        case UniformType_Vec2:
                            glUniform2fv(uniform->location, 1, value->v2.data);
                            break;
                        case UniformType_Vec3:
                            glUniform3fv(uniform->location, 1, value->v3.data);
                            break;
                        case UniformType_Vec4:
                            glUniform4fv(uniform->location, 1, value->v4.data);
                            break;
                        case UniformType_Mat4:
                            glUniformMatrix4fv(
                                uniform->location, 1, GL_FALSE, value->m4.data);
                            break;
                        case UniformType_Texture2D:
                        {
                            Assert(value->texture < MAX_TEXTURES);
                            u32 texture = state->textures[value->texture];
                            glActiveTexture(GL_TEXTURE0 + textureBindingCount);
                            glBindTexture(GL_TEXTURE_2D, texture);
                            glUniform1i(uniform->location, textureBindingCount);
                            textureBindingCount++;
                            break;
                        }
                        case UniformType_TextureCubeMap:
                        {
                            Assert(value->texture < MAX_TEXTURES);
                            u32 texture = state->textures[value->texture];
                            glActiveTexture(GL_TEXTURE0 + textureBindingCount);
                            glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
                            glUniform1i(uniform->location, textureBindingCount);
                            textureBindingCount++;
                            break;
                        }
                        case UniformType_Float:
                            glUniform1f(uniform->location, value->f);
                            break;
                        case UniformType_Buffer:
                        {
                            Assert(value->buffer < MAX_UNIFORM_BUFFERS);
                            u32 ubo = state->uniformBuffers[value->buffer].ubo;
                            glBindBufferBase(GL_UNIFORM_BUFFER, uniform->location, ubo);
                            break;
                        }
                        default:
                            InvalidCodePath();
                            break;
                        }
                    }
                    else
                    {
                        LogMessage(
                            "Wrong uniform value type, expected %u was %u, %u:%s",
                            uniform->type, value->type, value->id, uniform->name);
                        result = false;
                    }
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                LogMessage(
                    "Missing uniform value %u: %s", uniform->id, uniform->name);
                result = false;
            }
        }
    }
    else
    {
        LogMessage("Invalid shader");
        result = false;
    }

    return result;
}

// TODO: Better buffer usage hints
internal void InitializeVertexBuffer(VertexBuffer *buffer,
    u32 vertexDataCapacity, u32 indexDataCapacity, b32 isDynamic,
    u32 vertexLayout, u32 instanceDataCapacity)
{
    glGenVertexArrays(1, &buffer->vao);
    glBindVertexArray(buffer->vao);

    glGenBuffers(1, &buffer->vbo);
    glBindBuffer(GL_ARRAY_BUFFER, buffer->vbo);
    glBufferData(GL_ARRAY_BUFFER, vertexDataCapacity, 0,
        isDynamic ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW);
    buffer->vertexDataCapacity = vertexDataCapacity;

    buffer->indexDataCapacity = indexDataCapacity;
    if (indexDataCapacity > 0)
    {
        glGenBuffers(1, &buffer->ibo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexDataCapacity, 0,
            isDynamic ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW);
    }

    buffer->instanceDataCapacity = instanceDataCapacity;
    if (instanceDataCapacity > 0)
    {
        glGenBuffers(1, &buffer->instanceDataBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, buffer->instanceDataBuffer);
        glBufferData(GL_ARRAY_BUFFER, instanceDataCapacity, 0,
            isDynamic ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW);
    }

    glBindBuffer(GL_ARRAY_BUFFER, buffer->vbo);
    switch (vertexLayout)
    {
    case VertexLayout_VertexInstanced:
    case VertexLayout_Vertex:
        Assert((u64)OffsetOf(position, Vertex) == 0);
        Assert((u64)OffsetOf(normal, Vertex) == 12);
        Assert((u64)OffsetOf(tangent, Vertex) == 24);
        Assert((u64)OffsetOf(bitangent, Vertex) == 36);
        Assert((u64)OffsetOf(textureCoordinates, Vertex) == 48);

        // Position
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
            OffsetOf(position, Vertex));

        // Normal
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
            OffsetOf(normal, Vertex));

        // Tangent
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
            OffsetOf(tangent, Vertex));

        // Bitangent
        glEnableVertexAttribArray(3);
        glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
            OffsetOf(bitangent, Vertex));

        // Texture coordinates
        glEnableVertexAttribArray(4);
        glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
            OffsetOf(textureCoordinates, Vertex));

        if (vertexLayout == VertexLayout_VertexInstanced)
        {
            glBindBuffer(GL_ARRAY_BUFFER, buffer->instanceDataBuffer);
            glEnableVertexAttribArray(5);
            glVertexAttribPointer(
                5, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(vec4), (void *)0);
            glEnableVertexAttribArray(6);
            glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(vec4),
                (void *)(sizeof(vec4)));
            glEnableVertexAttribArray(7);
            glVertexAttribPointer(7, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(vec4),
                (void *)(2 * sizeof(vec4)));
            glEnableVertexAttribArray(8);
            glVertexAttribPointer(8, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(vec4),
                (void *)(3 * sizeof(vec4)));

            glVertexAttribDivisor(5, 1);
            glVertexAttribDivisor(6, 1);
            glVertexAttribDivisor(7, 1);
            glVertexAttribDivisor(8, 1);
        }
        break;
    case VertexLayout_VertexPC:
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexPC),
            OffsetOf(position, VertexText));
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(VertexPC),
            OffsetOf(color, VertexPC));
        glEnableVertexAttribArray(1);
        break;
    case VertexLayout_Text:
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(VertexText),
            OffsetOf(position, VertexText));
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(VertexText),
            OffsetOf(color, VertexText));
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, sizeof(VertexText),
            OffsetOf(textureCoordinates, VertexText));
        glEnableVertexAttribArray(4);
        break;
    default:
        InvalidCodePath();
        break;
    }
    glBindVertexArray(0);
}

internal void DeleteVertexBuffer(VertexBuffer *vertexBuffer)
{
    glBindVertexArray(0);
    glDeleteVertexArrays(1, &vertexBuffer->vao);
    glDeleteBuffers(1, &vertexBuffer->vbo);
    if (vertexBuffer->ibo > 0)
    {
        glDeleteBuffers(1, &vertexBuffer->ibo);
    }
}

internal void InitializeFramebuffer(RendererState *state,
    Framebuffer *framebuffer, u32 colorTextureIndex, u32 depthTextureIndex,
    u32 bufferWidth, u32 bufferHeight)
{
    Assert(framebuffer->fbo == 0);
    glGenFramebuffers(1, &framebuffer->fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer->fbo);

    framebuffer->width = bufferWidth;
    framebuffer->height = bufferHeight;
    framebuffer->colorTextureIndex = colorTextureIndex;
    framebuffer->depthTextureIndex = depthTextureIndex;

    Assert(colorTextureIndex < MAX_TEXTURES);
    Assert(depthTextureIndex < MAX_TEXTURES);
    Assert(state->textures[colorTextureIndex] == 0);
    Assert(state->textures[depthTextureIndex] == 0);

    u32 colorTexture = 0;
    u32 depthTexture = 0;
    glGenTextures(1, &colorTexture);
    glGenTextures(1, &depthTexture);

    state->textures[colorTextureIndex] = colorTexture;
    state->textures[depthTextureIndex] = depthTexture;

    glBindTexture(GL_TEXTURE_2D, colorTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, bufferWidth, bufferHeight, 0,
        GL_RGB, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glFramebufferTexture2D(
        GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorTexture, 0);

    glBindTexture(GL_TEXTURE_2D, depthTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, bufferWidth,
        bufferHeight, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    f32 borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);  
    glFramebufferTexture2D(
        GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTexture, 0);

    Assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

internal void DeinitializeFramebuffer(RendererState *state, Framebuffer *framebuffer)
{
    Assert(framebuffer->fbo != 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    Assert(framebuffer->colorTextureIndex < MAX_TEXTURES);
    Assert(framebuffer->depthTextureIndex < MAX_TEXTURES);
    u32 colorTexture = state->textures[framebuffer->colorTextureIndex];
    u32 depthTexture = state->textures[framebuffer->depthTextureIndex];

    Assert(colorTexture != 0);
    Assert(depthTexture != 0);

    glDeleteFramebuffers(1, &framebuffer->fbo);
    glDeleteTextures(1, &colorTexture);
    glDeleteTextures(1, &depthTexture);

    state->textures[framebuffer->colorTextureIndex] = 0;
    state->textures[framebuffer->depthTextureIndex] = 0;
    framebuffer->fbo = 0;
}

// TODO: Expose usage hint to support static uniform buffers
internal UniformBuffer CreateUniformBuffer(u32 capacity)
{
    UniformBuffer result = {};
    result.capacity = capacity;
    glGenBuffers(1, &result.ubo);
    glBindBuffer(GL_UNIFORM_BUFFER, result.ubo);
    glBufferData(GL_UNIFORM_BUFFER, capacity, NULL, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    return result;
}

internal void DeleteUniformBuffer(UniformBuffer *buffer)
{
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    glDeleteBuffers(1, &buffer->ubo);
}

internal void DeleteTexture(u32 texture)
{
    glDeleteTextures(1, &texture);
}

struct ColorFormat
{
    u32 internalFormat;
    u32 format;
    u32 type;
};

inline ColorFormat GetColorFormat(u32 colorFormat)
{
    ColorFormat result = {};

    switch (colorFormat)
    {
        case ColorFormat_RGBA8:
            result.internalFormat = GL_RGBA8;
            result.format = GL_RGBA;
            result.type = GL_UNSIGNED_BYTE;
            break;
        case ColorFormat_SRGBA8:
            result.internalFormat = GL_SRGB8_ALPHA8;
            result.format = GL_RGBA;
            result.type = GL_UNSIGNED_BYTE;
            break;
        case ColorFormat_RGBA32F:
            result.internalFormat = GL_RGBA32F;
            result.format = GL_RGBA;
            result.type = GL_FLOAT;
            break;
        case ColorFormat_R32F:
            result.internalFormat = GL_R32F;
            result.format = GL_RED;
            result.type = GL_FLOAT;
            break;
        default:
            InvalidCodePath();
            break;
    }

    return result;
}

internal u32 CreateTexture(u32 width, u32 height, const void *pixels,
    u32 colorFormat, u32 samplerMode, u32 wrappingMode)
{
    u32 texture = 0;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    u32 glWrapMode = wrappingMode == TextureWrappingMode_Clamp
                         ? GL_CLAMP_TO_EDGE
                         : GL_REPEAT;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, glWrapMode);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, glWrapMode);

    ColorFormat format = GetColorFormat(colorFormat);

    glTexImage2D(GL_TEXTURE_2D, 0, format.internalFormat, width, height, 0,
        format.format, format.type, pixels);

    if (samplerMode == TextureSamplerMode_Nearest)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    }
    else if (samplerMode == TextureSamplerMode_Linear)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }
    else if (samplerMode == TextureSamplerMode_LinearMipMapping)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glGenerateMipmap(GL_TEXTURE_2D);

        // TODO: Make this configurable
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16);
    }
    else
    {
        InvalidCodePath();
    }

    glBindTexture(GL_TEXTURE_2D, 0);

    return texture;
}

internal u32 CreateCubeMap(const void *pixels[CUBE_MAP_FACE_COUNT], u32 width, u32 colorFormat)
{
    u32 texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    ColorFormat format = GetColorFormat(colorFormat);

    // +Z
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0,
        format.internalFormat, width, width, 0, format.format,
        format.type, pixels[CUBE_MAP_FACE_Z_POS]);

    // -Z
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0,
        format.internalFormat, width, width, 0, format.format,
        format.type, pixels[CUBE_MAP_FACE_Z_NEG]);

    // +Y
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0,
        format.internalFormat, width, width, 0, format.format,
        format.type, pixels[CUBE_MAP_FACE_Y_POS]);

    // -Y
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0,
        format.internalFormat, width, width, 0, format.format,
        format.type, pixels[CUBE_MAP_FACE_Y_NEG]);

    // +X
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0,
        format.internalFormat, width, width, 0, format.format,
        format.type, pixels[CUBE_MAP_FACE_X_POS]);

    // -X
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0,
        format.internalFormat, width, width, 0, format.format,
        format.type, pixels[CUBE_MAP_FACE_X_NEG]);

    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    return texture;
}

internal u32 CreateCubeMapMipMap(
    CubeMapMipLevel *levels, u32 levelCount, u32 colorFormat)
{
    u32 texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_LEVEL, levelCount - 1);

    ColorFormat format = GetColorFormat(colorFormat);

    for (u32 mipLevel = 0; mipLevel < levelCount; ++mipLevel)
    {
        CubeMapMipLevel *level = levels + mipLevel;

        u32 width = level->width;

        // +Z
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, mipLevel,
                format.internalFormat, width, width, 0, format.format,
                format.type, level->pixels[CUBE_MAP_FACE_Z_POS]);

        // -Z
        glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, mipLevel,
                format.internalFormat, width, width, 0, format.format,
                format.type, level->pixels[CUBE_MAP_FACE_Z_NEG]);

        // +Y
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, mipLevel,
                format.internalFormat, width, width, 0, format.format,
                format.type, level->pixels[CUBE_MAP_FACE_Y_POS]);

        // -Y
        glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, mipLevel,
                format.internalFormat, width, width, 0, format.format,
                format.type, level->pixels[CUBE_MAP_FACE_Y_NEG]);

        // +X
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, mipLevel,
                format.internalFormat, width, width, 0, format.format,
                format.type, level->pixels[CUBE_MAP_FACE_X_POS]);

        // -X
        glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, mipLevel,
                format.internalFormat, width, width, 0, format.format,
                format.type, level->pixels[CUBE_MAP_FACE_X_NEG]);
    }

    return texture;
}

internal void OpenGLReportErrorMessage(GLenum source, GLenum type, GLuint id,
    GLenum severity, GLsizei length, const GLchar *message,
    const void *userParam)
{
    const char *typeStr = NULL;
    switch (type)
    {
    case GL_DEBUG_TYPE_ERROR:
        typeStr = "ERROR";
        break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
        typeStr = "DEPRECATED_BEHAVIOR";
        break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
        typeStr = "UNDEFINED_BEHAVIOR";
        break;
    case GL_DEBUG_TYPE_PORTABILITY:
        typeStr = "PORTABILITY";
        break;
    case GL_DEBUG_TYPE_PERFORMANCE:
        typeStr = "PERFORMANCE";
        break;
    case GL_DEBUG_TYPE_OTHER:
        typeStr = "OTHER";
        break;
    default:
        typeStr = "";
        break;
    }

    const char *severityStr = nullptr;
    switch (severity)
    {
    case GL_DEBUG_SEVERITY_LOW:
        severityStr = "LOW";
        break;
    case GL_DEBUG_SEVERITY_MEDIUM:
        severityStr = "MEDIUM";
        break;
    case GL_DEBUG_SEVERITY_HIGH:
        severityStr = "HIGH";
        break;
    default:
        severityStr = "";
        break;
    }

    LogMessage("OPENGL|%s:%s:%s", typeStr, severityStr, message);

    Assert(type != GL_DEBUG_TYPE_ERROR);
}

internal void InitRenderer(RendererState *state)
{
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(OpenGLReportErrorMessage, NULL);
    GLuint unusedIds = 0;
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0,
                          &unusedIds, GL_TRUE);
    glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
}

internal void ProcessRenderCommands(
    RendererState *state, RenderCommandQueue queue)
{
    u32 cursor = 0;
    while (cursor < queue.length)
    {
        RenderCommandHeader *header =
            ReadFromRenderCommandQueue(queue, &cursor, RenderCommandHeader);

        switch (header->type)
        {
            case RenderCommand_ClearTypeId:
            {
                RenderCommand_Clear *params = ReadFromRenderCommandQueue(
                    queue, &cursor, RenderCommand_Clear);
                vec4 color = params->color;
                glClearColor(color.r, color.g, color.b, color.a);
                glEnable(GL_DEPTH_TEST);
                glDepthMask(GL_TRUE);
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
                break;
            }
            case RenderCommand_DrawVertexBufferTypeId:
            {
                RenderCommand_DrawVertexBuffer *params = ReadFromRenderCommandQueue(
                    queue, &cursor, RenderCommand_DrawVertexBuffer);

                Assert(params->shaderInput.shader < MAX_SHADERS);
                Shader *shader = state->shaders + params->shaderInput.shader;
                if (!BindShader(state, shader, params->shaderInput.values,
                        params->shaderInput.valueCount))
                {
                    LogMessage("Error encountered when binding shader %s (%u)",
                        shader->name, params->shaderInput.shader);
                }

                u32 primitive = 0;
                switch (params->primitive)
                {
                    case Primitive_Triangles:
                        primitive = GL_TRIANGLES;
                        break;
                    case Primitive_Lines:
                        primitive = GL_LINES;
                        break;
                    default:
                        InvalidCodePath();
                        break;
                }

                Assert(params->id < MAX_VERTEX_BUFFERS);
                VertexBuffer *vertexBuffer = state->vertexBuffers + params->id;
                if (params->instanceCount > 0)
                {
                    // FIXME: Support non-index mesh for instancing
                    Assert(vertexBuffer->indexDataCapacity > 0);

                    glBindVertexArray(vertexBuffer->vao);

                    u64 offset = params->first * sizeof(u32);
                    glDrawElementsInstanced(primitive, params->count,
                        GL_UNSIGNED_INT, (const void *)offset,
                        params->instanceCount);
                }
                else
                {
                    glBindVertexArray(vertexBuffer->vao);
                    if (vertexBuffer->indexDataCapacity > 0)
                    {
                        u64 offset = params->first * sizeof(u32);
                        glDrawElements(primitive, params->count,
                            GL_UNSIGNED_INT, (const void *)offset);
                    }
                    else
                    {
                        glDrawArrays(primitive, params->first, params->count);
                    }
                }
                glBindVertexArray(0);

                break;
            }
            case RenderCommand_CreateTextureTypeId:
            {
                RenderCommand_CreateTexture *params =
                    ReadFromRenderCommandQueue(
                        queue, &cursor, RenderCommand_CreateTexture);

                Assert(params->id < MAX_TEXTURES);
                u32 texture = state->textures[params->id];
                if (texture != 0)
                {
                    DeleteTexture(texture);
                }

                state->textures[params->id] =
                    CreateTexture(params->width, params->height, params->pixels,
                        params->colorFormat, params->samplerMode, params->wrappingMode);
                break;
            }
            case RenderCommand_CreateShaderTypeId:
            {
                RenderCommand_CreateShader *params = ReadFromRenderCommandQueue(
                    queue, &cursor, RenderCommand_CreateShader);

                Assert(params->id < MAX_SHADERS);
                Shader *shader = state->shaders + params->id;
                if (shader->program != 0)
                {
                    DeleteShader(shader->program);
                }

                *shader = params->shader;
                InitializeShader(shader, params->source, params->stages,
                    params->defines, params->defineCount);
                break;
            }
            case RenderCommand_CreateVertexBufferTypeId:
            {
                RenderCommand_CreateVertexBuffer *params =
                    ReadFromRenderCommandQueue(
                        queue, &cursor, RenderCommand_CreateVertexBuffer);

                Assert(params->id < MAX_VERTEX_BUFFERS);
                VertexBuffer *vertexBuffer = state->vertexBuffers + params->id;
                if (vertexBuffer->vbo != 0)
                {
                    DeleteVertexBuffer(vertexBuffer);
                }

                InitializeVertexBuffer(vertexBuffer, params->length,
                    params->indicesLength, params->isDynamic,
                    params->vertexLayout, params->instanceDataLength);
                break;
            }
            case RenderCommand_UpdateVertexBufferTypeId:
            {
                RenderCommand_UpdateVertexBuffer *params =
                    ReadFromRenderCommandQueue(queue, &cursor,
                            RenderCommand_UpdateVertexBuffer);
                Assert(params->id < MAX_VERTEX_BUFFERS);
                VertexBuffer *vertexBuffer = state->vertexBuffers + params->id;

                if (params->length > 0)
                {
                    Assert(params->length <= vertexBuffer->vertexDataCapacity);
                    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer->vbo);
                    glBufferSubData(
                        GL_ARRAY_BUFFER, 0, params->length, params->data);
                }

                if (params->indicesLength > 0)
                {
                    Assert(params->indicesLength <=
                           vertexBuffer->indexDataCapacity);
                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexBuffer->ibo);
                    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0,
                        params->indicesLength, params->indexData);
                }

                if (params->instanceDataLength > 0)
                {
                    Assert(params->instanceDataLength <=
                           vertexBuffer->instanceDataCapacity);
                    glBindBuffer(
                        GL_ARRAY_BUFFER, vertexBuffer->instanceDataBuffer);
                    glBufferSubData(GL_ARRAY_BUFFER, 0,
                        params->instanceDataLength, params->instanceData);
                }
                break;
            }
            case RenderCommand_BindFramebufferTypeId:
            {
                RenderCommand_BindFramebuffer *params =
                    ReadFromRenderCommandQueue(
                        queue, &cursor, RenderCommand_BindFramebuffer);

                Assert(params->id < MAX_FRAMEBUFFERS);
                Framebuffer *framebuffer = &state->framebuffers[params->id];
                glBindFramebuffer(GL_FRAMEBUFFER, framebuffer->fbo);
                glViewport(0, 0, framebuffer->width, framebuffer->height);
                break;
            }
            case RenderCommand_CreateFramebufferTypeId:
            {
                RenderCommand_CreateFramebuffer *params =
                    ReadFromRenderCommandQueue(
                        queue, &cursor, RenderCommand_CreateFramebuffer);

                Assert(params->id < MAX_FRAMEBUFFERS);
                Framebuffer *framebuffer = state->framebuffers + params->id;
                if (params->id == Framebuffer_Backbuffer)
                {
                    framebuffer->width = params->width;
                    framebuffer->height = params->height;
                }
                else
                {
                    if (framebuffer->fbo != 0)
                    {
                        DeinitializeFramebuffer(state, framebuffer);
                    }
                    InitializeFramebuffer(state, framebuffer, params->colorTexture,
                            params->depthTexture, params->width, params->height);
                }
                break;
            }
            case RenderCommand_CreateUniformBufferTypeId:
            {
                RenderCommand_CreateUniformBuffer *params =
                    ReadFromRenderCommandQueue(
                        queue, &cursor, RenderCommand_CreateUniformBuffer);

                Assert(params->id < MAX_UNIFORM_BUFFERS);
                UniformBuffer *buffer = &state->uniformBuffers[params->id];
                if (buffer->ubo != 0)
                {
                    DeleteUniformBuffer(buffer);
                }
                *buffer = CreateUniformBuffer(params->capacity);

                break;
            }
            case RenderCommand_UpdateUniformBufferTypeId:
            {
                RenderCommand_UpdateUniformBuffer *params =
                    ReadFromRenderCommandQueue(
                        queue, &cursor, RenderCommand_UpdateUniformBuffer);

                Assert(params->id < MAX_UNIFORM_BUFFERS);
                UniformBuffer *buffer = &state->uniformBuffers[params->id];

                Assert(params->length + params->offset <= buffer->capacity);

                glBindBuffer(GL_UNIFORM_BUFFER, buffer->ubo);
                glBufferSubData(GL_UNIFORM_BUFFER, params->offset,
                    params->length, params->data);
                glBindBuffer(GL_UNIFORM_BUFFER, 0);
                break;
            }
            case RenderCommand_CreateCubeMapTypeId:
            {
                RenderCommand_CreateCubeMap *params =
                    ReadFromRenderCommandQueue(
                        queue, &cursor, RenderCommand_CreateCubeMap);

                Assert(params->id < MAX_TEXTURES);
                u32 texture = state->textures[params->id];
                if (texture != 0)
                {
                    DeleteTexture(texture);
                }

                state->textures[params->id] = CreateCubeMap(
                    params->pixels, params->width, params->colorFormat);
                break;
            }
            case RenderCommand_CreateCubeMapMipMapsTypeId:
            {
                RenderCommand_CreateCubeMapMipMaps *params =
                    ReadFromRenderCommandQueue(
                        queue, &cursor, RenderCommand_CreateCubeMapMipMaps);

                Assert(params->id < MAX_TEXTURES);
                u32 texture = state->textures[params->id];
                if (texture != 0)
                {
                    DeleteTexture(texture);
                }

                state->textures[params->id] = CreateCubeMapMipMap(
                    params->levels, params->levelCount, params->colorFormat);
                break;
            }
            default:
                InvalidCodePath();
                break;
        }
    }
}
