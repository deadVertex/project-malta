#define GAME_LIB "./game.so"

internal void* AllocateMemory(u64 size, u64 baseAddress = 0)
{
    // Memory is cleared to zero by the kernel
    void *result = mmap((void *)baseAddress, size, PROT_READ | PROT_WRITE,
        MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    Assert(result != MAP_FAILED);

    return result;
}

internal void FreeMemory(void *p)
{
    munmap(p, 0);
}

internal b32 ReadFile(i32 fd, void *buf, i32 length)
{
    i32 bytesRead = read(fd, buf, length);
    if (bytesRead < 0)
    {
        LogMessage("Error encountered while reading file. %s.", strerror(errno));
        return false;
    }

    // Should read the entire file in one go as we haven't opened the file
    // as non-blocking.
    Assert(bytesRead == length);
    return true;
}

internal DebugReadEntireFile(ReadEntireFile)
{
    DebugReadFileResult result = {};
    int handle = open(path, O_RDONLY);
    if (handle >= 0)
    {
        struct stat fileStatus;
        if (fstat(handle, &fileStatus) == 0)
        {
            result.length = SafeTruncateU64ToU32(fileStatus.st_size);
            result.contents = Linux_AllocateMemory(result.length);
            if (result.contents != NULL)
            {
                if (!ReadFile(handle, result.contents, result.length))
                {
                    LogMessage("Failed to read file %s", path);
                    Linux_FreeMemory(result.contents);
                    result.contents = NULL;
                    result.length = 0;
                }
            }
            else
            {
                // Allocate memory asserts on failure
                InvalidCodePath();
            }
        }
        else
        {
            LogMessage("Failed to get size of file \"%s\". %s.", path,
                strerror(errno));
        }
        close(handle);
    }
    else
    {
        LogMessage("Failed to open file \"%s\". %s.", path, strerror(errno));
    }

    return result;
}

internal void* GameLibGetProcAddress(GameLibrary *library, const char *name)
{
    void *result = dlsym(library->handle, name);
    if (result == NULL)
    {
        LogMessage("%s", dlerror());
    }

    return result;
}

internal time_t GetFileLastWriteTime(const char *path)
{
    struct stat attr;
    stat(path, &attr);
    return attr.st_mtime;
}

internal b32 LoadGameLibrary(GameLibrary *library, const char *libraryPath)
{
    b32 result = false;
    library->handle = dlopen(libraryPath, RTLD_NOW);
    library->lastWriteTime = GetFileLastWriteTime(libraryPath);
    if (library->handle != NULL)
    {
        library->clientUpdate =
            (GameClientUpdateFunction *)GameLibGetProcAddress(
                library, "GameClientUpdate");

        library->clientFixedUpdate =
            (GameClientFixedUpdateFunction *)GameLibGetProcAddress(
                library, "GameClientFixedUpdate");

        library->serverUpdate =
            (GameServerUpdateFunction *)GameLibGetProcAddress(
                library, "GameServerUpdate");

        if (library->clientUpdate != NULL &&
            library->clientFixedUpdate != NULL && library->serverUpdate != NULL)
        {
            // Successfully loaded game library
            LogMessage("Game library \"%s\" loaded", libraryPath);
            result = true;
        }
        else
        {
            LogMessage("Failed to load game library from \"%s\".", libraryPath);
        }
    }
    else
    {
        LogMessage("Failed to open game library \"%s\". %s.", libraryPath,
               dlerror());
    }

    return result;
}

internal void UnloadGameLibrary(GameLibrary *library)
{
    if (library->handle != NULL)
    {
        dlclose(library->handle);
        ClearToZero(library, sizeof(library));
    }
}


internal b32 WasGameCodeModified(GameLibrary *library)
{
    b32 result = false;
    time_t newWriteTime = GetFileLastWriteTime(GAME_LIB);
    if (newWriteTime != gameLibrary.lastWriteTime)
    {
        result = true;
    }
    return result;
}
