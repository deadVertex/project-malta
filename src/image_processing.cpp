inline vec2 SampleSphericalMap(vec3 v)
{
    vec2 uv = Vec2(atan2(v.z, v.x), asin(v.y));
    uv.x *= 0.1591f; // FIXME: Where have these numbers come from?!?!
    uv.y *= 0.3183f;
    uv += Vec2(0.5f);
    return uv;
}

vec3 GetSampleDir(f32 fx, f32 fy, u32 faceIndex)
{
    switch (faceIndex)
    {
        case CUBE_MAP_FACE_Z_POS:
            return Vec3(fx, fy, -0.5f);
        case CUBE_MAP_FACE_Z_NEG:
            return Vec3(-fx, fy, 0.5f);
        case CUBE_MAP_FACE_Y_POS:
            return Vec3(fx, 0.5f, fy);
        case CUBE_MAP_FACE_Y_NEG:
            return Vec3(fx, -0.5f, -fy);
        case CUBE_MAP_FACE_X_POS:
            return Vec3(0.5f, fy, fx);
        case CUBE_MAP_FACE_X_NEG:
            return Vec3(-0.5f, fy, -fx);
        default:
            InvalidCodePath();
            return Vec3(0);
    }
}

inline vec4 SampleImageRGBA32F(f32 *pixels, u32 width, u32 height, u32 x, u32 y)
{
    x = MinU32(x, width - 1);
    y = MinU32(y, height - 1);

    vec4 *pixelsV4 = (vec4 *)pixels;
    return pixelsV4[y * width + x];
}

inline vec4 SampleNearestRGBA32F(f32 *pixels, u32 width, u32 height, f32 u, f32 v)
{
    Assert(u >= 0.0f && u <= 1.0f);
    Assert(v >= 0.0f && v <= 1.0f);
    f32 sampleU = u * (width - 1);
    f32 sampleV = v * (height - 1);
    i32 x = (i32)Ceil(sampleU);
    i32 y = (i32)Ceil(sampleV);

    return SampleImageRGBA32F(pixels, width, height, x, y);
}

