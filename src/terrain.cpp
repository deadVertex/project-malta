#include "perlin.h"

#include "terrain.h"

internal void InitializeTerrain(GameAssets *assets, GameMemory *memory,
    MemoryArena *terrainArena, b32 createTextures)
{
    ClearMemoryArena(terrainArena);

    // Terrain heightmap
    u32 heightMapWidth = TERRAIN_HEIGHTMAP_SIZE;
    u32 heightMapHeight = TERRAIN_HEIGHTMAP_SIZE;
    HeightMap heightMap =
        GenerateHeightMap(terrainArena, heightMapWidth, heightMapHeight);
    assets->heightMaps[HeightMap_Terrain] = heightMap;

    if (createTextures)
    {
        RenderCommand_CreateTexture *createTexture = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateTexture);
        createTexture->id = Texture_HeightMap;
        createTexture->width = heightMapWidth;
        createTexture->height = heightMapHeight;
        createTexture->colorFormat = ColorFormat_R32F;
        createTexture->wrappingMode = TextureWrappingMode_Clamp;
        createTexture->samplerMode = TextureSamplerMode_LinearMipMapping;
        createTexture->pixels = heightMap.values;

        TerrainNormalMap normalMap =
            GenerateTerrainNormalMap(heightMap, terrainArena);
        RenderCommand_CreateTexture *createNormalMap = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateTexture);
        createNormalMap->id = Texture_TerrainNormalMap;
        createNormalMap->width = normalMap.width;
        createNormalMap->height = normalMap.height;
        createNormalMap->colorFormat = ColorFormat_RGBA8;
        createNormalMap->samplerMode = TextureSamplerMode_LinearMipMapping;
        createNormalMap->pixels =
            ConvertNormalMapToRGBA8(normalMap, terrainArena);
    }
    LogMessage("Terrain arena size: %u/%u KB", terrainArena->size / 1024,
        terrainArena->capacity / 1024);
}

inline f32 SampleHeightMap(HeightMap heightMap, u32 x, u32 y)
{
    x = MinU32(x, heightMap.width - 1);
    y = MinU32(y, heightMap.height - 1);
    return heightMap.values[y * heightMap.width + x];
}

// TODO: BUG: Doesn't seem to be clamping to edges properly
// TODO: I think we had a bug in this code that is fixed on the PBR branch
inline f32 HeightMapSampleBilinear(HeightMap heightMap, f32 u, f32 v)
{
    f32 s = u * heightMap.width - 0.5f;
    f32 t = v * heightMap.height - 0.5f;

    i32 x0 = (i32)Floor(s);
    i32 y0 = (i32)Floor(t);
    i32 x1 = x0 + 1;
    i32 y1 = y0 + 1;

    f32 fx = s - x0;
    f32 fy = t - y0;

    f32 samples[4];
    samples[0] = SampleHeightMap(heightMap, x0, y0);
    samples[1] = SampleHeightMap(heightMap, x1, y0);
    samples[2] = SampleHeightMap(heightMap, x0, y1);
    samples[3] = SampleHeightMap(heightMap, x1, y1);

    f32 p = Lerp(samples[0], samples[1], fx);
    f32 q = Lerp(samples[2], samples[3], fx);

    f32 result = Lerp(p, q, fy);
    return result;
}

// NOTE: With quad tree rendering terrain origin is bottom left not center!
inline f32 SampleTerrainAtWorldPosition(
    HeightMap heightMap, vec3 terrainPosition, vec3 terrainScale, f32 x, f32 z)
{
    f32 minx = terrainPosition.x;
    f32 maxx = terrainPosition.x + terrainScale.x;
    f32 minz = terrainPosition.z;
    f32 maxz = terrainPosition.z + terrainScale.z;

    x = Clamp(x, minx, maxx);
    z = Clamp(z, minz, maxz);
    f32 u = MapToUnitRange(x, minx, maxx);
    f32 v = MapToUnitRange(z, minz, maxz);

    // Flip Y axis
    //v = 1.0f - v;

    f32 height = HeightMapSampleBilinear(heightMap, u, v);

    height = terrainPosition.y + height * terrainScale.y;

    return height;
}

// NOTE: With quad tree rendering terrain origin is bottom left not center!
inline vec3 GetHeightMapWorldPosition(
    HeightMap heightMap, f32 u, f32 v, vec3 terrainPosition, vec3 terrainScale)
{
    f32 y = HeightMapSampleBilinear(heightMap, u, v);

    vec3 result = Vec3(u, y, v);
    result = Hadamard(result, terrainScale) + terrainPosition;

    return result;
}

// NOTE: With quad tree rendering terrain origin is bottom left not center!
internal RaycastResult RayIntersectHeightMap(
    HeightMap heightMap, vec3 position, vec3 scale, vec3 start, vec3 end)
{
    RaycastResult closestResult = {};

    vec3 dir = end - start;

#define GRID_DIM 8192

    f32 lowX = Min(start.x, end.x) - 0.5f;
    f32 lowZ = Min(start.z, end.z) - 0.5f;
    f32 upperX = Max(start.x, end.x) + 0.5f;
    f32 upperZ = Max(start.z, end.z) + 0.5f;

    f32 minx = position.x;
    f32 maxx = position.x + scale.x;
    f32 minz = position.z;
    f32 maxz = position.z + scale.z;

    lowX = Clamp(lowX, minx, maxx);
    lowZ = Clamp(lowZ, minz, maxz);
    upperX = Clamp(upperX, minx, maxx);
    upperZ = Clamp(upperZ, minz, maxz);

    f32 x0 = MapToUnitRange(lowX, minx, maxx);
    f32 x1 = MapToUnitRange(upperX, minx, maxx);
    f32 z0 = MapToUnitRange(lowZ, minz, maxz);
    f32 z1 = MapToUnitRange(upperZ, minz, maxz);

    u32 minCol = (u32)Floor(x0 * (f32)GRID_DIM);
    u32 maxCol = (u32)Ceil(x1 * (f32)GRID_DIM);
    u32 minRow = (u32)Floor(z0 * (f32)GRID_DIM);
    u32 maxRow = (u32)Ceil(z1 * (f32)GRID_DIM);

    Assert(maxRow - minRow >= 1);
    Assert(maxCol - minCol >= 1);

    for (u32 row = minRow; row < maxRow; ++row)
    {
        for (u32 col = minCol; col < maxCol; ++col)
        {
            f32 u0 = (f32)col / (f32)GRID_DIM;
            f32 v0 = (f32)row / (f32)GRID_DIM;
            f32 u1 = (f32)(col + 1) / (f32)GRID_DIM;
            f32 v1 = (f32)(row + 1) / (f32)GRID_DIM;

            vec3 a = GetHeightMapWorldPosition(heightMap, u0,
                    v0, position, scale);
            vec3 b = GetHeightMapWorldPosition(heightMap, u1,
                    v0, position, scale);
            vec3 c = GetHeightMapWorldPosition(heightMap, u1,
                    v1, position, scale);
            vec3 d = GetHeightMapWorldPosition(heightMap, u0,
                    v1, position, scale);

            //DrawTriangle(c, a, b, Vec4(0, 1, 0, 1));
            //DrawTriangle(d, a, c, Vec4(0, 1, 0, 1));

            RaycastResult r0 =
                RayIntersectTriangle(c, a, b, start, end);
            RaycastResult r1 =
                RayIntersectTriangle(d, a, c, start, end);

            if (r1.tmin > 0.0f)
            {
                if (r0.tmin <= 0.0f || r1.tmin < r0.tmin)
                {
                    r0 = r1;
                }
            }

            if (r0.tmin > 0.0f)
            {
                if (closestResult.tmin <= 0.0f || r0.tmin < closestResult.tmin)
                {
                    closestResult = r0;
                }
            }
        }
    }

    return closestResult;
}

internal void DebugDrawTerrain(EntityWorld *world, GameAssets *assets)
{
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(
        world, TerrainComponentTypeId, entities, ArrayCount(entities));

    // Render terrain entities
    for (u32 entityIndex = 0; entityIndex < entityCount; ++entityIndex)
    {
        EntityId entity = entities[entityIndex];

        // TODO: Support caching this query
        TransformComponent transform;
        ecs_GetComponent(world, TransformComponent, &entity, 1, &transform);

        TerrainComponent terrain;
        ecs_GetComponent(world, TerrainComponent, &entity, 1, &terrain);

        HeightMap heightMap = GetHeightMap(assets, terrain.heightMap);

        // TODO: Cache model matrix
        mat4 model = Translate(transform.position) *
                     Scale(transform.scale);

        u32 rowCount = 8;
        u32 colCount = 8;
        for (u32 y = 0; y < rowCount; ++y)
        {
            for (u32 x = 0; x < colCount; ++x)
            {
                f32 fx = (f32)x / (f32)colCount;
                f32 fy = (f32)y / (f32)rowCount;

                f32 sampleScale = 256.0f;
                f32 sampleX = (fx - 0.5f) * sampleScale;
                f32 sampleZ = (fy - 0.5f) * sampleScale;

                f32 height = SampleTerrainAtWorldPosition(heightMap,
                    transform.position, transform.scale, sampleX, sampleZ);

                DrawPoint(Vec3(sampleX, height, sampleZ), 1.0f, Vec4(0, 1, 0, 1), 0.01f);
            }
        }
    }
}

struct BuildTerrainQuadTreeStackFrame
{
    TerrainQuadTreeNode *node;
    u32 depth;
};

internal TerrainQuadTree BuildTerrainQuadTree(u32 maxDepth, MemoryArena *arena)
{
    TerrainQuadTree tree = {};

    BuildTerrainQuadTreeStackFrame stack[1024];
    u32 stackSize = 0;

    TerrainQuadTreeNode *root = AllocateStruct(arena, TerrainQuadTreeNode);
    tree.root = root;
    root->center = Vec2(0.5f);
    root->size = 1.0f;

    stack[0].node = root;
    stack[0].depth = 0;
    stackSize++;

    while (stackSize > 0)
    {
        BuildTerrainQuadTreeStackFrame stackFrame = stack[--stackSize];
        Assert(stackFrame.node);

        TerrainQuadTreeNode *node = stackFrame.node;
        f32 halfSize = node->size * 0.5f;

        vec2 childOffsets[4] = {
            Vec2(-1, -1), Vec2(1, -1), Vec2(1, 1), Vec2(-1, 1)};

        if (stackFrame.depth < maxDepth)
        {
            node->isLeaf = false;
            for (u32 childIndex = 0; childIndex < 4; ++childIndex)
            {
                TerrainQuadTreeNode *child =
                    AllocateStruct(arena, TerrainQuadTreeNode);

                child->size = halfSize;
                vec2 offset = Vec2(halfSize * 0.5f);
                offset.x *= childOffsets[childIndex].x;
                offset.y *= childOffsets[childIndex].y;

                child->center = node->center + offset;
                node->children[childIndex] = child;

                child->isLeaf = false;
                BuildTerrainQuadTreeStackFrame newFrame = {};
                newFrame.node = child;
                newFrame.depth = stackFrame.depth + 1;
                stack[stackSize++] = newFrame;
            }
        }
        else
        {
            node->isLeaf = true;
        }
    }

    return tree;
}

struct RenderTerrainQuadTreeStackFrame
{
    TerrainQuadTreeNode *node;
    u32 depth;
};

internal void RenderTerrainQuadTree(RenderCommandQueue *renderCommandQueue,
    GameAssets *assets, mat4 viewProjection, mat4 shadowViewProjection,
    vec3 terrainPosition, f32 scale, TerrainQuadTree tree, vec2 testP)
{
    RenderTerrainQuadTreeStackFrame stack[1024];
    u32 stackSize = 0;

    stack[0].node = tree.root;
    stack[0].depth = 0;
    stackSize = 1;

    f32 maxDistanceSq[8]; // 10.0f, 2.0f
    for (u32 i = 0; i < 8; ++i)
    {
        u32 pow2 = 1 << i;
        maxDistanceSq[i] = (1.0f / (f32)pow2) * scale;
    }

    while (stackSize > 0)
    {
        RenderTerrainQuadTreeStackFrame stackFrame = stack[--stackSize];

        u32 depth = stackFrame.depth;
        TerrainQuadTreeNode *node = stackFrame.node;
        f32 minX = (node->center.x - node->size * 0.5f) * scale;
        f32 maxX = (node->center.x + node->size * 0.5f) * scale;
        f32 minZ = (node->center.y - node->size * 0.5f) * scale;
        f32 maxZ = (node->center.y + node->size * 0.5f) * scale;

        vec3 position = terrainPosition + Vec3(node->center.x, 0, node->center.y) * scale;
#if 0
        DrawPoint(position, 0.2f, Vec4(1, 1, 0, 1));

        vec3 min = position - Vec3(node->size * 0.5f) * scale;
        vec3 max = position + Vec3(node->size * 0.5f) * scale;
        f32 heights[4];
        heights[0] = SampleTerrainAtWorldPosition(
            GetHeightMap(assets, HeightMap_Terrain), terrainPosition,
            Vec3(scale), min.x, min.z);
        heights[1] = SampleTerrainAtWorldPosition(
            GetHeightMap(assets, HeightMap_Terrain), terrainPosition,
            Vec3(scale), max.x, min.z);
        heights[2] = SampleTerrainAtWorldPosition(
            GetHeightMap(assets, HeightMap_Terrain), terrainPosition,
            Vec3(scale), max.x, max.z);
        heights[3] = SampleTerrainAtWorldPosition(
            GetHeightMap(assets, HeightMap_Terrain), terrainPosition,
            Vec3(scale), min.x, max.z);

        min.y = Min(Min(Min(heights[0], heights[1]), heights[2]), heights[3]);
        max.y = Max(Max(Min(heights[0], heights[1]), heights[2]), heights[3]);

        DrawBox(min, max, Vec4(0, 1, 0, 1));
#endif

        b32 drawNode = true;
        if (!node->isLeaf)
        {
            Assert(stackFrame.depth < ArrayCount(maxDistanceSq));
            f32 dist = LengthSq(testP - Vec2(position.x, position.z));
            
            f32 nodeScale = node->size * scale;
            f32 maxDist = maxDistanceSq[depth] + nodeScale * nodeScale;
            if (dist <= maxDist)
            {
                for (u32 childIndex = 0; childIndex < 4; ++childIndex)
                {
                    // TODO: Test nodes
                    RenderTerrainQuadTreeStackFrame newFrame;
                    newFrame.node = node->children[childIndex];
                    newFrame.depth = stackFrame.depth + 1;
                    Assert(newFrame.node);
                    stack[stackSize++] = newFrame;
                }

                drawNode = false;
            }
        }

        if (drawNode)
        {
            vec3 scaleV3 = Vec3(node->size * scale);
            scaleV3.y = scale;
            mat4 model = Translate(position) * Scale(scaleV3);

            RenderCommand_DrawVertexBuffer *drawTerrain =
                RenderCommandQueuePush(
                    renderCommandQueue, RenderCommand_DrawVertexBuffer);
            SetMesh(drawTerrain, assets, Mesh_Patch);

            // TODO: Use a terrain material
            drawTerrain->shaderInput.shader = Shader_Terrain;
            PushUniformTexture2D(&drawTerrain->shaderInput,
                UniformValue_Texture, Texture_Ground003);
            PushUniformFloat(
                &drawTerrain->shaderInput, UniformValue_Roughness, 0.95f);
            PushUniformFloat(
                &drawTerrain->shaderInput, UniformValue_Metallic, 0.0f);
            PushUniformBuffer(&drawTerrain->shaderInput,
                UniformValue_LightingData, UniformBuffer_LightingData);
            PushUniformTextureCubeMap(&drawTerrain->shaderInput,
                UniformValue_IrradianceCubeMap, Texture_Irradiance);
            PushUniformTextureCubeMap(&drawTerrain->shaderInput,
                UniformValue_RadianceCubeMap, Texture_Radiance);
            PushUniformTexture2D(&drawTerrain->shaderInput,
                UniformValue_BrdfLut, Texture_BrdfLut);
            PushUniformMat4(&drawTerrain->shaderInput,
                UniformValue_ViewProjectionMatrix, viewProjection);
            PushUniformMat4(
                &drawTerrain->shaderInput, UniformValue_ModelMatrix, model);
            SetEnvironment(&drawTerrain->shaderInput, shadowViewProjection,
                Texture_ShadowMap);
            //PushUniformVec2(&drawTerrain->shaderInput,
                //UniformValue_TextureScale, Vec2(node->size) * scale * 0.1f);
            PushUniformVec2(&drawTerrain->shaderInput,
                UniformValue_TextureScale, Vec2(0.1, 0.1));

            // TODO: Fetch height and normal maps based on TerrainComponent
            PushUniformTexture2D(&drawTerrain->shaderInput,
                UniformValue_HeightMap, Texture_HeightMap);
            PushUniformTexture2D(&drawTerrain->shaderInput,
                UniformValue_TerrainNormalMap, Texture_TerrainNormalMap);

            PushUniformVec2(&drawTerrain->shaderInput, UniformValue_UVScale,
                Vec2(node->size));
            vec2 textureOffset = node->center - Vec2(node->size * 0.5f);
            PushUniformVec2(&drawTerrain->shaderInput, UniformValue_UVOffset,
                textureOffset);
        }
    }
}

internal void DrawTerrainCollision(GameAssets *assets, vec3 terrainPosition, vec3 terrainScale)
{
    HeightMap heightMap = GetHeightMap(assets, HeightMap_Terrain);
    u32 gridDim = 16;
    for (u32 y = 0; y <= gridDim; ++y)
    {
        for (u32 x = 0; x <= gridDim; ++x)
        {
            f32 fx = (f32)x / (f32)gridDim;
            f32 fy = (f32)y / (f32)gridDim;

            f32 worldX = terrainPosition.x + fx * terrainScale.x;
            f32 worldZ = terrainPosition.z + fy * terrainScale.z;
            f32 worldY = SampleTerrainAtWorldPosition(
                heightMap, terrainPosition, terrainScale, worldX, worldZ);

            vec3 p0 = Vec3(worldX, worldY, worldZ);
            vec3 p1 = p0;
            p0.y -= 2.0f;
            p1.y += 2.0f;
            DrawLine(p0, p1, Vec4(1, 0, 0, 1));
        }
    }
}

internal void RenderGrass(RenderCommandQueue *renderCommandQueue,
    GameAssets *assets, mat4 viewProjection, mat4 shadowViewProjection,
    vec3 terrainPosition, vec3 terrainScale, vec2 cameraP,
    MemoryArena *tempArena)
{
    HeightMap heightMap = GetHeightMap(assets, HeightMap_Terrain);
    u32 gridDim = GRASS_GRID_DIM;
    u32 instanceCount = gridDim * gridDim;
    mat4 *modelMatrices = AllocateArray(tempArena, mat4, instanceCount);
    for (u32 y = 0; y <= gridDim; ++y)
    {
        for (u32 x = 0; x <= gridDim; ++x)
        {
            f32 fx = (f32)x / (f32)gridDim;
            f32 fy = (f32)y / (f32)gridDim;

            fx -= 0.5f;
            fy -= 0.5f;

            f32 worldX = Floor(cameraP.x + fx * gridDim);
            f32 worldZ = Floor(cameraP.y + fy * gridDim);
            f32 worldY = SampleTerrainAtWorldPosition(
                heightMap, terrainPosition, terrainScale, worldX, worldZ);

            mat4 model = Translate(Vec3(worldX, worldY, worldZ)) * Scale(Vec3(0.5f));
            modelMatrices[y * gridDim + x] = model;

#if 0
            RenderCommand_DrawVertexBuffer *drawGrass =
                RenderCommandQueuePush(
                    renderCommandQueue, RenderCommand_DrawVertexBuffer);
            SetMesh(drawGrass, assets, Mesh_Grass);
            SetMaterial(&drawGrass->shaderInput, assets, Material_Grass,
                viewProjection, model);
            SetEnvironment(&drawGrass->shaderInput, shadowViewProjection,
                Texture_ShadowMap);
#endif

#if 0
            vec3 p0 = Vec3(worldX, worldY, worldZ);
            vec3 p1 = p0 + Vec3(0, 0.5, 0);
            DrawLine(p0, p1, Vec4(0, 1, 1, 1));
#endif
        }
    }

    RenderCommand_UpdateVertexBuffer *updateInstanceData =
        RenderCommandQueuePush(
            renderCommandQueue, RenderCommand_UpdateVertexBuffer);
    updateInstanceData->id = VertexBuffer_Grass;
    updateInstanceData->instanceDataLength = sizeof(mat4) * instanceCount;
    updateInstanceData->instanceData = modelMatrices;

    RenderCommand_DrawVertexBuffer *drawMesh = RenderCommandQueuePush(
        renderCommandQueue, RenderCommand_DrawVertexBuffer);
    SetMesh(drawMesh, assets, Mesh_GrassInstanced);
    Material *material = GetMaterial(assets, Material_GrassInstanced);
    drawMesh->shaderInput = material->shaderInput;
    SetEnvironment(&drawMesh->shaderInput, shadowViewProjection,
        Texture_ShadowMap);
    PushUniformMat4(&drawMesh->shaderInput, UniformValue_ViewProjectionMatrix,
        viewProjection);
    drawMesh->instanceCount = instanceCount;
}

// NOTE: Terrain origin is bottom left corner rather than center!
internal void RenderTerrain(EntityWorld *world, GameAssets *assets,
    RenderCommandQueue *renderCommandQueue, mat4 viewProjection,
    mat4 shadowViewProjection, TerrainQuadTree tree, vec3 cameraP,
    MemoryArena *tempArena)
{
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(
        world, TerrainComponentTypeId, entities, ArrayCount(entities));

    // Render terrain entities
    for (u32 entityIndex = 0; entityIndex < entityCount; ++entityIndex)
    {
        EntityId entity = entities[entityIndex];

        TransformComponent transform;
        ecs_GetComponent(world, TransformComponent, &entity, 1, &transform);

        RenderTerrainQuadTree(renderCommandQueue, assets, viewProjection,
            shadowViewProjection, transform.position, transform.scale.x, tree,
            Vec2(cameraP.x, cameraP.z));

#if 0
        DrawTerrainCollision(assets, transform.position, transform.scale);
#endif
        RenderGrass(renderCommandQueue, assets, viewProjection,
            shadowViewProjection, transform.position, transform.scale,
            Vec2(cameraP.x, cameraP.z), tempArena);
    }
}
