internal void SpawnParticleSystem(ParticleSystem *system,
    RandomNumberGenerator *rng, vec3 origin, vec3 normal)
{
    u32 spawnCount = 5 + XorShift32(rng) % 15;
    for (u32 particleIndex = 0; particleIndex < spawnCount; ++particleIndex)
    {
        ParticleState *particle = system->particles + particleIndex;
        particle->position = origin;

        vec3 acceleration =
            normal * 4.6f + Vec3(RandomBilateral(rng), RandomBilateral(rng),
                                RandomBilateral(rng)) *
                                1.3f;
        particle->velocity = acceleration;
        particle->lifeTime = 0.5f * RandomUnilateral(rng);
    }

    system->particleCount = spawnCount;
}

internal void UpdateParticleSystem(ParticleSystem *system, f32 dt)
{
    u32 particleIndex = 0;
    while (particleIndex < system->particleCount)
    {
        ParticleState *particle = system->particles + particleIndex;
        particle->lifeTime -= dt;
        particle->velocity += -particle->velocity * 0.8f * dt;
        particle->velocity += Vec3(0, -10, 0) * dt;
        particle->position += particle->velocity * dt;
        if (particle->lifeTime <= 0.0f)
        {
            *particle = system->particles[--system->particleCount];
        }
        else
        {
            particleIndex++;
        }
    }
}

internal void DrawParticleSystem(ParticleSystem *system)
{
    for (u32 particleIndex = 0; particleIndex < system->particleCount;
            ++particleIndex)
    {
        ParticleState *particle = system->particles + particleIndex;
        DrawPoint(particle->position, 0.2f, Vec4(1));
    }
}
