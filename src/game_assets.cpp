#include "asset.cpp"

inline u32 GetColorFormatByteSize(u32 format)
{
    switch (format)
    {
        case ColorFormat_RGBA8:
        case ColorFormat_SRGBA8:
            return 4;
        case ColorFormat_RGBA32F:
            return 16;
        default:
            InvalidCodePath();
            return 0;
    }
}

internal void LoadTextureFromFile(GameMemory *memory, const char *path, u32 textureId)
{
    DebugReadFileResult fileResult = memory->readEntireFile(path);
    Assert(fileResult.contents != NULL);
    ImageFileHeader *header = (ImageFileHeader *)fileResult.contents;
    Assert((header->flags & ImageFileFlag_CubeMap) == 0);
    RenderCommand_CreateTexture *createTexture = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateTexture);
    createTexture->id = textureId;
    createTexture->width = header->width;
    createTexture->height = header->height;

    createTexture->colorFormat = header->colorFormat;
    createTexture->samplerMode = TextureSamplerMode_LinearMipMapping;
    createTexture->pixels = header + 1;

    // TODO: Automatically free file memory once data has been uploaded
    // to the GPU
    // memory->freeFileMemory(fileResult.contents);
}

internal void LoadCubeMapFromFile(GameMemory *memory, const char *path, u32 textureId)
{
    DebugReadFileResult fileResult = memory->readEntireFile(path);
    Assert(fileResult.contents != NULL);
    ImageFileHeader *header = (ImageFileHeader *)fileResult.contents;
    Assert(header->flags & ImageFileFlag_CubeMap);
    Assert(header->width == header->height);
    RenderCommand_CreateCubeMap *createCubeMap = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateCubeMap);
    createCubeMap->id = textureId;
    createCubeMap->width = header->width;
    createCubeMap->colorFormat = header->colorFormat;
    u32 faceSize = header->width * header->width *
        GetColorFormatByteSize(header->colorFormat);

    u8 *pixels = (u8 *)(header + 1);
    for (u32 i = 0; i < CUBE_MAP_FACE_COUNT; ++i)
    {
        createCubeMap->pixels[i] = pixels + faceSize * i;

        // Check that the we're trying to use for the cube map
        // faces is actually contained in the file.
        Assert(((u8 *)createCubeMap->pixels[i] + faceSize) -
                (u8 *)fileResult.contents <=
                fileResult.length);
    }

    // TODO: Automatically free file memory once data has been uploaded
    // to the GPU
    // memory->freeFileMemory(fileResult.contents);
}

internal void LoadMeshFromFile(GameMemory *memory, GameAssets *assets,
    VertexBufferAllocator *vertexBufferAllocator, const char *path, u32 meshId)
{
    DebugReadFileResult fileResult = memory->readEntireFile(path);
    Assert(fileResult.contents != NULL);
    MeshFileHeader *header = (MeshFileHeader *)fileResult.contents;

    void *vertices = header + 1;
    u32 *indices =
        (u32 *)((u8 *)vertices + sizeof(Vertex) * header->vertexCount);
    Assert(header->vertexLayout == VertexLayout_Vertex);
    Assert(meshId < ArrayCount(assets->meshes));
    assets->meshes[meshId] =
        AllocateMesh(vertexBufferAllocator, header->vertexLayout, vertices,
            header->vertexCount, indices, header->indexCount);
    memory->freeFileMemory(fileResult.contents);
}

internal void LoadFontFromFile(GameMemory *memory, GameAssets *assets,
    const char *path, u32 fontId, u32 textureId, MemoryArena *transientArena)
{
    DebugReadFileResult fileResult = memory->readEntireFile(path);
    Assert(fileResult.contents != NULL);
    FontFileHeader *header = (FontFileHeader *)fileResult.contents;

    FontGlyph *glyphs = (FontGlyph *)(header + 1);
    void *pixels = glyphs + header->characterCount;

    RenderCommand_CreateTexture *createTexture = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateTexture);
    createTexture->id = textureId;
    createTexture->width = header->width;
    createTexture->height = header->height;

    createTexture->colorFormat = ColorFormat_RGBA8;
    createTexture->samplerMode = TextureSamplerMode_Linear;
    createTexture->pixels = pixels;

    Assert(fontId < ArrayCount(assets->fonts));
    Font *font = assets->fonts + fontId;
    font->texture = textureId;
    font->glyphCount = header->characterCount;
    font->glyphs = AllocateArray(transientArena, FontGlyph, header->characterCount);
    CopyMemory(
        font->glyphs, glyphs, sizeof(FontGlyph) * header->characterCount);
}

internal void InitializeGrassInstancing(GameAssets *assets, GameMemory *memory)
{
    DebugReadFileResult fileResult = memory->readEntireFile("grass.mesh");
    Assert(fileResult.contents != NULL);
    MeshFileHeader *header = (MeshFileHeader *)fileResult.contents;

    void *vertices = header + 1;
    u32 *indices =
        (u32 *)((u8 *)vertices + sizeof(Vertex) * header->vertexCount);
    Assert(header->vertexLayout == VertexLayout_Vertex);

    RenderCommand_CreateVertexBuffer *createInstancedVertexBuffer =
        RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateVertexBuffer);
    createInstancedVertexBuffer->id = VertexBuffer_Grass;
    createInstancedVertexBuffer->length = header->vertexCount * sizeof(Vertex);
    createInstancedVertexBuffer->indicesLength = header->indexCount * sizeof(u32);
    createInstancedVertexBuffer->instanceDataLength =
        GRASS_GRID_DIM * GRASS_GRID_DIM * sizeof(mat4);
    createInstancedVertexBuffer->isDynamic = true;
    createInstancedVertexBuffer->vertexLayout = VertexLayout_VertexInstanced;

    RenderCommand_UpdateVertexBuffer *updateVertexBuffer =
        RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_UpdateVertexBuffer);
    updateVertexBuffer->id = VertexBuffer_Grass;
    updateVertexBuffer->length = createInstancedVertexBuffer->length;
    updateVertexBuffer->indicesLength = createInstancedVertexBuffer->indicesLength;
    updateVertexBuffer->data = vertices;
    updateVertexBuffer->indexData = indices;

    Mesh mesh = {};
    mesh.vertexBufferId = VertexBuffer_Grass;
    mesh.count = header->indexCount;
    mesh.first = 0;
    mesh.primitive = Primitive_Triangles;
    mesh.vertexLayout = VertexLayout_VertexInstanced;
    assets->meshes[Mesh_GrassInstanced] = mesh;

    // TODO: Allocate temporary memory of the vertices and indices
    //memory->freeFileMemory(fileResult.contents);
}

void InitializeAssets(GameAssets *assets, GameMemory *memory, MemoryArena *transientArena)
{
    {   // Load Radiance cube map mip chain
        const char *files[] = {"radiance_0.texture", "radiance_1.texture",
            "radiance_2.texture", "radiance_3.texture", "radiance_4.texture",
            "radiance_5.texture", "radiance_6.texture"};

        RenderCommand_CreateCubeMapMipMaps *createCubeMap = RenderCommandQueuePush(
                &memory->renderCommandQueue, RenderCommand_CreateCubeMapMipMaps);
        createCubeMap->id = Texture_Radiance;
        createCubeMap->levelCount = ArrayCount(files);
        createCubeMap->colorFormat = ColorFormat_SRGBA8;
        Assert(createCubeMap->levelCount <= ArrayCount(createCubeMap->levels));

        for (u32 mipIndex = 0; mipIndex < ArrayCount(files); ++mipIndex)
        {
            CubeMapMipLevel *mipLevel = createCubeMap->levels + mipIndex;

            const char *path = files[mipIndex];
            DebugReadFileResult fileResult = memory->readEntireFile(path);
            Assert(fileResult.contents != NULL);
            ImageFileHeader *header = (ImageFileHeader *)fileResult.contents;
            Assert(header->flags & ImageFileFlag_CubeMap);
            Assert(header->width == header->height);
            Assert(header->colorFormat == ColorFormat_SRGBA8);


            mipLevel->width = header->width;
            u32 faceSize = header->width * header->width *
                           GetColorFormatByteSize(header->colorFormat);

            u8 *pixels = (u8 *)(header + 1);
            for (u32 i = 0; i < CUBE_MAP_FACE_COUNT; ++i)
            {
                mipLevel->pixels[i] = pixels + faceSize * i;

                // Check that the we're trying to use for the cube map
                // faces is actually contained in the file.
                Assert(((u8 *)mipLevel->pixels[i] + faceSize) -
                           (u8 *)fileResult.contents <=
                       fileResult.length);
            }
        }
    }

    LoadCubeMapFromFile(memory, "skybox_clear.texture", Texture_SkyboxClear);
    LoadCubeMapFromFile(memory, "irradiance.texture", Texture_Irradiance);

    LoadTextureFromFile(memory, "dev_grid512.texture", Texture_DevGrid512);
    LoadTextureFromFile(memory, "grass_alpha.texture", Texture_GrassAlpha);
    LoadTextureFromFile(memory, "PavingStones055_1K_Color.texture", Texture_PavingStonesColor);
    LoadTextureFromFile(memory, "PavingStones055_1K_Normal.texture", Texture_PavingStonesNormal);
    LoadTextureFromFile(memory, "Ground003_1K_Color.texture", Texture_Ground003);
    LoadTextureFromFile(memory, "tree_bark.texture", Texture_TreeBark);
    LoadTextureFromFile(memory, "tree_leaves.texture", Texture_TreeLeaves);
    LoadTextureFromFile(memory, "icons_pistol_ammo.texture", Texture_PistolAmmoIcon);
    LoadTextureFromFile(memory, "icons_pistol.texture", Texture_PistolIcon);
    LoadTextureFromFile(memory, "icons_shotgun.texture", Texture_ShotgunIcon);
    LoadTextureFromFile(memory, "ibl_brdf_lut.texture", Texture_BrdfLut);

    RenderCommand_CreateTexture *createTexture = RenderCommandQueuePush(
        &memory->renderCommandQueue, RenderCommand_CreateTexture);

    createTexture->id = Texture_Checkerboard;
    createTexture->width = 16;
    createTexture->height = 16;
    createTexture->pixels = CreateCheckerboardTexture(
        transientArena, createTexture->width, createTexture->height,
        2, Vec4(0.1, 0.1, 0.1, 1), Vec4(0.05, 0.05, 0.05, 1));

    // clang-format off
        Vertex quadVertices[] = {
            {{-0.5f,  0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
            {{ 0.5f,  0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
            {{ 0.5f, -0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
            {{-0.5f, -0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}}
        };

        u32 quadIndices[] = {
            3, 1, 2, 3, 0, 1
        };

        Vertex cubeVertices[] = {
            // Top
            {{-0.5f, 0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
            {{0.5f, 0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
            {{0.5f, 0.5f, 0.5f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
            {{-0.5f, 0.5f, 0.5f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},

            // Bottom
            {{-0.5f, -0.5f, -0.5f}, {0.0f, -1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
            {{0.5f, -0.5f, -0.5f}, {0.0f, -1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
            {{0.5f, -0.5f, 0.5f}, {0.0f, -1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
            {{-0.5f, -0.5f, 0.5f}, {0.0f, -1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},

            // Back
            {{-0.5f, 0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
            {{0.5f, 0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
            {{0.5f, -0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
            {{-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},

            // Front
            {{-0.5f, 0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
            {{0.5f, 0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
            {{0.5f, -0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
            {{-0.5f, -0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},

            // Left
            {{-0.5f, 0.5f, -0.5f}, {-1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
            {{-0.5f, -0.5f, -0.5f}, {-1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
            {{-0.5f, -0.5f, 0.5f}, {-1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
            {{-0.5f, 0.5f, 0.5f}, {-1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},

            // Right
            {{0.5f, 0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
            {{0.5f, -0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
            {{0.5f, -0.5f, 0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
            {{0.5f, 0.5f, 0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
        };

        u32 cubeIndices[] = {
            2, 1, 0,
            0, 3, 2,

            4, 5, 6,
            6, 7, 4,

            8, 9, 10,
            10, 11, 8,

            14, 13, 12,
            12, 15, 14,

            16, 17, 18,
            18, 19, 16,

            22, 21, 20,
            20, 23, 22
        };
    // clang-format on

    CalculateTangentsAndBitangents(quadVertices, ArrayCount(quadVertices),
        quadIndices, ArrayCount(quadIndices));
    CalculateTangentsAndBitangents(cubeVertices, ArrayCount(cubeVertices),
        cubeIndices, ArrayCount(cubeIndices));

    MeshData sphereData = CreateIcosahedronMesh(2, transientArena);
    u32 totalVboSize = 0x10000 * sizeof(Vertex);
    u32 totalIboSize = 0x10000 * sizeof(u32);

    MeshData patchData = CreatePatchMesh(32, transientArena);

    RenderCommand_CreateVertexBuffer *createVertexBuffer =
        RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateVertexBuffer);
    createVertexBuffer->id = VertexBuffer_Vertex;
    createVertexBuffer->length = totalVboSize;
    createVertexBuffer->indicesLength = totalIboSize;
    createVertexBuffer->isDynamic = false;
    createVertexBuffer->vertexLayout = VertexLayout_Vertex;

    VertexBufferAllocator vertexBufferAllocator = CreateVertexBufferAllocator(
        VertexBuffer_Vertex, VertexLayout_Vertex, totalVboSize, totalIboSize,
        Primitive_Triangles, transientArena);

    assets->meshes[Mesh_Cube] =
        AllocateMesh(&vertexBufferAllocator, VertexLayout_Vertex, cubeVertices,
            ArrayCount(cubeVertices), cubeIndices, ArrayCount(cubeIndices));
    assets->meshes[Mesh_Quad] =
        AllocateMesh(&vertexBufferAllocator, VertexLayout_Vertex, quadVertices,
            ArrayCount(quadVertices), quadIndices, ArrayCount(quadIndices));
    assets->meshes[Mesh_Sphere] = AllocateMesh(&vertexBufferAllocator,
        VertexLayout_Vertex, sphereData.vertices, sphereData.vertexCount,
        sphereData.indices, sphereData.indexCount);
    assets->meshes[Mesh_Patch] = AllocateMesh(&vertexBufferAllocator,
        VertexLayout_Vertex, patchData.vertices, patchData.vertexCount,
        patchData.indices, patchData.indexCount);

    LoadMeshFromFile(memory, assets, &vertexBufferAllocator, "rock01.mesh", Mesh_Rock01);
    LoadMeshFromFile(memory, assets, &vertexBufferAllocator, "tree_body.mesh", Mesh_TreeBody);
    LoadMeshFromFile(memory, assets, &vertexBufferAllocator, "tree_leaves.mesh", Mesh_TreeLeaves);
    LoadMeshFromFile(memory, assets, &vertexBufferAllocator, "pistol.mesh", Mesh_Pistol);
    LoadMeshFromFile(memory, assets, &vertexBufferAllocator, "target.mesh", Mesh_Target);
    LoadMeshFromFile(memory, assets, &vertexBufferAllocator, "warehouse.mesh", Mesh_Warehouse);
    LoadMeshFromFile(memory, assets, &vertexBufferAllocator, "shack01.mesh", Mesh_Shack01);
    LoadMeshFromFile(memory, assets, &vertexBufferAllocator, "shotgun.mesh", Mesh_Shotgun);

    UpdateVertexBuffer(&vertexBufferAllocator, &memory->renderCommandQueue);

    RenderCommand_CreateVertexBuffer *createInstancedVertexBuffer =
        RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateVertexBuffer);
    createInstancedVertexBuffer->id = VertexBuffer_VertexInstanced;
    createInstancedVertexBuffer->length = sphereData.vertexCount * sizeof(Vertex);
    createInstancedVertexBuffer->indicesLength = sphereData.indexCount * sizeof(u32);
    createInstancedVertexBuffer->instanceDataLength = 0x1000;
    createInstancedVertexBuffer->isDynamic = false;
    createInstancedVertexBuffer->vertexLayout = VertexLayout_VertexInstanced;

    VertexBufferAllocator instancedVertexBufferAllocator =
        CreateVertexBufferAllocator(VertexBuffer_VertexInstanced,
            VertexLayout_VertexInstanced, createInstancedVertexBuffer->length,
            createInstancedVertexBuffer->indicesLength, Primitive_Triangles,
            transientArena);
    assets->meshes[Mesh_SphereInstanced] =
        AllocateMesh(&instancedVertexBufferAllocator,
            VertexLayout_VertexInstanced, sphereData.vertices,
            sphereData.vertexCount, sphereData.indices, sphereData.indexCount);

    UpdateVertexBuffer(&instancedVertexBufferAllocator, &memory->renderCommandQueue);

    RenderCommand_CreateVertexBuffer *createDebugDataVertexBuffer =
        RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateVertexBuffer);
    createDebugDataVertexBuffer->id = VertexBuffer_DebugData;
    createDebugDataVertexBuffer->length = POSITION_COLOR_VERTEX_BUFFER_SIZE;
    createDebugDataVertexBuffer->indicesLength = 0;
    createDebugDataVertexBuffer->isDynamic = true;
    createDebugDataVertexBuffer->vertexLayout = VertexLayout_VertexPC;

    RenderCommand_CreateVertexBuffer *createTextVertexBuffer =
        RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateVertexBuffer);
    createTextVertexBuffer->id = VertexBuffer_Text;
    createTextVertexBuffer->length = sizeof(VertexText) * MAX_TEXT_BUFFER_VERTICES;
    createTextVertexBuffer->indicesLength = 0;
    createTextVertexBuffer->isDynamic = true;
    createTextVertexBuffer->vertexLayout = VertexLayout_Text;

    LoadFontFromFile(memory, assets, "inconsolata_regular_32.font",
        Font_InconsolataRegular32, Texture_FontInconsolataRegular32,
        transientArena);

    RenderCommand_CreateFramebuffer *createShadowMap = RenderCommandQueuePush(
        &memory->renderCommandQueue, RenderCommand_CreateFramebuffer);
    createShadowMap->id = Framebuffer_ShadowMap;
    createShadowMap->colorTexture = Texture_ShadowMapDebug;
    createShadowMap->depthTexture = Texture_ShadowMap;
    createShadowMap->width = 2048;
    createShadowMap->height = 2048;

    RenderCommand_CreateUniformBuffer *createLightingData =
        RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateUniformBuffer);
    createLightingData->id = UniformBuffer_LightingData;
    createLightingData->capacity = sizeof(LightingDataUniformBuffer);

    InitializeGrassInstancing(assets, memory);
}

inline void PushDefine(RenderCommand_CreateShader *createShader, const char *define)
{
    Assert(createShader->defineCount < ArrayCount(createShader->defines));
    createShader->defines[createShader->defineCount++] = define;
}
internal void EnableUseTexture(RenderCommand_CreateShader *createShader)
{
    PushDefine(createShader, "#define USE_TEXTURE\n");
    Shader *textureShader = &createShader->shader;
    PushUniform(textureShader, UniformValue_Texture, "albedoTexture",
        UniformType_Texture2D);
    PushUniform(textureShader, UniformValue_TextureScale, "textureScale",
        UniformType_Vec2);
}

internal void EnableLighting(RenderCommand_CreateShader *createShader,
    b32 useRoughnessTexture = false, b32 useMetallicTexture = false)
{
    PushDefine(createShader, "#define CALCULATE_LIGHTING\n");
    if (useRoughnessTexture)
    {
        PushDefine(createShader, "#define USE_ROUGHNESS_TEXTURE\n");
    }
    if (useMetallicTexture)
    {
        PushDefine(createShader, "#define USE_METALLIC_TEXTURE\n");
    }

    Shader *basicShader = &createShader->shader;
    PushUniform(basicShader, UniformValue_ShadowViewProjectionMatrix,
        "shadowViewProjectionMatrix", UniformType_Mat4);
    PushUniform(basicShader, UniformValue_ShadowMap, "shadowMap",
        UniformType_Texture2D);

    if (useRoughnessTexture)
    {
        PushUniform(basicShader, UniformValue_RoughnessTexture,
            "roughnessTexture", UniformType_Texture2D);
    }
    else
    {
        PushUniform(basicShader, UniformValue_Roughness, "roughness",
            UniformType_Float);
    }

    if (useMetallicTexture)
    {
        PushUniform(basicShader, UniformValue_MetallicTexture,
            "metallicTexture", UniformType_Texture2D);
    }
    else
    {
        PushUniform(
            basicShader, UniformValue_Metallic, "metallic", UniformType_Float);
    }

    PushUniform(basicShader, UniformValue_LightingData,
        "LightingDataUniformBuffer", UniformType_Buffer);
    PushUniform(basicShader, UniformValue_IrradianceCubeMap,
        "irradianceCubeMap", UniformType_TextureCubeMap);
    PushUniform(basicShader, UniformValue_RadianceCubeMap, "radianceCubeMap",
        UniformType_TextureCubeMap);
    PushUniform(
        basicShader, UniformValue_BrdfLut, "brdfLut", UniformType_Texture2D);
}

internal void EnableNormalMapping(RenderCommand_CreateShader *createShader)
{
    PushDefine(createShader, "#define USE_NORMAL_MAPPING\n");

    Shader *textureShader = &createShader->shader;
    PushUniform(textureShader, UniformValue_NormalMap, "normalMap",
        UniformType_Texture2D);
}

internal void EnableAlphaCutoutTexture(RenderCommand_CreateShader *createShader)
{
    PushDefine(createShader, "#define ALPHA_CUTOUT_TEXTURE\n");

    Shader *textureShader = &createShader->shader;
    PushUniform(textureShader, UniformValue_AlphaTexture, "alphaTexture",
        UniformType_Texture2D);
}

internal void EnableHeightMap(RenderCommand_CreateShader *createShader)
{
    PushDefine(createShader, "#define USE_HEIGHT_MAP\n");

    Shader *textureShader = &createShader->shader;
    PushUniform(textureShader, UniformValue_HeightMap, "heightMap",
        UniformType_Texture2D);
    PushUniform(textureShader, UniformValue_TerrainNormalMap,
        "terrainNormalMap", UniformType_Texture2D);
    PushUniform(textureShader, UniformValue_UVScale, "uvScale", UniformType_Vec2);
    PushUniform(textureShader, UniformValue_UVOffset, "uvOffset", UniformType_Vec2);
}

internal void SetupDefaultMatrixInputs(Shader *shader)
{
    PushUniform(shader, UniformValue_ViewProjectionMatrix,
        "viewProjectionMatrix", UniformType_Mat4);
    PushUniform(
        shader, UniformValue_ModelMatrix, "modelMatrix", UniformType_Mat4);
}

void InitializeShadersAndMaterials(GameAssets *assets, GameMemory *memory, MemoryArena *tempArena)
{
    DebugReadFileResult fileResult = memory->readEntireFile("uber_shader.glsl");
    Assert(fileResult.contents != NULL);
    char *uberShaderSource = (char *)AllocateBytes(tempArena, fileResult.length + 1);
    CopyMemory(uberShaderSource, fileResult.contents, fileResult.length);
    uberShaderSource[fileResult.length] = '\0';
    memory->freeFileMemory(fileResult.contents);

    // Color shadeless shader
    {
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_ColorShadeless;
        createShader->stages = ShaderStage_Vertex | ShaderStage_Fragment;
        createShader->source = uberShaderSource;

        Shader *shader = &createShader->shader;
        shader->name = "color_shadeless";
        SetupDefaultMatrixInputs(shader);
        PushUniform(
            shader, UniformValue_Color, "uniformColor", UniformType_Vec4);
        shader->depthWriteEnabled = true;
        shader->depthTestEnabled = true;
    }

    // Color shadeless no depth shader
    {
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_ColorShadelessNoDepth;
        createShader->stages = ShaderStage_Vertex | ShaderStage_Fragment;
        createShader->source = uberShaderSource;

        Shader *shader = &createShader->shader;
        shader->name = "color_shadeless_no_depth";
        SetupDefaultMatrixInputs(shader);
        PushUniform(
            shader, UniformValue_Color, "uniformColor", UniformType_Vec4);
        shader->depthWriteEnabled = true;
        shader->depthTestEnabled = false;
    }

    // Texture shadeless shader
    {
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_TextureShadeless;
        createShader->stages = ShaderStage_Vertex | ShaderStage_Fragment;
        createShader->source = uberShaderSource;
        EnableUseTexture(createShader);

        Shader *shader = &createShader->shader;
        shader->name = "texture_shadeless";
        SetupDefaultMatrixInputs(shader);
        shader->depthWriteEnabled = true;
        shader->depthTestEnabled = true;
    }

    // Texture shadeless no depth with alpha blending shader
    {
        // NOTE: Also performs its own gamma correction
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_TextureShadelessNoDepth;
        createShader->stages = ShaderStage_Vertex | ShaderStage_Fragment;
        createShader->source = uberShaderSource;
        EnableUseTexture(createShader);
        PushDefine(createShader, "#define USE_TEXTURE_ALPHA\n");
        PushDefine(createShader, "#define USE_ALPHA_BLENDING\n");

        Shader *shader = &createShader->shader;
        shader->name = "texture_shadeless_no_depth_alpha_blending";
        SetupDefaultMatrixInputs(shader);
        shader->depthWriteEnabled = false;
        shader->depthTestEnabled = false;
        shader->alphaBlendingEnabled = true;
    }

    // Wireframe shader
    {
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_Wireframe;
        createShader->stages =
            ShaderStage_Vertex | ShaderStage_Fragment | ShaderStage_Geometry;
        createShader->source = uberShaderSource;
        PushDefine(createShader, "#define DRAW_WIREFRAME\n");

        Shader *shader = &createShader->shader;
        shader->name = "wireframe";
        SetupDefaultMatrixInputs(shader);
        PushUniform(
            shader, UniformValue_Color, "uniformColor", UniformType_Vec4);
        shader->depthWriteEnabled = true;
        shader->depthTestEnabled = true;
    }

    // Color diffuse shader
    {
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_ColorDiffuse;
        createShader->stages = ShaderStage_Vertex | ShaderStage_Fragment;
        createShader->source = uberShaderSource;
        EnableLighting(createShader);

        Shader *shader = &createShader->shader;
        shader->name = "color_diffuse";
        SetupDefaultMatrixInputs(shader);
        PushUniform(
            shader, UniformValue_Color, "uniformColor", UniformType_Vec4);
        shader->depthWriteEnabled = true;
        shader->depthTestEnabled = true;
    }

    // Color diffuse shader, roughness texture
    {
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_ColorDiffuseRoughnessTexture;
        createShader->stages = ShaderStage_Vertex | ShaderStage_Fragment;
        createShader->source = uberShaderSource;
        EnableLighting(createShader, true);

        Shader *shader = &createShader->shader;
        shader->name = "color_diffuse_roughness_texture";
        SetupDefaultMatrixInputs(shader);
        PushUniform(
            shader, UniformValue_Color, "uniformColor", UniformType_Vec4);
        shader->depthWriteEnabled = true;
        shader->depthTestEnabled = true;
    }

    // Color diffuse shader, metallic texture
    {
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_ColorDiffuseMetallicTexture;
        createShader->stages = ShaderStage_Vertex | ShaderStage_Fragment;
        createShader->source = uberShaderSource;
        EnableLighting(createShader, false, true);

        Shader *shader = &createShader->shader;
        shader->name = "color_diffuse_metallic_texture";
        SetupDefaultMatrixInputs(shader);
        PushUniform(
            shader, UniformValue_Color, "uniformColor", UniformType_Vec4);
        shader->depthWriteEnabled = true;
        shader->depthTestEnabled = true;
    }

    // Color diffuse instanced shader
    {
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_ColorDiffuseInstanced;
        createShader->stages = ShaderStage_Vertex | ShaderStage_Fragment;
        createShader->source = uberShaderSource;
        EnableLighting(createShader);
        PushDefine(createShader, "#define USE_INSTANCING\n");

        Shader *shader = &createShader->shader;
        shader->name = "color_diffuse_instanced";
        PushUniform(
            shader, UniformValue_Color, "uniformColor", UniformType_Vec4);
        PushUniform(shader, UniformValue_ViewProjectionMatrix,
            "viewProjectionMatrix", UniformType_Mat4);
        shader->depthWriteEnabled = true;
        shader->depthTestEnabled = true;
    }

    // Texture shader
    {
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_Texture;
        createShader->stages = ShaderStage_Vertex | ShaderStage_Fragment;
        createShader->source = uberShaderSource;
        EnableUseTexture(createShader);
        EnableLighting(createShader);

        Shader *shader = &createShader->shader;
        shader->name = "texture";
        SetupDefaultMatrixInputs(shader);
        shader->depthWriteEnabled = true;
        shader->depthTestEnabled = true;
    }

    // Texture with alpha shader
    {
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_TextureWithAlpha;
        createShader->stages = ShaderStage_Vertex | ShaderStage_Fragment;
        createShader->source = uberShaderSource;
        EnableUseTexture(createShader);
        EnableLighting(createShader);
        PushDefine(createShader, "#define USE_TEXTURE_ALPHA\n");

        Shader *shader = &createShader->shader;
        shader->name = "texture_alpha";
        SetupDefaultMatrixInputs(shader);
        shader->depthWriteEnabled = true;
        shader->depthTestEnabled = true;
    }

    // Normal mapping shader
    {
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_NormalMapping;
        createShader->stages = ShaderStage_Vertex | ShaderStage_Fragment;
        createShader->source = uberShaderSource;
        EnableUseTexture(createShader);
        EnableLighting(createShader);
        EnableNormalMapping(createShader);

        Shader *shader = &createShader->shader;
        shader->name = "normal_mapping";
        SetupDefaultMatrixInputs(shader);
        shader->depthWriteEnabled = true;
        shader->depthTestEnabled = true;
    }

    // Alpha Cutout Texture
    {
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_TextureAlphaCutout;
        createShader->stages = ShaderStage_Vertex | ShaderStage_Fragment;
        createShader->source = uberShaderSource;
        EnableLighting(createShader);
        EnableAlphaCutoutTexture(createShader);

        Shader *shader = &createShader->shader;
        shader->name = "alpha_cutout";
        SetupDefaultMatrixInputs(shader);
        shader->depthWriteEnabled = true;
        shader->depthTestEnabled = true;
    }

    // Alpha Cutout Texture Instanced
    {
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_TextureAlphaCutoutInstanced;
        createShader->stages = ShaderStage_Vertex | ShaderStage_Fragment;
        createShader->source = uberShaderSource;
        EnableLighting(createShader);
        EnableAlphaCutoutTexture(createShader);
        PushDefine(createShader, "#define USE_INSTANCING\n");

        Shader *shader = &createShader->shader;
        shader->name = "alpha_cutout_instanced";
        PushUniform(shader, UniformValue_ViewProjectionMatrix,
            "viewProjectionMatrix", UniformType_Mat4);
        shader->depthWriteEnabled = true;
        shader->depthTestEnabled = true;
    }

    // Terrain shader
    {
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_Terrain;
        createShader->stages = ShaderStage_Vertex | ShaderStage_Fragment;
        createShader->source = uberShaderSource;
        EnableUseTexture(createShader);
        EnableLighting(createShader);
        EnableHeightMap(createShader);
        PushDefine(createShader, "#define USE_WORLD_SPACE_TEXTURE_COORDINATES\n");

        Shader *shader = &createShader->shader;
        shader->name = "terrain";
        SetupDefaultMatrixInputs(shader);
        shader->depthWriteEnabled = true;
        shader->depthTestEnabled = true;
    }

    // Terrain wireframe shader
    {
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_TerrainWireframe;
        createShader->stages =
            ShaderStage_Vertex | ShaderStage_Fragment | ShaderStage_Geometry;
        createShader->source = uberShaderSource;
        EnableHeightMap(createShader);
        PushDefine(createShader, "#define DRAW_WIREFRAME\n");

        Shader *shader = &createShader->shader;
        shader->name = "terrain_wireframe";
        SetupDefaultMatrixInputs(shader);
        PushUniform(
            shader, UniformValue_Color, "uniformColor", UniformType_Vec4);
        shader->depthWriteEnabled = true;
        shader->depthTestEnabled = true;
    }

    // Terrain normals shader
    {
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_TerrainNormals;
        createShader->stages =
            ShaderStage_Vertex | ShaderStage_Fragment | ShaderStage_Geometry;
        createShader->source = uberShaderSource;
        EnableHeightMap(createShader);
        PushDefine(createShader, "#define DRAW_NORMALS\n");

        Shader *shader = &createShader->shader;
        shader->name = "terrain_normals";
        SetupDefaultMatrixInputs(shader);
        PushUniform(
            shader, UniformValue_Color, "uniformColor", UniformType_Vec4);
        PushUniform(shader, UniformValue_ViewProjectionMatrix,
            "viewProjectionMatrix", UniformType_Mat4);
        PushUniform(shader, UniformValue_ViewMatrix, "viewMatrix", UniformType_Mat4);
        PushUniform(shader, UniformValue_ProjectionMatrix, "projectionMatrix", UniformType_Mat4);
        shader->depthWriteEnabled = true;
        shader->depthTestEnabled = true;
    }

    // Tone Mapping shader
    {
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_ToneMapping;
        createShader->stages = ShaderStage_Vertex | ShaderStage_Fragment;
        createShader->source = uberShaderSource;
        PushDefine(createShader, "#define PERFORM_TONE_MAPPING\n");

        Shader *shader = &createShader->shader;
        shader->name = "tone_mapping";
        SetupDefaultMatrixInputs(shader);
        PushUniform(shader, UniformValue_Texture, "albedoTexture",
            UniformType_Texture2D);
        PushUniform(shader, UniformValue_TextureScale, "textureScale",
            UniformType_Vec2);
        shader->depthWriteEnabled = true;
        shader->depthTestEnabled = false;
    }

    // Sky gradient shader
    {
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_SkyGradient;
        createShader->stages = ShaderStage_Vertex | ShaderStage_Fragment;
        createShader->source = uberShaderSource;
        PushDefine(createShader, "#define USE_SKY_GRADIENT\n");

        Shader *shader = &createShader->shader;
        shader->name = "sky_gradient";
        SetupDefaultMatrixInputs(shader);
        PushUniform(shader, UniformValue_LightingData,
            "LightingDataUniformBuffer", UniformType_Buffer);
        shader->depthWriteEnabled = false;
        shader->depthTestEnabled = false;
    }

    // Cube map shader
    {
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_CubeMap;
        createShader->stages = ShaderStage_Vertex | ShaderStage_Fragment;
        createShader->source = uberShaderSource;
        PushDefine(createShader, "#define USE_CUBE_MAP\n");

        Shader *shader = &createShader->shader;
        shader->name = "cube_map";
        SetupDefaultMatrixInputs(shader);
        PushUniform(shader, UniformValue_CubeMap, "cubeMap",
            UniformType_TextureCubeMap);
        shader->depthWriteEnabled = false;
        shader->depthTestEnabled = false;
        shader->faceCullingMode = FaceCulling_FrontFace;
    }

    // PositionColor shader
    {
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_PositionColor;
        createShader->stages = ShaderStage_Vertex | ShaderStage_Fragment;
        createShader->source = positionColorSource;

        Shader *shader = &createShader->shader;
        shader->name = "position_color";
        PushUniform(shader, UniformValue_Mvp, "mvp", UniformType_Mat4);
    }

    // Text shader
    {
        RenderCommand_CreateShader *createShader = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_CreateShader);

        createShader->id = Shader_Text;
        createShader->stages = ShaderStage_Vertex | ShaderStage_Fragment;
        createShader->source = textShaderSource;

        Shader *shader = &createShader->shader;
        shader->name = "text";
        PushUniform(shader, UniformValue_GlyphSheet, "glyphSheet",
            UniformType_Texture2D);
        PushUniform(shader, UniformValue_Mvp, "mvp", UniformType_Mat4);
        shader->alphaBlendingEnabled = true;
    }

    {
        Material *material = GetMaterial(assets, Material_Checkerboard);
        *material = CreateTextureMaterial(
            Shader_Texture, Texture_Checkerboard, Vec2(1, 1), 0.9f, 0.0f);
    }

    {
        Material *material = GetMaterial(assets, Material_AlphaCutout);
        *material = CreateTextureMaterial(Shader_TextureAlphaCutout,
            Texture_DevGrid512, Vec2(1, 1), 0.9f, 0.0f);
        PushUniformTexture2D(&material->shaderInput, UniformValue_AlphaTexture,
            Texture_GrassAlpha);
    }

    {
        Material *material = GetMaterial(assets, Material_GrassInstanced);
        *material = CreateBasicMaterial(Shader_TextureAlphaCutoutInstanced,
            SrgbToLinear(Vec3(0.2, 0.6, 0.1)), 0.04f, 0.0f);
        PushUniformTexture2D(&material->shaderInput, UniformValue_AlphaTexture,
            Texture_GrassAlpha);
    }

    {
        Material *material = GetMaterial(assets, Material_PavingStones);
        *material = CreateTextureMaterial(Shader_NormalMapping,
            Texture_PavingStonesColor, Vec2(1, 1), 0.2f, 0.0f);
        PushUniformTexture2D(&material->shaderInput, UniformValue_NormalMap,
            Texture_PavingStonesNormal);
    }

    {
        Material *skyboxClear = GetMaterial(assets, Material_SkyboxClear);
        skyboxClear->shaderInput.shader = Shader_CubeMap;
        PushUniformTextureCubeMap(&skyboxClear->shaderInput,
            UniformValue_CubeMap, Texture_SkyboxClear);
    }

    assets->materials[Material_DevGrid512] = CreateTextureMaterial(Shader_Texture,
        Texture_DevGrid512, Vec2(1, 1), 0.9f, 0.0f);
    assets->materials[Material_Grey] = CreateBasicMaterial(
        Shader_ColorDiffuse, SrgbToLinear(Vec3(0.6, 0.6, 0.6)), 0.9f, 0.0f);
    assets->materials[Material_Enemy] = CreateBasicMaterial(
        Shader_ColorDiffuse, SrgbToLinear(Vec3(1, 0.4, 0)), 0.9f, 0.0f);
    assets->materials[Material_Player] = CreateBasicMaterial(
        Shader_ColorDiffuse, SrgbToLinear(Vec3(0, 0.8, 1)), 0.9f, 0.0f);
    assets->materials[Material_TreeBark] = CreateTextureMaterial(Shader_Texture,
        Texture_TreeBark, Vec2(1, 1), 0.9f, 0.0f);
    assets->materials[Material_TreeLeaves] =
        CreateTextureMaterial(Shader_TextureWithAlpha, Texture_TreeLeaves,
            Vec2(1, 1), 0.9f, 0.0f);
    assets->materials[Material_Warehouse] = CreateTextureMaterial(Shader_Texture,
        Texture_DevGrid512, Vec2(64, 64), 0.95f, 0.0f);
    assets->materials[Material_GreenInstanced] =
        CreateBasicMaterial(Shader_ColorDiffuseInstanced,
            SrgbToLinear(Vec3(0.9, 0.6, 0.2)), 0.1f, 0.0f);
    assets->materials[Material_BuildingPreview] =
        CreateBasicMaterial(Shader_ColorDiffuse,
            SrgbToLinear(Vec3(0.9, 0.6, 0.2)), 0.1f, 0.0f);

    {
        Material *material = GetMaterial(assets, Material_RoughnessTexture);
        *material = CreateBasicMaterial(Shader_ColorDiffuseRoughnessTexture,
            SrgbToLinear(Vec3(0.6, 0.6, 0.6)), 0.9f, 0.0f, true);
        PushUniformTexture2D(&material->shaderInput,
            UniformValue_RoughnessTexture, Texture_Checkerboard);
        PushUniformVec2(
            &material->shaderInput, UniformValue_TextureScale, Vec2(1, 1));
    }

    {
        Material *material = GetMaterial(assets, Material_MetallicTexture);
        *material = CreateBasicMaterial(Shader_ColorDiffuseMetallicTexture,
            SrgbToLinear(Vec3(0.6, 0.6, 0.6)), 0.8f, 0.0f, false, true);
        PushUniformTexture2D(&material->shaderInput,
            UniformValue_MetallicTexture, Texture_Checkerboard);
        PushUniformVec2(
            &material->shaderInput, UniformValue_TextureScale, Vec2(1, 1));
    }
}

internal void LoadCollisionMeshFromFile(GameAssets *assets, GameMemory *memory,
    MemoryArena *collisionMeshArena, const char *path, u32 triangleMeshId)
{
    Assert(triangleMeshId < ArrayCount(assets->triangleMeshes));
    TriangleMesh *mesh = assets->triangleMeshes + triangleMeshId;

    DebugReadFileResult fileResult = memory->readEntireFile(path);
    Assert(fileResult.contents != NULL);
    MeshFileHeader *header = (MeshFileHeader *)fileResult.contents;
    void *vertices = header + 1;
    u32 *indices =
        (u32 *)((u8 *)vertices + sizeof(Vertex) * header->vertexCount);
    Assert(header->vertexLayout == VertexLayout_Vertex);

    // Must be at triangle mesh!
    Assert(header->indexCount % 3 == 0);

    mesh->vertices = AllocateArray(collisionMeshArena, vec3, header->vertexCount);
    mesh->vertexCount = header->vertexCount;
    mesh->indices = AllocateArray(collisionMeshArena, u32, header->indexCount);
    mesh->indexCount = header->indexCount;

    // Copy vertex positions only
    for (u32 vertexIndex = 0; vertexIndex < header->vertexCount; ++vertexIndex)
    {
        Vertex *vertex = (Vertex *)vertices + vertexIndex;
        mesh->vertices[vertexIndex] = vertex->position;
    }

    // Copy indices
    CopyMemory(mesh->indices, indices, header->indexCount * sizeof(u32));

    //BuildAabbTree(mesh, collisionMeshArena);

    // Calculate radius
    f32 radiusSq = 0.0f;
    for (u32 vertexIndex = 0; vertexIndex < mesh->vertexCount; ++vertexIndex)
    {
        f32 lengthSq = LengthSq(mesh->vertices[vertexIndex]);
        if (lengthSq > radiusSq)
        {
            radiusSq = lengthSq;
        }
    }
    mesh->radius = Sqrt(radiusSq);

    memory->freeFileMemory(fileResult.contents);
}

internal void LoadCollisionMeshes(
    GameAssets *assets, GameMemory *memory, MemoryArena *collisionMeshArena)
{
    LoadCollisionMeshFromFile(assets, memory, collisionMeshArena,
        "warehouse.mesh", TriangleMesh_Warehouse);
    LoadCollisionMeshFromFile(
        assets, memory, collisionMeshArena, "rock01.mesh", TriangleMesh_Rock01);
    LoadCollisionMeshFromFile(assets, memory, collisionMeshArena,
        "shack01.mesh", TriangleMesh_Shack01);
}
