#pragma once

#include "platform.h"
#include "math_lib.h"
#include "input.h"
#include "renderer.h"
#include "debug_drawing.h"
#include "asset.h"

struct DebugReadFileResult
{
    void *contents;
    u32 length;
};

#define DebugReadEntireFile(NAME) DebugReadFileResult NAME(const char *path)
typedef DebugReadEntireFile(DebugReadEntireFileFunction);
#define DebugFreeFileMemory(NAME) void NAME(void *memory)
typedef DebugFreeFileMemory(DebugFreeFileMemoryFunction);
#define ShowMouseCursor(NAME) void NAME(b32 isVisible)
typedef ShowMouseCursor(ShowMouseCursorFunction);

// From ecs.h
typedef u32 EntityId;
typedef u16 NetEntityId;

#define MAX_ENTITIES 256
#define MAX_NETWORK_REPLICATED_ENTITIES 64
#define NULL_ENTITY_ID 0
#define NULL_NET_ENTITY_ID 0


#define MAX_CLIENTS 64
#define MAX_PACKET_DATA 1024
struct Packet
{
    u32 clientId;
    u32 length;
    u8 data[MAX_PACKET_DATA];
};

enum
{
    NetworkEvent_ClientConnected,
    NetworkEvent_ClientDisconnected,
};

struct NetworkEvent
{
    u32 type;
    u32 clientId;
};

struct GameMemory
{
    RenderCommandQueue renderCommandQueue;

    void *persistentStorage;
    u64 persistentStorageSize;

    void *transientStorage;
    u64 transientStorageSize;

    Packet *incomingPackets;
    u32 incomingPacketCount;

    Packet *outgoingPackets;
    u32 outgoingPacketCount;
    u32 maxOutgoingPackets;

    NetworkEvent *networkEvents;
    u32 networkEventCount;

    DebugLogMessageFunction *logMessage;
    DebugReadEntireFileFunction *readEntireFile;
    DebugFreeFileMemoryFunction *freeFileMemory;
    ShowMouseCursorFunction *showMouseCursor;

    b32 wasCodeReloaded;

    u32 framebufferWidth;
    u32 framebufferHeight;
};

#define GAME_CLIENT_UPDATE(NAME)                                               \
    void NAME(GameInput *input, GameMemory *memory, f32 frameTime, f32 interp)
typedef GAME_CLIENT_UPDATE(GameClientUpdateFunction);

#define GAME_CLIENT_FIXED_UPDATE(NAME)                                         \
    void NAME(GameInput *input, GameMemory *memory, f32 dt)
typedef GAME_CLIENT_FIXED_UPDATE(GameClientFixedUpdateFunction);

#define GAME_SERVER_UPDATE(NAME) void NAME(GameMemory *memory, f32 dt)
typedef GAME_SERVER_UPDATE(GameServerUpdateFunction);

extern "C" GAME_CLIENT_UPDATE(GameClientUpdate);
extern "C" GAME_CLIENT_FIXED_UPDATE(GameClientFixedUpdate);
extern "C" GAME_SERVER_UPDATE(GameServerUpdate);

#define DebugStartServer(NAME) b32 NAME(u16 port)
typedef DebugStartServer(DebugStartServerFunction);

#define DebugStartClient(NAME) b32 NAME(const char *address, u16 port)
typedef DebugStartClient(DebugStartClientFunction);

#define DebugStopServer(NAME) void NAME(void)
typedef DebugStopServer(DebugStopServerFunction);

#define DebugStopClient(NAME) void NAME(void)
typedef DebugStopClient(DebugStopClientFunction);

enum
{
    PlayerAction_Jump = 0x1,
    PlayerAction_Sprint = 0x2,
    PlayerAction_Shoot = 0x4,
    PlayerAction_Aim = 0x8,
    PlayerAction_Interact = 0x10,
};

// TODO: Tweak this based on tickrate and max in flight packets
#define MAX_PLAYER_INPUT_ID 0x1000
struct PlayerInput
{
    u32 id;
    vec3 viewAngles;
    f32 forward;
    f32 right;
    u32 actions;
    u32 activeEquipmentSlot;
};

struct PlayerSnapshot
{
    quat rotation;
    vec3 velocity;
    vec3 position;
    b32 isGrounded;
    b32 isJumping;
};

struct BulletImpactEvent
{
    vec3 position;
    vec3 normal;
};

struct HitReactionEvent
{
    EntityId entityId;
};

struct OpenContainerEvent
{
    EntityId entityId; // Player entity which opened the container
};

enum
{
    EventType_BulletImpact,
    EventType_HitReaction,
    EventType_OpenContainer,
};

struct GameEvent
{
    u32 type;
    union
    {
        BulletImpactEvent bulletImpact;
        HitReactionEvent hitReaction;
        OpenContainerEvent openContainer;
    };
};

struct GameEventQueue
{
    GameEvent events[64];
    u32 length;
};

struct NetEntityCreationEvent
{
    NetEntityId netEntityId;
    u32 type;
    union
    {
        vec3 position;
        struct
        {
            u32 itemId;
            u32 slotIndex;
            u32 quantity;
            NetEntityId owner;
        };
    };
};

struct NetEntityDeletionEvent
{
    NetEntityId netEntityId;
};

struct NetEntityUpdateEvent
{
    NetEntityId netEntityId;
    union
    {
        struct // Player/enemy
        {
            PlayerSnapshot snapshot;
            NetEntityId openContainerNetEntityId;
            i32 currentHealth;
            u32 activeEquipmentSlot;
        };
        struct
        {
            NetEntityId ownerNetEntityId;
            u32 slotIndex;
            u32 quantity;
        };
    };
};

#define MAX_NETWORKED_ENTITIES 4

struct GameSnapshot
{
    NetEntityCreationEvent entityCreationEvents[MAX_NETWORKED_ENTITIES];
    u32 entityCreationEventCount;
    NetEntityDeletionEvent entityDeletionEvents[MAX_NETWORKED_ENTITIES];
    u32 entityDeletionEventCount;
    NetEntityUpdateEvent entityUpdateEvents[MAX_NETWORKED_ENTITIES];
    u32 entityUpdateEventCount;

    NetEntityId localPlayerNetEntityId;
    GameEvent events[4];
    u32 eventCount;

    u32 playerInputId;
    u32 currentTick; // TODO: Deal with wrapping
};

enum
{
    EntityType_Block,
    EntityType_Enemy,
    EntityType_Player,
    EntityType_StaticMesh,
    EntityType_InventoryItem,
    EntityType_Container,
    EntityType_Foundation,
    EntityType_Pillar,
};

#define LERP_BUFFER_LENGTH 32
#define LERP_BUFFER_MASK (LERP_BUFFER_LENGTH - 1)
struct LerpEntry
{
    u32 tick;
    PlayerSnapshot snapshot;
};

struct LerpBuffer
{
    LerpEntry entries[LERP_BUFFER_LENGTH];
    u32 head;
    u32 length;
};

struct ParticleState
{
    vec3 position;
    vec3 velocity;
    f32 lifeTime;
};

struct ParticleSystem
{
    ParticleState particles[32];
    u32 particleCount;
};

struct Prediction
{
    PlayerInput input;
    PlayerSnapshot snapshot;
};

#define PREDICTION_BUFFER_MAX_LENGTH 128 // 2 seconds at 64 HZ
#define PREDICTION_BUFFER_MASK (PREDICTION_BUFFER_MAX_LENGTH - 1)
struct PredictionBuffer
{
    Prediction values[PREDICTION_BUFFER_MAX_LENGTH];
    u32 head;
    u32 length;
};

struct FreeRoamCamera
{
    vec3 position;
    vec3 velocity;
    vec3 rotation;
};

enum
{
    Mesh_Cube,
    Mesh_Quad,
    Mesh_Sphere,
    Mesh_Rock01,
    Mesh_Patch,
    Mesh_TreeBody,
    Mesh_TreeLeaves,
    Mesh_Pistol,
    Mesh_Target,
    Mesh_Warehouse,
    Mesh_Shack01,
    Mesh_Shotgun,
    Mesh_SphereInstanced,
    Mesh_GrassInstanced,
    MAX_MESHES,
};

enum
{
    Material_Checkerboard,
    Material_DevGrid512,
    Material_Grey,
    Material_Enemy,
    Material_Player,
    Material_AlphaCutout,
    Material_SkyboxClear,
    Material_PavingStones,
    Material_TreeBark,
    Material_TreeLeaves,
    Material_Warehouse,
    Material_RoughnessTexture,
    Material_MetallicTexture,
    Material_GreenInstanced,
    Material_GrassInstanced,
    Material_BuildingPreview,
    MAX_MATERIALS,
};

enum
{
    Font_InconsolataRegular32,
    MAX_FONTS,
};

struct Mesh
{
    u32 vertexBufferId;
    u32 count;
    u32 first;
    u32 primitive;
    u32 vertexLayout;
};

struct Material
{
    ShaderInput shaderInput;
};

struct Font
{
    FontGlyph *glyphs;
    u32 glyphCount;
    u32 texture;
};

struct HeightMap
{
    f32 *values;
    u32 width;
    u32 height;
};

struct AabbTreeNode
{
    vec3 min;
    vec3 max;
    b32 isLeaf;
    union
    {
        AabbTreeNode *children[2];
        struct
        {
            u32 triangleCount;
            u32 *triangles;
        };
    };
};

struct TriangleMesh
{
    vec3 *vertices;
    u32 *indices;
    u32 vertexCount;
    u32 indexCount;
    f32 radius;

    AabbTreeNode *root;
};

enum
{
    HeightMap_Terrain,
    MAX_HEIGHT_MAPS,
};

enum
{
    TriangleMesh_Warehouse,
    TriangleMesh_Rock01,
    TriangleMesh_Shack01,
    MAX_TRIANGLE_MESHES,
};

struct GameAssets
{
    Mesh meshes[MAX_MESHES];
    Material materials[MAX_MATERIALS];
    Font fonts[MAX_FONTS];
    HeightMap heightMaps[MAX_HEIGHT_MAPS];
    TriangleMesh triangleMeshes[MAX_TRIANGLE_MESHES];
};

inline Font GetFont(GameAssets *assets, u32 index)
{
    Assert(index < ArrayCount(assets->fonts));
    return assets->fonts[index];
}

inline HeightMap GetHeightMap(GameAssets *assets, u32 index)
{
    Assert(index < ArrayCount(assets->heightMaps));
    return assets->heightMaps[index];
}

inline TriangleMesh GetTriangleMesh(GameAssets *assets, u32 index)
{
    Assert(index < ArrayCount(assets->triangleMeshes));
    return assets->triangleMeshes[index];
}
// FIXME: Remove this (depends on PlayerSnapshot)
#include "ecs.h"

enum
{
    InventoryAction_MoveItem,
    InventoryAction_CloseContainer,
};
/*
 * We use a combination of the slot and the entity ID of the item residing in
 * that slot to make sure that when the command is received on the server we
 * only act on it if it matches the entity that the client requested. This is
 * to make inventory interaction more predictable when multiple players are
 * interacting with the same container. The owner field should always be set
 * but is only really used in the case where we are transfering an item from a
 * container to a player inventory or vice versa.
 */
struct InventoryCommand
{
    u32 action;

    u32 fromSlot;
    EntityId fromEntity; // NOTE: Cannot be NULL_ENTITY_ID
    EntityId fromOwner; // NOTE: Owner fields cannot be NULL_ENTITY_ID

    u32 toSlot;
    EntityId toEntity; // NOTE: Can be NULL_ENTITY_ID for empty slot
    EntityId toOwner;
};

struct NetInventoryCommand
{
    u32 action;
    u32 fromSlot;
    NetEntityId fromNetEntity;
    NetEntityId fromNetOwner;

    u32 toSlot;
    NetEntityId toNetEntity;
    NetEntityId toNetOwner;
};

// TODO: Make this a component?
struct InventoryUIState
{
    // TODO: Move showInventory into here?
    b32 isDragInProgress;

    // NOTE: Use both slot index and entityId accurately handle the inventory
    // being changed underneath us while we are dragging, either by other
    // players or some other gameplay system.
    u32 dragStartingSlot;
    EntityId dragStartingEntity;
    EntityId dragStartingOwner;

    InventoryCommand commandQueue[4];
    u32 commandQueueLength;
};

#define MAX_TEXT_BUFFER_VERTICES 0x1000
struct TextBuffer
{
    VertexText vertices[MAX_TEXT_BUFFER_VERTICES];
    u32 count;
};

enum
{
    ViewModel_Pistol,
    ViewModel_Shotgun,
    MAX_VIEW_MODELS,
};

struct TerrainQuadTreeNode
{
    vec2 center;
    f32 size;
    b32 isLeaf;
    TerrainQuadTreeNode *children[4];
};

struct TerrainQuadTree
{
    TerrainQuadTreeNode *root;
};

enum
{
    BuildingPart_Foundation,
    BuildingPart_Pillar,
    MAX_BUILDING_PARTS,
};

#define MAX_PARTICLE_SYSTEMS 32
struct GameState
{
    b32 isInitialized;
    u32 localPlayerEntityId;

    f32 headBobT;

    i32 mouseRelX;
    i32 mouseRelY;

    MemoryArena persitentArena;
    MemoryArena transientArena;
    MemoryArena frameArena;
    MemoryArena terrainArena;
    MemoryArena collisionMeshArena;

    EntityWorld world;
    DebugDrawingSystem debugDrawingSystem;

    ParticleSystem particleSystems[MAX_PARTICLE_SYSTEMS];
    u32 particleSystemCount;

    RandomNumberGenerator rng;

    PredictionBuffer predictionBuffer;
    u32 nextPlayerInputId;

    PlayerInput prevPlayerInput;
    vec3 recoilOffset;

    f32 adsAmount;
    b32 isAimed;
    f32 adsRate;

    b32 showInventory;

    u32 lastReceivedTick;

    b32 useFreeRoamCamera;
    FreeRoamCamera freeRoamCamera;

    GameAssets assets;

    f32 framebufferWidth;
    f32 framebufferHeight;

    InventoryUIState inventoryUIState;

    EntityId viewModels[MAX_VIEW_MODELS];
    EntityId buildingPartPreviews[MAX_BUILDING_PARTS];

    TextBuffer textBuffer;

    TerrainQuadTree terrainQuadTree;
};

#define DEFAULT_ENTITY_NET_PRIORITY 0x32
#define DEFAULT_PLAYER_NET_PRIORITY 0x64
#define DEFAULT_INVENTORY_ITEM_NET_PRIORITY 0x32
#define DEFAULT_ENEMY_NET_PRIORITY 0x40
struct ClientState
{
    u32 clientId;
    u32 playerEntityId;
    PlayerInput prevPlayerInput;
    NetEntityIdBitset netEntityIdBitset;
    u8 entityPriorities[MAX_NETWORK_REPLICATED_ENTITIES];

    GameEventQueue eventQueue;
};

struct ServerGameState
{
    b32 isInitialized;
    MemoryArena persitentArena;
    MemoryArena transientArena;
    MemoryArena terrainArena;
    MemoryArena collisionMeshArena;

    ClientState clients[MAX_CLIENTS];
    u32 clientCount;

    EntityWorld world;
    GameEventQueue eventQueue;
    GameAssets assets;

    u32 currentTick;

    RandomNumberGenerator rng;
};

#define TICK_RATE 64
#define SEND_RATE 64
#define UPDATE_RATE 64

#define INTERPOLATION_DELAY 0.3f

enum
{
    UniformValue_Texture,
    UniformValue_TextureScale,
    UniformValue_Color,
    UniformValue_Mvp,
    UniformValue_ViewProjectionMatrix,
    UniformValue_ShadowViewProjectionMatrix,
    UniformValue_ShadowMap,
    UniformValue_ModelMatrix,
    UniformValue_LightingData,
    UniformValue_AlphaTexture,
    UniformValue_CubeMap,
    UniformValue_NormalMap,
    UniformValue_HeightMap,
    UniformValue_ProjectionMatrix,
    UniformValue_ViewMatrix,
    UniformValue_TerrainNormalMap,
    UniformValue_GlyphSheet,
    UniformValue_IrradianceCubeMap,
    UniformValue_RadianceCubeMap,
    UniformValue_BrdfLut,
    UniformValue_Roughness,
    UniformValue_Metallic,
    UniformValue_RoughnessTexture,
    UniformValue_MetallicTexture,
    UniformValue_UVScale,
    UniformValue_UVOffset,
};

// NOTE: Must match shader
#define MAX_POINT_LIGHTS 16
#define MAX_SPOT_LIGHTS 16

struct PointLight
{
    vec4 position;
    vec4 color;
    vec4 attenuation;
};

struct SpotLight
{
    vec4 position;
    vec4 direction;
    vec4 color;
    vec4 attenuation[2];
};

struct LightingDataUniformBuffer
{
    vec4 sunDirection;
    vec4 cameraPosition;
    vec4 sunColor;
    vec4 ambientColor;

    PointLight pointLights[MAX_POINT_LIGHTS];
    SpotLight spotLights[MAX_SPOT_LIGHTS];
    u32 pointLightCount;
    u32 spotLightCount;
};

struct PlayerInputPacket
{
    PlayerInput playerInput;
    u32 inventoryCommandQueueLength;
    NetInventoryCommand inventoryCommandQueue[4];
};

#define PLAYER_INVENTORY_SLOT_COUNT 24
#define PLAYER_EQUIPMENT_BAR_SLOT_COUNT 6
#define PLAYER_TOTAL_INVENTORY_SLOT_COUNT                                      \
    (PLAYER_INVENTORY_SLOT_COUNT + PLAYER_EQUIPMENT_BAR_SLOT_COUNT)

#define TERRAIN_ARENA_SIZE Megabytes(32)
#define TERRAIN_HEIGHTMAP_SIZE 256
#define GRASS_GRID_DIM 32

enum
{
    Deployable_None,
    Deployable_Foundation,
    Deployable_Pillar,
};
