#include "ecs.h"

internal void InitializeComponentTable(
    ComponentTable *table, u32 objectSize, u32 capacity, MemoryArena *arena)
{
    table->capacity = capacity;
    table->objectSize = objectSize;
    table->count = 0;

    table->keys = AllocateArray(arena, EntityId, capacity);
    table->values = AllocateBytes(arena, capacity * objectSize);
}

#define ComponentTableValueLookUp(TABLE, INDEX) \
    (void *)((u8 *)(TABLE)->values + (INDEX) * (TABLE)->objectSize)

internal void* ComponentTableAllocate(ComponentTable *table, EntityId entity)
{
    Assert(table->count < table->capacity);
    u32 index = table->count++;
    table->keys[index] = entity;
    return ComponentTableValueLookUp(table, index);
}

inline u32 ComponentTableFindKey(ComponentTable *table, EntityId key)
{
    u32 keyIndex = table->count;
    for (u32 i = 0; i < table->count; ++i)
    {
        if (table->keys[i] == key)
        {
            keyIndex = i;
            break;
        }
    }
    return keyIndex;
}

internal void ComponentTableFree(ComponentTable *table, EntityId entity)
{
    u32 keyIndex = ComponentTableFindKey(table, entity);
    if (keyIndex != table->count)
    {
        u32 last = --table->count;
        void *src = ComponentTableValueLookUp(table, keyIndex);
        void *dst = ComponentTableValueLookUp(table, last);
        CopyMemory(src, dst, table->objectSize);

        table->keys[keyIndex] = table->keys[last];
    }
}

internal void ComponentTableGetValues(ComponentTable *table, EntityId *entities,
    u32 entityCount, void *values, u32 objectSize)
{
    Assert(objectSize == table->objectSize);
    for (u32 entityIndex = 0; entityIndex < entityCount; entityIndex++)
    {
        u32 keyIndex = ComponentTableFindKey(table, entities[entityIndex]);
       Assert(keyIndex != table->count);
        void *src = ComponentTableValueLookUp(table, keyIndex);
        void *dst = (void *)((u8 *)values + entityIndex * objectSize);
        CopyMemory(dst, src, objectSize);
    }
}

internal void ComponentTableSetValues(ComponentTable *table, EntityId *entities,
    u32 entityCount, void *values, u32 objectSize)
{
    Assert(objectSize == table->objectSize);
    for (u32 entityIndex = 0; entityIndex < entityCount; entityIndex++)
    {
        u32 keyIndex = ComponentTableFindKey(table, entities[entityIndex]);
        Assert(keyIndex != table->count);
        void *dst = ComponentTableValueLookUp(table, keyIndex);
        void *src = (void *)((u8 *)values + entityIndex * objectSize);
        CopyMemory(dst, src, objectSize);
    }
}

internal u32 ComponentTableGetKeys(ComponentTable *table, EntityId *entities, u32 capacity)
{
    Assert(capacity >= table->count);
    CopyMemory(entities, table->keys, table->count * sizeof(EntityId));
    return table->count;
}

internal b32 ComponentTableHasKey(ComponentTable *table, EntityId entity)
{
    u32 keyIndex = ComponentTableFindKey(table, entity);
    return (keyIndex != table->count);
}

internal EntityId AllocateEntityId(EntityWorld *world)
{
    Assert(world->entityCount < MAX_ENTITIES);
    EntityId result = world->nextEntityId++;
    world->entities[world->entityCount++] = result;
    return result;
}

#define ecs_GetComponent(                                                      \
    WORLD, COMPONENT_TYPE, ENTITIES, ENTITY_COUNT, VALUES)                     \
    ecs_GetComponent_(WORLD, COMPONENT_TYPE##TypeId, ENTITIES, ENTITY_COUNT,   \
        VALUES, sizeof((VALUES)[0]))

internal void ecs_GetComponent_(EntityWorld *world, u32 componentId,
    EntityId *entities, u32 entityCount, void *values, u32 objectSize)
{
    Assert(componentId < ArrayCount(world->tables));
    ComponentTable *table = world->tables + componentId;
    ComponentTableGetValues(table, entities, entityCount, values, objectSize);
}

internal u32 ecs_GetEntitiesWithComponent(
    EntityWorld *world, u32 componentId, EntityId *entities, u32 capacity)
{
    Assert(componentId < ArrayCount(world->tables));
    ComponentTable *table = world->tables + componentId;
    return ComponentTableGetKeys(table, entities, capacity);
}

internal b32 ecs_HasComponent(EntityWorld *world, u32 componentId, EntityId entity)
{
    Assert(componentId < ArrayCount(world->tables));
    ComponentTable *table = world->tables + componentId;
    return ComponentTableHasKey(table, entity);
}

#define ecs_SetComponent(                                                      \
    WORLD, COMPONENT_TYPE, ENTITIES, ENTITY_COUNT, VALUES)                     \
    ecs_SetComponent_(WORLD, COMPONENT_TYPE##TypeId, ENTITIES, ENTITY_COUNT,   \
        VALUES, sizeof((VALUES)[0]))

internal void ecs_SetComponent_(EntityWorld *world, u32 componentId,
    EntityId *entities, u32 entityCount, void *values, u32 objectSize)
{
    Assert(componentId < ArrayCount(world->tables));
    ComponentTable *table = world->tables + componentId;
    ComponentTableSetValues(table, entities, entityCount, values, objectSize);
}

internal void ecs_AddComponent(EntityWorld *world, u32 componentId, EntityId entityId)
{
    Assert(componentId < ArrayCount(world->tables));
    ComponentTable *table = world->tables + componentId;
    ComponentTableAllocate(table, entityId);
}

inline NetEntityId AllocateNetEntityId(EntityWorld *world)
{
    Assert(world->nextNetEntityId < MAX_NETWORK_REPLICATED_ENTITIES);
    NetEntityId netEntityId = world->nextNetEntityId++;
    SetNetEntityIdPresent(&world->netEntityIdBitset, netEntityId, true);
    return netEntityId;
}

// NOTE: This should only be called at the end of a frame/tick after all entity
// data reads are completed
internal void ProcessEntityDeletionQueue(EntityWorld *world)
{
    for (u32 entityIndex = 0; entityIndex < world->deletionQueueLength;
         ++entityIndex)
    {
        EntityId entityId = world->deletionQueue[entityIndex];
        if (ecs_HasComponent(
                world, NetworkReplicationComponentTypeId, entityId))
        {
            NetworkReplicationComponent network;
            ecs_GetComponent(
                world, NetworkReplicationComponent, &entityId, 1, &network);

            if (IsNetEntityIdPresent(
                    &world->netEntityIdBitset, network.netEntityId))
            {
                SetNetEntityIdPresent(
                    &world->netEntityIdBitset, network.netEntityId, false);
            }
        }

        for (u32 i = 0; i < ArrayCount(world->tables); ++i)
        {
            ComponentTableFree(&world->tables[i], entityId);
        }

        for (u32 i = 0; i < world->entityCount; ++i)
        {
            if (world->entities[i] == entityId)
            {
                world->entities[i] = world->entities[--world->entityCount];
                break;
            }
        }
    }

    world->deletionQueueLength = 0;
}

internal void RemoveEntity(EntityWorld *world, EntityId entityId)
{
    Assert(world->deletionQueueLength < ArrayCount(world->deletionQueue));
    world->deletionQueue[world->deletionQueueLength++] = entityId;
}
