#include "perlin.h"

// Based on implementation from https://flafla2.github.io/2014/08/09/perlinnoise.html
//
// TODO: Experiment with other fade functions
inline f32 Fade(f32 t)
{
    f32 result = t * t * t * (t * (t * 6 - 15) + 10);
    return result;
}

// Source: http://riven8192.blogspot.com/2010/08/calculate-perlinnoise-twice-as-fast.html
inline f32 grad(i32 hash, f32 x, f32 y, f32 z)
{
    switch(hash & 0xF)
    {
        case 0x0: return  x + y;
        case 0x1: return -x + y;
        case 0x2: return  x - y;
        case 0x3: return -x - y;
        case 0x4: return  x + z;
        case 0x5: return -x + z;
        case 0x6: return  x - z;
        case 0x7: return -x - z;
        case 0x8: return  y + z;
        case 0x9: return -y + z;
        case 0xA: return  y - z;
        case 0xB: return -y - z;
        case 0xC: return  y + x;
        case 0xD: return -y + z;
        case 0xE: return  y - x;
        case 0xF: return -y - z;
        default: return 0; // never happens
    }
}

f32 PerlinImproved(f32 x, f32 y, f32 z)
{
    // clang-format off
    // TODO: Generate our own permution table
    u8 p[512] = { 151,160,137,91,90,15,                 // Hash lookup table as defined by Ken Perlin.  This is a randomly
        131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,    // arranged array of all numbers from 0-255 inclusive.
        190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
        88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
        77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
        102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
        135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
        5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
        223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
        129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
        251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
        49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
        138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180,

        151,160,137,91,90,15,                 // Hash lookup table as defined by Ken Perlin.  This is a randomly
        131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,    // arranged array of all numbers from 0-255 inclusive.
        190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
        88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
        77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
        102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
        135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
        5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
        223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
        129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
        251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
        49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
        138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180

    };
    // clang-format on

    i32 xi = (int)x & 0xFF;
    i32 yi = (int)y & 0xFF;
    i32 zi = (int)z & 0xFF;

    f32 xf = Fmod(x, 1.0);
    f32 yf = Fmod(y, 1.0);
    f32 zf = Fmod(z, 1.0);

    f32 u = Fade(xf);
    f32 v = Fade(yf);
    f32 w = Fade(zf);

    i32 hashes[8];
    hashes[0] = p[p[p[xi] + yi] + zi];
    hashes[1] = p[p[p[xi] + yi+1] + zi];
    hashes[2] = p[p[p[xi+1] + yi+1] + zi];
    hashes[3] = p[p[p[xi+1] + yi] + zi];
    hashes[4] = p[p[p[xi] + yi] + zi+1];
    hashes[5] = p[p[p[xi] + yi+1] + zi+1];
    hashes[6] = p[p[p[xi+1] + yi+1] + zi+1];
    hashes[7] = p[p[p[xi+1] + yi] + zi+1];

    f32 x1, x2, y1, y2;
    x1 = Lerp(grad(hashes[0], xf, yf, zf), grad(hashes[3], xf-1.0f, yf, zf), u);
    x2 = Lerp(grad(hashes[1], xf, yf-1.0f, zf), grad(hashes[2], xf-1.0f, yf-1.0f, zf), u);
    y1 = Lerp(x1, x2, v);

    x1 = Lerp(grad(hashes[4], xf, yf, zf-1.0f), grad(hashes[7], xf-1.0f, yf, zf-1.0f), u);
    x2 = Lerp(grad(hashes[5], xf, yf-1.0f, zf-1.0f), grad(hashes[6], xf-1.0f, yf-1.0f, zf-1.0f), u);
    y2 = Lerp(x1, x2, v);

    f32 result = Lerp(y1, y2, w);

    // Map to 0-1 range
    result = (result + 1) * 0.5f;

    return result;
}

f32 OctavePerlin(f32 x, f32 y, f32 z, u32 octaves, f32 persistence)
{
    f32 total = 0.0f;
    f32 frequency = 1.0f;
    f32 amplitude = 1.0f;
    f32 maxValue = 0.0f;
    for (u32 i = 0; i < octaves; ++i)
    {
        total += PerlinImproved(x * frequency, y * frequency, z * frequency) *
                 amplitude;

        maxValue += amplitude;

        amplitude *= persistence;
        frequency *= 2;
    }

    f32 result = total / maxValue;
    return result;
}
