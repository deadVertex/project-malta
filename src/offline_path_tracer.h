#pragma once

struct Image
{
    u32 width;
    u32 height;
    u32 *pixels;
};

// Surely this is already defined somewhere?!?!?
enum
{
    AXIS_X,
    AXIS_Y,
    AXIS_Z,
};

struct CubeMap
{
    u32 faceWidth;
    vec4 *faces[CUBE_MAP_FACE_COUNT];
};

struct WorkOrder
{
    Image *image;
    u32 minX;
    u32 minY;
    u32 onePastMaxX;
    u32 onePastMaxY;
};

struct WorkQueue
{
    WorkOrder *contents;
    volatile u32 length;
    volatile u32 head;
    volatile u32 tilesProcessed;
};

