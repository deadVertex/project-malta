#include "game.h"

#include <cstring>

#include "debug_drawing.cpp"
#include "collision_detection.cpp"
#include "particle.cpp"
#include "entity_networking.cpp"
#include "ecs.cpp"

#include "mesh.cpp"

#include "shaders.cpp"

#include "game_assets.cpp"
#include "terrain.cpp"
#include "player_movement.cpp"
#include "ui.cpp"
#include "inventory.cpp"

#define DT_SCALE 1.0f

//#include "asset.cpp"

#define ACTIVE_BUILDING_PART BuildingPart_Foundation
internal void UpdateFreeRoamCamera(FreeRoamCamera *camera, GameMemory *memory, GameInput *input, f32 dt)
{
    vec3 position = camera->position;
    vec3 velocity = camera->velocity;
    vec3 rotation = camera->rotation;

    f32 speed = 200.0f;
    f32 friction = 7.0f;

    f32 sens = 0.005f;
    f32 mouseX = -input->mouseRelPosY * sens;
    f32 mouseY = -input->mouseRelPosX * sens;

    vec3 newRotation = Vec3(mouseX, mouseY, 0.0f);

    if (input->buttonStates[KEY_MOUSE_BUTTON_RIGHT].isDown)
    {
        rotation += newRotation;
        memory->showMouseCursor(false);
    }
    else
    {
        memory->showMouseCursor(true);
    }

    rotation.x = Clamp(rotation.x, -PI * 0.5f, PI * 0.5f);

    if (rotation.y > 2.0f * PI)
    {
        rotation.y -= 2.0f * PI;
    }
    if (rotation.y < 2.0f * -PI)
    {
        rotation.y += 2.0f * PI;
    }

    f32 forwardMove = 0.0f;
    f32 rightMove = 0.0f;
    if (input->buttonStates[KEY_W].isDown)
    {
        forwardMove = -1.0f;
    }

    if (input->buttonStates[KEY_S].isDown)
    {
        forwardMove = 1.0f;
    }

    if (input->buttonStates[KEY_A].isDown)
    {
        rightMove = -1.0f;
    }
    if (input->buttonStates[KEY_D].isDown)
    {
        rightMove = 1.0f;
    }

    if (input->buttonStates[KEY_LEFT_SHIFT].isDown)
    {
        speed *= 10.0f;
    }

    mat4 rotationMatrix = RotateY(rotation.y) * RotateX(rotation.x);

    vec3 forward = (rotationMatrix * Vec4(0, 0, 1, 0)).xyz;
    vec3 right = (rotationMatrix * Vec4(1, 0, 0, 0)).xyz;

    vec3 targetDir = Normalize(forward * forwardMove + right * rightMove);

    velocity -= velocity * friction * dt;
    velocity += targetDir * speed * dt;

    position = position + velocity * dt;

    camera->position = position;
    camera->velocity = velocity;
    camera->rotation = rotation;
}

internal void InitializeEntityWorld(EntityWorld *world, MemoryArena *worldArena)
{
    world->nextEntityId = 1;
    world->nextNetEntityId = 1;

    InitializeComponentTable(world->tables + TransformComponentTypeId,
        sizeof(TransformComponent), MAX_ENTITIES, worldArena);
    InitializeComponentTable(world->tables + StaticMeshComponentTypeId,
        sizeof(StaticMeshComponent), MAX_ENTITIES, worldArena);
    InitializeComponentTable(world->tables + NetworkReplicationComponentTypeId,
        sizeof(NetworkReplicationComponent), MAX_ENTITIES, worldArena);
    InitializeComponentTable(world->tables + BoxCollisionComponentTypeId,
        sizeof(BoxCollisionComponent), MAX_ENTITIES, worldArena);
    InitializeComponentTable(world->tables + EnemyComponentTypeId,
        sizeof(EnemyComponent), MAX_ENTITIES, worldArena);
    InitializeComponentTable(world->tables + TerrainComponentTypeId,
        sizeof(TerrainComponent), MAX_ENTITIES, worldArena);
    InitializeComponentTable(world->tables + InventoryItemComponentTypeId,
        sizeof(InventoryItemComponent), MAX_ENTITIES, worldArena);
    InitializeComponentTable(world->tables + InventoryOwnerComponentTypeId,
        sizeof(InventoryOwnerComponent), MAX_ENTITIES, worldArena);
    InitializeComponentTable(world->tables + ContainerComponentTypeId,
        sizeof(ContainerComponent), MAX_ENTITIES, worldArena);
    InitializeComponentTable(world->tables + HealthComponentTypeId,
        sizeof(HealthComponent), MAX_ENTITIES, worldArena);
    InitializeComponentTable(world->tables + TriangleCollisionMeshComponentTypeId,
        sizeof(TriangleCollisionMeshComponent), MAX_ENTITIES, worldArena);
    InitializeComponentTable(world->tables + DeployableComponentTypeId,
        sizeof(DeployableComponent), MAX_ENTITIES, worldArena);
}

internal vec3 GetPositionOnTerrain(EntityWorld *world, GameAssets *assets, f32 x, f32 z)
{
    vec3 result = Vec3(x, 0, z);
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(
        world, TerrainComponentTypeId, entities, ArrayCount(entities));

    for (u32 entityIndex = 0; entityIndex < entityCount; ++entityIndex)
    {
        EntityId entity = entities[entityIndex];

        TransformComponent transform;
        ecs_GetComponent(world, TransformComponent, &entity, 1, &transform);

        TerrainComponent terrain;
        ecs_GetComponent(world, TerrainComponent, &entity, 1, &terrain);

        HeightMap heightMap = GetHeightMap(assets, terrain.heightMap);

        x = Clamp(x, transform.position.x - transform.scale.x * 0.5f,
            transform.position.x + transform.scale.x * 0.5f);
        z = Clamp(z, transform.position.z - transform.scale.z * 0.5f,
            transform.position.z + transform.scale.z * 0.5f);
        result.y = SampleTerrainAtWorldPosition(
            heightMap, transform.position, transform.scale, x, z);
    }

    return result;
}


internal EntityId CreatePlayerEntity(EntityWorld *world, vec3 position,
    NetEntityId netEntityId = NULL_NET_ENTITY_ID, b32 isLocalPlayer = false)
{
    EntityId entityId = AllocateEntityId(world);
    ecs_AddComponent(world, TransformComponentTypeId, entityId);
    ecs_AddComponent(world, NetworkReplicationComponentTypeId, entityId);
    ecs_AddComponent(world, BoxCollisionComponentTypeId, entityId);
    ecs_AddComponent(world, StaticMeshComponentTypeId, entityId);
    ecs_AddComponent(world, InventoryOwnerComponentTypeId, entityId);
    ecs_AddComponent(world, HealthComponentTypeId, entityId);

    if (netEntityId == NULL_NET_ENTITY_ID)
    {
        netEntityId = AllocateNetEntityId(world);
    }

    NetworkReplicationComponent networkingComponent = {};
    networkingComponent.netEntityId = netEntityId;
    networkingComponent.snapshots[0].position = position;
    networkingComponent.snapshots[0].isGrounded = true;
    networkingComponent.priority = DEFAULT_PLAYER_NET_PRIORITY;
    networkingComponent.type = EntityType_Player;

    ecs_SetComponent(
        world, NetworkReplicationComponent, &entityId, 1, &networkingComponent);

    TransformComponent transform = {};
    transform.position = position;
    transform.rotation = Quat();
    transform.scale = Vec3(1, 2, 1);
    ecs_SetComponent(world, TransformComponent, &entityId, 1, &transform);

    StaticMeshComponent staticMesh = {};
    staticMesh.materials[0] = Material_Player;
    staticMesh.meshes[0] = Mesh_Cube;
    staticMesh.count = 1;
    staticMesh.isVisible = !isLocalPlayer;
    ecs_SetComponent(world, StaticMeshComponent, &entityId, 1, &staticMesh);

    BoxCollisionComponent box;
    box.halfDims = Vec3(0.5, 1, 0.5);
    ecs_SetComponent(world, BoxCollisionComponent, &entityId, 1, &box);

    HealthComponent health = {};
    health.currentHealth = 100;
    health.maxHealth = 100;
    ecs_SetComponent(world, HealthComponent, &entityId, 1, &health);

    return entityId;
}

internal EntityId CreateEnemyEntity(
    EntityWorld *world, vec3 position, NetEntityId netEntityId = NULL_NET_ENTITY_ID)
{
    EntityId entityId = AllocateEntityId(world);

    ecs_AddComponent(world, TransformComponentTypeId, entityId);
    ecs_AddComponent(world, NetworkReplicationComponentTypeId, entityId);
    ecs_AddComponent(world, BoxCollisionComponentTypeId, entityId);
    ecs_AddComponent(world, StaticMeshComponentTypeId, entityId);
    ecs_AddComponent(world, EnemyComponentTypeId, entityId);
    ecs_AddComponent(world, HealthComponentTypeId, entityId);

    TransformComponent transform = {};
    transform.rotation = Quat();
    transform.scale = Vec3(1, 2, 1);
    ecs_SetComponent(world, TransformComponent, &entityId, 1, &transform);

    if (netEntityId == NULL_NET_ENTITY_ID)
    {
        netEntityId = AllocateNetEntityId(world);
    }

    NetworkReplicationComponent network = {};
    network.netEntityId = netEntityId;
    network.snapshots[0].position = position;
    network.snapshots[0].isGrounded = true;
    network.priority = DEFAULT_ENTITY_NET_PRIORITY;
    network.type = EntityType_Enemy;
    ecs_SetComponent(
        world, NetworkReplicationComponent, &entityId, 1, &network);

    BoxCollisionComponent box;
    box.halfDims = Vec3(0.5, 1, 0.5);
    ecs_SetComponent(world, BoxCollisionComponent, &entityId, 1, &box);

    StaticMeshComponent staticMesh = {};
    staticMesh.materials[0] = Material_Enemy;
    staticMesh.meshes[0] = Mesh_Cube;
    staticMesh.count = 1;
    staticMesh.isVisible = true;
    ecs_SetComponent(world, StaticMeshComponent, &entityId, 1, &staticMesh);

    EnemyComponent enemy = {};
    enemy.sineT = 0.0f;
    ecs_SetComponent(world, EnemyComponent, &entityId, 1, &enemy);

    HealthComponent health = {};
    health.currentHealth = 100;
    health.maxHealth = 100;
    ecs_SetComponent(world, HealthComponent, &entityId, 1, &health);

    return entityId;
}

internal EntityId CreateContainerEntity(
    EntityWorld *world, vec3 position, NetEntityId netEntityId = NULL_NET_ENTITY_ID)
{
    EntityId entityId = AllocateEntityId(world);

    ecs_AddComponent(world, TransformComponentTypeId, entityId);
    ecs_AddComponent(world, NetworkReplicationComponentTypeId, entityId);
    ecs_AddComponent(world, BoxCollisionComponentTypeId, entityId);
    ecs_AddComponent(world, StaticMeshComponentTypeId, entityId);
    ecs_AddComponent(world, ContainerComponentTypeId, entityId);

    vec3 dims = Vec3(0.8, 0.4, 0.4) * 2.0f;
    TransformComponent transform = {};
    transform.position = position;
    transform.rotation = Quat();
    transform.scale = dims;
    ecs_SetComponent(world, TransformComponent, &entityId, 1, &transform);

    if (netEntityId == NULL_NET_ENTITY_ID)
    {
        netEntityId = AllocateNetEntityId(world);
    }

    NetworkReplicationComponent network = {};
    network.netEntityId = netEntityId;
    network.snapshots[0].position = position;
    network.priority = DEFAULT_ENTITY_NET_PRIORITY;
    network.type = EntityType_Container;
    ecs_SetComponent(
        world, NetworkReplicationComponent, &entityId, 1, &network);

    BoxCollisionComponent box;
    box.halfDims = dims * 0.5f;
    ecs_SetComponent(world, BoxCollisionComponent, &entityId, 1, &box);

    StaticMeshComponent staticMesh = {};
    staticMesh.materials[0] = Material_DevGrid512;
    staticMesh.meshes[0] = Mesh_Cube;
    staticMesh.count = 1;
    staticMesh.isVisible = true;
    ecs_SetComponent(world, StaticMeshComponent, &entityId, 1, &staticMesh);

    return entityId;
}

internal void CreateTargetEntity(EntityWorld *world, GameAssets *assets)
{
    EntityId entityId = AllocateEntityId(world);

    ecs_AddComponent(world, TransformComponentTypeId, entityId);
    ecs_AddComponent(world, BoxCollisionComponentTypeId, entityId);
    ecs_AddComponent(world, StaticMeshComponentTypeId, entityId);

    TransformComponent transform = {};
    transform.position = GetPositionOnTerrain(world, assets, 14.0f, 22.0f);
    transform.rotation = Quat();
    transform.scale = Vec3(1);
    ecs_SetComponent(world, TransformComponent, &entityId, 1, &transform);

    BoxCollisionComponent box;
    box.halfDims = Vec3(0.2f, 1.0f, 0.02f);
    ecs_SetComponent(world, BoxCollisionComponent, &entityId, 1, &box);

    StaticMeshComponent staticMesh = {};
    staticMesh.materials[0] = Material_DevGrid512;
    staticMesh.meshes[0] = Mesh_Target;
    staticMesh.count = 1;
    staticMesh.isVisible = true;
    ecs_SetComponent(world, StaticMeshComponent, &entityId, 1, &staticMesh);
}

inline PlayerSnapshot LerpSnapshot(PlayerSnapshot a, PlayerSnapshot b, f32 t)
{
    PlayerSnapshot result = {};
    result.rotation = LerpQuat(a.rotation, b.rotation, t);
    result.position = Lerp(a.position, b.position, t);
    result.velocity = Lerp(a.velocity, b.velocity, t);
    result.isGrounded = t > 0.5f ? b.isGrounded : a.isGrounded;

    return result;
}

internal ParticleSystem* AllocateParticleSystem(GameState *gameState)
{
    ParticleSystem *result = NULL;
    if (gameState->particleSystemCount < ArrayCount(gameState->particleSystems))
    {
        result = gameState->particleSystems + gameState->particleSystemCount++;
    }

    return result;
}

internal void CleanUpEmptyParticleSystems(GameState *gameState)
{
    u32 particleSystemIndex = 0;
    while (particleSystemIndex < gameState->particleSystemCount)
    {
        ParticleSystem *particleSystem =
            gameState->particleSystems + particleSystemIndex;

        if (particleSystem->particleCount == 0)
        {
            *particleSystem =
                gameState->particleSystems[--gameState->particleSystemCount];
        }
        else
        {
            particleSystemIndex++;
        }
    }
}

internal void UpdateAimDownSights(GameState *gameState, f32 dt)
{
    gameState->adsAmount += gameState->adsRate * dt;
    if (gameState->adsAmount >= 1.0f)
    {
        gameState->adsAmount = 1.0f;
        gameState->adsRate = 0.0f;
        gameState->isAimed = true;
    }
    if (gameState->adsAmount <= 0.0f)
    {
        gameState->adsAmount = 0.0f;
        gameState->adsRate = 0.0f;
        gameState->isAimed = false;
    }
}

internal void CreateViewModelEntities(GameState *gameState)
{
    EntityWorld *world = &gameState->world;
    {   // Pistol
        EntityId pistol = AllocateEntityId(world);
        ecs_AddComponent(world, TransformComponentTypeId, pistol);
        ecs_AddComponent(world, StaticMeshComponentTypeId, pistol);

        TransformComponent transform = {};
        transform.position = Vec3(2, 2, 0);
        transform.rotation = Quat();
        transform.scale = Vec3(1);
        ecs_SetComponent(
            world, TransformComponent, &pistol, 1, &transform);

        StaticMeshComponent staticMesh = {};
        staticMesh.meshes[0] = Mesh_Pistol;
        staticMesh.materials[0] = Material_Grey;
        staticMesh.count = 1;
        staticMesh.isVisible = false;
        staticMesh.useViewModelProjectionMatrix = true;
        ecs_SetComponent(
            world, StaticMeshComponent, &pistol, 1, &staticMesh);

        gameState->viewModels[ViewModel_Pistol] = pistol;
    }
    {   // Shotgun
        EntityId shotgun = AllocateEntityId(world);
        ecs_AddComponent(world, TransformComponentTypeId, shotgun);
        ecs_AddComponent(world, StaticMeshComponentTypeId, shotgun);

        TransformComponent transform = {};
        transform.position = Vec3(2, 2, 0);
        transform.rotation = Quat();
        transform.scale = Vec3(1);
        ecs_SetComponent(
            world, TransformComponent, &shotgun, 1, &transform);

        StaticMeshComponent staticMesh = {};
        staticMesh.meshes[0] = Mesh_Shotgun;
        staticMesh.materials[0] = Material_Grey;
        staticMesh.count = 1;
        staticMesh.isVisible = false;
        staticMesh.useViewModelProjectionMatrix = true;
        ecs_SetComponent(
            world, StaticMeshComponent, &shotgun, 1, &staticMesh);

        gameState->viewModels[ViewModel_Shotgun] = shotgun;
    }
}

// WARNING: Data is duplicated with SpawnFoundationEntity
internal void CreateBuildingPartPreviewEntities(GameState *gameState)
{
    EntityWorld *world = &gameState->world;

    {   // Foundation
        EntityId foundation = AllocateEntityId(world);
        ecs_AddComponent(world, TransformComponentTypeId, foundation);
        ecs_AddComponent(world, StaticMeshComponentTypeId, foundation);

        TransformComponent transform = {};
        transform.position = Vec3(0, 0, 0);
        transform.rotation = Quat();
        transform.scale = Vec3(3, 1, 3);
        ecs_SetComponent(
            world, TransformComponent, &foundation, 1, &transform);

        StaticMeshComponent staticMesh = {};
        staticMesh.meshes[0] = Mesh_Cube;
        staticMesh.materials[0] = Material_BuildingPreview;
        staticMesh.count = 1;
        staticMesh.isVisible = false;
        staticMesh.useViewModelProjectionMatrix = false;
        ecs_SetComponent(
            world, StaticMeshComponent, &foundation, 1, &staticMesh);

        gameState->buildingPartPreviews[BuildingPart_Foundation] = foundation;
    }

    {   // Pillar
        EntityId pillar = AllocateEntityId(world);
        ecs_AddComponent(world, TransformComponentTypeId, pillar);
        ecs_AddComponent(world, StaticMeshComponentTypeId, pillar);

        TransformComponent transform = {};
        transform.position = Vec3(0, 0, 0);
        transform.rotation = Quat();
        transform.scale = Vec3(0.3, 3.5, 0.3);
        ecs_SetComponent(
            world, TransformComponent, &pillar, 1, &transform);

        StaticMeshComponent staticMesh = {};
        staticMesh.meshes[0] = Mesh_Cube;
        staticMesh.materials[0] = Material_BuildingPreview;
        staticMesh.count = 1;
        staticMesh.isVisible = false;
        staticMesh.useViewModelProjectionMatrix = false;
        ecs_SetComponent(
            world, StaticMeshComponent, &pillar, 1, &staticMesh);

        gameState->buildingPartPreviews[BuildingPart_Pillar] = pillar;
    }
}

internal void SpawnTreesOnTerrain(EntityWorld *world, HeightMap heightMap,
    vec3 terrainPosition, vec3 terrainScale)
{
    RandomNumberGenerator rng;
    rng.state = 0x345A091;

    u32 treeCount = 64;
    for (u32 treeIndex = 0; treeIndex < treeCount; ++treeIndex)
    {
        EntityId tree = AllocateEntityId(world);
        ecs_AddComponent(world, TransformComponentTypeId, tree);
        ecs_AddComponent(world, StaticMeshComponentTypeId, tree);

        vec3 center = terrainPosition + terrainScale * 0.5f;
        vec3 position = {};
        position.x = center.x + RandomBilateral(&rng) * terrainScale.x * 0.1f;
        position.z = center.z + RandomBilateral(&rng) * terrainScale.z * 0.1f;
        position.y = SampleTerrainAtWorldPosition(
            heightMap, terrainPosition, terrainScale, position.x, position.z);
        position.y -= 0.4f;

        TransformComponent transform;
        transform.position = position;
        transform.rotation = Quat(Vec3(0, 1, 0), RandomBilateral(&rng) * PI);
        transform.scale = Vec3(0.8f + RandomUnilateral(&rng) * 0.5f);
        ecs_SetComponent(world, TransformComponent, &tree, 1, &transform);

        StaticMeshComponent staticMesh = {};
        staticMesh.meshes[0] = Mesh_TreeBody;
        staticMesh.materials[0] = Material_TreeBark;
        staticMesh.meshes[1] = Mesh_TreeLeaves;
        staticMesh.materials[1] = Material_TreeLeaves;
        staticMesh.count = 2;
        staticMesh.isVisible = true;
        ecs_SetComponent(world, StaticMeshComponent, &tree, 1, &staticMesh);
    }
}

internal void SpawnBoxesOnTerrain(EntityWorld *world, HeightMap heightMap,
    vec3 terrainPosition, vec3 terrainScale)
{
    RandomNumberGenerator rng;
    rng.state = 0x24FA7902;

    u32 boxCount = 64;
    for (u32 boxIndex = 0; boxIndex < boxCount; ++boxIndex)
    {
        EntityId box = AllocateEntityId(world);
        ecs_AddComponent(world, TransformComponentTypeId, box);
        ecs_AddComponent(world, StaticMeshComponentTypeId, box);
        ecs_AddComponent(world, BoxCollisionComponentTypeId, box);

        vec3 scale = Vec3(3.4f + RandomUnilateral(&rng) * 8.5f,
                               2.8f + RandomUnilateral(&rng) * 5.4f,
                               3.4f + RandomUnilateral(&rng) * 8.6f);
        vec3 position = {};
        position.x = terrainPosition.x + RandomBilateral(&rng) * terrainScale.x * 0.1f;
        position.z = terrainPosition.z + RandomBilateral(&rng) * terrainScale.z * 0.1f;
        position.y = SampleTerrainAtWorldPosition(
            heightMap, terrainPosition, terrainScale, position.x, position.z);
        position.y += scale.y * 0.2f;

        TransformComponent transform;
        transform.position = position;
        transform.rotation = Quat();
        transform.scale = scale;

        ecs_SetComponent(world, TransformComponent, &box, 1, &transform);

        StaticMeshComponent staticMesh = {};
        staticMesh.meshes[0] = Mesh_Cube;
        staticMesh.materials[0] = Material_Checkerboard;
        staticMesh.count = 1;
        staticMesh.isVisible = true;
        ecs_SetComponent(world, StaticMeshComponent, &box, 1, &staticMesh);

        BoxCollisionComponent boxCollision;
        boxCollision.halfDims = transform.scale * 0.5f;
        ecs_SetComponent(world, BoxCollisionComponent, &box, 1, &boxCollision);
    }
}

internal void CreateWarehouseEntity(EntityWorld *world, vec3 position, f32 rotation)
{
    EntityId warehouse = AllocateEntityId(world);
    ecs_AddComponent(world, TransformComponentTypeId, warehouse);
    ecs_AddComponent(world, StaticMeshComponentTypeId, warehouse);
    ecs_AddComponent(world, TriangleCollisionMeshComponentTypeId, warehouse);

    TransformComponent transform = {};
    transform.position = position;
    transform.rotation = Quat(Vec3(0, 1, 0), rotation);
    transform.scale = Vec3(1);
    ecs_SetComponent(world, TransformComponent, &warehouse, 1, &transform);

    StaticMeshComponent staticMesh = {};
    staticMesh.meshes[0] = Mesh_Warehouse;
    staticMesh.materials[0] = Material_Warehouse;
    staticMesh.count = 1;
    staticMesh.isVisible = true;
    ecs_SetComponent(world, StaticMeshComponent, &warehouse, 1, &staticMesh);

    TriangleCollisionMeshComponent triangleMesh = {};
    triangleMesh.triangleMeshId = TriangleMesh_Warehouse;
    ecs_SetComponent(
        world, TriangleCollisionMeshComponent, &warehouse, 1, &triangleMesh);
}

internal void CreateShackEntity(EntityWorld *world, vec3 position, f32 rotation)
{
    EntityId shack = AllocateEntityId(world);
    ecs_AddComponent(world, TransformComponentTypeId, shack);
    ecs_AddComponent(world, StaticMeshComponentTypeId, shack);
    ecs_AddComponent(world, TriangleCollisionMeshComponentTypeId, shack);

    TransformComponent transform = {};
    transform.position = position;
    transform.rotation = Quat(Vec3(0, 1, 0), rotation);
    transform.scale = Vec3(1);
    ecs_SetComponent(
            world, TransformComponent, &shack, 1, &transform);

    StaticMeshComponent staticMesh = {};
    staticMesh.meshes[0] = Mesh_Shack01;
    staticMesh.materials[0] = Material_Grey;
    staticMesh.count = 1;
    staticMesh.isVisible = true;
    ecs_SetComponent(
            world, StaticMeshComponent, &shack, 1, &staticMesh);

    TriangleCollisionMeshComponent triangleMesh = {};
    triangleMesh.triangleMeshId = TriangleMesh_Shack01;
    ecs_SetComponent(
            world, TriangleCollisionMeshComponent, &shack, 1, &triangleMesh);
}

internal void SpawnPointsOfInterest(EntityWorld *world, HeightMap heightMap,
    vec3 terrainPosition, vec3 terrainScale, b32 isServer)
{
    RandomNumberGenerator rng;
    rng.state = 0x3FC225C3;

    RandomNumberGenerator serverRng;
    serverRng.state = 0x9AC29413;

    u32 shackCount = 4 + XorShift32(&rng) % 6;
    for (u32 shackIndex = 0; shackIndex < shackCount; ++shackIndex)
    {
        vec3 center = terrainPosition + terrainScale * 0.5f;
        vec3 position = {};
        position.x =
            center.x + RandomBilateral(&rng) * terrainScale.x * 0.2f;
        position.z =
            center.z + RandomBilateral(&rng) * terrainScale.z * 0.2f;
        position.y = SampleTerrainAtWorldPosition(
            heightMap, terrainPosition, terrainScale, position.x, position.z);

        u32 index = XorShift32(&rng) % 2;
        if (index == 0)
        {
            // Spawn camp
            CreateShackEntity(world, position, 0.0f);

            if (isServer)
            {
                EntityId container = CreateContainerEntity(world, position + Vec3(-4.0, 0.5, 0));
                // TODO: Randomize loot

                u32 enemyCount = XorShift32(&serverRng) % 4;
                for (u32 enemyIndex = 0; enemyIndex < enemyCount; ++enemyIndex)
                {
                    f32 spawnAngle = RandomBilateral(&serverRng) * PI;
                    quat rotation = Quat(Vec3(0, 1, 0), spawnAngle);
                    f32 radius = 2.0f + RandomUnilateral(&serverRng) * 6.0f;
                    vec3 offset = RotateVector(Vec3(1, 0, 0), rotation) * radius;

                    vec3 enemyPosition = position + offset;
                    enemyPosition.y =
                        SampleTerrainAtWorldPosition(heightMap, terrainPosition,
                            terrainScale, enemyPosition.x, enemyPosition.z);
                    enemyPosition.y += PLAYER_HEIGHT;
                    CreateEnemyEntity(world, enemyPosition);
                }
            }
        }
        else
        {
            CreateWarehouseEntity(world, position, 0.0f);

            if (isServer)
            {
                CreateContainerEntity(world, position + Vec3(-4.0, 0.5, 0));
                CreateContainerEntity(world, position + Vec3(-38, 6.5, 2));
                // TODO: Randomize loot

                u32 enemyCount = XorShift32(&serverRng) % 4;
                for (u32 enemyIndex = 0; enemyIndex < enemyCount; ++enemyIndex)
                {
                    f32 spawnAngle = RandomBilateral(&serverRng) * PI;
                    quat rotation = Quat(Vec3(0, 1, 0), spawnAngle);
                    f32 radius = 8.0f + RandomUnilateral(&serverRng) * 20.0f;
                    vec3 offset =
                        RotateVector(Vec3(1, 0, 0), rotation) * radius;

                    vec3 enemyPosition = position + offset;
                    enemyPosition.y =
                        SampleTerrainAtWorldPosition(heightMap, terrainPosition,
                            terrainScale, enemyPosition.x, enemyPosition.z);
                    enemyPosition.y += PLAYER_HEIGHT;
                    CreateEnemyEntity(world, enemyPosition);
                }
            }
        }
    }
}

internal void PopulateTerrainSystem(EntityWorld *world, GameAssets *assets, b32 isServer)
{
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(
        world, TerrainComponentTypeId, entities, ArrayCount(entities));

    for (u32 entityIndex = 0; entityIndex < entityCount; ++entityIndex)
    {
        EntityId entity = entities[entityIndex];

        TransformComponent transform;
        ecs_GetComponent(world, TransformComponent, &entity, 1, &transform);

        TerrainComponent terrain;
        ecs_GetComponent(world, TerrainComponent, &entity, 1, &terrain);

        HeightMap heightMap = GetHeightMap(assets, terrain.heightMap);

        SpawnTreesOnTerrain(world, heightMap, transform.position, transform.scale);
        SpawnPointsOfInterest(world, heightMap, transform.position, transform.scale, isServer);
        //SpawnBoxesOnTerrain(world, heightMap, transform.position, transform.scale);
        //SpawnWarehousesOnTerrain(world, heightMap, transform.position, transform.scale);
        //SpawnShacksOnTerrain(world, heightMap, transform.position, transform.scale);
    }
}

internal void SpawnEnemies(EntityWorld *world, GameAssets *assets)
{
    RandomNumberGenerator rng;
    rng.state = 0x76AEF35;
    for (u32 i = 0; i < 16; ++i)
    {
        vec3 position = {};
        position.x = RandomBilateral(&rng) * 128.0f;
        position.z = RandomBilateral(&rng) * 128.1f;
        position = GetPositionOnTerrain(world, assets, position.x, position.z);
        position.y += PLAYER_HEIGHT;
        CreateEnemyEntity(world, position);
    }
}

internal void CreateTestLevel(EntityWorld *world)
{
    {
        EntityId ground = AllocateEntityId(world);
        ecs_AddComponent(world, TransformComponentTypeId, ground);
        ecs_AddComponent(world, BoxCollisionComponentTypeId, ground);
        ecs_AddComponent(world, StaticMeshComponentTypeId, ground);

        TransformComponent transform = {};
        transform.position = Vec3(0, 0, 0);
        transform.rotation = Quat();
        transform.scale = Vec3(400, 1, 400);
        ecs_SetComponent(world, TransformComponent, &ground, 1, &transform);

        BoxCollisionComponent box = {};
        box.halfDims = transform.scale * 0.5f;
        ecs_SetComponent(world, BoxCollisionComponent, &ground, 1, &box);

        StaticMeshComponent staticMesh = {};
        staticMesh.meshes[0] = Mesh_Cube;
        staticMesh.materials[0] = Material_Checkerboard;
        staticMesh.count = 1;
        staticMesh.isVisible = true;
        ecs_SetComponent(world, StaticMeshComponent, &ground, 1, &staticMesh);

    }
    {   // Warehouse
        EntityId warehouse = AllocateEntityId(world);
        ecs_AddComponent(world, TransformComponentTypeId, warehouse);
        ecs_AddComponent(world, StaticMeshComponentTypeId, warehouse);
        ecs_AddComponent(world, TriangleCollisionMeshComponentTypeId, warehouse);

        TransformComponent transform = {};
        transform.position = Vec3(-10, 0.49, -30);
        transform.rotation = Quat(Vec3(0, 1, 0), PI * 0.3f);
        transform.scale = Vec3(1);
        ecs_SetComponent(
            world, TransformComponent, &warehouse, 1, &transform);

        StaticMeshComponent staticMesh = {};
        staticMesh.meshes[0] = Mesh_Warehouse;
        staticMesh.materials[0] = Material_Warehouse;
        staticMesh.count = 1;
        staticMesh.isVisible = true;
        ecs_SetComponent(
            world, StaticMeshComponent, &warehouse, 1, &staticMesh);

        TriangleCollisionMeshComponent triangleMesh = {};
        triangleMesh.triangleMeshId = TriangleMesh_Warehouse;
        ecs_SetComponent(
            world, TriangleCollisionMeshComponent, &warehouse, 1, &triangleMesh);
    }
}

internal void CreateLevel(EntityWorld *world, GameAssets *assets, b32 isServer)
{
#ifdef TERRAIN
    {   // Terrain
        EntityId terrain = AllocateEntityId(world);
        ecs_AddComponent(world, TransformComponentTypeId, terrain);
        ecs_AddComponent(world, TerrainComponentTypeId, terrain);

        f32 terrainScale = 4000.0f;
        TransformComponent terrainTransform = {};
        terrainTransform.position =
            Vec3(-terrainScale * 0.5f, -800.0f, -terrainScale * 0.5f);
        terrainTransform.rotation = Quat();
        terrainTransform.scale = Vec3(terrainScale);
        ecs_SetComponent(
            world, TransformComponent, &terrain, 1, &terrainTransform);

        TerrainComponent terrainComponent = {};
        terrainComponent.heightMap = HeightMap_Terrain;
        ecs_SetComponent(
            world, TerrainComponent, &terrain, 1, &terrainComponent);
    }

    CreateTargetEntity(world, assets);
    PopulateTerrainSystem(world, assets, isServer);

    if (isServer)
    {
        CreateTargetEntity(world, assets);
        EntityId container = CreateContainerEntity(
            world, GetPositionOnTerrain(world, assets, 4, 4));
        AddItemToInventory(world, container, Item_PistolAmmo, 32);
        AddItemToInventory(world, container, Item_PistolAmmo, 32);
        AddItemToInventory(world, container, Item_Shotgun);
        //SpawnEnemies(world, assets);
    }
#else
    {   // Ground
        EntityId ground = AllocateEntityId(world);
        ecs_AddComponent(world, TransformComponentTypeId, ground);
        ecs_AddComponent(world, StaticMeshComponentTypeId, ground);
        ecs_AddComponent(world, BoxCollisionComponentTypeId, ground);

        f32 scale = 1000.0f;
        TransformComponent transform = {};
        transform.position = Vec3(0, -1, 0);
        transform.rotation = Quat();
        transform.scale = Vec3(scale, 1, scale);
        ecs_SetComponent(world, TransformComponent, &ground, 1, &transform);

        StaticMeshComponent staticMesh = {};
        staticMesh.meshes[0] = Mesh_Cube;
        staticMesh.materials[0] = Material_Checkerboard;
        staticMesh.count = 1;
        staticMesh.isVisible = true;
        ecs_SetComponent(world, StaticMeshComponent, &ground, 1, &staticMesh);

        BoxCollisionComponent boxCollision = {};
        boxCollision.halfDims = transform.scale * 0.5f;
        ecs_SetComponent(world, BoxCollisionComponent, &ground, 1, &boxCollision);
    }
#endif
}

internal void RenderStaticMeshes(EntityWorld *world, GameAssets *assets,
    RenderCommandQueue *renderCommandQueue, mat4 projection, mat4 view,
    mat4 shadowProjection, mat4 shadowView, mat4 viewModelProjection)
{
    mat4 viewProjection = projection * view;
    mat4 viewModelViewProjection = viewModelProjection * view;
    mat4 shadowViewProjection = shadowProjection * shadowView;

    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(
        world, StaticMeshComponentTypeId, entities, ArrayCount(entities));

    // Render static meshes
    for (u32 entityIndex = 0; entityIndex < entityCount; ++entityIndex)
    {
        EntityId entity = entities[entityIndex];

        // TODO: Support caching this query
        TransformComponent transform;
        ecs_GetComponent(world, TransformComponent, &entity, 1, &transform);

        StaticMeshComponent staticMesh;
        ecs_GetComponent(world, StaticMeshComponent, &entity, 1, &staticMesh);

        if (staticMesh.isVisible)
        {
            // TODO: Cache model matrix
            mat4 model = Translate(transform.position) *
                         Rotate(transform.rotation) * Scale(transform.scale);

            for (u32 meshIndex = 0; meshIndex < staticMesh.count; ++meshIndex)
            {
                RenderCommand_DrawVertexBuffer *drawMesh =
                    RenderCommandQueuePush(
                        renderCommandQueue, RenderCommand_DrawVertexBuffer);
                SetMesh(drawMesh, assets, staticMesh.meshes[meshIndex]);

                mat4 vp = viewProjection;
                if (staticMesh.useViewModelProjectionMatrix)
                {
                    vp = viewModelViewProjection;
                }
                SetMaterial(&drawMesh->shaderInput, assets,
                    staticMesh.materials[meshIndex], vp, model);

                // Because we were too lazy to do world texture coords
                if (staticMesh.materials[meshIndex] == Material_Checkerboard)
                {
                    // Override texture scale
                    UniformValue *textureScale =
                        FindUniformValue(&drawMesh->shaderInput, UniformValue_TextureScale);
                    Assert(textureScale != NULL);
                    Assert(textureScale->type == UniformType_Vec2);
                    textureScale->v2 = Vec2(transform.scale.x * 0.25f);
                }

                SetEnvironment(&drawMesh->shaderInput, shadowViewProjection,
                    Texture_ShadowMap);
            }
        }
    }
}

internal void AddToLerpBuffer(
        LerpBuffer *buffer, u32 tick, PlayerSnapshot snapshot)
{
    Assert(buffer->length < LERP_BUFFER_LENGTH);
    u32 index =
        (buffer->head + buffer->length++) & LERP_BUFFER_MASK;
    buffer->entries[index].tick = tick;
    buffer->entries[index].snapshot = snapshot;
}

struct LerpResult
{
    LerpEntry *lower = NULL;
    LerpEntry *upper = NULL;
};

internal LerpResult GetLerpEntries(LerpBuffer *buffer, u32 tick)
{
    LerpResult result = {};
    for (u32 cursor = 0; cursor < buffer->length; ++cursor)
    {
        u32 index = (buffer->head + cursor) & LERP_BUFFER_MASK;
        LerpEntry *entry = buffer->entries + index;
        if (entry->tick < tick)
        {
            if (result.lower == NULL || entry->tick > result.lower->tick)
            {
                result.lower = entry;
            }
        }
        else if (entry->tick > tick)
        {
            if (result.upper == NULL || entry->tick < result.upper->tick)
            {
                result.upper = entry;
            }
        }
        else // (entry->tick == tick)
        {
            result.lower = entry;
            result.upper = entry;
            break;
        }
    }

    if (result.lower != NULL)
    {
        // Delete all entries that are older than lower
        // Entries in the circular should be sorted by tick, this may break
        // when packets arrive out of order however in that case we should be
        // inserting into the buffer in the correct order.
        u32 cursor = 0;
        while (cursor < buffer->length)
        {
            u32 index = (buffer->head + cursor) & LERP_BUFFER_MASK;
            LerpEntry *entry = buffer->entries + index;
            if (entry->tick < result.lower->tick)
            {
                Assert(cursor == 0); // Current implementation only supports removing from the head
                buffer->head = (buffer->head + 1) & LERP_BUFFER_MASK;
                buffer->length--;
            }
            else
            {
                break;
            }
        }
    }

    return result;
}

internal void AddPredictionToBuffer(
    PredictionBuffer *buffer, PlayerInput input, PlayerSnapshot snapshot)
{
    Assert(buffer->length < PREDICTION_BUFFER_MAX_LENGTH);
    u32 index =
        (buffer->head + buffer->length++) & PREDICTION_BUFFER_MASK;
    buffer->values[index].input = input;
    buffer->values[index].snapshot = snapshot;
}

inline b32 CompareSnapshots(PlayerSnapshot a, PlayerSnapshot b)
{
    // Only really care about the position
    return (LengthSq(a.position - b.position) <= EPSILON);
}

internal void VerifyPrediction(PredictionBuffer *buffer, u32 playerInputId,
    PlayerSnapshot serverSnapshot, EntityWorld *world, f32 dt,
    NetworkReplicationComponent *networkingComponent, GameAssets *assets)
{
    b32 found = false;
    for (u32 cursor = 0; cursor < buffer->length; ++cursor)
    {
        u32 index = (buffer->head + cursor) & PREDICTION_BUFFER_MASK;
        Prediction *prediction = buffer->values + index;
        if (prediction->input.id == playerInputId)
        {
            // Remove prediction from prediction buffer whether it was correct or not.
            buffer->head = (buffer->head + cursor + 1) & PREDICTION_BUFFER_MASK;
            buffer->length -= (cursor + 1);

            // Check if state matches
            if (!CompareSnapshots(prediction->snapshot, serverSnapshot))
            {
                LogMessage("Prediction error");
                u32 replayCount = buffer->length; // We've removed all acked commands at this point
                PlayerSnapshot currentState = serverSnapshot;
                for (u32 i = 0; i < replayCount; ++i)
                {
                    u32 newIndex = (buffer->head + i) & PREDICTION_BUFFER_MASK;
                    Prediction *newPrediction = buffer->values + index;
                    PlayerSnapshot newState = UpdatePlayer(newPrediction->input,
                        currentState, dt, world, assets);
                    newPrediction->snapshot = newState;
                    currentState = newState;
                }

                // Set both here because we copy snapshots[0] to snapshots[1]
                // for all other entities just before we receive entity updates
                networkingComponent->snapshots[0] = currentState;
                networkingComponent->snapshots[1] = currentState;
            }
            found = true;
            break;
        }
    }

    //Assert(found);
}

struct FindPlayerPositionResult
{
    vec3 position;
    b32 isValid;
};

internal FindPlayerPositionResult FindPlayerPosition(EntityWorld *world)
{
    FindPlayerPositionResult result = {};

    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(world,
        NetworkReplicationComponentTypeId, entities, ArrayCount(entities));

    for (u32 entityIndex = 0; entityIndex < entityCount; ++entityIndex)
    {
        EntityId entity = entities[entityIndex];

        NetworkReplicationComponent network;
        ecs_GetComponent(
            world, NetworkReplicationComponent, &entity, 1, &network);

        if (network.type == EntityType_Player)
        {
            result.position = network.snapshots[0].position;
            result.isValid = true;
        }
    }

    return result;
}

internal void MoveEnemies(EntityWorld *world, GameAssets *assets, f32 dt, GameEventQueue *eventQueue)
{
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(
        world, EnemyComponentTypeId, entities, ArrayCount(entities));

    FindPlayerPositionResult findPlayer = FindPlayerPosition(world);

    for (u32 entityIndex = 0; entityIndex < entityCount; ++entityIndex)
    {
        EntityId entity = entities[entityIndex];

        EnemyComponent enemy;
        ecs_GetComponent(world, EnemyComponent, &entity, 1, &enemy);

        NetworkReplicationComponent network;
        ecs_GetComponent(world, NetworkReplicationComponent, &entity, 1, &network);

        enemy.timeSinceLastAttack += dt;

        PlayerInput input = {};
        if (findPlayer.isValid)
        {
            vec3 position = network.snapshots[0].position;
            vec3 delta = findPlayer.position - position;
            delta.y = 0.0f;

            vec3 worldForward = Vec3(0, 0, -1);
            vec3 dir = Normalize(delta);

            //NOTE: X comes first for calculating the angle on the y axis for a right handed coordinate system.
            f32 angle = Atan2(delta.x, delta.z);

            // NOTE: We subtract PI from the angle because we use -Z as our forward access for movement
            input.viewAngles.y = angle - PI;
            f32 maxDistance = 30.0f;
            f32 t = MapToUnitRange(Length(delta), 0.0f, maxDistance);
            input.forward = 1.0f - t;

            // TODO: Set other view angles for vertical aim

            f32 attackRange = 10.0f;
            if (Length(delta) < attackRange)
            {
                f32 attackRechargeTime = 4.0f;
                if (enemy.timeSinceLastAttack >= attackRechargeTime)
                {
                    input.actions |= PlayerAction_Shoot;
                    enemy.timeSinceLastAttack = 0.0f;
                }
            }
        }
        //else
        //{
            //input.forward = Sin(enemy.sineT);
            //enemy.sineT += dt;
        //}
        ecs_SetComponent(world, EnemyComponent, &entity, 1, &enemy);

        PlayerSnapshot snapshot =
            UpdatePlayer(input, network.snapshots[0], dt, world, assets);

        network.snapshots[0] = snapshot;

        ecs_SetComponent(
            world, NetworkReplicationComponent, &entity, 1, &network);

        if (input.actions & PlayerAction_Shoot)
        {
            ProcessPlayerShooting(input, snapshot, dt, world, assets, eventQueue, entity);
        }
    }
}

internal void DrawBoxCollisionVolumes(EntityWorld *world)
{
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(
        world, BoxCollisionComponentTypeId, entities, ArrayCount(entities));

    for (u32 entityIndex = 0; entityIndex < entityCount; ++entityIndex)
    {
        EntityId entity = entities[entityIndex];

        BoxCollisionComponent box;
        ecs_GetComponent(world, BoxCollisionComponent, &entity, 1, &box);

        TransformComponent transform;
        ecs_GetComponent(world, TransformComponent, &entity, 1, &transform);

        DrawBox(transform.position - box.halfDims,
            transform.position + box.halfDims, Vec4(0, 1, 0, 1));
    }
}

internal void DrawTriangleMeshCollisionVolumes(EntityWorld *world, GameAssets *assets)
{
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(world,
        TriangleCollisionMeshComponentTypeId, entities, ArrayCount(entities));

    for (u32 entityIndex = 0; entityIndex < entityCount; ++entityIndex)
    {
        EntityId entity = entities[entityIndex];

        TriangleCollisionMeshComponent mesh;
        ecs_GetComponent(world, TriangleCollisionMeshComponent, &entity, 1, &mesh);

        TransformComponent transform;
        ecs_GetComponent(world, TransformComponent, &entity, 1, &transform);

        // TODO: Accessor function
        Assert(mesh.triangleMeshId < ArrayCount(assets->triangleMeshes));
        TriangleMesh triangleMesh = assets->triangleMeshes[mesh.triangleMeshId];

        // TODO: Figure out a better way of reducing our 3D scale to a scalar value
        DrawSphere(transform.position, triangleMesh.radius * transform.scale.x, Vec4(0, 1, 0, 1));
    }
}

internal void DrawEntityCollisionVolumes(EntityWorld *world, GameAssets *assets)
{
    DrawBoxCollisionVolumes(world);
    DrawTriangleMeshCollisionVolumes(world, assets);
}

internal void DrawHealthBar(RenderCommandQueue *renderCommandQueue,
    GameAssets *assets, mat4 projection, vec2 position, f32 healthPercent)
{
    f32 width = 260.0f;
    f32 height = 30.0f;

    DrawColouredQuad(renderCommandQueue, assets, projection, position,
        Vec2(width, height), Vec4(0.06, 0.06, 0.06, 1));

    f32 healthWidth = width * healthPercent;

    // Offset center position to the left so that width increase is only visible on the right side
    vec2 healthPosition = position;
    healthPosition.x -= (width - healthWidth) * 0.5f;
    DrawColouredQuad(renderCommandQueue, assets, projection, healthPosition,
        Vec2(healthWidth, height), Vec4(0.75, 0.05, 0.0, 1));
}

internal void DrawHUD(RenderCommandQueue *renderCommandQueue,
    GameAssets *assets, mat4 projection, f32 framebufferWidth,
    f32 framebufferHeight, f32 healthPercent)
{
    // Draw crosshair
    DrawColouredQuad(renderCommandQueue, assets, projection,
        Vec2(framebufferWidth, framebufferHeight) * 0.5f, Vec2(2, 2),
        Vec4(1, 0, 1, 1));

    vec2 healthBarPosition = Vec2(framebufferWidth - 300.0f, 100);
    DrawHealthBar(renderCommandQueue, assets, projection,
        healthBarPosition, healthPercent);
}

internal void InterpolateEntitySnapshots(EntityWorld *world, f32 interp)
{
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(world,
        NetworkReplicationComponentTypeId, entities, ArrayCount(entities));

    for (u32 entityIndex = 0; entityIndex < entityCount; ++entityIndex)
    {
        EntityId entity = entities[entityIndex];

        NetworkReplicationComponent network;
        ecs_GetComponent(world, NetworkReplicationComponent, &entity, 1, &network);

        if (ecs_HasComponent(world, TransformComponentTypeId, entity))
        {
            TransformComponent transform;
            ecs_GetComponent(world, TransformComponent, &entity, 1, &transform);

            transform.rotation = LerpQuat(network.snapshots[1].rotation,
                network.snapshots[0].rotation, interp);
            transform.position = Lerp(network.snapshots[1].position,
                network.snapshots[0].position, interp);

            ecs_SetComponent(world, TransformComponent, &entity, 1, &transform);
        }
    }
}

internal void SetLocalPlayerMeshVisibility(GameState *gameState, b32 isVisible)
{
    if (gameState->localPlayerEntityId != MAX_ENTITIES)
    {
        if (ecs_HasComponent(&gameState->world, StaticMeshComponentTypeId,
                gameState->localPlayerEntityId))
        {
            StaticMeshComponent staticMesh;
            ecs_GetComponent(&gameState->world, StaticMeshComponent,
                &gameState->localPlayerEntityId, 1, &staticMesh);
            staticMesh.isVisible = isVisible;
            ecs_SetComponent(&gameState->world, StaticMeshComponent,
                &gameState->localPlayerEntityId, 1, &staticMesh);
        }
    }
}

internal b32 IsControllingPlayer(GameState *gameState)
{
    b32 result = false;
    if (gameState->localPlayerEntityId != MAX_ENTITIES)
    {
        if (ecs_HasComponent(&gameState->world, TransformComponentTypeId,
                gameState->localPlayerEntityId))
        {
            result = true;
        }
    }

    return result;
}

internal void TestCollisionDetection(GameAssets *assets)
{
    RandomNumberGenerator rng;
    rng.state = 0x77AC123;
    TriangleMesh triangleMesh = GetTriangleMesh(assets, TriangleMesh_Warehouse);

    vec3 center = Vec3(-4, 1, 0);
    f32 radius = 32.0f;
    u32 rayCount = 64;

    u64 totalCycleCount = 0;
    for (u32 rayIndex = 0; rayIndex < rayCount; ++rayIndex)
    {
        f32 y = RandomBilateral(&rng) * PI;
        f32 x = RandomBilateral(&rng) * PI;

        quat rotation = Quat(Vec3(1, 0, 0), x) * Quat(Vec3(0, 1, 0), y);
        vec3 v = RotateVector(Vec3(0, 0, 1), rotation);
        vec3 start = center + v * radius;
        vec3 end = center - v * radius;
        u64 cycleCountStart = __rdtsc();
        RaycastResult raycastResult = RayIntersectTriangleMesh(
                triangleMesh, Vec3(0), Quat(), Vec3(1), start, end, rayIndex == 0, false);
        totalCycleCount += __rdtsc() - cycleCountStart;
        if (raycastResult.tmin > 0.0f)
        {
            vec3 hitPoint = Lerp(start, end, raycastResult.tmin);
            DrawPoint(hitPoint, 0.5f, Vec4(1));
            DrawLine(start, hitPoint, Vec4(1));
        }
        else
        {
            DrawPoint(start, 0.5f, Vec4(1));
            DrawLine(start, end, Vec4(1, 0, 0, 1));
        }
    }

    // (cold data): RayIntersectTriangleMesh: 5,035,978 cycles
    // Averaging 32: ~3.5e6 cycles
    f64 averageCycleCount = (f64)totalCycleCount / (f64)rayCount;
    LogMessage("RayIntersectTriangleMesh: %g Kc", averageCycleCount / 1000.0);

    // Od: Warehouse ~3.4 Mcycles -> ~180 Kcycles (savings of 95%)
    // O2: Warehouse ~130 Kcycles -> ~15 Kcycles (savings of 89%)

#if 0
    vec3 start = Vec3(-100, 2, 2);
    vec3 end = Vec3(100, 2, 2);
    u64 cycleCountStart = __rdtsc();
    RaycastResult raycastResult = RayIntersectTriangleMesh(
        triangleMesh, Vec3(0), Quat(), Vec3(1), start, end, true);
    //LogMessage("RayIntersectTriangleMesh: %llu cycles", __rdtsc() - cycleCountStart);
    if (raycastResult.tmin > 0.0f)
    {
        vec3 hitPoint = Lerp(start, end, raycastResult.tmin);
        DrawPoint(hitPoint, 0.5f, Vec4(1));
        DrawLine(start, hitPoint, Vec4(1));
    }
    else
    {
        DrawLine(start, end, Vec4(1, 0, 0, 1));
    }
#endif
}

internal void DrawPBRTestScene(RenderCommandQueue *renderCommandQueue,
    GameAssets *assets, mat4 viewProjection, mat4 shadowViewProjection)
{
    u32 rows = 4;
    u32 cols = 4;
    for (u32 y = 0; y <= rows; ++y)
    {
        for (u32 x = 0; x <= cols; ++x)
        {
            f32 roughness = Clamp((f32)x / (f32)cols, 0.01f, 1.0f);
            f32 metallic = (f32)y / (f32)rows;

            vec3 position = Vec3(x * 2.5f, y * 2.5f, 0);
            mat4 model = Translate(position);
            RenderCommand_DrawVertexBuffer *drawMesh = RenderCommandQueuePush(
                renderCommandQueue, RenderCommand_DrawVertexBuffer);
            SetMesh(drawMesh, assets, Mesh_Sphere);
            drawMesh->shaderInput.shader = Shader_ColorDiffuse;
            PushUniformMat4(&drawMesh->shaderInput,
                UniformValue_ViewProjectionMatrix, viewProjection);
            PushUniformMat4(
                &drawMesh->shaderInput, UniformValue_ModelMatrix, model);
            PushUniformVec4(&drawMesh->shaderInput, UniformValue_Color,
                Vec4(0.2, 0.9, 0.1, 1));
            PushUniformFloat(
                &drawMesh->shaderInput, UniformValue_Roughness, roughness);
            PushUniformFloat(
                &drawMesh->shaderInput, UniformValue_Metallic, metallic);
            PushUniformBuffer(&drawMesh->shaderInput, UniformValue_LightingData,
                UniformBuffer_LightingData);
            PushUniformTextureCubeMap(&drawMesh->shaderInput,
                UniformValue_IrradianceCubeMap, Texture_Irradiance);
            PushUniformTextureCubeMap(&drawMesh->shaderInput,
                UniformValue_RadianceCubeMap, Texture_Radiance);
            PushUniformTexture2D(
                &drawMesh->shaderInput, UniformValue_BrdfLut, Texture_BrdfLut);
            SetEnvironment(&drawMesh->shaderInput, shadowViewProjection,
                Texture_ShadowMap);
        }
    }

    { // Roughness Test
        mat4 model = Translate(Vec3(0, 0, 5));
        RenderCommand_DrawVertexBuffer *drawMesh = RenderCommandQueuePush(
            renderCommandQueue, RenderCommand_DrawVertexBuffer);
        SetMesh(drawMesh, assets, Mesh_Sphere);
        SetMaterial(&drawMesh->shaderInput, assets, Material_RoughnessTexture,
            viewProjection, model);
        SetEnvironment(
            &drawMesh->shaderInput, shadowViewProjection, Texture_ShadowMap);
    }

    {
        mat4 model = Translate(Vec3(5, 0, 5));
        RenderCommand_DrawVertexBuffer *drawMesh = RenderCommandQueuePush(
            renderCommandQueue, RenderCommand_DrawVertexBuffer);
        SetMesh(drawMesh, assets, Mesh_Sphere);
        SetMaterial(&drawMesh->shaderInput, assets, Material_MetallicTexture,
            viewProjection, model);
        SetEnvironment(
            &drawMesh->shaderInput, shadowViewProjection, Texture_ShadowMap);
    }
}

internal void DrawInstancingTest(RenderCommandQueue *renderCommandQueue,
    GameAssets *assets, mat4 projectionMatrix, mat4 viewMatrix,
    mat4 shadowViewProjection, MemoryArena *tempArea)
{
    u32 instanceCount = 32;
    mat4 *modelMatrices = AllocateArray(tempArea, mat4, instanceCount);
    for (u32 i = 0; i < instanceCount; ++i)
    {
        modelMatrices[i] = Translate(Vec3(i * 2.5f,-2.0f, 10.0f));
    }
    RenderCommand_UpdateVertexBuffer *updateInstanceData =
        RenderCommandQueuePush(
            renderCommandQueue, RenderCommand_UpdateVertexBuffer);
    updateInstanceData->id = VertexBuffer_VertexInstanced;
    updateInstanceData->instanceDataLength = sizeof(mat4) * instanceCount;
    updateInstanceData->instanceData = modelMatrices;

    RenderCommand_DrawVertexBuffer *drawMesh = RenderCommandQueuePush(
        renderCommandQueue, RenderCommand_DrawVertexBuffer);
    SetMesh(drawMesh, assets, Mesh_SphereInstanced);
    Material *material = GetMaterial(assets, Material_GreenInstanced);
    drawMesh->shaderInput = material->shaderInput;
    SetEnvironment(&drawMesh->shaderInput, shadowViewProjection,
        Texture_ShadowMap);
    PushUniformMat4(&drawMesh->shaderInput, UniformValue_ViewProjectionMatrix,
        projectionMatrix * viewMatrix);
    drawMesh->instanceCount = instanceCount;
}

GAME_CLIENT_UPDATE(GameClientUpdate)
{
    LogMessage = memory->logMessage;

    Assert(sizeof(GameState) <= memory->persistentStorageSize);
    GameState *gameState = (GameState *)memory->persistentStorage;

    g_DebugDrawingSystem = &gameState->debugDrawingSystem;

    b32 loadShaders = false;
    if (!gameState->isInitialized)
    {
        InitializeMemoryArena(&gameState->persitentArena,
            (u8 *)memory->persistentStorage + sizeof(GameState),
            memory->persistentStorageSize - sizeof(GameState));
        InitializeMemoryArena(&gameState->transientArena,
            memory->transientStorage, memory->transientStorageSize);

        gameState->frameArena =
            SubAllocateArena(&gameState->transientArena, Megabytes(16));
        gameState->collisionMeshArena =
            SubAllocateArena(&gameState->transientArena, Megabytes(2));

        gameState->terrainArena =
            SubAllocateArena(&gameState->transientArena, TERRAIN_ARENA_SIZE);

        // TODO: Sub allocate an arena for entity data
        InitializeEntityWorld(&gameState->world, &gameState->persitentArena);

        gameState->localPlayerEntityId = MAX_ENTITIES;

        gameState->rng.state = 0x5023FA21;

        InitializeAssets(&gameState->assets, memory, &gameState->transientArena);
        LoadCollisionMeshes(&gameState->assets, memory, &gameState->collisionMeshArena);

        MemoryArena tempArea = SubAllocateArena(&gameState->transientArena, Megabytes(64));
        for (u32 i = 0; i < MAX_TRIANGLE_MESHES; ++i)
        {
            TriangleMesh *triangleMesh = &gameState->assets.triangleMeshes[i];
            BuildAabbTree(
                triangleMesh, &gameState->collisionMeshArena, &tempArea);
            ClearMemoryArena(&tempArea);
        }

        InitializeTerrain(&gameState->assets, memory, &gameState->terrainArena, true);
        gameState->terrainQuadTree = BuildTerrainQuadTree(8, &gameState->terrainArena);
        CreateLevel(&gameState->world, &gameState->assets, false);
        CreateViewModelEntities(gameState);
        CreateBuildingPartPreviewEntities(gameState);

        loadShaders = true;

        gameState->isInitialized = true;
    }

    ClearMemoryArena(&gameState->frameArena);

    if (memory->wasCodeReloaded)
    {
        LogMessage("Game code was reloaded!");
        loadShaders = true;
    }

    if (loadShaders)
    {
        InitializeShadersAndMaterials(&gameState->assets, memory, &gameState->frameArena);
    }

    if (gameState->framebufferWidth != memory->framebufferWidth ||
        gameState->framebufferHeight != memory->framebufferHeight)
    {
        LogMessage("Framebuffer resized, recreating frame buffers");
        gameState->framebufferWidth = (f32)memory->framebufferWidth;
        gameState->framebufferHeight = (f32)memory->framebufferHeight;
        RenderCommand_CreateFramebuffer *resizeBackbuffer = RenderCommandQueuePush(
                &memory->renderCommandQueue, RenderCommand_CreateFramebuffer);
        resizeBackbuffer->id = Framebuffer_Backbuffer;
        resizeBackbuffer->width = memory->framebufferWidth;
        resizeBackbuffer->height = memory->framebufferHeight;

        f32 resolutionScalingFactor = 1.0f;
        RenderCommand_CreateFramebuffer *createHdrBuffer = RenderCommandQueuePush(
                &memory->renderCommandQueue, RenderCommand_CreateFramebuffer);
        createHdrBuffer->id = Framebuffer_HdrBuffer;
        createHdrBuffer->colorTexture = Texture_HdrBuffer;
        createHdrBuffer->depthTexture = Texture_DepthBuffer;
        createHdrBuffer->width = (u32)(gameState->framebufferWidth * resolutionScalingFactor);
        createHdrBuffer->height = (u32)(gameState->framebufferHeight * resolutionScalingFactor);

    }

    gameState->mouseRelX += input->mouseRelPosX;
    gameState->mouseRelY += input->mouseRelPosY;

    if (WasPressed(input->buttonStates[KEY_F8]))
    {
        gameState->useFreeRoamCamera = !gameState->useFreeRoamCamera;
        SetLocalPlayerMeshVisibility(gameState, gameState->useFreeRoamCamera);
    }

    //TestCollisionDetection(&gameState->assets);

    AgeOut(&gameState->debugDrawingSystem, frameTime);

    EntityWorld *world = &gameState->world;

    if (IsControllingPlayer(gameState))
    {
        if (WasPressed(input->buttonStates[KEY_TAB]))
        {
            if (gameState->showInventory)
            {
                InventoryOwnerComponent inventoryOwner;
                ecs_GetComponent(world, InventoryOwnerComponent,
                    &gameState->localPlayerEntityId, 1, &inventoryOwner);
                if (inventoryOwner.openContainer != NULL_ENTITY_ID)
                {
                    // Send close container command
                    InventoryUIState *uiState = &gameState->inventoryUIState;

                    // Generate inventory close container command
                    Assert(uiState->commandQueueLength <
                            ArrayCount(uiState->commandQueue));
                    InventoryCommand *command =
                        uiState->commandQueue + uiState->commandQueueLength++;
                    ClearToZero(command, sizeof(*command));
                    command->action = InventoryAction_CloseContainer;
                }
            }

            gameState->showInventory = !gameState->showInventory;
        }
    }

    InterpolateEntitySnapshots(world, interp);

    for (u32 particleSystemIndex = 0;
         particleSystemIndex < gameState->particleSystemCount;
         particleSystemIndex++)
    {
        ParticleSystem *particleSystem =
            gameState->particleSystems + particleSystemIndex;
        UpdateParticleSystem(particleSystem, frameTime);
        DrawParticleSystem(particleSystem);
    }
    CleanUpEmptyParticleSystems(gameState);

    //DrawEntityCollisionVolumes(world, &gameState->assets);

    {
        RenderCommand_Clear *clear = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_Clear);
        clear->color = Vec4(0.0f, 0.0f, 0.0f, 1.0f);
    }

    f32 aspect = gameState->framebufferWidth / gameState->framebufferHeight;
    f32 adsFov = 50.0f;
    f32 defaultFov = 90.0f;
    f32 defaultViewModelFov = 60.0f;
    f32 fov = defaultFov;
    f32 viewModelFov = defaultViewModelFov;

    // Hide other view models
    // TODO: Or just store the previous index
    for (u32 i = 0; i < MAX_VIEW_MODELS; ++i)
    {
        EntityId entityId = gameState->viewModels[i];
        StaticMeshComponent staticMesh;
        ecs_GetComponent(
                world, StaticMeshComponent, &entityId, 1, &staticMesh);
        staticMesh.isVisible = false;
        ecs_SetComponent(
                world, StaticMeshComponent, &entityId, 1, &staticMesh);
    }

    // Imagine if used intermediate mode rendering scheme for this, would need
    // to hide all these entities each frame
    for (u32 i = 0; i < MAX_BUILDING_PARTS; ++i)
    {
        EntityId entityId = gameState->buildingPartPreviews[i];
        StaticMeshComponent staticMesh;
        ecs_GetComponent(
                world, StaticMeshComponent, &entityId, 1, &staticMesh);
        staticMesh.isVisible = false;
        ecs_SetComponent(
                world, StaticMeshComponent, &entityId, 1, &staticMesh);
    }


    b32 showInventory = false;
    b32 showEquipmentBar = false;
    b32 showHud = false;
    quat rotation = Quat();
    vec3 position = Vec3(0, 0, 0);
    vec3 velocity = Vec3(0, 0, 0);
    if (IsControllingPlayer(gameState))
    {
        TransformComponent transform;
        ecs_GetComponent(world, TransformComponent,
            &gameState->localPlayerEntityId, 1, &transform);

        rotation = transform.rotation;
        position = GetPlayerCameraPosition(transform.position);

        InventorySlot activeSlot =
            GetActiveEquipmentSlot(world, gameState->localPlayerEntityId);
        if (activeSlot.itemId == Item_BuildingPlan)
        {
            PreviewBuilding(world, &gameState->assets,
                gameState->localPlayerEntityId, ACTIVE_BUILDING_PART,
                gameState->buildingPartPreviews,
                ArrayCount(gameState->buildingPartPreviews));
        }

        u32 viewModelIndex = GetItemViewModel(activeSlot.itemId);
        if (viewModelIndex < MAX_VIEW_MODELS)
        {
            if (viewModelIndex == ViewModel_Pistol)
            {
                EntityId entityId = gameState->viewModels[viewModelIndex];
                TransformComponent viewModelTransform;
                ecs_GetComponent(world, TransformComponent, &entityId, 1,
                    &viewModelTransform);
                vec3 forward = RotateVector(Vec3(0, 0, -1), rotation);

                vec3 baseOffset = Vec3(0.04, -0.03, 0);
                vec3 adsOffset = Vec3(0.0, -0.02, 0);
                vec3 offset = Lerp(
                    baseOffset, adsOffset, EaseOutQuad(gameState->adsAmount));
                vec3 rotatedOffset = RotateVector(offset, rotation);
                viewModelTransform.position =
                    position + forward * 0.09f + rotatedOffset;
                viewModelTransform.rotation =
                    rotation * Quat(Vec3(0, 1, 0), PI);
                ecs_SetComponent(world, TransformComponent, &entityId, 1,
                    &viewModelTransform);

                StaticMeshComponent viewModelStaticMesh;
                ecs_GetComponent(world, StaticMeshComponent, &entityId, 1,
                    &viewModelStaticMesh);
                viewModelStaticMesh.isVisible = true;
                ecs_SetComponent(world, StaticMeshComponent, &entityId, 1,
                    &viewModelStaticMesh);

                fov =
                    Lerp(defaultFov, adsFov, EaseOutQuad(gameState->adsAmount));
                viewModelFov = Lerp(defaultViewModelFov, adsFov,
                    EaseOutQuad(gameState->adsAmount));
            }
            else if (viewModelIndex == ViewModel_Shotgun)
            {
                EntityId entityId = gameState->viewModels[viewModelIndex];
                TransformComponent viewModelTransform;
                ecs_GetComponent(world, TransformComponent, &entityId, 1,
                    &viewModelTransform);
                vec3 forward = RotateVector(Vec3(0, 0, -1), rotation);

                vec3 baseOffset = Vec3(0.03, -0.03, 0);
                vec3 adsOffset = Vec3(0.0, -0.025, 0);
                vec3 offset = Lerp(
                    baseOffset, adsOffset, EaseOutQuad(gameState->adsAmount));
                vec3 rotatedOffset = RotateVector(offset, rotation);
                viewModelTransform.position =
                    position + forward * 0.07f + rotatedOffset;
                viewModelTransform.rotation =
                    rotation * Quat(Vec3(0, 1, 0), PI);
                ecs_SetComponent(world, TransformComponent, &entityId, 1,
                    &viewModelTransform);

                StaticMeshComponent viewModelStaticMesh;
                ecs_GetComponent(world, StaticMeshComponent, &entityId, 1,
                    &viewModelStaticMesh);
                viewModelStaticMesh.isVisible = true;
                ecs_SetComponent(world, StaticMeshComponent, &entityId, 1,
                    &viewModelStaticMesh);

                fov =
                    Lerp(defaultFov, adsFov, EaseOutQuad(gameState->adsAmount));
                viewModelFov = Lerp(defaultViewModelFov, adsFov,
                    EaseOutQuad(gameState->adsAmount));
            }
            else
            {
                // Clear ADS state
                gameState->adsAmount = 0.0f;
                gameState->adsRate = 0.0f;
                gameState->isAimed = false;
            }
        }

        showInventory = gameState->showInventory;
        showEquipmentBar = true;
        showHud = true;
    }

    if (gameState->useFreeRoamCamera)
    {
        UpdateFreeRoamCamera(&gameState->freeRoamCamera, memory, input, frameTime);
        position = gameState->freeRoamCamera.position;
        velocity = gameState->freeRoamCamera.velocity;
        vec3 cameraRotation = gameState->freeRoamCamera.rotation;
        rotation = Quat(Vec3(0, 1, 0), cameraRotation.y) *
            Quat(Vec3(1, 0, 0), cameraRotation.x);
        showInventory = false;

        fov = defaultFov;
    }

    mat4 projection = Perspective(fov, aspect, 0.01f, 10000.0f);
    mat4 viewModelProjection = Perspective(viewModelFov, aspect, 0.01f, 100.0f);
    mat4 skyProjection = Perspective(fov, aspect, 0.01f, 1000000.0f);
    mat4 uiProjection = Orthographic(
        0.0f, gameState->framebufferWidth, 0.0f, gameState->framebufferHeight);


    f32 sunIntensity = 0.8f; //1.5f;
    f32 ambientIntensity = 0.04f; //1.2;
    vec3 sunColor = Vec3(0.764, 0.451, 0.194) * sunIntensity;
    vec3 ambientColor = SrgbToLinear(Vec3(0.26, 0.45, 0.73)) * ambientIntensity;
    vec3 cameraPosition = position;
    mat4 view = Rotate(Conjugate(rotation)) * Translate(-position);
    mat4 skyView = Rotate(Conjugate(rotation));

    quat sunRotation = Quat(Vec3(1, 0, 0), -PI * 0.2f);
    vec3 sunDirection = RotateVector(Vec3(0, 0, 1), sunRotation);
    mat4 shadowProjection = Orthographic(-20, 20, -20, 20, 1.0f, 60.0f);
    mat4 shadowView = Rotate(Conjugate(sunRotation)) * Translate(-position -sunDirection * 15.0f);

    // Update lighting data
    {   
        LightingDataUniformBuffer *lightingData = AllocateStruct(
                &gameState->frameArena, LightingDataUniformBuffer);
        lightingData->sunDirection = Vec4(sunDirection, 0);
        lightingData->cameraPosition = Vec4(cameraPosition, 0);
        lightingData->sunColor = Vec4(sunColor, 0);
        lightingData->ambientColor = Vec4(ambientColor, 0);

        lightingData->pointLights[0].position = Vec4(4, 1, -2, 0);
        lightingData->pointLights[0].color = Vec4(1, 0.2, 0.1, 0);
        lightingData->pointLights[0].attenuation = Vec4(2.0f, 0.2f, 7.0f, 0.0f);
        lightingData->pointLightCount = 1;

        lightingData->spotLights[0].position = Vec4(-2, 1, 0, 0);
        lightingData->spotLights[0].color = Vec4(0.6, 0.2, 0.05, 0);
        lightingData->spotLights[0].direction = Vec4(Normalize(Vec3(0, -0.1, -1)), 0.0);
        lightingData->spotLights[0].attenuation[0] = Vec4(10.0f, 0.2f, 16.0f, 0.0f);
        lightingData->spotLights[0].attenuation[1] = Vec4(Cos(PI * 0.3), Cos(PI * 0.45), 0.0f, 0.0f);
        lightingData->spotLightCount = 1;

        RenderCommand_UpdateUniformBuffer *updateLightingData =
            RenderCommandQueuePush(&memory->renderCommandQueue,
                RenderCommand_UpdateUniformBuffer);
        updateLightingData->id = UniformBuffer_LightingData;
        updateLightingData->data = lightingData;
        updateLightingData->length = sizeof(*lightingData);
        updateLightingData->offset = 0;
    }

    // Render shadows
    {
        RenderCommand_BindFramebuffer *bindShadowMap = RenderCommandQueuePush(
                &memory->renderCommandQueue, RenderCommand_BindFramebuffer);
        bindShadowMap->id = Framebuffer_ShadowMap;

        RenderCommand_Clear *clear = RenderCommandQueuePush(
            &memory->renderCommandQueue, RenderCommand_Clear);
        clear->color = Vec4(0, 0, 0, 1);

        RenderStaticMeshes(&gameState->world, &gameState->assets,
            &memory->renderCommandQueue, shadowProjection, shadowView,
            shadowProjection, shadowView, viewModelProjection);
    }

    // Render opaque geometry
    {
        RenderCommand_BindFramebuffer *bindFramebuffer = RenderCommandQueuePush(
                &memory->renderCommandQueue, RenderCommand_BindFramebuffer);
        bindFramebuffer->id = Framebuffer_HdrBuffer;

        RenderCommand_Clear *clear = RenderCommandQueuePush(
                &memory->renderCommandQueue, RenderCommand_Clear);
        clear->color = Vec4(0.173, 0.451, 0.663, 1.0f);

#if 0
        {   // Render skydome
            RenderCommand_DrawVertexBuffer *drawSphere = RenderCommandQueuePush(
                    &memory->renderCommandQueue, RenderCommand_DrawVertexBuffer);
            SetMesh(drawSphere, &gameState->assets, Mesh_Sphere);

            mat4 skyDomeModelMatrix = Scale(100000.0f);
            drawSphere->shader = Shader_SkyGradient;
            PushUniformMat4(drawSphere, UniformValue_Mvp,
                    skyProjection * skyView * skyDomeModelMatrix);
            PushUniformMat4(
                    drawSphere, UniformValue_ModelMatrix, skyDomeModelMatrix);
            PushUniformBuffer(drawSphere, UniformValue_LightingData,
                UniformBuffer_LightingData);
        }
#else
        {   // Render skybox
            RenderCommand_DrawVertexBuffer *drawSkyBox = RenderCommandQueuePush(
                    &memory->renderCommandQueue, RenderCommand_DrawVertexBuffer);
            SetMesh(drawSkyBox, &gameState->assets, Mesh_Cube);

            mat4 model = Scale(100000.0f);
            drawSkyBox->shaderInput.shader = Shader_CubeMap;
            PushUniformMat4(&drawSkyBox->shaderInput, UniformValue_ViewProjectionMatrix,
                skyProjection * skyView);
            PushUniformMat4(
                &drawSkyBox->shaderInput, UniformValue_ModelMatrix, model);
            PushUniformTextureCubeMap(&drawSkyBox->shaderInput,
                UniformValue_CubeMap, Texture_Irradiance);
        }
#endif
#if 0
        DrawPBRTestScene(&memory->renderCommandQueue, &gameState->assets,
            projection * view, shadowProjection * shadowView);
        DrawInstancingTest(&memory->renderCommandQueue, &gameState->assets,
            projection, view, shadowProjection * shadowView,
            &gameState->frameArena);
#endif

#if 1
        RenderStaticMeshes(&gameState->world, &gameState->assets,
            &memory->renderCommandQueue, projection, view, shadowProjection,
            shadowView, viewModelProjection);
#endif

        RenderTerrain(&gameState->world, &gameState->assets,
            &memory->renderCommandQueue, projection * view,
            shadowProjection * shadowView, gameState->terrainQuadTree,
            cameraPosition, &gameState->frameArena);

#if 0
        {   // Terrain normals
            mat4 model = Translate(Vec3(-2, 1, 12)) * Scale(Vec3(4.0f)); // NOTE: Duplicate of Terrain
            RenderCommand_DrawVertexBuffer *drawTerrainNormals = RenderCommandQueuePush(
                    &memory->renderCommandQueue, RenderCommand_DrawVertexBuffer);
            drawTerrainNormals->id = VertexBuffer_Vertex;
            SetMesh(drawTerrainNormals, Mesh_Patch);
            drawTerrainNormals->shader = Shader_TerrainNormals;
            PushUniformMat4(drawTerrainNormals, UniformValue_Mvp, projection * view * model);
            PushUniformVec4(drawTerrainNormals, UniformValue_Color, Vec4(1, 0, 1, 0));
            PushUniformVec2(drawTerrainNormals, UniformValue_TextureScale, Vec2(1, 1));
            PushUniformMat4(drawTerrainNormals, UniformValue_ProjectionMatrix, projection);
            PushUniformMat4(drawTerrainNormals, UniformValue_ViewMatrix, view);
            PushUniformMat4(drawTerrainNormals, UniformValue_ModelMatrix, model);
            PushUniformTexture2D(drawTerrainNormals, UniformValue_HeightMap, Texture_HeightMap);
        }
#endif
    }

    // Render debug drawing
    {
        VertexPC *debugDrawingVertices = AllocateArray(
                &gameState->frameArena, VertexPC, MAX_DEBUG_DRAWING_VERTICES);

        u32 debugDrawingVertexCount =
            GetVertices(&gameState->debugDrawingSystem, debugDrawingVertices,
                    MAX_DEBUG_DRAWING_VERTICES);

        RenderCommand_UpdateVertexBuffer *updateDebugData =
            RenderCommandQueuePush(
                    &memory->renderCommandQueue, RenderCommand_UpdateVertexBuffer);
        updateDebugData->id = VertexBuffer_DebugData;
        updateDebugData->length =
            sizeof(VertexPC) * debugDrawingVertexCount;
        updateDebugData->data = debugDrawingVertices;

        RenderCommand_DrawVertexBuffer *drawDebugData = RenderCommandQueuePush(
                &memory->renderCommandQueue, RenderCommand_DrawVertexBuffer);
        drawDebugData->id = VertexBuffer_DebugData;
        drawDebugData->first = 0;
        drawDebugData->count = debugDrawingVertexCount;
        drawDebugData->primitive = Primitive_Lines;
        drawDebugData->shaderInput.shader = Shader_PositionColor;
        PushUniformMat4(&drawDebugData->shaderInput, UniformValue_Mvp, projection * view);
    }

    // Clear text buffer
    gameState->textBuffer.count = 0;

    // FIXME: Don't render HUD to the HDR buffer
    if (showInventory)
    {
        // NOTE: I'm assuming we can only show the inventory if the player is alive
        Assert(IsControllingPlayer(gameState));

        // TODO: Probably want a way to only get the inventory slots
        InventorySlot inventory[PLAYER_TOTAL_INVENTORY_SLOT_COUNT];
        GetInventorySlots(world, gameState->localPlayerEntityId, inventory,
            ArrayCount(inventory));

        InventorySlot container[PLAYER_TOTAL_INVENTORY_SLOT_COUNT];
        u32 containerSize = 0;
        EntityId containerOwner = NULL_ENTITY_ID;
        InventoryOwnerComponent ownerComponent;
        ecs_GetComponent(world, InventoryOwnerComponent,
            &gameState->localPlayerEntityId, 1, &ownerComponent);
        if (ecs_HasComponent(
                world, ContainerComponentTypeId, ownerComponent.openContainer))
        {
            GetInventorySlots(world, ownerComponent.openContainer, container,
                ArrayCount(container));
            containerSize = ArrayCount(container);
            containerOwner = ownerComponent.openContainer;
        }

        // Only pass the inventory slots, not the equipment slots
        DrawInventory(&gameState->inventoryUIState, &memory->renderCommandQueue,
            &gameState->assets, &gameState->textBuffer, input,
            gameState->framebufferWidth, gameState->framebufferHeight,
            gameState->localPlayerEntityId, inventory, ArrayCount(inventory),
            containerOwner, container, containerSize);

        // Don't show the equipment bar as we draw it when calling DrawInventory
        showEquipmentBar = false;
    }
    else
    {
        // Clear any in progress inventory operations
        gameState->inventoryUIState.isDragInProgress = false;
    }

    if (showEquipmentBar)
    {
        // NOTE: I'm assuming we can only show the equipment bar if the player is alive
        Assert(IsControllingPlayer(gameState));

        // TODO: Probably want a way to only get the equipment slots
        InventorySlot inventory[PLAYER_TOTAL_INVENTORY_SLOT_COUNT];
        GetInventorySlots(world, gameState->localPlayerEntityId, inventory,
            ArrayCount(inventory));

        InventoryOwnerComponent inventoryOwner;
        ecs_GetComponent(world, InventoryOwnerComponent,
            &gameState->localPlayerEntityId, 1, &inventoryOwner);

        InventorySlot *equipmentSlots = inventory + PLAYER_INVENTORY_SLOT_COUNT;
        DrawEquipmentBar(&gameState->inventoryUIState,
            &memory->renderCommandQueue, &gameState->assets,
            &gameState->textBuffer, input, uiProjection,
            gameState->framebufferWidth, gameState->framebufferHeight,
            gameState->localPlayerEntityId, equipmentSlots,
            PLAYER_EQUIPMENT_BAR_SLOT_COUNT, true,
            inventoryOwner.activeEquipmentSlot);
    }

    if (showHud)
    {
        HealthComponent health;
        ecs_GetComponent(world, HealthComponent,
            &gameState->localPlayerEntityId, 1, &health);

        f32 healthPercent = (f32)health.currentHealth / (f32)health.maxHealth;
        DrawHUD(&memory->renderCommandQueue, &gameState->assets, uiProjection,
            gameState->framebufferWidth, gameState->framebufferHeight,
            healthPercent);
    }


    {
        // Draw FPS counter
        char buffer[80];
        snprintf(buffer, sizeof(buffer), "FPS: %d (%0.4gms)", (i32)(1.0f / frameTime), frameTime);
        DrawString(&memory->renderCommandQueue, &gameState->assets,
            &gameState->textBuffer, buffer,
            Vec2(gameState->framebufferWidth - 250,
                gameState->framebufferHeight - 30) + Vec2(1, -1),
            uiProjection, Vec4(0, 0, 0, 1));
        DrawString(&memory->renderCommandQueue, &gameState->assets,
            &gameState->textBuffer, buffer,
            Vec2(gameState->framebufferWidth - 250,
                gameState->framebufferHeight - 30),
            uiProjection, Vec4(1));
    }

    // Update text buffer
    {

        RenderCommand_UpdateVertexBuffer *updateText =
            RenderCommandQueuePush(
                    &memory->renderCommandQueue, RenderCommand_UpdateVertexBuffer);
        updateText->id = VertexBuffer_Text;
        updateText->length = sizeof(Vertex) * gameState->textBuffer.count;
        updateText->data = gameState->textBuffer.vertices;
    }

    // Final composite
    {
        RenderCommand_BindFramebuffer *bindBackBuffer = RenderCommandQueuePush(
                &memory->renderCommandQueue, RenderCommand_BindFramebuffer);
        bindBackBuffer->id = Framebuffer_Backbuffer;

        // Draw hdr buffer
        RenderCommand_DrawVertexBuffer *drawHdrBuffer = RenderCommandQueuePush(
                &memory->renderCommandQueue, RenderCommand_DrawVertexBuffer);
        SetMesh(drawHdrBuffer, &gameState->assets, Mesh_Quad);
        drawHdrBuffer->shaderInput.shader = Shader_ToneMapping;
        PushUniformTexture2D(&drawHdrBuffer->shaderInput, UniformValue_Texture,
            Texture_HdrBuffer);
        PushUniformMat4(&drawHdrBuffer->shaderInput,
            UniformValue_ViewProjectionMatrix, Scale(Vec3(2, 2, 1)));
        PushUniformMat4(
            &drawHdrBuffer->shaderInput, UniformValue_ModelMatrix, Identity());
        PushUniformVec2(
            &drawHdrBuffer->shaderInput, UniformValue_TextureScale, Vec2(1, 1));
    }

    ProcessEntityDeletionQueue(world);
}

internal void BeginEntitySnapshotUpdate(EntityWorld *world)
{
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(world,
        NetworkReplicationComponentTypeId, entities, ArrayCount(entities));

    // Copy current state to previous state
    for (u32 entityIndex = 0; entityIndex < entityCount; ++entityIndex)
    {
        EntityId entity = entities[entityIndex];

        NetworkReplicationComponent network;
        ecs_GetComponent(world, NetworkReplicationComponent, &entity, 1, &network);

        network.snapshots[1] = network.snapshots[0];

        ecs_SetComponent(world, NetworkReplicationComponent, &entity, 1, &network);
    }
}

internal void LerpEntitySnapshots(
    EntityWorld *world, EntityId localPlayerEntity, u32 currentTick)
{
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(world,
        NetworkReplicationComponentTypeId, entities, ArrayCount(entities));

    for (u32 entityIndex = 0; entityIndex < entityCount; entityIndex++)
    {
        EntityId entity = entities[entityIndex];

        if (entity != localPlayerEntity)
        {
            NetworkReplicationComponent network;
            ecs_GetComponent(
                world, NetworkReplicationComponent, &entity, 1, &network);

            if (network.type == EntityType_Enemy || network.type == EntityType_Player)
            {
                LerpResult result =
                    GetLerpEntries(&network.lerpBuffer, currentTick);
                if (result.lower != NULL && result.upper != NULL)
                {
                    f32 t = MapToUnitRange((f32)currentTick,
                        (f32)result.lower->tick, (f32)result.upper->tick);
                    network.snapshots[0] = LerpSnapshot(
                        result.lower->snapshot, result.upper->snapshot, t);
                }
                else if (result.lower != NULL)
                {
                    network.snapshots[0] = result.lower->snapshot;
                }
                else
                {
                    LogMessage("Could not find suitable snapshot for entity %u "
                               "for tick %u",
                        entity, currentTick);
                    continue; // Don't update network component
                }

                ecs_SetComponent(
                    world, NetworkReplicationComponent, &entity, 1, &network);
            }
        }
    }
}

inline void PrintPlayerSnapshot(PlayerSnapshot snapshot)
{
    LogMessage("VEL: (%g, %g %g), POS: (%g, %g, %g), GRD: %u, JMP: %u",
            snapshot.velocity.x, snapshot.velocity.y, snapshot.velocity.z,
            snapshot.position.x, snapshot.position.y, snapshot.position.z,
            snapshot.isGrounded, snapshot.isJumping);
}

internal void PerformClientsidePrediction(EntityWorld *world,
    GameAssets *assets, EntityId localPlayerEntity, PlayerInput playerInput,
    f32 dt, PredictionBuffer *predictionBuffer)
{
    if (ecs_HasComponent(world, NetworkReplicationComponentTypeId, localPlayerEntity))
    {
        NetworkReplicationComponent network;
        ecs_GetComponent(world, NetworkReplicationComponent, &localPlayerEntity,
            1, &network);

        PlayerSnapshot newState =
            UpdatePlayer(playerInput, network.snapshots[0], dt, world, assets);
        network.snapshots[0] = newState;

        //PrintPlayerSnapshot(newState);

        AddPredictionToBuffer(predictionBuffer, playerInput, newState);

        ecs_SetComponent(world, NetworkReplicationComponent, &localPlayerEntity,
            1, &network);
    }
}

// TODO: Accelerate these
internal EntityId ToEntityId(EntityWorld *world, NetEntityId netEntityId)
{
    EntityId result = NULL_ENTITY_ID;

    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(world,
        NetworkReplicationComponentTypeId, entities, ArrayCount(entities));

    for (u32 entityIndex = 0; entityIndex < entityCount; ++entityIndex)
    {
        EntityId entityId = entities[entityIndex];

        NetworkReplicationComponent network;
        ecs_GetComponent(world, NetworkReplicationComponent, &entityId, 1, &network);

        if (network.netEntityId == netEntityId)
        {
            result = entityId;
            break;
        }
    }

    return result;
}

internal NetEntityId ToNetEntityId(EntityWorld *world, EntityId entityId)
{
    NetEntityId result = NULL_NET_ENTITY_ID;
    if (entityId != NULL_ENTITY_ID)
    {
        if (ecs_HasComponent(
                world, NetworkReplicationComponentTypeId, entityId))
        {
            NetworkReplicationComponent network;
            ecs_GetComponent(
                world, NetworkReplicationComponent, &entityId, 1, &network);
            result = network.netEntityId;
        }
    }
    return result;
}

internal NetInventoryCommand ToNetInventoryCommand(
    EntityWorld *world, InventoryCommand src)
{
    NetInventoryCommand result = {};
    result.action = src.action;
    result.fromSlot = src.fromSlot;
    result.toSlot = src.toSlot;

    result.fromNetEntity = ToNetEntityId(world, src.fromEntity);
    result.fromNetOwner = ToNetEntityId(world, src.fromOwner);

    if (src.toEntity != NULL_ENTITY_ID)
    {
        result.toNetEntity = ToNetEntityId(world, src.toEntity);
    }
    if (src.toOwner != NULL_ENTITY_ID)
    {
        result.toNetOwner = ToNetEntityId(world, src.toOwner);
    }

    return result;
}

internal InventoryCommand FromNetInventoryCommand(EntityWorld *world, NetInventoryCommand src)
{
    InventoryCommand result = {};

    result.action = src.action;
    result.fromSlot = src.fromSlot;
    result.toSlot = src.toSlot;

    result.fromEntity = ToEntityId(world, src.fromNetEntity);
    result.fromOwner = ToEntityId(world, src.fromNetOwner);

    if (src.toNetEntity != NULL_NET_ENTITY_ID)
    {
        result.toEntity = ToEntityId(world, src.toNetEntity);
    }
    if (src.toNetOwner != NULL_NET_ENTITY_ID)
    {
        result.toOwner = ToEntityId(world, src.toNetOwner);
    }

    return result;
}

// WARNING: Transient memory allocations made in fixed update will be
// overwritten by any allocations in GameClientUpdate!
GAME_CLIENT_FIXED_UPDATE(GameClientFixedUpdate)
{
    LogMessage = memory->logMessage;

    dt *= DT_SCALE;

    Assert(sizeof(GameState) < memory->persistentStorageSize);
    GameState *gameState = (GameState *)memory->persistentStorage;

    g_DebugDrawingSystem = &gameState->debugDrawingSystem;

    EntityWorld *world = &gameState->world;

    // TODO: Combine with LerpEntitySnapshots, although there is some temporal
    // coupling with VerifyPrediction to be aware of
    BeginEntitySnapshotUpdate(world);

    for (u32 i = 0; i < memory->incomingPacketCount; ++i)
    {
        Packet *incomingPacket = memory->incomingPackets + i;
        Assert(incomingPacket->length == sizeof(GameSnapshot));

        GameSnapshot *snapshot = (GameSnapshot *)incomingPacket->data;

        gameState->localPlayerEntityId = ToEntityId(world, snapshot->localPlayerNetEntityId);
        gameState->lastReceivedTick = snapshot->currentTick;

        // Process entity delete events
        for (u32 eventIndex = 0 ; eventIndex < snapshot->entityDeletionEventCount; ++eventIndex)
        {
            NetEntityDeletionEvent *event =
                snapshot->entityDeletionEvents + eventIndex;

            EntityId entityId = ToEntityId(world, event->netEntityId);
            if (entityId != NULL_ENTITY_ID)
            {
                RemoveEntity(world, entityId);
                LogMessage("Deleting entity with ID %u", entityId);
            }
            else
            {
                LogMessage("Tried to remove net entity %u on client but it does not "
                       "exist",
                    event->netEntityId);
            }
        }

        // Process entity creation events
        for (u32 eventIndex = 0; eventIndex < snapshot->entityCreationEventCount; ++eventIndex)
        {
            NetEntityCreationEvent *event = snapshot->entityCreationEvents + eventIndex;
            LogMessage("Creating net entity with id %u, type %u", event->netEntityId, event->type);
            switch (event->type)
            {
            case EntityType_Player:
            {
                EntityId playerEntity =
                    CreatePlayerEntity(world, event->position, event->netEntityId,
                        event->netEntityId == snapshot->localPlayerNetEntityId);

                // Always hide inventory when player spawns
                gameState->showInventory = false;
                break;
            }
            case EntityType_Enemy:
                CreateEnemyEntity(world, event->position, event->netEntityId);
                break;
            case EntityType_InventoryItem:
                CreateInventoryItemEntity(world, event->itemId,
                    event->slotIndex, ToEntityId(world, event->owner),
                    event->quantity, event->netEntityId);
                break;
            case EntityType_Container:
                CreateContainerEntity(world, event->position, event->netEntityId);
                break;
            case EntityType_Foundation:
                SpawnFoundationEntity(world, event->position, event->netEntityId);
                break;
            case EntityType_Pillar:
                SpawnPillarEntity(world, event->position, event->netEntityId);
                break;
            default:
                InvalidCodePath();
                break;
            }
        }

        // Process entity update events
        for (u32 eventIndex = 0; eventIndex < snapshot->entityUpdateEventCount; ++eventIndex)
        {
            NetEntityUpdateEvent *event = snapshot->entityUpdateEvents + eventIndex;

            EntityId entityId = ToEntityId(world, event->netEntityId);
            if (ecs_HasComponent(
                    world, NetworkReplicationComponentTypeId, entityId))
            {
                NetworkReplicationComponent network;
                ecs_GetComponent(
                    world, NetworkReplicationComponent, &entityId, 1, &network);

                switch (network.type)
                {
                case EntityType_Player:
                case EntityType_Enemy:
                {
                    if (entityId == gameState->localPlayerEntityId)
                    {
                        VerifyPrediction(&gameState->predictionBuffer,
                            snapshot->playerInputId, event->snapshot, world, dt,
                            &network, &gameState->assets);
                    }
                    else
                    {
                        AddToLerpBuffer(&network.lerpBuffer,
                            snapshot->currentTick, event->snapshot);
                    }

                    ecs_SetComponent(world, NetworkReplicationComponent,
                        &entityId, 1, &network);

                    // TODO: Should this be part of the player snapshot instead?
                    if (ecs_HasComponent(
                            world, InventoryOwnerComponentTypeId, entityId))
                    {
                        InventoryOwnerComponent inventoryOwner;
                        ecs_GetComponent(world, InventoryOwnerComponent,
                            &entityId, 1, &inventoryOwner);
                        inventoryOwner.openContainer = ToEntityId(world, event->openContainerNetEntityId);
                        inventoryOwner.activeEquipmentSlot = event->activeEquipmentSlot;
                        ecs_SetComponent(world, InventoryOwnerComponent,
                            &entityId, 1, &inventoryOwner);
                    }

                    if (entityId == gameState->localPlayerEntityId)
                    {
                        HealthComponent health;
                        ecs_GetComponent(world, HealthComponent, &entityId, 1, &health);
                        health.currentHealth = event->currentHealth;
                        ecs_SetComponent(world, HealthComponent, &entityId, 1, &health);
                    }
                    break;
                }
                case EntityType_InventoryItem:
                {
                    InventoryItemComponent item;
                    ecs_GetComponent(
                        world, InventoryItemComponent, &entityId, 1, &item);
                    item.owner = ToEntityId(world, event->ownerNetEntityId);
                    item.slotIndex = event->slotIndex;
                    item.quantity = event->quantity;
                    ecs_SetComponent(
                        world, InventoryItemComponent, &entityId, 1, &item);
                    break;
                }
                case EntityType_Container:
                    break; // Skip updates for containers
                case EntityType_Foundation:
                    break; // Skip updates for containers
                case EntityType_Pillar:
                    break; // Skip updates for containers
                default:
                    InvalidCodePath();
                    break;
                }
            }
        }

        // Process events
        for (u32 eventIndex = 0; eventIndex < snapshot->eventCount; ++eventIndex)
        {
            GameEvent *event = snapshot->events + eventIndex;
            switch (event->type)
            {
            case EventType_BulletImpact:
            {
                ParticleSystem *bulletImpact =
                    AllocateParticleSystem(gameState);
                if (bulletImpact != NULL)
                {
                    SpawnParticleSystem(bulletImpact, &gameState->rng,
                        event->bulletImpact.position,
                        event->bulletImpact.normal);
                }
                break;
            }
            case EventType_HitReaction:
            {
#if 0
                u32 remoteEntityId = event->hitReaction.entityId;
                for (u32 localEntityIndex = 0;
                     localEntityIndex < world->entityCount; ++localEntityIndex)
                {
                    Entity *localEntity = world->entities + localEntityIndex;
                    if (localEntity->id == remoteEntityId)
                    {
                        Assert(localEntity->type == EntityType_Enemy);
                        localEntity->wasHit = true;
                    }
                }
#endif
                break;
            }
            case EventType_OpenContainer:
            {
                EntityId entityId = ToEntityId(world, (NetEntityId)event->openContainer.entityId);
                if (entityId == gameState->localPlayerEntityId)
                {
                    gameState->showInventory = true;
                }
                break;
            }
            default:
                InvalidCodePath();
                break;
            }
        }
    }

    u32 interpDelayInTicks = (u32)Ceil(INTERPOLATION_DELAY / dt);
    if (gameState->lastReceivedTick > interpDelayInTicks)
    {
        u32 currentTick = gameState->lastReceivedTick - interpDelayInTicks;
        // Lerp entity snapshots
        LerpEntitySnapshots(world, gameState->localPlayerEntityId, currentTick);
    }

    f32 sensitivity = 0.005f;
    if (gameState->isAimed)
    {
        sensitivity *= 0.5f;
    }

    PlayerInput playerInput =
        GetPlayerInput(input, gameState->mouseRelX, gameState->mouseRelY,
            gameState->prevPlayerInput, gameState->showInventory, sensitivity);
    if (gameState->useFreeRoamCamera)
    {
        playerInput = {};
    }
    else
    {
        memory->showMouseCursor(gameState->showInventory);
    }
    playerInput.id = gameState->nextPlayerInputId++;
    playerInput.viewAngles += gameState->recoilOffset;

    gameState->recoilOffset = Vec3(0, 0, 0);

    gameState->mouseRelX = 0;
    gameState->mouseRelY = 0;

    if (IsControllingPlayer(gameState))
    {
        if ((playerInput.actions & PlayerAction_Shoot) &&
            !(gameState->prevPlayerInput.actions & PlayerAction_Shoot))
        {
            InventorySlot activeSlot =
                GetActiveEquipmentSlot(world, gameState->localPlayerEntityId);

            if (activeSlot.itemId == Item_Pistol ||
                activeSlot.itemId == Item_Shotgun)
            {
                // Apply clientside recoil
                gameState->recoilOffset.x =
                    0.05f + 0.03f * RandomUnilateral(&gameState->rng);
                gameState->recoilOffset.y =
                    0.02f * RandomBilateral(&gameState->rng);
            }
        }

        // Clientside prediction of Aim down sights
        f32 adsRate = 10.0f;
        if ((playerInput.actions & PlayerAction_Aim) &&
            !(gameState->prevPlayerInput.actions & PlayerAction_Aim))
        {
            if (gameState->isAimed)
            {
                gameState->adsRate = -adsRate;
            }
            else
            {
                gameState->adsRate = adsRate;
            }
        }

        UpdateAimDownSights(gameState, dt);

        // Client side prediction
        PerformClientsidePrediction(world, &gameState->assets,
            gameState->localPlayerEntityId, playerInput, dt,
            &gameState->predictionBuffer);
    }

    gameState->prevPlayerInput = playerInput;

    // Write out playerInput + inventoryCommands
    InventoryUIState *inventoryUIState = &gameState->inventoryUIState;
    PlayerInputPacket packet = {};
    packet.playerInput = playerInput;
    packet.inventoryCommandQueueLength = inventoryUIState->commandQueueLength;
    for (u32 i = 0; i < packet.inventoryCommandQueueLength; ++i)
    {
        packet.inventoryCommandQueue[i] =
            ToNetInventoryCommand(world, inventoryUIState->commandQueue[i]);
    }
    inventoryUIState->commandQueueLength = 0; 

    // Write outgoing packet
    memory->outgoingPackets[0].length = sizeof(packet);
    CopyMemory(memory->outgoingPackets[0].data, &packet, sizeof(packet));
    memory->outgoingPacketCount = 1;
}

internal void SyncEntityTransformWithSnapshot(EntityWorld *world)
{
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(world,
        NetworkReplicationComponentTypeId, entities, ArrayCount(entities));

    for (u32 entityIndex = 0; entityIndex < entityCount; ++entityIndex)
    {
        EntityId entity = entities[entityIndex];

        NetworkReplicationComponent network;
        ecs_GetComponent(world, NetworkReplicationComponent, &entity, 1, &network);

        if (ecs_HasComponent(world, TransformComponentTypeId, entity))
        {
            TransformComponent transform;
            ecs_GetComponent(world, TransformComponent, &entity, 1, &transform);

            transform.rotation = network.snapshots[0].rotation;
            transform.position = network.snapshots[0].position;

            ecs_SetComponent(world, TransformComponent, &entity, 1, &transform);
        }
    }
}

// NOTE: This function should only be called on the server
internal void RemoveDeadEntities(EntityWorld *world)
{
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(world,
        HealthComponentTypeId, entities, ArrayCount(entities));

    for (u32 entityIndex = 0; entityIndex < entityCount; ++entityIndex)
    {
        EntityId entity = entities[entityIndex];

        HealthComponent health;
        ecs_GetComponent(world, HealthComponent, &entity, 1, &health);

        if (health.currentHealth <= 0)
        {
            b32 isEnemy = ecs_HasComponent(world, EnemyComponentTypeId, entity);
            if (isEnemy)
            {
                TransformComponent transform;
                ecs_GetComponent(world, TransformComponent, &entity, 1, &transform);

                // TODO: Snap position to ground
                vec3 position = transform.position;

                // Spawn loot container
                EntityId container = CreateContainerEntity(world, position);
                AddItemToInventory(world, container, Item_PistolAmmo);
                AddItemToInventory(world, container, Item_Pistol);
            }

            RemoveEntity(world, entity);
        }
    }
}

// TODO: How do we ensure that an entities item owner field can only be set to
// entities which have an inventory owner component?
internal void RemoveItemsOwnedByDeletedEntities(EntityWorld *world)
{
    for (u32 entityIndex = 0; entityIndex < world->deletionQueueLength; ++entityIndex)
    {
        EntityId entityId = world->deletionQueue[entityIndex];

        if (ecs_HasComponent(world, InventoryOwnerComponentTypeId, entityId))
        {
            EntityId items[MAX_ENTITIES];
            u32 itemCount = GetItemEntitiesWithOwner(
                world, entityId, items, ArrayCount(items));

            for (u32 itemIndex = 0; itemIndex < itemCount; ++itemIndex)
            {
                RemoveEntity(world, items[itemIndex]);
            }
        }
    }
}

internal EntityId SpawnPlayer(EntityWorld *world, GameAssets *assets)
{
    // TODO: Pull out minY, maxY into constants
    vec3 start = Vec3(0, 4000, 3);
    vec3 end = Vec3(0, -4000, 3);
    RaycastWorldResult raycastResult =
        RaycastWorld(world, assets, start, end);
    Assert(raycastResult.closestHit.tmin > 0.0f);
    vec3 spawnPosition = Lerp(start, end, raycastResult.closestHit.tmin);
    spawnPosition += Vec3(0, PLAYER_HEIGHT, 0);

    EntityId entity = CreatePlayerEntity(world, spawnPosition);
    AddItemToInventory(world, entity, Item_PistolAmmo, 64);
    AddItemToInventory(world, entity, Item_Pistol);
    AddItemToInventory(world, entity, Item_BuildingPlan);

    return entity;
}

GAME_SERVER_UPDATE(GameServerUpdate)
{
    dt *= DT_SCALE;

    LogMessage = memory->logMessage;

    Assert(sizeof(ServerGameState) <= memory->persistentStorageSize);
    ServerGameState *gameState = (ServerGameState *)memory->persistentStorage;

    if (!gameState->isInitialized)
    {
        InitializeMemoryArena(&gameState->persitentArena,
            (u8 *)memory->persistentStorage + sizeof(ServerGameState),
            memory->persistentStorageSize - sizeof(ServerGameState));
        InitializeMemoryArena(&gameState->transientArena,
            memory->transientStorage, memory->transientStorageSize);

        gameState->collisionMeshArena =
            SubAllocateArena(&gameState->transientArena, Megabytes(2));
        gameState->terrainArena =
            SubAllocateArena(&gameState->transientArena, TERRAIN_ARENA_SIZE);

        gameState->rng.state = 0x234FAA8;

        InitializeEntityWorld(&gameState->world, &gameState->persitentArena);
        InitializeTerrain(&gameState->assets, memory, &gameState->terrainArena, false);
        LoadCollisionMeshes(&gameState->assets, memory, &gameState->collisionMeshArena);

        CreateLevel(&gameState->world, &gameState->assets, true);
        gameState->isInitialized = true;
    }

    EntityWorld *world = &gameState->world;

    gameState->eventQueue.length = 0;
    gameState->currentTick++;

    // Update entity position to match snapshot position
    SyncEntityTransformWithSnapshot(world);

    for (u32 eventIndex = 0; eventIndex < memory->networkEventCount; ++eventIndex)
    {
        NetworkEvent *event = memory->networkEvents + eventIndex;
        switch (event->type)
        {
            case NetworkEvent_ClientConnected:
            {
                LogMessage("Client %u connected to server!", event->clientId);

                // Allocate client
                Assert(gameState->clientCount < ArrayCount(gameState->clients));
                ClientState *client =
                    gameState->clients + gameState->clientCount++;
                client->clientId = event->clientId;

                ClearNetEntityIdBitset(&client->netEntityIdBitset);

                client->playerEntityId = SpawnPlayer(world, &gameState->assets);
                break;
            }
            case NetworkEvent_ClientDisconnected:
            {
                LogMessage(
                    "Client %u disconnected from server!", event->clientId);

                // Find client
                ClientState *client = NULL;
                for (u32 clientIndex = 0; clientIndex < gameState->clientCount; ++clientIndex)
                {
                    if (gameState->clients[clientIndex].clientId == event->clientId)
                    {
                        client = gameState->clients + clientIndex;
                        break;
                    }
                }
                Assert(client != NULL);

#if 0
                // Delete player owned by client
                for (u32 entityIndex = 0; entityIndex < world->entityCount;
                     ++entityIndex)
                {
                    Entity *entity = world->entities + entityIndex;
                    if (entity->id == client->playerEntityId)
                    {
                        RemoveEntity(world, entity);
                        break;
                    }
                }
#endif

                // Delete the client
                *client = gameState->clients[--gameState->clientCount];
                break;
            }
            default:
                break;
        }
    }

    for (u32 packetIndex = 0; packetIndex < memory->incomingPacketCount;
         ++packetIndex)
    {
        Assert(packetIndex < memory->maxOutgoingPackets);
        Packet *incomingPacket = memory->incomingPackets + packetIndex;

        PlayerInputPacket inputPacket;
        Assert(incomingPacket->length == sizeof(inputPacket));
        CopyMemory(&inputPacket, incomingPacket->data, sizeof(inputPacket));

        // Find client
        ClientState *client = NULL;
        for (u32 clientIndex = 0; clientIndex < gameState->clientCount;
             ++clientIndex)
        {
            if (gameState->clients[clientIndex].clientId ==
                incomingPacket->clientId)
            {
                client = gameState->clients + clientIndex;
                break;
            }
        }
        Assert(client != NULL);

        // TODO: Add player input commands to a queue to allow for packet loss

        PlayerInput playerInput = inputPacket.playerInput;

        b32 isPlayerAlive = ecs_HasComponent(
            world, NetworkReplicationComponentTypeId, client->playerEntityId);

        if (isPlayerAlive)
        {
            // Find player
            NetworkReplicationComponent network;
            ecs_GetComponent(world, NetworkReplicationComponent,
                &client->playerEntityId, 1, &network);

            // Handle player inventory
            for (u32 commandIndex = 0;
                 commandIndex < inputPacket.inventoryCommandQueueLength;
                 ++commandIndex)
            {
                InventoryCommand command = FromNetInventoryCommand(
                    world, inputPacket.inventoryCommandQueue[commandIndex]);
                ProcessInventoryCommand(world, client->playerEntityId, command);
            }

            // Set active equipment slot index
            InventoryOwnerComponent inventoryOwner;
            ecs_GetComponent(world, InventoryOwnerComponent,
                &client->playerEntityId, 1, &inventoryOwner);
            inventoryOwner.activeEquipmentSlot =
                playerInput.activeEquipmentSlot;
            ecs_SetComponent(world, InventoryOwnerComponent,
                &client->playerEntityId, 1, &inventoryOwner);

            // Handle player shooting
            if ((playerInput.actions & PlayerAction_Shoot) &&
                !(client->prevPlayerInput.actions & PlayerAction_Shoot))
            {
                InventorySlot activeSlot =
                    GetActiveEquipmentSlot(world, client->playerEntityId);

                if (activeSlot.itemId == Item_Pistol)
                {
                    if (ConsumeItem(
                            world, client->playerEntityId, Item_PistolAmmo))
                    {
                        ProcessPlayerShooting(playerInput, network.snapshots[0],
                            dt, world, &gameState->assets,
                            &gameState->eventQueue, client->playerEntityId);
                    }
                }
                else if (activeSlot.itemId == Item_Shotgun)
                {
                    // TODO: Shotgun ammo
                    if (ConsumeItem(
                            world, client->playerEntityId, Item_PistolAmmo))
                    {
                        ProcessPlayerShooting(playerInput, network.snapshots[0],
                            dt, world, &gameState->assets,
                            &gameState->eventQueue, client->playerEntityId, 8,
                            0.08f, &gameState->rng);
                    }
                }
                else if (activeSlot.itemId == Item_BuildingPlan)
                {
                    PlaceBuildingPart(world, &gameState->assets,
                        network.snapshots[0], client->playerEntityId,
                        playerInput, ACTIVE_BUILDING_PART);
                }
            }

            if ((playerInput.actions & PlayerAction_Interact) &&
                !(client->prevPlayerInput.actions & PlayerAction_Interact))
            {
                // TODO: Disable interaction if player is in a container
                ProcessPlayerInteraction(playerInput, network.snapshots[0], dt,
                    world, &gameState->assets, client->playerEntityId,
                    &gameState->eventQueue);
            }

#if 0
            if (playerInput.actions & PlayerAction_Interact)
            {
                HealthComponent health;
                ecs_GetComponent(world, HealthComponent,
                    &client->playerEntityId, 1, &health);
                health.currentHealth -= 10;
                ecs_SetComponent(world, HealthComponent,
                    &client->playerEntityId, 1, &health);
            }
#endif

            // Handle player movement
            PlayerSnapshot newState = UpdatePlayer(playerInput,
                network.snapshots[0], dt, world, &gameState->assets);
            network.snapshots[0] = newState;
            ecs_SetComponent(world, NetworkReplicationComponent,
                &client->playerEntityId, 1, &network);
        }
        else
        {
            if (playerInput.actions & PlayerAction_Jump)
            {
                // Respawn player
                client->playerEntityId = SpawnPlayer(world, &gameState->assets);
            }
        }

        client->prevPlayerInput = playerInput;
    }

    // Server tick processing
    MoveEnemies(&gameState->world, &gameState->assets, dt, &gameState->eventQueue);
    //DrawEntityCollisionVolumes(&gameState->world, &gameState->assets);

    RemoveDeadEntities(&gameState->world);

    RemoveItemsOwnedByDeletedEntities(&gameState->world);

    for (u32 clientIndex = 0; clientIndex < gameState->clientCount;
         ++clientIndex)
    {
        ClientState *client = gameState->clients + clientIndex;

        NetEntityId entitiesToCreate[64];
        NetEntityId entitiesToDelete[64];
        NetEntityId entitiesToUpdate[64];

        GameSnapshot snapshot = {};
        snapshot.localPlayerNetEntityId = ToNetEntityId(world, client->playerEntityId);
        snapshot.playerInputId = client->prevPlayerInput.id;
        snapshot.currentTick = gameState->currentTick;

        // Entity creation events
        NetEntityIdBitset creationBitset = {};
        creationBitset.data =
            world->netEntityIdBitset.data & ~client->netEntityIdBitset.data;
        u32 createCount = ConvertBitsetToNetEntityIds(&creationBitset, entitiesToCreate);
        createCount = MinU32(createCount, ArrayCount(snapshot.entityCreationEvents));
        for (u32 i = 0; i < createCount; ++i)
        {
            NetEntityId netEntityId = entitiesToCreate[i];

            EntityId entity = ToEntityId(world, netEntityId);
            NetworkReplicationComponent network;
            ecs_GetComponent(
                world, NetworkReplicationComponent, &entity, 1, &network);

            NetEntityCreationEvent *event = snapshot.entityCreationEvents + i;
            event->netEntityId = netEntityId;
            event->type = network.type;

            // TODO: System for mapping entity type to a replication policy? e.g. Create_PositionOnly, NoUpdate
            switch (network.type)
            {
                case EntityType_Player:
                case EntityType_Enemy:
                    // TODO: Position
                    break;
                case EntityType_InventoryItem:
                {
                    InventoryItemComponent item;
                    ecs_GetComponent(
                        world, InventoryItemComponent, &entity, 1, &item);
                    event->itemId = item.itemId;
                    event->slotIndex = item.slotIndex;
                    event->owner = ToNetEntityId(world, item.owner);
                    event->quantity = item.quantity;
                    break;
                }
                case EntityType_Container:
                {
                    TransformComponent transform;
                    ecs_GetComponent(world, TransformComponent, &entity, 1, &transform);
                    event->position = transform.position;
                    break;
                }
                case EntityType_Foundation:
                {
                    TransformComponent transform;
                    ecs_GetComponent(world, TransformComponent, &entity, 1, &transform);
                    event->position = transform.position;
                    break;
                }
                case EntityType_Pillar:
                {
                    TransformComponent transform;
                    ecs_GetComponent(world, TransformComponent, &entity, 1, &transform);
                    event->position = transform.position;
                    break;
                }
                default:
                    InvalidCodePath();
                    break;
            }
        }
        snapshot.entityCreationEventCount = createCount;

        // Entity deletion events
        NetEntityIdBitset deletionBitset = {};
        deletionBitset.data =
            client->netEntityIdBitset.data & ~world->netEntityIdBitset.data;
        u32 deleteCount = ConvertBitsetToNetEntityIds(&deletionBitset, entitiesToDelete);
        deleteCount = MinU32(deleteCount, ArrayCount(snapshot.entityDeletionEvents));
        for (u32 i = 0; i < deleteCount; ++i)
        {
            snapshot.entityDeletionEvents[i].netEntityId = entitiesToDelete[i];
        }
        snapshot.entityDeletionEventCount = deleteCount;

        // Entity update events
        NetEntityIdBitset updateBitset = {};
        updateBitset.data = world->netEntityIdBitset.data & client->netEntityIdBitset.data;
        u32 fullUpdateCount = ConvertBitsetToNetEntityIds(&updateBitset, entitiesToUpdate);

        // Sort based on priority
        for (u32 pass = 0; pass < fullUpdateCount; ++pass)
        {
            b32 sorted = true;
            for (u32 index = 0; index < fullUpdateCount - 1; ++index)
            {
                NetEntityId *netEntityId0 = entitiesToUpdate + index;
                NetEntityId *netEntityId1 = entitiesToUpdate + index + 1;
                u8 priority0 = client->entityPriorities[*netEntityId0];
                u8 priority1 = client->entityPriorities[*netEntityId1];

                if (priority0 < priority1)
                {
                    NetEntityId temp = *netEntityId1;
                    *netEntityId1 = *netEntityId0;
                    *netEntityId0 = temp;
                    sorted = false;
                }
            }

            if (sorted)
            {
                break;
            }
        }

        u32 updateCount = MinU32(fullUpdateCount, ArrayCount(snapshot.entityUpdateEvents));
        for (u32 i = 0; i < updateCount; ++i)
        {
            NetEntityUpdateEvent *event = snapshot.entityUpdateEvents + i;

            NetEntityId netEntityId = entitiesToUpdate[i];
            event->netEntityId = netEntityId;

            EntityId entity = ToEntityId(world, netEntityId);

            NetworkReplicationComponent network;
            ecs_GetComponent(
                world, NetworkReplicationComponent, &entity, 1, &network);

            switch (network.type)
            {
                case EntityType_Player:
                case EntityType_Enemy:
                    event->snapshot = network.snapshots[0];
                    if (ecs_HasComponent(world, InventoryOwnerComponentTypeId, entity))
                    {
                        InventoryOwnerComponent inventoryOwner;
                        ecs_GetComponent(world, InventoryOwnerComponent, &entity,
                            1, &inventoryOwner);
                        event->openContainerNetEntityId =
                            ToNetEntityId(world, inventoryOwner.openContainer);
                        event->activeEquipmentSlot = inventoryOwner.activeEquipmentSlot;
                    }
                    if (ecs_HasComponent(world, HealthComponentTypeId, entity))
                    {
                        HealthComponent health;
                        ecs_GetComponent(world, HealthComponent, &entity,
                            1, &health);
                        event->currentHealth = health.currentHealth;
                    }
                    break;
                case EntityType_InventoryItem:
                {
                    InventoryItemComponent item;
                    ecs_GetComponent(world, InventoryItemComponent, &entity, 1, &item);
                    event->ownerNetEntityId = ToNetEntityId(world, item.owner);
                    event->slotIndex = item.slotIndex;
                    event->quantity = item.quantity;
                    break;
                }
                case EntityType_Container: // FIXME: Don't consume update event for containers
                    break;
                case EntityType_Foundation: // FIXME: Don't consume update event for building parts
                    break;
                case EntityType_Pillar: // FIXME: Don't consume update event for building parts
                    break;
                default:
                    InvalidCodePath();
                    break;
            }

            client->entityPriorities[netEntityId] = network.priority;
        }
        snapshot.entityUpdateEventCount = updateCount;

        for (u32 i = updateCount; i < fullUpdateCount; ++i)
        {
            NetEntityId netEntityId = entitiesToUpdate[i];
            Assert(netEntityId < ArrayCount(client->entityPriorities));
            u8 priority = client->entityPriorities[netEntityId];
            if (priority < 0xFF)
            {
                client->entityPriorities[netEntityId]++;
            }

        }

#ifdef DEBUG_ENTITY_PRIORITIES
        LogMessage("Update priorities");
        for (u32 i = 0; i < fullUpdateCount; ++i)
        {
            NetEntityId netEntityId = entitiesToUpdate[i];
            Assert(netEntityId < ArrayCount(client->entityPriorities));
            LogMessage("%u: %hhu", netEntityId, client->entityPriorities[netEntityId]);
        }
#endif

        for (u32 i = 0; i < createCount; ++i)
        {
            NetEntityId netEntityId = entitiesToCreate[i];

            EntityId entity = ToEntityId(world, netEntityId);

            // TODO: Lookup NetworkReplicationComponent based on netEntityId
            NetworkReplicationComponent network;
            ecs_GetComponent(
                world, NetworkReplicationComponent, &entity, 1, &network);

            SetNetEntityIdPresent(&client->netEntityIdBitset, netEntityId, true);

            Assert(netEntityId < ArrayCount(client->entityPriorities));
            client->entityPriorities[netEntityId] = network.priority;
        }
        for (u32 i = 0; i < deleteCount; ++i)
        {
            NetEntityId netEntityId = entitiesToDelete[i];
            SetNetEntityIdPresent(&client->netEntityIdBitset, netEntityId, false);
        }

        // Game events
        // Add events to client event queue
        if ((client->eventQueue.length + gameState->eventQueue.length <
                ArrayCount(client->eventQueue.events)))
        {
            CopyMemory(client->eventQueue.events + client->eventQueue.length,
                gameState->eventQueue.events,
                sizeof(GameEvent) * gameState->eventQueue.length);
            client->eventQueue.length += gameState->eventQueue.length;
        }
        else
        {
            LogMessage("Failed to add %u events to client %u event queue",
                gameState->eventQueue.length, client->clientId);
        }

        // Copy events out of the client event queue
        snapshot.eventCount =
            MinU32(client->eventQueue.length, ArrayCount(snapshot.events));
        CopyMemory(snapshot.events, client->eventQueue.events,
            sizeof(GameEvent) * snapshot.eventCount);

        // Shift remaining events down
        u32 shiftCount = client->eventQueue.length - snapshot.eventCount;
        if (shiftCount > 0)
        {
            CopyMemory(client->eventQueue.events,
                client->eventQueue.events + snapshot.eventCount,
                shiftCount * sizeof(GameEvent));
        }
        client->eventQueue.length -= snapshot.eventCount;

        // TODO: Better way of translating events
        for (u32 eventIndex = 0; eventIndex < snapshot.eventCount; ++eventIndex)
        {
            GameEvent *event = snapshot.events + eventIndex;
            switch(event->type)
            {
                case EventType_BulletImpact:
                    break;
                case EventType_HitReaction:
                    event->hitReaction.entityId = ToNetEntityId(world, event->hitReaction.entityId);
                    break;
                case EventType_OpenContainer:
                    event->openContainer.entityId = ToNetEntityId(world, event->openContainer.entityId);
                    break;
                default:
                    InvalidCodePath();
                    break;
            }
        }

        Packet *outgoingPacket =
            memory->outgoingPackets + memory->outgoingPacketCount++;
        outgoingPacket->clientId = client->clientId;
        outgoingPacket->length = sizeof(snapshot);
        Assert(sizeof(snapshot) < sizeof(outgoingPacket->data));
        CopyMemory(outgoingPacket->data, &snapshot, outgoingPacket->length);
    }

    ProcessEntityDeletionQueue(world);
}
