#define GAME_LIB "game.dll"
#define GAME_LIB_TEMP "game_temp.dll"
#define GAME_LIB_LOCK "lock.tmp"

internal void *AllocateMemory(u64 size, u64 baseAddress)
{
    void *result =
        VirtualAlloc((void*)baseAddress, size, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
    return result;
}

internal void FreeMemory(void *p)
{
    VirtualFree(p, 0, MEM_RELEASE);
}

inline FILETIME GetLastWriteTime(const char *filename)
{
    FILETIME lastWriteTime = {};

    WIN32_FILE_ATTRIBUTE_DATA data;
    if (GetFileAttributesEx(filename, GetFileExInfoStandard, &data))
    {
        lastWriteTime = data.ftLastWriteTime;
    }
    return lastWriteTime;
}

internal b32 IsLockFileActive(const char *lockFileName)
{
    WIN32_FILE_ATTRIBUTE_DATA ignored;
    if (!GetFileAttributesEx(lockFileName, GetFileExInfoStandard, &ignored))
        return false;

    return true;
}


internal b32 WasGameCodeModified(GameLibrary *library)
{
    b32 result = false;
    FILETIME newDllWriteTime = GetLastWriteTime(GAME_LIB);
    if (CompareFileTime(&newDllWriteTime, &library->lastWriteTime) != 0)
    {
        if (!IsLockFileActive(GAME_LIB_LOCK))
        {
            result = true;
        }
    }

    return result;
}

internal void UnloadGameLibrary(GameLibrary *library)
{
    if (library->handle != NULL)
    {
        FreeLibrary(library->handle);
    }
}

DebugReadEntireFile(ReadEntireFile)
{
    DebugReadFileResult result = {};
    HANDLE file = CreateFileA(path, GENERIC_READ, FILE_SHARE_READ, 0,
                              OPEN_EXISTING, 0, 0);
    if (file != INVALID_HANDLE_VALUE)
    {
        LARGE_INTEGER tempSize;
        if (GetFileSizeEx(file, &tempSize))
        {
            result.length = SafeTruncateU64ToU32(tempSize.QuadPart);
            result.contents = AllocateMemory(result.length);

            if (result.contents)
            {
                DWORD bytesRead;
                if (!ReadFile(file, result.contents, result.length, &bytesRead,
                              0) ||
                    (result.length != bytesRead))
                {
                    LogMessage("Failed to read file %s", path);
                    FreeMemory(result.contents);
                    result.contents = NULL;
                    result.length = 0;
                }
            }
            else
            {
                LogMessage("Failed to allocate %d bytes for file %s",
                          result.length, path);
            }
        }
        else
        {
            LogMessage("Failed to read file length for file %s", path);
        }
        CloseHandle(file);
    }
    else
    {
        LogMessage("Failed to open file %s", path);
    }
    return result;
}

internal void* GameLibGetProcAddress(GameLibrary *library, const char *name)
{
    void *result = GetProcAddress(library->handle, name);
    if (result == NULL)
    {
        LogMessage("Error: GetProcAddress failed for %s", name);
    }

    return result;
}

internal b32 LoadGameLibrary(GameLibrary *library)
{
    b32 result = false;
    library->lastWriteTime = GetLastWriteTime(GAME_LIB);
    CopyFile(GAME_LIB, GAME_LIB_TEMP, FALSE);

    library->handle = LoadLibraryA(GAME_LIB_TEMP);
    if (library->handle != NULL)
    {
        library->clientUpdate =
            (GameClientUpdateFunction *)GameLibGetProcAddress(
                library, "GameClientUpdate");

        library->clientFixedUpdate =
            (GameClientFixedUpdateFunction *)GameLibGetProcAddress(
                library, "GameClientFixedUpdate");

        library->serverUpdate =
            (GameServerUpdateFunction *)GameLibGetProcAddress(
                library, "GameServerUpdate");

        if (library->clientUpdate != NULL &&
            library->clientFixedUpdate != NULL && library->serverUpdate != NULL)
        {
            // Successfully loaded game library
            LogMessage("Game library \"%s\" loaded", GAME_LIB);
            result = true;
        }
        else
        {
            LogMessage("Failed to load game library from \"%s\".", GAME_LIB);
        }
    }
    else
    {
        LogMessage("Failed to open game library \"%s\".", GAME_LIB);
    }

    return result;
}
