#define PLAYER_HEIGHT 2.0f

struct RaycastWorldResult
{
    RaycastResult closestHit;
    EntityId closestHitEntity;
};

inline b32 ContainsEntityId(EntityId entityId, EntityId *buffer, u32 count)
{
    b32 result = false;
    for (u32 entityIndex = 0; entityIndex < count; ++entityIndex)
    {
        if (entityId == buffer[entityIndex])
        {
            result = true;
            break;
        }
    }

    return result;
}

internal RaycastWorldResult RaycastWorld(EntityWorld *world, GameAssets *assets,
    vec3 start, vec3 end, EntityId *ignoredEntities = NULL,
    u32 ignoredEntityCount = 0)
{
    RaycastResult closestHit = {};
    closestHit.tmin = 9999.0f;
    u32 closestHitEntity = NULL_ENTITY_ID;

    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(
        world, BoxCollisionComponentTypeId, entities, ArrayCount(entities));

    BoxCollisionComponent boxes[MAX_ENTITIES];
    ecs_GetComponent(
        world, BoxCollisionComponent, entities, entityCount, boxes);

    TransformComponent transforms[MAX_ENTITIES];
    ecs_GetComponent(
        world, TransformComponent, entities, entityCount, transforms);

    for (u32 entityIndex = 0; entityIndex < entityCount; ++entityIndex)
    {
        EntityId entityId = entities[entityIndex];
        if (!ContainsEntityId(entityId, ignoredEntities, ignoredEntityCount))
        {
            TransformComponent *transform = transforms + entityIndex;
            BoxCollisionComponent *box = boxes + entityIndex;

            vec3 boxMin = transform->position - box->halfDims;
            vec3 boxMax = transform->position + box->halfDims;

            RaycastResult result = RayIntersectAabb(boxMin, boxMax, start, end);

            if (result.tmin > 0.0f)
            {
                if (result.tmin < closestHit.tmin)
                {
                    closestHit = result;
                    closestHitEntity = entities[entityIndex];
                }
            }
        }
    }

    entityCount = ecs_GetEntitiesWithComponent(
        world, TerrainComponentTypeId, entities, ArrayCount(entities));

    TerrainComponent terrains[MAX_ENTITIES];
    ecs_GetComponent(world, TerrainComponent, entities, entityCount, terrains);
    ecs_GetComponent(
        world, TransformComponent, entities, entityCount, transforms);
    for (u32 entityIndex = 0; entityIndex < entityCount; ++entityIndex)
    {
        EntityId entityId = entities[entityIndex];
        if (!ContainsEntityId(entityId, ignoredEntities, ignoredEntityCount))
        {
            TerrainComponent *terrain = terrains + entityIndex;
            TransformComponent *transform = transforms + entityIndex;

            HeightMap heightMap = GetHeightMap(assets, terrain->heightMap);
            RaycastResult result = RayIntersectHeightMap(
                heightMap, transform->position, transform->scale, start, end);

            if (result.tmin > 0.0f)
            {
                if (result.tmin < closestHit.tmin)
                {
                    closestHit = result;
                    closestHitEntity = entities[entityIndex];
                }
            }
        }
    }

    entityCount = ecs_GetEntitiesWithComponent(
        world, TriangleCollisionMeshComponentTypeId, entities, ArrayCount(entities));

    TriangleCollisionMeshComponent triangleMeshes[MAX_ENTITIES];
    ecs_GetComponent(world, TriangleCollisionMeshComponent, entities, entityCount, triangleMeshes);
    ecs_GetComponent(
        world, TransformComponent, entities, entityCount, transforms);
    for (u32 entityIndex = 0; entityIndex < entityCount; ++entityIndex)
    {
        EntityId entityId = entities[entityIndex];
        if (!ContainsEntityId(entityId, ignoredEntities, ignoredEntityCount))
        {
            TriangleCollisionMeshComponent *collisionMesh =
                triangleMeshes + entityIndex;
            TransformComponent *transform = transforms + entityIndex;

            TriangleMesh triangleMesh =
                GetTriangleMesh(assets, collisionMesh->triangleMeshId);

            // Terrible broad phase test to avoid transforming all triangles
            b32 containsStart =
                Length(start - transform->position) < triangleMesh.radius;
            b32 broadPhasePassed = false;

            if (!containsStart)
            {
                // TODO: Figure out a better way of reducing our 3D scale to a
                // scalar value
                RaycastResult broadPhase =
                    RayIntersectSphere(transform->position,
                        triangleMesh.radius * transform->scale.x, start, end);
                broadPhasePassed = (broadPhase.tmin > 0.0f &&
                                    broadPhase.tmin < closestHit.tmin);
            }

            if (containsStart || broadPhasePassed)
            {
                RaycastResult result =
                    RayIntersectTriangleMesh(triangleMesh, transform->position,
                        transform->rotation, transform->scale, start, end);

                if (result.tmin > 0.0f)
                {
                    if (result.tmin < closestHit.tmin)
                    {
                        closestHit = result;
                        closestHitEntity = entities[entityIndex];
                    }
                }
            }
        }
    }

    RaycastWorldResult result;
    result.closestHit = closestHit;
    result.closestHitEntity = closestHitEntity;

    return result;
}

inline vec3 GetPlayerCameraPosition(vec3 basePosition)
{
    vec3 result = basePosition + Vec3(0, PLAYER_HEIGHT * 0.5f, 0);
    return result;
}

internal PlayerInput GetPlayerInput(GameInput *input, i32 mouseRelX,
    i32 mouseRelY, PlayerInput prevInput, b32 showInventory, f32 sensitivity)
{
    // TODO: May need to divide mouseRelPosX/Y by window dimensions as the
    // values we are getting back from GLFW seem quite high even for raw mouse
    // input.
    vec3 viewAngles = prevInput.viewAngles;
    viewAngles.x += -mouseRelY * sensitivity;
    viewAngles.y += -mouseRelX * sensitivity;

    viewAngles.x = Clamp(viewAngles.x, -0.5f * PI, 0.5f * PI);

    if (viewAngles.y > 2.0f * PI)
    {
        viewAngles.y -= 2.0f * PI;
    }
    if (viewAngles.y < -2.0f * PI)
    {
        viewAngles.y += 2.0f * PI;
    }

    f32 forward = 0.0f;
    f32 right = 0.0f;
    if (input->buttonStates[KEY_W].isDown)
    {
        forward = 1.0f;
    }
    if (input->buttonStates[KEY_S].isDown)
    {
        forward = -1.0f;
    }
    if (input->buttonStates[KEY_A].isDown)
    {
        right = -1.0f;
    }
    if (input->buttonStates[KEY_D].isDown)
    {
        right = 1.0f;
    }

    PlayerInput result = {};
    result.viewAngles = showInventory ? prevInput.viewAngles : viewAngles;
    result.forward = forward;
    result.right = right;

    if (input->buttonStates[KEY_SPACE].isDown)
    {
        result.actions |= PlayerAction_Jump;
    }

    if (input->buttonStates[KEY_LEFT_SHIFT].isDown)
    {
        result.actions |= PlayerAction_Sprint;
    }

    result.activeEquipmentSlot = prevInput.activeEquipmentSlot;
    for (u32 slotIndex = 0; slotIndex < PLAYER_EQUIPMENT_BAR_SLOT_COUNT; ++slotIndex)
    {
        Assert(KEY_1 + slotIndex < KEY_9);
        if (input->buttonStates[KEY_1 + slotIndex].isDown)
        {
            result.activeEquipmentSlot = slotIndex;
        }
    }

    if (!showInventory)
    {
        if (input->buttonStates[KEY_MOUSE_BUTTON_LEFT].isDown)
        {
            result.actions |= PlayerAction_Shoot;
        }

        if (input->buttonStates[KEY_MOUSE_BUTTON_RIGHT].isDown)
        {
            result.actions |= PlayerAction_Aim;
        }

        if (input->buttonStates[KEY_F].isDown)
        {
            result.actions |= PlayerAction_Interact;
        }
    }

    return result;
}

// TODO: Do we need to pass in the player entity here to make sure we don't collide with ourselves?
internal PlayerSnapshot UpdatePlayer(PlayerInput playerInput,
    PlayerSnapshot currentState, f32 dt, EntityWorld *world, GameAssets *assets)
{
    PlayerSnapshot newState = currentState;

    quat rotation = Quat(Vec3(0, 1, 0), playerInput.viewAngles.y);
    vec3 forwardVector = RotateVector(Vec3(0, 0, -1), rotation);
    vec3 rightVector = RotateVector(Vec3(1, 0, 0), rotation);
    vec3 direction =
        forwardVector * playerInput.forward + rightVector * playerInput.right;
    direction = Normalize(direction);

    f32 speed = (playerInput.actions & PlayerAction_Sprint) ? 75.0f : 30.0f;

    // Round player velocity to 0
    if (LengthSq(newState.velocity) < 0.0001f)
    {
        newState.velocity = Vec3(0);
    }

    // Apply friction
    f32 friction = 9.0f;
    newState.velocity +=
        Vec3(-currentState.velocity.x, 0.0f, -currentState.velocity.z) *
        friction * dt;

    // Apply acceleration
    newState.velocity += direction * speed * dt;

    // Ground check
    f32 halfPlayerHeight = PLAYER_HEIGHT * 0.5f;
    f32 epsilon = 0.25f; // Extra ray distance
    vec3 groundTraceStart = currentState.position;
    vec3 groundTraceEnd = currentState.position +
                          Vec3(0, currentState.velocity.y * dt, 0) +
                          Vec3(0, -halfPlayerHeight - epsilon, 0);

    RaycastWorldResult groundTrace =
        RaycastWorld(world, assets, groundTraceStart, groundTraceEnd);
    newState.isGrounded = (groundTrace.closestHitEntity != NULL_ENTITY_ID);

    if (newState.isJumping)
    {
        newState.isGrounded = false;

        if (currentState.velocity.y < 0.0f)
        {
            newState.isJumping = false;
        }
    }

    if (newState.isGrounded)
    {
        // Snap player to ground and remove vertical velocity
        currentState.position =
            Lerp(groundTraceStart, groundTraceEnd, groundTrace.closestHit.tmin);
        currentState.position.y += halfPlayerHeight;
        newState.velocity.y = 0.0f;

        // Handle jump
        if (playerInput.actions & PlayerAction_Jump)
        {
            newState.velocity += Vec3(0, 600, 0) * dt;
            newState.isGrounded = false;
            newState.isJumping = true;
        }

    }
    else
    {
        // Apply gravity
        f32 gravity = -30.0f;
        newState.velocity.y += gravity * dt;
    }

    // Collision detection
    vec3 start = currentState.position;
    vec3 end = currentState.position + newState.velocity * dt;
    vec3 targetVelocity = newState.velocity;

    if (LengthSq(end - start) > EPSILON)
    {
        for (u32 iteration = 0; iteration < 4; ++iteration)
        {
            RaycastWorldResult collision = RaycastWorld(world, assets, start, end);

            if (collision.closestHit.tmin <= 1.0f && collision.closestHit.tmin > 0.0f)
            {
                targetVelocity =
                    targetVelocity - Dot(targetVelocity, collision.closestHit.normal) *
                    collision.closestHit.normal;

                // TODO: Shouldn't we try move the player up to the hit point?

                end = start + targetVelocity * dt;
            }
            else
            {
                break;
            }
        }
    }

    newState.position = end;
    newState.velocity = targetVelocity;

    newState.rotation = FromEulerAngles(playerInput.viewAngles);

    return newState;
}

internal void ProcessPlayerShooting(PlayerInput playerInput,
    PlayerSnapshot currentState, f32 dt, EntityWorld *world, GameAssets *assets,
    GameEventQueue *eventQueue, EntityId playerEntity, u32 bulletCount = 1,
    f32 bulletSpread = 0.0f, RandomNumberGenerator *rng = NULL)
{
    vec3 start = GetPlayerCameraPosition(currentState.position);

    for (u32 bulletIndex = 0; bulletIndex < bulletCount; ++bulletIndex)
    {
        quat rotation = FromEulerAngles(playerInput.viewAngles);
        quat spread = Quat();
        if (bulletSpread > 0.0f)
        {
            Assert(rng);
            f32 spreadX = bulletSpread * RandomBilateral(rng);
            f32 spreadY = bulletSpread * RandomBilateral(rng);
            spread = Quat(Vec3(1, 0, 0), spreadX) * Quat(Vec3(0, 1, 0), spreadY);
        }
        vec3 forward = RotateVector(Vec3(0, 0, -1), rotation * spread);
        vec3 end = start + forward * 100.0f;

        // DrawLine(start, end, Vec4(1, 0, 0, 1), 30.0f);

        // Collision detection
        RaycastWorldResult raycastResult =
            RaycastWorld(world, assets, start, end, &playerEntity, 1);

        if (raycastResult.closestHit.tmin <= 1.0f &&
            raycastResult.closestHit.tmin > 0.0f)
        {
            Assert(eventQueue->length < ArrayCount(eventQueue->events));
            GameEvent *event = eventQueue->events + eventQueue->length++;
            event->type = EventType_BulletImpact;
            event->bulletImpact.position =
                start + (end - start) * raycastResult.closestHit.tmin;
            event->bulletImpact.normal = raycastResult.closestHit.normal;

            EntityId hitEntity = raycastResult.closestHitEntity;
            if (ecs_HasComponent(world, HealthComponentTypeId, hitEntity))
            {
                HealthComponent health;
                ecs_GetComponent(
                    world, HealthComponent, &hitEntity, 1, &health);
                health.currentHealth -= 30;
                ecs_SetComponent(
                    world, HealthComponent, &hitEntity, 1, &health);

                Assert(eventQueue->length < ArrayCount(eventQueue->events));
                GameEvent *hitEvent = eventQueue->events + eventQueue->length++;
                hitEvent->type = EventType_HitReaction;
                hitEvent->hitReaction.entityId = hitEntity;
            }
        }
    }
}

internal void ProcessPlayerInteraction(PlayerInput playerInput,
    PlayerSnapshot currentState, f32 dt, EntityWorld *world, GameAssets *assets,
    EntityId playerEntity, GameEventQueue *eventQueue)
{
    vec3 start = GetPlayerCameraPosition(currentState.position);

    f32 interactionRange = 2.5f;
    quat rotation = FromEulerAngles(playerInput.viewAngles);
    vec3 forward = RotateVector(Vec3(0, 0, -1), rotation);
    vec3 end = start + forward * interactionRange;

    //DrawLine(start, end, Vec4(0, 1, 1, 1), 30.0f);

    // Collision detection
    RaycastWorldResult raycastResult = RaycastWorld(world, assets, start, end, &playerEntity, 1);

    if (raycastResult.closestHit.tmin <= 1.0f &&
        raycastResult.closestHit.tmin > 0.0f)
    {
        EntityId hitEntity = raycastResult.closestHitEntity;
        if (ecs_HasComponent(world, ContainerComponentTypeId, hitEntity))
        {
            InventoryOwnerComponent inventoryOwner;
            ecs_GetComponent(world, InventoryOwnerComponent,
                &playerEntity, 1, &inventoryOwner);

            inventoryOwner.openContainer = hitEntity;

            ecs_SetComponent(world, InventoryOwnerComponent,
                &playerEntity, 1, &inventoryOwner);

            Assert(eventQueue->length < ArrayCount(eventQueue->events));
            GameEvent *event = eventQueue->events + eventQueue->length++;
            event->type = EventType_OpenContainer;
            event->openContainer.entityId = playerEntity;
        }
    }
}

struct EntityPlacementResult
{
    vec3 position;
    b32 isValid;
};

#define FOUNDATION_WIDTH 3.0f
#define FOUNDATION_HEIGHT 1.0f
#define PILLAR_HEIGHT 3.5f
internal EntityPlacementResult CalculatePlacement(EntityWorld *world,
    GameAssets *assets, EntityId playerEntity, vec3 start, vec3 end,
    u32 buildingPartIndex)
{
    EntityPlacementResult result = {};
    RaycastWorldResult raycastResult =
        RaycastWorld(world, assets, start, end, &playerEntity, 1);

    if (raycastResult.closestHitEntity != NULL_ENTITY_ID)
    {
        vec3 hitPoint = start + (end - start) * raycastResult.closestHit.tmin;
        result.position = hitPoint;
        result.isValid = true;

        EntityId hitEntity = raycastResult.closestHitEntity;
        TransformComponent hitEntityTransform;
        ecs_GetComponent(
            world, TransformComponent, &hitEntity, 1, &hitEntityTransform);

        vec3 hitEntityPosition = hitEntityTransform.position;

        b32 isPlacingOnDeployable =
            ecs_HasComponent(world, DeployableComponentTypeId, hitEntity);

        u32 deployableType = Deployable_None;
        if (isPlacingOnDeployable)
        {
            DeployableComponent deployable = {};
            ecs_GetComponent(world, DeployableComponent, &hitEntity, 1, &deployable);
            deployableType = deployable.type;
        }

        switch (buildingPartIndex)
        {
            case BuildingPart_Foundation:
                if (isPlacingOnDeployable && deployableType == Deployable_Foundation)
                {
                    vec3 d = hitPoint - hitEntityTransform.position;
                    d.y = 0.0f;
                    d = Normalize(d);

                    // Assuming that foundations have no rotation
                    u32 largestAxis = CalculateLargestAxis(d);
                    vec3 offset = {};
                    offset.data[largestAxis] = SignBit(d.data[largestAxis]) ? -1.0f : 1.0f;

                    result.position = hitEntityTransform.position + offset * FOUNDATION_WIDTH;
                }
                break;
            case BuildingPart_Pillar:
                if (isPlacingOnDeployable && deployableType == Deployable_Foundation)
                {
                    vec3 corners[4];
                    corners[0] = hitEntityPosition + Vec3(-1, 0, -1) * FOUNDATION_WIDTH * 0.5f;
                    corners[1] = hitEntityPosition + Vec3(1, 0, -1) * FOUNDATION_WIDTH * 0.5f;
                    corners[2] = hitEntityPosition + Vec3(1, 0, 1) * FOUNDATION_WIDTH * 0.5f;
                    corners[3] = hitEntityPosition + Vec3(-1, 0, 1) * FOUNDATION_WIDTH * 0.5f;

                    u32 closestCorner = 0;
                    f32 minDistSq = F32_MAX;
                    for (u32 i = 0; i < 4; ++i)
                    {
                        f32 distSq = LengthSq(hitPoint - corners[i]);
                        if (distSq < minDistSq)
                        {
                            closestCorner = i;
                            minDistSq = distSq;
                        }
                    }

                    // FIXME: Remove 0.5f multipler
                    f32 yOffset = FOUNDATION_HEIGHT * 0.5f;
                    result.position =
                        corners[closestCorner] +
                        Vec3(0, yOffset + PILLAR_HEIGHT * 0.5f, 0);
                }
                else
                {
                    result.isValid = false;
                }
                break;
            default:
                InvalidCodePath();
                break;
        }
    }

    return result;
}

#define BUILDING_RANGE 10.0f
// FIXME: Don't pass in preview entities array?
internal void PreviewBuilding(EntityWorld *world, GameAssets *assets,
    EntityId playerEntity, u32 buildingPartIndex, EntityId *previewEntities,
    u32 previewCount)
{
    TransformComponent transform;
    ecs_GetComponent(world, TransformComponent, &playerEntity, 1, &transform);

    vec3 start = GetPlayerCameraPosition(transform.position);

    vec3 forward = RotateVector(Vec3(0, 0, -1), transform.rotation);
    vec3 end = start + forward * BUILDING_RANGE;

    EntityPlacementResult placementResult = CalculatePlacement(
        world, assets, playerEntity, start, end, buildingPartIndex);

    if (placementResult.isValid)
    {
        Assert(previewCount == MAX_BUILDING_PARTS);
        Assert(buildingPartIndex < previewCount);
        EntityId entity = previewEntities[buildingPartIndex];

        TransformComponent previewTransform;
        ecs_GetComponent(world, TransformComponent, &entity, 1, &previewTransform);
        previewTransform.position = placementResult.position;
        ecs_SetComponent(world, TransformComponent, &entity, 1, &previewTransform);

        StaticMeshComponent previewStaticMesh;
        ecs_GetComponent(world, StaticMeshComponent, &entity, 1, &previewStaticMesh);
        previewStaticMesh.isVisible = true;
        ecs_SetComponent(world, StaticMeshComponent, &entity, 1, &previewStaticMesh);
    }
}

// TODO: Need entity prefabs!
// WARNING: Data is duplicated with CreateBuildingPartPreviewEntities()
internal EntityId SpawnFoundationEntity(EntityWorld *world, vec3 position,
    NetEntityId netEntityId = NULL_NET_ENTITY_ID)
{
    EntityId foundation = AllocateEntityId(world);
    ecs_AddComponent(world, TransformComponentTypeId, foundation);
    ecs_AddComponent(world, StaticMeshComponentTypeId, foundation);
    ecs_AddComponent(world, BoxCollisionComponentTypeId, foundation);
    ecs_AddComponent(world, NetworkReplicationComponentTypeId, foundation);
    ecs_AddComponent(world, DeployableComponentTypeId, foundation);

    if (netEntityId == NULL_NET_ENTITY_ID)
    {
        netEntityId = AllocateNetEntityId(world);
    }

    // FIXME: Why does a building part need entity snapshots!
    // We have another bug because of this, even though the entity is created
    // on the client at the correct position it is still being lerped from
    // 0,0,0 which is visible to the player
    NetworkReplicationComponent network = {};
    network.netEntityId = netEntityId;
    network.snapshots[0].position = position;
    network.snapshots[0].isGrounded = true;
    network.priority = DEFAULT_ENTITY_NET_PRIORITY;
    network.type = EntityType_Foundation;
    ecs_SetComponent(
        world, NetworkReplicationComponent, &foundation, 1, &network);

    TransformComponent transform = {};
    transform.position = position;
    transform.rotation = Quat();
    transform.scale = Vec3(3, 1, 3);
    ecs_SetComponent(
            world, TransformComponent, &foundation, 1, &transform);

    BoxCollisionComponent box = {};
    box.halfDims = transform.scale * 0.5f;
    ecs_SetComponent(
            world, BoxCollisionComponent, &foundation, 1, &box);

    StaticMeshComponent staticMesh = {};
    staticMesh.meshes[0] = Mesh_Cube;
    staticMesh.materials[0] = Material_DevGrid512;
    staticMesh.count = 1;
    staticMesh.isVisible = true;
    staticMesh.useViewModelProjectionMatrix = false;
    ecs_SetComponent(
            world, StaticMeshComponent, &foundation, 1, &staticMesh);

    DeployableComponent deployable = {};
    deployable.type = Deployable_Foundation;
    ecs_SetComponent(
            world, DeployableComponent, &foundation, 1, &deployable);

    return foundation;
}

internal EntityId SpawnPillarEntity(EntityWorld *world, vec3 position,
    NetEntityId netEntityId = NULL_NET_ENTITY_ID)
{
    EntityId pillar = AllocateEntityId(world);
    ecs_AddComponent(world, TransformComponentTypeId, pillar);
    ecs_AddComponent(world, StaticMeshComponentTypeId, pillar);
    ecs_AddComponent(world, BoxCollisionComponentTypeId, pillar);
    ecs_AddComponent(world, NetworkReplicationComponentTypeId, pillar);
    ecs_AddComponent(world, DeployableComponentTypeId, pillar);

    if (netEntityId == NULL_NET_ENTITY_ID)
    {
        netEntityId = AllocateNetEntityId(world);
    }

    // FIXME: Why does a building part need entity snapshots!
    // We have another bug because of this, even though the entity is created
    // on the client at the correct position it is still being lerped from
    // 0,0,0 which is visible to the player
    NetworkReplicationComponent network = {};
    network.netEntityId = netEntityId;
    network.snapshots[0].position = position;
    network.snapshots[0].isGrounded = true;
    network.priority = DEFAULT_ENTITY_NET_PRIORITY;
    network.type = EntityType_Pillar;
    ecs_SetComponent(
        world, NetworkReplicationComponent, &pillar, 1, &network);

    TransformComponent transform = {};
    transform.position = position;
    transform.rotation = Quat();
    transform.scale = Vec3(0.3, 3.5, 0.3);
    ecs_SetComponent(
            world, TransformComponent, &pillar, 1, &transform);

    BoxCollisionComponent box = {};
    box.halfDims = transform.scale * 0.5f;
    ecs_SetComponent(
            world, BoxCollisionComponent, &pillar, 1, &box);

    StaticMeshComponent staticMesh = {};
    staticMesh.meshes[0] = Mesh_Cube;
    staticMesh.materials[0] = Material_DevGrid512;
    staticMesh.count = 1;
    staticMesh.isVisible = true;
    staticMesh.useViewModelProjectionMatrix = false;
    ecs_SetComponent(
            world, StaticMeshComponent, &pillar, 1, &staticMesh);

    DeployableComponent deployable = {};
    deployable.type = Deployable_Pillar;
    ecs_SetComponent(
            world, DeployableComponent, &pillar, 1, &deployable);

    return pillar;
}

internal void PlaceBuildingPart(EntityWorld *world, GameAssets *assets,
    PlayerSnapshot currentState, EntityId playerEntity, PlayerInput playerInput,
    u32 buildingPartIndex)
{
    vec3 start = GetPlayerCameraPosition(currentState.position);
    f32 interactionRange = 2.5f;
    quat rotation = FromEulerAngles(playerInput.viewAngles);
    vec3 forward = RotateVector(Vec3(0, 0, -1), rotation);
    vec3 end = start + forward * BUILDING_RANGE;

    EntityPlacementResult placementResult =
        CalculatePlacement(world, assets, playerEntity, start, end, buildingPartIndex);

    if (placementResult.isValid)
    {
        if (buildingPartIndex == BuildingPart_Foundation)
        {
            SpawnFoundationEntity(world, placementResult.position);
        }
        else if (buildingPartIndex == BuildingPart_Pillar)
        {
            SpawnPillarEntity(world, placementResult.position);
        }
    }
}
