#pragma once
struct TerrainNormalMap
{
    vec3 *values;
    u32 width;
    u32 height;
};

HeightMap GenerateHeightMap(MemoryArena *arena, u32 width, u32 height);

TerrainNormalMap GenerateTerrainNormalMap(
    HeightMap heightMap, MemoryArena *arena);
u32 *ConvertNormalMapToRGBA8(TerrainNormalMap normalMap, MemoryArena *arena);
