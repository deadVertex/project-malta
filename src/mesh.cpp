struct VertexBufferAllocator
{
    void *vertexData;
    u32 *indexData;
    u32 vertexBufferId;
    u32 vertexLayout;
    u32 vertexBufferCapacity;
    u32 vertexBufferSize;
    u32 indexBufferCapacity;
    u32 indexBufferSize;
    u32 primitive;
};

inline u32 GetVertexSize(u32 vertexLayout)
{
    u32 result = 0;
    switch (vertexLayout)
    {
        case VertexLayout_Vertex:
        case VertexLayout_VertexInstanced:
            result = sizeof(Vertex);
            break;
        case VertexLayout_VertexPC:
            result = sizeof(VertexPC);
            break;
        default:
            InvalidCodePath();
            break;
    }

    return result;
}

internal VertexBufferAllocator CreateVertexBufferAllocator(
        u32 vertexBufferId,
        u32 vertexLayout,
        u32 vertexBufferLength,
        u32 indexBufferLength,
        u32 primitive,
        MemoryArena *arena)
{
    Assert(indexBufferLength % sizeof(u32) == 0);

    u32 vertexSize = GetVertexSize(vertexLayout);

    VertexBufferAllocator result = {};
    result.vertexData = AllocateBytes(arena, vertexBufferLength);
    result.indexData = (u32 *)AllocateBytes(arena, indexBufferLength);
    result.vertexBufferId = vertexBufferId;
    result.vertexLayout = vertexLayout;
    result.vertexBufferCapacity = vertexBufferLength;
    result.indexBufferCapacity = indexBufferLength;
    result.primitive = primitive;

    return result;
}

internal void UpdateVertexBuffer(
    VertexBufferAllocator *allocator, RenderCommandQueue *queue)
{
    RenderCommand_UpdateVertexBuffer *updateVertexBuffer =
        RenderCommandQueuePush(queue, RenderCommand_UpdateVertexBuffer);
    updateVertexBuffer->id = allocator->vertexBufferId;
    updateVertexBuffer->length = allocator->vertexBufferSize;
    updateVertexBuffer->data = allocator->vertexData;
    updateVertexBuffer->indicesLength = allocator->indexBufferSize;
    updateVertexBuffer->indexData = allocator->indexData;
}

internal Mesh AllocateMesh(VertexBufferAllocator *allocator, u32 vertexLayout,
    void *vertices, u32 vertexCount, u32 *indices, u32 indexCount)
{
    Assert(vertexLayout == allocator->vertexLayout);

    u32 vertexSize = GetVertexSize(vertexLayout);
    Assert(allocator->vertexBufferSize + vertexCount * vertexSize <=
           allocator->vertexBufferCapacity);
    Assert(allocator->indexBufferSize + indexCount * sizeof(u32) <=
           allocator->indexBufferCapacity);

    u32 currentVertexCount = allocator->vertexBufferSize / vertexSize;
    u32 currentIndexCount = allocator->indexBufferSize / sizeof(u32);

    void *vertexCursor = (u8 *)allocator->vertexData + allocator->vertexBufferSize;
    u32 *indexCursor = allocator->indexData + (allocator->indexBufferSize / sizeof(u32));

    CopyMemory(vertexCursor, vertices, vertexCount * vertexSize);
    allocator->vertexBufferSize += vertexCount * vertexSize;

    // TODO: Why aren't we using copy memory here?
    for (u32 i = 0; i < indexCount; ++i)
    {
        *indexCursor++ = indices[i] + currentVertexCount;
    }
    allocator->indexBufferSize += indexCount * sizeof(u32);

    Mesh result = {};
    result.vertexBufferId = allocator->vertexBufferId;
    result.count = indexCount > 0 ? indexCount : vertexCount;
    result.first = indexCount > 0 ? currentIndexCount : currentVertexCount;
    result.primitive = allocator->primitive;
    result.vertexLayout = allocator->vertexLayout;

    return result;
}

// TODO: Function for pushing on uniform values to make this less error-prone
internal Material CreateTextureMaterial(u32 shader, u32 texture,
    vec2 textureScale, f32 roughness, f32 metallic)
{
    Material result = {};
    result.shaderInput.shader = shader;
    PushUniformTexture2D(&result.shaderInput, UniformValue_Texture, texture);
    PushUniformVec2(
        &result.shaderInput, UniformValue_TextureScale, textureScale);
    PushUniformFloat(&result.shaderInput, UniformValue_Roughness, roughness);
    PushUniformFloat(&result.shaderInput, UniformValue_Metallic, metallic);
    PushUniformBuffer(&result.shaderInput, UniformValue_LightingData,
        UniformBuffer_LightingData);
    PushUniformTextureCubeMap(&result.shaderInput,
        UniformValue_IrradianceCubeMap, Texture_Irradiance);
    PushUniformTextureCubeMap(
        &result.shaderInput, UniformValue_RadianceCubeMap, Texture_Radiance);
    PushUniformTexture2D(
        &result.shaderInput, UniformValue_BrdfLut, Texture_BrdfLut);

    return result;
}

internal Material CreateBasicMaterial(u32 shader, vec3 color, f32 roughness,
    f32 metallic, b32 useRoughnessTexture = false,
    b32 useMetallicTexture = false)
{
    Material result = {};
    result.shaderInput.shader = shader;
    PushUniformVec4(&result.shaderInput, UniformValue_Color, Vec4(color, 1));
    if (!useRoughnessTexture)
    {
        PushUniformFloat(&result.shaderInput, UniformValue_Roughness, roughness);
    }

    if (!useMetallicTexture)
    {
        PushUniformFloat(&result.shaderInput, UniformValue_Metallic, metallic);
    }
    PushUniformBuffer(&result.shaderInput, UniformValue_LightingData,
        UniformBuffer_LightingData);
    PushUniformTextureCubeMap(&result.shaderInput,
        UniformValue_IrradianceCubeMap, Texture_Irradiance);
    PushUniformTextureCubeMap(
        &result.shaderInput, UniformValue_RadianceCubeMap, Texture_Radiance);
    PushUniformTexture2D(
        &result.shaderInput, UniformValue_BrdfLut, Texture_BrdfLut);

    return result;
}

inline void SetEnvironment(
    ShaderInput *input, mat4 shadowViewProjection, u32 shadowMap)
{
    PushUniformMat4(input, UniformValue_ShadowViewProjectionMatrix, shadowViewProjection);
    PushUniformTexture2D(input, UniformValue_ShadowMap, shadowMap);
}

inline Mesh *GetMesh(GameAssets *assets, u32 meshId)
{
    Assert(meshId < ArrayCount(assets->meshes));
    Mesh *mesh = assets->meshes + meshId;
    return mesh;
}

inline void SetMesh(RenderCommand_DrawVertexBuffer *cmd, GameAssets *assets, u32 meshId)
{
    Mesh *mesh = GetMesh(assets, meshId);
    cmd->id = mesh->vertexBufferId;
    cmd->first = mesh->first;
    cmd->count = mesh->count;
    cmd->primitive = mesh->primitive;
}

inline Material *GetMaterial(GameAssets *assets, u32 materialId)
{
    Assert(materialId < ArrayCount(assets->materials));
    Material *material = assets->materials + materialId;
    return material;
}

inline void SetMaterial(ShaderInput *input, GameAssets *assets, u32 materialId,
    mat4 viewProjection, mat4 modelMatrix)
{
    Material *material = GetMaterial(assets, materialId);
    *input = material->shaderInput;

    PushUniformMat4(input, UniformValue_ViewProjectionMatrix, viewProjection);
    PushUniformMat4(input, UniformValue_ModelMatrix, modelMatrix);
}
