/*
TODO:
 - Memory arenas
 - UVs
 - Smooth normals
*/
#include "platform.h"
#include "game.h"
//#include "math_lib.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "offline_path_tracer.h"

#include "image_processing.cpp"
#include "asset.cpp"

#ifdef PLATFORM_WINDOWS
#include <Windows.h>
#else
#include <pthread.h>
#include <sys/time.h>
#endif

global MeshData g_SphereMesh;

internal Image AllocateImage(u32 width, u32 height)
{
    Image result = {};
    result.width = width;
    result.height = height;
    result.pixels = (u32*)malloc(sizeof(u32) * width * height);

    return result;
}

inline vec3 FresnelSchlick(vec3 f0, vec3 n, vec3 l)
{
    f32 nDotL = Max(Dot(n, l), 0.0);
    f32 a = Pow(1.0f - nDotL, 5.0f);
    vec3 result = f0 + (Vec3(1) - f0) * a;
    return result;
}

inline vec3 ReinhardToneMap(vec3 v)
{
    vec3 denom = Vec3(1.0f) + v;

    vec3 result;
    result.r = v.r / denom.r;
    result.g = v.g / denom.g;
    result.b = v.b / denom.b;

    return result;
}

global CubeMap g_CubeMap;

struct SampleCubeMapResult
{
    vec2 uv;
    u32 faceIndex;
};

internal SampleCubeMapResult SampleCubeMap(vec3 v)
{
    SampleCubeMapResult result = {};
    u32 axis = CalculateLargestAxis(v);

    vec3 vAbs = Abs(v);
    f32 ma = 0.0f;
    switch (axis)
    {
    case AXIS_X:
        result.faceIndex =
            v.x < 0.0f ? CUBE_MAP_FACE_X_NEG : CUBE_MAP_FACE_X_POS;
        ma = 0.5f / vAbs.x;
        result.uv = Vec2(v.x < 0.0f ? -v.z : v.z, v.y);
        break;
    case AXIS_Y:
        result.faceIndex =
            v.y < 0.0f ? CUBE_MAP_FACE_Y_NEG : CUBE_MAP_FACE_Y_POS;
        ma = 0.5f / vAbs.y;
        result.uv = Vec2(v.x, v.y < 0.0f ? -v.z : v.z);
        break;
    case AXIS_Z:
        // FIXME: Asset builder has 
        // result.faceIndex = v.z < 0.0f ? CUBE_MAP_FACE_Z_NEG : CUBE_MAP_FACE_Z_POS;
        // result.uv = Vec2(v.z < 0.0f ? -v.x : v.x, v.y);
        // WHY IS THIS AXIS INVERTED?!?!?
        result.faceIndex =
            v.z < 0.0f ? CUBE_MAP_FACE_Z_POS : CUBE_MAP_FACE_Z_NEG;
        ma = 0.5f / vAbs.z;
        result.uv = Vec2(v.z < 0.0f ? v.x : -v.x, v.y);
        break;
    default:
        InvalidCodePath();
        break;
    }

    result.uv = result.uv * ma + Vec2(0.5f);

    return result;
}

struct SceneRaycastResult
{
    f32 tmin;
    vec3 position;
    vec3 normal;

    // Ray information
    vec3 rayOrigin;
    vec3 rayDirection;

    // Surface information
    vec3 surfaceColor;
    vec3 surfaceEmission;

    b32 spawnReflectionRay;
};

internal f32 RayIntersectPlane(vec3 planeNormal, f32 distance, vec3 rayOrigin, vec3 rayDirection)
{
    f32 a = Dot(planeNormal, rayOrigin);
    f32 denom = Dot(planeNormal, rayDirection);
    f32 t = (distance - a) / denom;
    return t;
}

// A more optimized implementation of this would store the plane for each triangle
// NOTE: This expects vertices in clockwise winding order!
internal f32 RayIntersectTriangle(vec3 a, vec3 b, vec3 c, vec3 rayOrigin, vec3 rayDirection)
{
    f32 tmin = -1.0f;

    // Calculate triangle face plane
    vec3 ab = b - a;
    vec3 ca = a - c;
    vec3 normal = Cross(ab, ca);
    Assert(LengthSq(normal) > 0.0f);
    normal = Normalize(normal);
    f32 distance = Dot(normal, a);

    // Ignore any collision if we are not raycasting in the direction opposite
    // to the normal.
    // This creates the effect of single sided triangles, double sided would be
    // more complex to implement.
    if (Dot(normal, rayDirection) <= 0.0f)
    {
        f32 t = RayIntersectPlane(normal, distance, rayOrigin, rayDirection);
        if (t > EPSILON)
        {
            vec3 normalAb = Normalize(Cross(normal, ab));
            vec3 normalCa = Normalize(Cross(normal, ca));

            vec3 bc = c - b;
            vec3 normalBc = Normalize(Cross(normal, bc));

            vec3 p = rayOrigin + rayDirection * t;
            if (Dot(normalAb, p - a) <= 0.0f)
            {
                if (Dot(normalCa, p - a) <= 0.0f)
                {
                    if (Dot(normalBc, p - b) <= 0.0f)
                    {
                        tmin = t;
                    }
                }
            }
        }
    }

    return tmin;
}

internal f32 RayIntersectSphere(vec3 sphereCenter, f32 sphereRadius, vec3 rayOrigin, vec3 rayDirection)
{
    f32 tmin = -1.0f;
    vec3 m = rayOrigin - sphereCenter;
    f32 a = Dot(rayDirection, rayDirection);
    f32 b = 2.0f * Dot(m, rayDirection);
    f32 c = Dot(m, m) - sphereRadius * sphereRadius;

    // Calculate discrimanant
    f32 d = b * b - 4 * a * c;
    if (d > 0.0f)
    {
        f32 denom = 2.0f * a;
        f32 e = Sqrt(d);
        f32 t0 = (-b + e) / denom;
        f32 t1 = (-b - e) / denom;
        Assert(t1 <= t0);
        tmin = t1;
    }
    return tmin;
}

internal f32 RayIntersectAabb(vec3 boxMin, vec3 boxMax, vec3 rayOrigin, vec3 rayDirection)
{
    f32 tmin = 0.0f;
    f32 tmax = F32_MAX;

    // TODO: Remove breaks so we refactor for SIMD in the future
    for (u32 axis = 0; axis < 3; ++axis)
    {
        f32 s = rayOrigin.data[axis];
        f32 d = rayDirection.data[axis];
        f32 min = boxMin.data[axis];
        f32 max = boxMax.data[axis];

        if (Abs(d) < EPSILON)
        {
            // Ray is parallel to axis plane
            if (s < min || s > max)
            {
                tmin = -1.0f;
                break;
            }
        }
        else
        {
            f32 ood = 1.0f / d;
            f32 a = (min - s) * ood;
            f32 b = (max - s) * ood;

            f32 t0 = Min(a, b);
            f32 t1 = Max(a, b);

            tmin = Max(t0, tmin);
            tmax = Min(t1, tmax);

            if (tmin > tmax)
            {
                tmin = -1.0f;
                break;
            }
        }
    }

    return tmin;
}

inline vec3 CalculateSphereNormal(vec3 center, vec3 p)
{
    vec3 result = Normalize(p - center);
    return result;
}

inline vec3 CalculateAabbNormal(vec3 boxMin, vec3 boxMax, vec3 p)
{
    vec3 center = (boxMin + boxMax) * 0.5f;
    p = p - center;
    vec3 r = (boxMax - boxMin) * 0.5f;
    f32 x = p.x / r.x;
    f32 y = p.y / r.y;
    f32 z = p.z / r.z;
    f32 fx = Truncate(x);
    f32 fy = Truncate(y);
    f32 fz = Truncate(z);
    vec3 n = Normalize(Vec3(fx, fy, fz));

    return n;
}

inline vec3 CalculateTriangleNormal(vec3 a, vec3 b, vec3 c)
{
    vec3 ab = b - a;
    vec3 ca = a - c;
    vec3 normal = Cross(ab, ca);
    Assert(LengthSq(normal) > 0.0f);
    normal = Normalize(normal);

    return normal;
}

internal SceneRaycastResult RayIntersectScene(vec3 rayOrigin, vec3 rayDirection)
{
    SceneRaycastResult result = {};
    result.rayOrigin = rayOrigin;
    result.rayDirection = rayDirection;

    f32 maxRayDist = 100.0f;

    f32 tmin = F32_MAX;

//#define TRIANGLE_TEST
//#define MESH_TEST
#define MATERIAL_ONLY
#ifdef MATERIAL_ONLY
    {
        vec3 center = Vec3(0, 0, 2);
        f32 t = RayIntersectSphere(center, 1.0f, rayOrigin, rayDirection);
        if (t > 0.0f && t < tmin)
        {
            result.tmin = t;
            result.position = rayOrigin + rayDirection * t;
            result.normal = CalculateSphereNormal(center, result.position);
            result.surfaceEmission = Vec3(0);
            result.surfaceColor = Vec3(0.18, 0.18, 0.18);
            tmin = result.tmin;
        }
    }
#elif defined(TRIANGLE_TEST)
    {
        vec3 a = Vec3(0, 1, 1);
        vec3 b = Vec3(1, -1, 1.5);
        vec3 c = Vec3(-1, -1, 2);
        f32 t = RayIntersectTriangle(a, b, c, rayOrigin, rayDirection);
        if (t > EPSILON && t < tmin)
        {
            result.tmin = t;
            result.position = rayOrigin + rayDirection * t;
            result.normal = CalculateTriangleNormal(a, b, c);
            result.surfaceEmission = Vec3(0);
            result.surfaceColor = Vec3(0.18, 0.18, 0.18);
            tmin = result.tmin;
        }
    }
#elif defined(MESH_TEST)
    {
        u32 triangleCount = g_SphereMesh.indexCount / 3;
        for (u32 triangleIndex = 0; triangleIndex < triangleCount; ++triangleIndex)
        {
            u32 i = g_SphereMesh.indices[triangleIndex * 3 + 0];
            u32 j = g_SphereMesh.indices[triangleIndex * 3 + 1];
            u32 k = g_SphereMesh.indices[triangleIndex * 3 + 2];

            vec3 c = g_SphereMesh.vertices[i].position;
            vec3 b = g_SphereMesh.vertices[j].position;
            vec3 a = g_SphereMesh.vertices[k].position;

            f32 t = RayIntersectTriangle(a, b, c, rayOrigin, rayDirection);
            if (t > EPSILON && t < tmin)
            {
                result.tmin = t;
                result.position = rayOrigin + rayDirection * t;
                result.normal = CalculateTriangleNormal(a, b, c);
                result.surfaceEmission = Vec3(0);
                result.surfaceColor = Vec3(0.18, 0.18, 0.18);
                tmin = result.tmin;
            }
        }
    }
#else
    {
        vec3 planeNormal = Vec3(0, 1, 0);
        f32 distance = 0.0f;
        f32 t = RayIntersectPlane(planeNormal, distance, rayOrigin, rayDirection);
        if (t > 0.0f && t < tmin)
        {
            result.tmin = t;
            result.position = rayOrigin + rayDirection * t;
            result.normal = planeNormal;
            result.surfaceColor = Vec3(1);
            result.surfaceEmission = Vec3(0);
            tmin = t;
        }
    }

    {
        vec3 planeNormal = Vec3(0, -1, 0);
        f32 distance = -5.0f;
        f32 t = RayIntersectPlane(planeNormal, distance, rayOrigin, rayDirection);
        if (t > 0.0f && t < tmin)
        {
            result.tmin = t;
            result.position = rayOrigin + rayDirection * t;
            result.normal = planeNormal;
            result.surfaceColor = Vec3(1);
            result.surfaceEmission = Vec3(0);
            tmin = t;
        }
    }

    {
        vec3 planeNormal = Vec3(1, 0, 0);
        f32 distance = -2.0f;
        f32 t = RayIntersectPlane(planeNormal, distance, rayOrigin, rayDirection);
        if (t > 0.0f && t < tmin)
        {
            result.tmin = t;
            result.position = rayOrigin + rayDirection * t;
            result.normal = planeNormal;
            result.surfaceColor = Vec3(1);
            result.surfaceEmission = Vec3(0);
            tmin = t;
        }
    }

    {
        vec3 planeNormal = Vec3(-1, 0, 0);
        f32 distance = -2.0f;
        f32 t = RayIntersectPlane(planeNormal, distance, rayOrigin, rayDirection);
        if (t > 0.0f && t < tmin)
        {
            result.tmin = t;
            result.position = rayOrigin + rayDirection * t;
            result.normal = planeNormal;
            result.surfaceColor = Vec3(1);
            result.surfaceEmission = Vec3(0);
            tmin = t;
        }
    }

    {
        vec3 planeNormal = Vec3(0, 0, 1);
        f32 distance = -2.0f;
        f32 t = RayIntersectPlane(planeNormal, distance, rayOrigin, rayDirection);
        if (t > 0.0f && t < tmin)
        {
            result.tmin = t;
            result.position = rayOrigin + rayDirection * t;
            result.normal = planeNormal;
            result.surfaceColor = Vec3(1);
            result.surfaceEmission = Vec3(0);
            tmin = t;
        }
    }

    {
        vec3 center = Vec3(0, 1, 0);
        f32 t = RayIntersectSphere(center, 1.0f, rayOrigin, rayDirection);
        if (t > 0.0f && t < tmin)
        {
            result.tmin = t;
            result.position = rayOrigin + rayDirection * t;
            result.normal = CalculateSphereNormal(center, result.position);
            result.surfaceEmission = Vec3(0);
            result.surfaceColor = Vec3(1, 0, 0);
            tmin = result.tmin;
        }
    }

    {
        vec3 boxMin = Vec3(1, 0, 2);
        vec3 boxMax = Vec3(2, 1.5, 3);
        f32 t = RayIntersectAabb(boxMin, boxMax, rayOrigin, rayDirection);
        if (t > 0.0f && t < tmin)
        {
            result.tmin = t;
            result.position = rayOrigin + rayDirection * t;
            result.normal = CalculateAabbNormal(boxMin, boxMax, result.position);
            result.surfaceEmission = Vec3(0);
            result.surfaceColor = Vec3(0.4, 0.8, 0.1);
            tmin = result.tmin;
        }
    }

    {
        vec3 boxMin = Vec3(-0.5, 4, -0.5);
        vec3 boxMax = Vec3(0.5, 4.2, 0.5);
        f32 t = RayIntersectAabb(boxMin, boxMax, rayOrigin, rayDirection);
        if (t > 0.0f && t < tmin)
        {
            result.tmin = t;
            result.position = rayOrigin + rayDirection * t;
            result.normal = CalculateAabbNormal(boxMin, boxMax, result.position);
            result.surfaceEmission = Vec3(1, 0.9, 0.6);
            result.surfaceColor = Vec3(0);
            tmin = result.tmin;
        }
    }
#endif

    if (tmin > maxRayDist)
    {
        // Hit nothing
        result.surfaceColor = Vec3(0);
        SampleCubeMapResult sampleResult = SampleCubeMap(rayDirection);
        vec4 sample = SampleNearestRGBA32F(
            (f32 *)g_CubeMap.faces[sampleResult.faceIndex], g_CubeMap.faceWidth,
            g_CubeMap.faceWidth, sampleResult.uv.u, sampleResult.uv.v);
        result.surfaceEmission = sample.rgb;
    }

    return result;
}

#define MAX_DEPTH 3

#if 0 // Hold off on this for now, need to play around with BRDFs a bit first
      // so we better understand what we need
struct RecursionEntry
{
    vec3 rayOrigin;
    vec3 rayDirection;
    u32 depth;
};

struct LightPath
{
};

// TODO: We want this function structured so that it can trace multiple paths
// in parallel so we can take advantage of SIMD
internal vec3 TracePath(vec3 rayOrigin, vec3 rayDirection, u32 depth, RandomNumberGenerator *rng)
{
    RecursionEntry queue[1024];
    u32 queueLength = 1;

    queue[0].rayOrigin = rayOrigin;
    queue[0].rayDirection = rayDirection;
    queue[0].depth = depth;

    // TODO: Make this branchless
    while (queueLength > 0)
    {
        RecursionEntry *entry = queue[--queueLength];
        SceneRaycastResult raycastResult =
            RayIntersectScene(entry->rayOrigin, entry->rayDirection);

        if (raycastResult.tmin > EPSILON)
        {
            vec3 surfaceNormal = raycastResult.normal;
            vec3 surfaceColor = raycastResult.surfaceColor;
            vec3 surfaceEmission = raycastResult.surfaceEmission;
            vec3 surfacePosition = raycastResult.position;
            vec3 viewVector = raycastResult.rayDirection;

            f32 tileWidth = 0.2f;
            u32 value = (int)(surfacePosition.x * 10.0f) +
                        (int)(surfacePosition.z * 10.0f);
            f32 mul = (value % 2) ? 0.5f : 1.0f;

            // FIXME: Better way of calculating a uniform distribution of
            // vectors in a hemisphere!
            // TODO: Statistical analysis?
            vec3 randomDirection = Normalize(Vec3(RandomBilateral(rng),
                RandomBilateral(rng), RandomBilateral(rng)));
            if (Dot(randomDirection, surfaceNormal) <= 0.0f)
            {
                randomDirection = -randomDirection;
            }
            vec3 diffuseDirection = surfaceNormal + randomDirection;

            if (entry->depth < MAX_DEPTH)
            {
                Assert(queueLength < ArrayCount(queue));
                RecursionEntry *newEntry = queue + queueLength++;
                newEntry->rayOrigin = surfacePosition + surfaceNormal * EPSILON;
                newEntry->rayDirection = diffuseDirection;
                newEntry->depth = depth + 1;
            }

            vec3 incomingRadiance =
                RecursiveRayTrace(surfacePosition + surfaceNormal * EPSILON,
                    diffuseDirection, depth + 1, rng);

            vec3 lightDirection = diffuseDirection;
            f32 cosine =
                Max(Dot(surfaceNormal, diffuseDirection), 0.0f);

            // incomingRadiance = surfaceEmission + Hadamard(surfaceColor, incomingRadiance) * cosine
            radiance = surfaceEmission + Hadamard(surfaceColor, incomingRadiance) * cosine;
        }
    }
}
#endif

internal vec3 RecursiveRayTrace(vec3 rayOrigin, vec3 rayDirection, u32 depth, RandomNumberGenerator *rng)
{
    vec3 radiance = Vec3(0, 0, 0);

    if (depth <= MAX_DEPTH)
    {
        SceneRaycastResult raycastResult =
            RayIntersectScene(rayOrigin, rayDirection);

        if (raycastResult.tmin > EPSILON)
        {
            vec3 surfaceNormal = raycastResult.normal;
            vec3 surfaceColor = raycastResult.surfaceColor;
            vec3 surfaceEmission = raycastResult.surfaceEmission;
            vec3 surfacePosition = raycastResult.position;
            vec3 viewVector = raycastResult.rayDirection;

            f32 tileWidth = 0.2f;
            u32 value = (int)(surfacePosition.x * 10.0f) +
                        (int)(surfacePosition.z * 10.0f);
            if (value % 2)
            {
                surfaceColor = surfaceColor * 0.5f;
            }

            // FIXME: Better way of calculating a uniform distribution of vectors in a hemisphere!
            // TODO: Statistical analysis?
            vec3 randomDirection = Normalize(Vec3(RandomBilateral(rng),
                RandomBilateral(rng), RandomBilateral(rng)));
            if (Dot(randomDirection, surfaceNormal) <= 0.0f)
            {
                randomDirection = -randomDirection;
            }
            vec3 diffuseDirection = Normalize(surfaceNormal + randomDirection);

            vec3 incomingRadiance =
                RecursiveRayTrace(surfacePosition + surfaceNormal * EPSILON,
                    diffuseDirection, depth + 1, rng);

            vec3 lightDirection = diffuseDirection;
            f32 cosine =
                Max(Dot(surfaceNormal, diffuseDirection), 0.0f);

            vec3 diffuseRadiance = Hadamard(surfaceColor, incomingRadiance);

            // FIXME: Not energy conserving
            vec3 halfVector = Normalize(-diffuseDirection + viewVector);
            f32 hardness = 16.0f;
            vec3 specularRadiance =
                incomingRadiance *
                Pow(Max(Dot(surfaceNormal, halfVector), 0.0), hardness);

            radiance = surfaceEmission + (specularRadiance) * cosine;
        }
        else
        {
            radiance = raycastResult.surfaceEmission;
        }
    }

    return radiance;
}

internal void GenerateCubeMap(CubeMap *cubeMap, u32 faceWidth)
{
    u32 bytesPerPixel = sizeof(vec4);
    u32 faceSize = faceWidth * faceWidth * bytesPerPixel;

    cubeMap->faceWidth = faceWidth;

    vec4 faceIndexColors[CUBE_MAP_FACE_COUNT] = {
        Vec4(1.0f, 0.0f, 0.0f, 1.0f),
        Vec4(1.0f, 0.0f, 0.0f, 1.0f),
        Vec4(0.0f, 1.0f, 0.0f, 1.0f),
        Vec4(0.0f, 1.0f, 0.0f, 1.0f),
        Vec4(0.0f, 0.0f, 1.0f, 1.0f),
        Vec4(0.0f, 0.0f, 1.0f, 1.0f),
    };

    Assert(bytesPerPixel == sizeof(vec4));
    for (u32 faceIndex = 0; faceIndex < CUBE_MAP_FACE_COUNT; ++faceIndex)
    {
        // TODO: Memory arenas
        cubeMap->faces[faceIndex] = (vec4 *)malloc(faceSize);

        for (u32 y = 0; y < faceWidth; ++y)
        {
            for (u32 x = 0; x < faceWidth; ++x)
            {
                f32 fx = (f32)x / (f32)faceWidth;
                f32 fy = (f32)y / (f32)faceWidth;

                fy = 1.0f - fy;

                // Map to -0.5 to 0.5 range
                fx -= 0.5f;
                fy -= 0.5f;

                cubeMap->faces[faceIndex][y * faceWidth + x] =
                    faceIndexColors[faceIndex];
            }
        }
    }
}

// OOF!
// Copied from asset_builder.cpp
struct ReadEntireFileResult
{
    void *contents;
    u32 length;
};

internal ReadEntireFileResult ReadEntireFile(const char *path)
{
    ReadEntireFileResult result = {};
    FILE *f = fopen(path, "rb");
    if (f != NULL)
    {
        fseek(f, 0, SEEK_END);
        result.length = ftell(f);
        fseek(f, 0, SEEK_SET);
        result.contents = malloc(result.length);
        size_t ret = fread(result.contents, 1, result.length, f);
        Assert(ret == result.length);
        fclose(f);
    }
    else
    {
        LogMessage("Failed to open file %s", path);
    }

    return result;
}

internal void FreeFileResult(ReadEntireFileResult result)
{
    free(result.contents);
}

inline u32 LockedAddAndReturnPreviousValue(volatile u32 *value, u32 addend)
{
#ifdef PLATFORM_WINDOWS
    u32 result = InterlockedExchangeAdd(value, addend);
#else
    u32 result = __atomic_fetch_add(value, addend, __ATOMIC_RELAXED);
#endif
    return result;
}

inline u64 LockedAddAndReturnPreviousValue(volatile u64 *value, u64 addend)
{
#ifdef PLATFORM_WINDOWS
    u64 result = InterlockedExchangeAdd64((volatile LONG64*)value, addend);
#else
    u32 result = __atomic_fetch_add(value, addend, __ATOMIC_RELAXED);
#endif
    return result;
}

internal void PathTraceRegion(Image image, u32 minX, u32 minY, u32 onePastMaxX,
    u32 onePastMaxY, RandomNumberGenerator *rng)
{
    vec3 cameraPosition = Vec3(0, 0, 5);
    vec3 cameraForward = Normalize(Vec3(0, 0, -1));
    vec3 cameraRight = Vec3(1, 0, 0);
    vec3 cameraUp = Vec3(0, 1, 0);

    // Doing a old-school pin-hole style camera for now
    f32 filmDistance = 1.0f;
    vec3 filmCenter = cameraForward * filmDistance + cameraPosition;

    f32 pixelWidth = 1.0f / (f32)image.width;
    f32 pixelHeight = 1.0f / (f32)image.height;

    f32 halfPixelWidth = 0.5f * pixelWidth;
    f32 halfPixelHeight = 0.5f * pixelHeight;

    f32 fovX = Radians(40.0f);

    Assert(image.width == image.height);
    f32 filmWidth = 1.0f;
    f32 filmHeight = 1.0f;
    for (u32 py = minY; py < onePastMaxY; ++py)
    {
        for (u32 px = minX; px < onePastMaxX; ++px)
        {
            f32 width = (f32)image.width;
            f32 height = (f32)image.height;

            f32 fx = (f32)px / (f32)image.width;
            f32 fy = 1.0f - ((f32)py / (f32)image.height);

            // Map from 0 to 1 range to -1 to 1
            f32 filmX = 2.0f * fx - 1.0f;
            f32 filmY = 2.0f * fy - 1.0f;

            // Perspective camera
            filmX *= Tan(fovX);
            filmY *= Tan(height / width * fovX);

            vec3 cameraRadiance = Vec3(0, 0, 0);

#define SAMPLE_COUNT 128
            f32 sampleContribution = 1.0f / (f32)SAMPLE_COUNT;
            for (u32 sampleIndex = 0; sampleIndex < SAMPLE_COUNT; ++sampleIndex)
            {
                f32 offsetX = filmX + halfPixelWidth +
                              halfPixelWidth * RandomBilateral(rng);
                f32 offsetY = filmY + halfPixelHeight +
                              halfPixelHeight * RandomBilateral(rng);

                vec3 filmP = 0.5f * filmWidth * offsetX * cameraRight +
                             0.5f * filmHeight * offsetY * cameraUp +
                             filmCenter;

                vec3 rayOrigin = cameraPosition;
                vec3 rayDirection = Normalize(filmP - rayOrigin);

                vec3 radiance = RecursiveRayTrace(rayOrigin, rayDirection, 0, rng);

                // Radiance clamping to reduce fireflies
                radiance = Clamp(radiance, Vec3(0), Vec3(2));

//#define NORMAL_DEBUG
#ifdef NORMAL_DEBUG
                radiance = surfaceNormal;
#endif

                cameraRadiance += radiance * sampleContribution;
            }

            // TODO: Need to actually calculate scene luminance and feed that as an input into this
            vec3 outputColor = ReinhardToneMap(cameraRadiance);
            // Linear to sRGB conversion
            outputColor = Pow(outputColor, Vec3(1.0f / 2.2f));

            outputColor = outputColor * 255.0f;

            // Output color format is RGBA little endian
            u32 a = 0xFF000000;
            u32 r = (u32)outputColor.r;
            u32 g = ((u32)outputColor.g) << 8;
            u32 b = ((u32)outputColor.b) << 16;
            image.pixels[py * image.width + px] = a | b | g | r;
        }
    }
}

internal b32 ProcessWorkOrder(WorkQueue *queue)
{
    u32 head = LockedAddAndReturnPreviousValue(&queue->head, 1);

    if (head < queue->length)
    {
        printf("Processing work order %u/%u\n", head, queue->length);
        WorkOrder *order = queue->contents + head;

        RandomNumberGenerator rng;
        rng.state = 747 + 345345 * order->minX ^ order->minY * 79852;

        PathTraceRegion(*order->image, order->minX, order->minY,
            order->onePastMaxX, order->onePastMaxY, &rng);

        LockedAddAndReturnPreviousValue(&queue->tilesProcessed, 1);
        return true;
    }

    return false;
}

internal void* WorkerThread(void *data)
{
    WorkQueue *queue = (WorkQueue *)data;
    while (ProcessWorkOrder(queue))
    {
    }

    return NULL;
}

#ifdef PLATFORM_WINDOWS
DWORD Win32_ThreadProc(void *data)
{
    WorkerThread(data);
    return 0;
}
#endif

internal void CreateWorkerThread(void *data)
{
#ifdef PLATFORM_WINDOWS
    CreateThread(NULL, 0, &Win32_ThreadProc, data, 0, NULL);
#else
    pthread_t thread;
    pthread_create(&thread, NULL, WorkerThread, data);
#endif
}

internal b32 LoadCubeMapFromDisk(CubeMap *cubeMap, u32 faceWidth, const char *path)
{
    b32 result = false;

    ReadEntireFileResult fileResult = ReadEntireFile(path);

    b32 flipYAxis = true;
    i32 width, height, channels;
    stbi_set_flip_vertically_on_load(flipYAxis);

    f32 *pixels = stbi_loadf_from_memory(
        (const u8 *)fileResult.contents, fileResult.length, &width, &height, &channels, STBI_rgb_alpha);
    FreeFileResult(fileResult);
    if (pixels != NULL)
    {
        u32 bytesPerPixel = sizeof(vec4);
        u32 faceSize = faceWidth * faceWidth * bytesPerPixel;

        cubeMap->faceWidth = faceWidth;

        Assert(bytesPerPixel == sizeof(vec4));
        for (u32 faceIndex = 0; faceIndex < CUBE_MAP_FACE_COUNT; ++faceIndex)
        {
            cubeMap->faces[faceIndex] = (vec4 *)malloc(faceSize);

            for (u32 y = 0; y < faceWidth; ++y)
            {
                for (u32 x = 0; x < faceWidth; ++x)
                {
                    f32 fx = (f32)x / (f32)faceWidth;
                    f32 fy = (f32)y / (f32)faceWidth;

                    // Only do this for opengl
                    //fy = 1.0f - fy;

                    // Map to -0.5 to 0.5 range
                    fx -= 0.5f;
                    fy -= 0.5f;

                    vec2 uv = SampleSphericalMap(Normalize(GetSampleDir(fx, fy, faceIndex)));

                    // Sample pixels
                    vec4 sample = SampleNearestRGBA32F(pixels, width, height, uv.x, uv.y);


                    cubeMap->faces[faceIndex][y * faceWidth + x] = sample;
                }
            }
        }
        stbi_image_free(pixels);

        result = true;
    }

    return result;
}

int main(int argc, char **argv)
{
    Image image = AllocateImage(512, 512);

    //GenerateCubeMap(&g_CubeMap, 512);
    LoadCubeMapFromDisk(&g_CubeMap, 512, "../content/qwantani_2k.hdr");

    u32 meshArenaSize = Megabytes(64);
    MemoryArena meshArea;
    InitializeMemoryArena(&meshArea, malloc(meshArenaSize), meshArenaSize);
    g_SphereMesh = CreateIcosahedronMesh(0, &meshArea);

    u32 maxThreads = 16;
    u32 tileWidth = 64;
    u32 tileHeight = 64;
    u32 tileCountX = (image.width + tileWidth - 1) / tileWidth;
    u32 tileCountY = (image.height + tileHeight - 1) / tileHeight;

    // Populate work queue
    WorkQueue queue = {};
    queue.contents =
        (WorkOrder *)malloc(sizeof(WorkOrder) * tileCountX * tileCountY);

    for (u32 tileY = 0; tileY < tileCountY; ++tileY)
    {
        for (u32 tileX = 0; tileX < tileCountX; ++tileX)
        {
            Assert(queue.length < tileCountX * tileCountY);
            WorkOrder *order = queue.contents + queue.length++;

            order->image = &image;
            order->minX = tileX * tileWidth;
            order->minY = tileY * tileHeight;
            order->onePastMaxX = MinU32(image.width, order->minX + tileWidth);
            order->onePastMaxY = MinU32(image.height, order->minY + tileHeight);
        }
    }

    // Create worker threads
    for (u32 threadCount = 1; threadCount < maxThreads; ++threadCount)
    {
        CreateWorkerThread(&queue);
    }

    u32 totalTileCount = tileCountX * tileCountY;
    while (queue.tilesProcessed < totalTileCount)
    {
        // Additional check as ProcessWorkOrder increments queue.head
        if (queue.head < queue.length)
        {
            ProcessWorkOrder(&queue);
        }
    }

    if (!stbi_write_bmp("output.bmp", image.width, image.height, 4, image.pixels))
    {
        printf("Failed to write image!\n");
    }

    return 0;
}
