inline void ClearNetEntityIdBitset(NetEntityIdBitset *bitset)
{
    bitset->data = 0;
}

inline b32 IsNetEntityIdPresent(NetEntityIdBitset *bitset, NetEntityId entityId)
{
    Assert(entityId < 64);
    u64 mask = 1ULL << (u64)entityId;
    return (bitset->data & mask) != 0;
}

inline void SetNetEntityIdPresent(NetEntityIdBitset *bitset, NetEntityId entityId, b32 value)
{
    Assert(entityId < 64);
    u64 mask = 1ULL << (u64)entityId;
    if (value)
    {
        bitset->data |= mask;
    }
    else
    {
        bitset->data &= ~mask;
    }
}

internal u32 ConvertBitsetToNetEntityIds(NetEntityIdBitset *bitset, NetEntityId *entities)
{
    u32 count = 0;

    for (NetEntityId entityId = 0; entityId < 64; ++entityId)
    {
        u64 mask = (1ULL << (u64)entityId);
        if (bitset->data & mask)
        {
            entities[count++] = entityId;
        }
    }

    return count;
}
