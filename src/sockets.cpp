#ifdef PLATFORM_WINDOWS
#include <winsock2.h>
typedef i32 socklen_t;

#elif defined(PLATFORM_LINUX)
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <errno.h>
#include <cstring>

#define SOCKET i32
#endif

#define INACTIVITY_THRESHOLD 30 // Assuming 30hz fixed update rate

#define PROTOCOL_VERSION 1
#define PROTOCOL_ID (((u16)'M' << 8) | (u16)'A')
struct PacketHeader
{
    u16 protocolId;
    u16 protocolVersion;
};

#define MAX_PACKET_LENGTH (MAX_PACKET_DATA + sizeof(PacketHeader))

// TODO: Test error handling
struct ClientEntry
{
    struct sockaddr_storage address;
    u32 inactivityCounter;
    b32 isConnected;
};

struct ServerSocketState
{
    ClientEntry clients[MAX_CLIENTS]; // Non-continguous
    u32 clientCount;
    SOCKET handle;
};

struct ClientSocketState
{
    // TODO: Support IPv6 server addresses!
    struct sockaddr_storage serverAddress;
    u32 ticksSinceLastPacket;
    b32 isConnected;
    SOCKET handle;
};

internal void Win32_LogErrorMessage(const char *prefix, i32 errorCode)
{
    char *message;
    FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
            FORMAT_MESSAGE_FROM_SYSTEM |
            FORMAT_MESSAGE_IGNORE_INSERTS,
            NULL, errorCode, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
            (LPTSTR)&message, 0, NULL);

    LogMessage("%s: %s", prefix, message);
    LocalFree(message);
}

internal b32 InitializeSockets()
{
#ifdef PLATFORM_WINDOWS
    WSADATA wsaData;
    if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
    {
        Win32_LogErrorMessage("Failed to initialize WSA", WSAGetLastError());
        return false;
    }
    LogMessage("WSA initialized");
#endif

    return true;
}

internal void DeinitializeSockets()
{
#ifdef PLATFORM_WINDOWS
    WSACleanup();
#endif
}

internal b32 SetSocketNonBlocking(SOCKET handle)
{
#ifdef PLATFORM_LINUX
    int nonBlocking = 1;
    if (fcntl(handle, F_SETFL, O_NONBLOCK, 1) == -1)
    {
        LogMessage("Failed to set non-blocking. %s.", strerror(errno));
        return false;
    }
#elif defined(PLATFORM_WINDOWS)
    DWORD nonBlocking = 1;
    if (ioctlsocket(handle, FIONBIO, &nonBlocking) != 0)
    {
        Win32_LogErrorMessage("Failed to set non-blocking.", WSAGetLastError());
        return false;
    }
#endif

    return true;
}

internal void CloseSocket(SOCKET handle)
{
#ifdef PLATFORM_WINDOWS
    closesocket(handle);
#elif defined(PLATFORM_LINUX)
    close(handle);
#endif
}

internal b32 SendPacket(SOCKET handle, Packet *packet, sockaddr_storage address)
{
    Assert(packet->length < sizeof(packet->data));

    u32 totalLength = packet->length + sizeof(PacketHeader);
    Assert(totalLength < MAX_PACKET_LENGTH);

    char buffer[MAX_PACKET_LENGTH];

    PacketHeader header = {};
    header.protocolId = PROTOCOL_ID;
    header.protocolVersion = PROTOCOL_VERSION;
    CopyMemory(buffer, &header, sizeof(header));

    CopyMemory(buffer + sizeof(header), packet->data, packet->length);

    i32 bytesSent = sendto(handle, (const char *)buffer, totalLength, 0,
        (sockaddr *)&address, sizeof(address));

    if (bytesSent != (i32)totalLength)
    {
#ifdef PLATFORM_WINDOWS
        Win32_LogErrorMessage("Failed to send packet data", WSAGetLastError());
#else
        LogMessage("Failed to send %u bytes of packet data. %s.", totalLength,
            strerror(errno));
#endif
        return false;
    }

    return true;
}

internal b32 ReceivePacket(SOCKET handle, Packet *packet, sockaddr_storage *from)
{
    // TODO: Maybe put an upper limit on this loop
    while (true)
    {
        socklen_t fromLength = sizeof(*from);
        u8 packetData[MAX_PACKET_LENGTH];
        i32 bytesReceived = recvfrom(handle, (char *)packetData,
            sizeof(packetData), 0, (sockaddr *)from, &fromLength);

        if (bytesReceived > 0)
        {
            // Should be able to assume that if we set the socket to a
            // particular address family we will only receive packets from
            // addresses that belong to that address family.
            Assert(from->ss_family == AF_INET || from->ss_family == AF_INET6);

            // Check packet header
            PacketHeader *header = (PacketHeader *)packetData;

            // Check that the packet conforms to our protocol
            // TODO: Report mis-match of protocol version to connecting client
            if ((header->protocolId == PROTOCOL_ID) &&
                (header->protocolVersion == PROTOCOL_VERSION) &&
                (bytesReceived > sizeof(PacketHeader)))
            {
                // Populate packet->data and packet->length
                packet->length = bytesReceived - sizeof(PacketHeader);
                CopyMemory(packet->data, packetData + sizeof(PacketHeader),
                    packet->length);
                return true;
            }
            // else: Packet is silently dropped (Probably not a good idea)
        }
        else if (bytesReceived < 0)
        {
#ifdef PLATFORM_WINDOWS
            if (bytesReceived == SOCKET_ERROR)
            {
                i32 error = WSAGetLastError();
                if (error == WSAEWOULDBLOCK)
                {
                    // No more data to receive
                    return false;
                }
                else if (error == WSAEINTR)
                {
                    // Interrupt occurred, this can occur during normal operation.
                    // Ignore and check for data again
                    Win32_LogErrorMessage("recvfrom interrupted", error);
                }
                else
                {
                    Win32_LogErrorMessage("recvfrom error", error);
                    return false;
                }
            }
#else
            if (errno == EAGAIN || errno == EWOULDBLOCK)
            {
                // No more data to receive
                return false;
            }
            else if (errno == EINTR)
            {
                // Interrupt occurred, this can occur during normal operation.
                // Ignore and check for data again
                LogMessage("recvfrom interrupted. %s", strerror(errno));
            }
            else
            {
                LogMessage("recvfrom error: %s.", strerror(errno));
                return false;
            }
#endif
        }
        else // (bytesReceived == 0)
        {
            // This could indicate that the connection is closed or that we
            // received a zero length packet, should be safe to ignore these as
            // we're not using a connection oriented protocol.
        }
    }

    InvalidCodePath();
    return false;
}


inline b32 CompareAddresses(
    struct sockaddr_storage *a, struct sockaddr_storage *b)
{
    if (a->ss_family == b->ss_family)
    {
        if (a->ss_family == AF_INET)
        {
            sockaddr_in *a4 = (sockaddr_in *)a;
            sockaddr_in *b4 = (sockaddr_in *)b;
            return ((a4->sin_addr.s_addr == b4->sin_addr.s_addr) &&
                    (a4->sin_port == b4->sin_port));
        }
        else if (a->ss_family == AF_INET6)
        {
            sockaddr_in6 *a6 = (sockaddr_in6 *)a;
            sockaddr_in6 *b6 = (sockaddr_in6 *)b;
            u32 *addr1 = (u32 *)a6->sin6_addr.s6_addr;
            u32 *addr2 = (u32 *)b6->sin6_addr.s6_addr;
            return ((addr1[0] == addr2[0]) && (addr1[1] == addr2[1]) &&
                    (addr1[2] == addr2[2]) && (addr1[3] == addr2[3]) &&
                    (a6->sin6_port == b6->sin6_port));
        }
        else
        {
            InvalidCodePath();
        }
    }
    return false;
}


internal b32 InitializeClientSocket(
    ClientSocketState *state, const char *address, u16 port)
{
    ClearToZero(state, sizeof(*state));

    state->serverAddress.ss_family = AF_INET;
    sockaddr_in *serverAddress = (sockaddr_in *)&state->serverAddress;

    serverAddress->sin_port = htons(port);
    inet_pton(AF_INET, address, &serverAddress->sin_addr);

    SOCKET handle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (handle <= 0)
    {
#ifdef PLATFORM_WINDOWS
        Win32_LogErrorMessage("Failed to create client socket", WSAGetLastError());
#else
        LogMessage("Failed to create client socket. %s.", strerror(errno));
#endif
        return false;
    }

    if (!SetSocketNonBlocking(handle))
    {
        CloseSocket(handle);
        return false;
    }

    state->handle = handle;
    return true;
}

internal b32 InitializeSeverSocket(ServerSocketState *state, u16 port)
{
    ClearToZero(state, sizeof(*state));

    SOCKET handle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (handle < 0)
    {
#ifdef PLATFORM_WINDOWS
        Win32_LogErrorMessage("Failed to create server socket", WSAGetLastError());
#else
        LogMessage("Failed to create server socket. %s.", strerror(errno));
#endif
        return false;
    }

    if (!SetSocketNonBlocking(handle))
    {
        CloseSocket(handle);
        return false;
    }

    sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);

    if (bind(handle, (const sockaddr *)&address, sizeof(address)) != 0)
    {
#ifdef PLATFORM_WINDOWS
        Win32_LogErrorMessage("Failed to bind server socket", WSAGetLastError());
#else
        LogMessage("Failed to bind server socket. %s.", strerror(errno));
#endif
        CloseSocket(handle);
        return false;
    }

    // Server socket is ready to use
    state->handle = handle;
    return true;
}

internal void DeinitializeServer(ServerSocketState *state)
{
    CloseSocket(state->handle);
}

internal void DeinitializeClient(ClientSocketState *state)
{
    CloseSocket(state->handle);
}

inline u32 FindClient(
    ServerSocketState *state, sockaddr_storage *receiveAddress)
{
    u32 idx = MAX_CLIENTS;
    for (u32 i = 0; i < MAX_CLIENTS; ++i)
    {
        if (state->clients[i].isConnected)
        {
            if (CompareAddresses(&state->clients[i].address, receiveAddress))
            {
                idx = i;
                break;
            }
        }
    }

    return idx;
}

inline u32 FindFreeClient(ServerSocketState *state)
{
    u32 idx = MAX_CLIENTS;
    for (u32 i = 0; i < MAX_CLIENTS; ++i)
    {
        if (!state->clients[i].isConnected)
        {
            idx = i;
            break;
        }
    }

    return idx;
}

internal void SendPacketToServer(ClientSocketState *state, Packet *packet)
{
    if (!SendPacket(state->handle, packet, state->serverAddress))
    {
        LogMessage("Client failed to send packet.");
    }
}

internal void SendPacketsToClients(
    ServerSocketState *state, Packet *packets, u32 count)
{
    for (u32 packetIndex = 0; packetIndex < count; packetIndex++)
    {
        Packet *packet = packets + packetIndex;
        u32 clientId = packet->clientId;

        Assert(clientId < MAX_CLIENTS);
        Assert(state->clients[clientId].isConnected);

        if (!SendPacket(
                state->handle, packet, state->clients[clientId].address))
        {
            LogMessage("Server failed to send packet for client %u.", clientId);
        }
    }
}

internal u32 ReceivePacketsFromServer(ClientSocketState *state, Packet *packets, u32 maxPackets)
{
    // Increment connection inactivity counter
    state->ticksSinceLastPacket++;

    u32 packetCount = 0;
    while (packetCount < maxPackets)
    {
        struct sockaddr_storage from;

        Assert(packetCount < maxPackets);
        Packet *packet = packets + packetCount;

        if (ReceivePacket(state->handle, packet, &from))
        {
            // Drop packets that are not from the server
            if (CompareAddresses(&state->serverAddress, &from))
            {
                // Accept packet
                packet->clientId = 0; // Ignore client id
                packetCount++;

                state->ticksSinceLastPacket = 0;
                if (!state->isConnected)
                {
                    state->isConnected = true;
                }
            }
            // else: To get this far the packet would have to be valid, this
            // could only really happen if a client tries to connect to another
            // client by mistake.
        }
        else
        {
            // Either an error occurred, or there is no more data to read
            break;
        }
    }

    if (state->ticksSinceLastPacket > INACTIVITY_THRESHOLD)
    {
        state->isConnected = false;
        LogMessage("Client lost connection to server.");
    }

    return packetCount;
}

internal u32 ServerReceivePackets(
    ServerSocketState *state, Packet *packets, u32 maxPackets,
    NetworkEvent *events, u32 maxEvents, u32 *eventCount)
{
    for (u32 clientId = 0; clientId < MAX_CLIENTS; ++clientId)
    {
        if (state->clients[clientId].isConnected)
        {
            state->clients[clientId].inactivityCounter++;
        }
    }

    *eventCount = 0;
    u32 packetCount = 0;
    while (packetCount < maxPackets)
    {
        Packet *packet = packets + packetCount;
        struct sockaddr_storage from;

        if (ReceivePacket(state->handle, packet, &from))
        {
            // Map client address to a clientId
            u32 clientId = FindClient(state, &from);
            if (clientId == MAX_CLIENTS)
            {
                // Try allocate new client
                clientId = FindFreeClient(state);
                if (clientId != MAX_CLIENTS)
                {
                    ClientEntry *client = state->clients + clientId;
                    client->address = from;
                    client->isConnected = true;

                    Assert(state->clientCount < MAX_CLIENTS);
                    state->clientCount++;

                    // Client connected event
                    Assert(*eventCount < maxEvents);
                    NetworkEvent *event = events + (*eventCount)++;
                    event->type = NetworkEvent_ClientConnected;
                    event->clientId = clientId;
                }
                else
                {
                    // Server is full, drop packet
                    LogMessage("Server is full, dropping packet");
                    continue;
                }

            }

            // Accept the packet
            packet->clientId = clientId;
            packetCount++;

            state->clients[clientId].inactivityCounter = 0;
        }
        else
        {
            // Either an error occurred, or there is no more data to read
            break;
        }
    }

    for (u32 clientId = 0; clientId < MAX_CLIENTS; ++clientId)
    {
        ClientEntry *client = state->clients + clientId;
        if (client->inactivityCounter > INACTIVITY_THRESHOLD)
        {
            LogMessage("Client %u disconnected", clientId);
            ClearToZero(client, sizeof(*client));

            Assert(state->clientCount > 0);
            state->clientCount--;

            // Client disconnected event
            Assert(*eventCount < maxEvents);
            NetworkEvent *event = events + (*eventCount)++;
            event->type = NetworkEvent_ClientDisconnected;
            event->clientId = clientId;
        }
    }

    return packetCount;
}
