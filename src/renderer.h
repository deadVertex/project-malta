#pragma once

#include "asset.h"

#pragma pack(push, 1)
struct Vertex
{
    vec3 position;
    vec3 normal;
    vec3 tangent;
    vec3 bitangent;
    vec2 textureCoordinates;
};

struct VertexPC
{
    vec3 position;
    f32 __unused;
    vec4 color;
};

struct VertexText
{
    vec2 position;
    vec2 textureCoordinates;
    vec4 color;
};
#pragma pack(pop)

#define MAX_DEBUG_DRAWING_VERTICES 4096
#define POSITION_COLOR_VERTEX_BUFFER_SIZE                                      \
    (sizeof(VertexPC) * MAX_DEBUG_DRAWING_VERTICES)

struct ShaderUniform
{
    u32 id;
    u32 type;
    const char *name;
    i32 location;
};

enum
{
    UniformType_Vec2,
    UniformType_Vec3,
    UniformType_Vec4,
    UniformType_Mat4,
    UniformType_Texture2D,
    UniformType_Float,
    UniformType_Buffer,
    UniformType_TextureCubeMap,
};

struct UniformValue
{
    u32 id;
    u32 type;
    union
    {
        vec2 v2;
        vec3 v3;
        vec4 v4;
        mat4 m4;
        u32 texture;
        f32 f;
        u32 buffer;
    };
};

enum
{
    FaceCulling_None,
    FaceCulling_BackFace,
    FaceCulling_FrontFace,
};

#define MAX_SHADER_UNIFORMS 16
struct Shader
{
    u32 program;
    ShaderUniform uniforms[MAX_SHADER_UNIFORMS];
    u32 uniformCount;
    b32 depthWriteEnabled;
    b32 depthTestEnabled;
    b32 alphaBlendingEnabled;
    u32 faceCullingMode;
    const char *name;
};

inline void PushUniform(Shader *shader, u32 id, const char *name, u32 type)
{
    Assert(shader->uniformCount < MAX_SHADER_UNIFORMS);
    u32 index = shader->uniformCount++;
    ShaderUniform *uniform = shader->uniforms + index;
    uniform->id = id;
    uniform->name = name;
    uniform->type = type;
}

enum
{
    ShaderType_Vertex,
    ShaderType_Fragment,
    ShaderType_Geometry,
    MAX_SHADER_TYPES,
};

enum
{
    ShaderStage_Vertex   = Bit(ShaderType_Vertex),
    ShaderStage_Fragment = Bit(ShaderType_Fragment),
    ShaderStage_Geometry = Bit(ShaderType_Geometry),
};


enum
{
    Shader_ColorShadeless,
    Shader_ColorDiffuse,
    Shader_Texture,
    Shader_PositionColor,
    Shader_ToneMapping,
    Shader_SkyGradient,
    Shader_TextureAlphaCutout,
    Shader_CubeMap,
    Shader_Wireframe,
    Shader_TextureShadeless,
    Shader_NormalMapping,
    Shader_Terrain,
    Shader_TerrainWireframe,
    Shader_TerrainNormals,
    Shader_TextureWithAlpha,
    Shader_TextureShadelessNoDepth,
    Shader_ColorShadelessNoDepth,
    Shader_Text,
    Shader_ColorDiffuseRoughnessTexture,
    Shader_ColorDiffuseMetallicTexture,
    Shader_ColorDiffuseInstanced,
    Shader_TextureAlphaCutoutInstanced,
    MAX_SHADERS,
};

enum
{
    ColorFormat_RGBA8,
    ColorFormat_SRGBA8,
    ColorFormat_RGBA32F,
    ColorFormat_R32F,
};

enum
{
    TextureSamplerMode_Nearest,
    TextureSamplerMode_Linear,
    TextureSamplerMode_LinearMipMapping,
};

enum
{
    Texture_Checkerboard,
    Texture_DevGrid512,
    Texture_HdrBuffer,
    Texture_DepthBuffer,
    Texture_ShadowMapDebug,
    Texture_ShadowMap,
    Texture_GrassAlpha,
    Texture_SkyboxClear,
    Texture_PavingStonesColor,
    Texture_PavingStonesNormal,
    Texture_HeightMap,
    Texture_TerrainNormalMap,
    Texture_Ground003,
    Texture_TreeBark,
    Texture_TreeLeaves,
    Texture_PistolAmmoIcon,
    Texture_PistolIcon,
    Texture_ShotgunIcon,
    Texture_FontInconsolataRegular32,
    Texture_Irradiance,
    Texture_BrdfLut,
    Texture_Radiance,
    MAX_TEXTURES,
};

struct VertexBuffer
{
    u32 vbo;
    u32 vao;
    u32 ibo;
    u32 vertexDataCapacity;
    u32 indexDataCapacity;
    u32 instanceDataCapacity;
    u32 instanceDataBuffer;
};

enum
{
    VertexLayout_Vertex,
    VertexLayout_VertexPC,
    VertexLayout_Text,
    VertexLayout_VertexInstanced,
    MAX_VERTEX_LAYOUTS,
};

enum
{
    VertexBuffer_Vertex,
    VertexBuffer_DebugData,
    VertexBuffer_Text,
    VertexBuffer_VertexInstanced,
    VertexBuffer_Grass,
    MAX_VERTEX_BUFFERS,
};

enum
{
    InstanceDataBuffer_Test,
    MAX_INSTANCE_DATA_BUFFERS,
};

struct Framebuffer
{
    u32 fbo;
    u32 width;
    u32 height;
    u32 colorTextureIndex;
    u32 depthTextureIndex;
};

enum
{
    Framebuffer_Backbuffer,
    Framebuffer_HdrBuffer,
    Framebuffer_ShadowMap,
    MAX_FRAMEBUFFERS,
};

struct UniformBuffer
{
    u32 ubo;
    u32 capacity;
};

enum
{
    UniformBuffer_LightingData,
    MAX_UNIFORM_BUFFERS,
};

enum
{
    RenderCommand_ClearTypeId,
    RenderCommand_DrawVertexBufferTypeId,
    RenderCommand_CreateTextureTypeId,
    RenderCommand_CreateShaderTypeId,
    RenderCommand_CreateVertexBufferTypeId,
    RenderCommand_UpdateVertexBufferTypeId,
    RenderCommand_BindFramebufferTypeId,
    RenderCommand_CreateFramebufferTypeId,
    RenderCommand_CreateUniformBufferTypeId,
    RenderCommand_UpdateUniformBufferTypeId,
    RenderCommand_CreateCubeMapTypeId,
    RenderCommand_CreateCubeMapMipMapsTypeId,
};

struct RenderCommand_Clear
{
    vec4 color;
};

enum
{
    Primitive_Triangles,
    Primitive_Lines,
};

enum
{
    TextureWrappingMode_Repeat,
    TextureWrappingMode_Clamp,
};

struct ShaderInput
{
    u32 shader;
    UniformValue values[MAX_SHADER_UNIFORMS];
    u32 valueCount;
};

struct RenderCommand_DrawVertexBuffer
{
    u32 id;
    u32 count;
    u32 first;
    u32 primitive;

    u32 instanceDataBuffer;
    u32 instanceCount;

    ShaderInput shaderInput;
};

struct RenderCommand_CreateTexture
{
    u32 id;
    u32 width;
    u32 height;
    u32 colorFormat;
    u32 samplerMode;
    u32 wrappingMode;
    void *pixels;
};

#define MAX_SHADER_DEFINES 8
struct RenderCommand_CreateShader
{
    u32 id;
    u32 stages;
    Shader shader;
    const char *source;
    const char *defines[MAX_SHADER_DEFINES];
    u32 defineCount;
};

struct RenderCommand_CreateVertexBuffer
{
    u32 id;
    u32 length;
    u32 indicesLength;
    b32 isDynamic;
    u32 vertexLayout;
    u32 instanceDataLength;
};

struct RenderCommand_UpdateVertexBuffer
{
    u32 id;
    u32 length;
    u32 indicesLength;
    u32 instanceDataLength;
    void *data;
    void *indexData;
    void *instanceData;
};

struct RenderCommand_BindFramebuffer
{
    u32 id;
};

struct RenderCommand_CreateFramebuffer
{
    u32 id;
    u32 colorTexture;
    u32 depthTexture;
    u32 width;
    u32 height;
};

struct RenderCommand_CreateUniformBuffer
{
    u32 id;
    u32 capacity;
};

struct RenderCommand_UpdateUniformBuffer
{
    u32 id;
    const void *data;
    u32 offset;
    u32 length;
};

struct RenderCommand_CreateCubeMap
{
    u32 id;
    const void *pixels[CUBE_MAP_FACE_COUNT];
    u32 width;
    u32 colorFormat;
};

struct CubeMapMipLevel
{
    const void *pixels[CUBE_MAP_FACE_COUNT];
    u32 width;
};

#define MAX_MIP_MAP_LEVELS 7

struct RenderCommand_CreateCubeMapMipMaps
{
    u32 id;
    u32 colorFormat;
    CubeMapMipLevel levels[MAX_MIP_MAP_LEVELS];
    u32 levelCount;
};

struct RenderCommandHeader
{
    u32 type;
};

struct RenderCommandQueue
{
    void *data;
    u32 capacity;
    u32 length;
};

inline void *RenderCommandQueuePush_(
    RenderCommandQueue *queue, u32 type, u32 size)
{
    u32 totalSize = sizeof(RenderCommandHeader) + size;
    Assert(queue->length + totalSize <= queue->capacity);

    RenderCommandHeader *header =
        (RenderCommandHeader *)((u8 *)queue->data + queue->length);
    header->type = type;

    queue->length += totalSize;

    void *result = header + 1;
    return result;
}

#define RenderCommandQueuePush(QUEUE, TYPE)                                    \
    (TYPE *)RenderCommandQueuePush_(QUEUE, TYPE##TypeId, sizeof(TYPE))

inline void PushUniformVec3(ShaderInput *input, u32 id, vec3 value)
{
    Assert(input->valueCount < MAX_SHADER_UNIFORMS);
    u32 index = input->valueCount++;
    input->values[index].id = id;
    input->values[index].type = UniformType_Vec3;
    input->values[index].v3 = value;
}

inline void PushUniformVec2(ShaderInput *input, u32 id, vec2 value)
{
    Assert(input->valueCount < MAX_SHADER_UNIFORMS);
    u32 index = input->valueCount++;
    input->values[index].id = id;
    input->values[index].type = UniformType_Vec2;
    input->values[index].v2 = value;
}

inline void PushUniformVec4(ShaderInput *input, u32 id, vec4 value)
{
    Assert(input->valueCount < MAX_SHADER_UNIFORMS);
    u32 index = input->valueCount++;
    input->values[index].id = id;
    input->values[index].type = UniformType_Vec4;
    input->values[index].v4 = value;
}

inline void PushUniformMat4(ShaderInput *input, u32 id, mat4 value)
{
    Assert(input->valueCount < MAX_SHADER_UNIFORMS);
    u32 index = input->valueCount++;
    input->values[index].id = id;
    input->values[index].type = UniformType_Mat4;
    input->values[index].m4 = value;
}

inline void PushUniformTexture2D(ShaderInput *input, u32 id, u32 value)
{
    Assert(input->valueCount < MAX_SHADER_UNIFORMS);
    u32 index = input->valueCount++;
    input->values[index].id = id;
    input->values[index].type = UniformType_Texture2D;
    input->values[index].texture = value;
}

inline void PushUniformTextureCubeMap(ShaderInput *input, u32 id, u32 value)
{
    Assert(input->valueCount < MAX_SHADER_UNIFORMS);
    u32 index = input->valueCount++;
    input->values[index].id = id;
    input->values[index].type = UniformType_TextureCubeMap;
    input->values[index].texture = value;
}

inline void PushUniformFloat(ShaderInput *input, u32 id, f32 value)
{
    Assert(input->valueCount < MAX_SHADER_UNIFORMS);
    u32 index = input->valueCount++;
    input->values[index].id = id;
    input->values[index].type = UniformType_Float;
    input->values[index].f = value;
}

inline void PushUniformBuffer(ShaderInput *input, u32 id, u32 value)
{
    Assert(input->valueCount < MAX_SHADER_UNIFORMS);
    u32 index = input->valueCount++;
    input->values[index].id = id;
    input->values[index].type = UniformType_Buffer;
    input->values[index].buffer = value;
}

inline UniformValue* FindUniformValue(ShaderInput *input, u32 id)
{
    UniformValue *result = NULL;
    for (u32 i = 0; i < input->valueCount; ++i)
    {
        if (input->values[i].id == id)
        {
            result = input->values + i;
            break;
        }
    }

    return result;
}
