// NOTE: Assuming triangles
internal void CalculateTangentsAndBitangents(
    Vertex *vertices, u32 vertexCount, u32 *indices, u32 indicesCount)
{
    Assert(indicesCount % 3 == 0);
    for (u32 triangleIndex = 0; triangleIndex < indicesCount / 3;
         ++triangleIndex)
    {
        u32 i = indices[triangleIndex * 3];
        u32 j = indices[triangleIndex * 3 + 1];
        u32 k = indices[triangleIndex * 3 + 2];

        Vertex *a = vertices + i;
        Vertex *b = vertices + j;
        Vertex *c = vertices + k;

        vec3 deltaP1 = b->position - a->position;
        vec3 deltaP2 = c->position - a->position;

        vec2 deltaUV1 = b->textureCoordinates - a->textureCoordinates;
        vec2 deltaUV2 = c->textureCoordinates - a->textureCoordinates;

        // Calculate inverse of 2x2 matrix
        f32 r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
        vec3 tangent = (deltaP1 * deltaUV2.y - deltaP2 * deltaUV1.y) * r;

        a->tangent += tangent;
        b->tangent += tangent;
        c->tangent += tangent;
    }

    // Normalize tangent vectors
    for (u32 vertexIdx = 0; vertexIdx < vertexCount; ++vertexIdx)
    {
        Vertex *v = vertices + vertexIdx;
        v->tangent = Normalize(v->tangent);
    }

    // Calculate bitangent vectors
    for (u32 vertexIdx = 0; vertexIdx < vertexCount; ++vertexIdx)
    {
        Vertex *v = vertices + vertexIdx;
        v->bitangent = Normalize(Cross(v->normal, v->tangent));
    }
}

u32 *CreateCheckerboardTexture(MemoryArena *transArena, u32 width, u32 height,
    u32 tileWidth, vec4 evenColor, vec4 oddColor)
{
    u32 *pixels = AllocateArray(transArena, u32, width * height);

    u32 even = ToColor(evenColor);
    u32 odd = ToColor(oddColor);

    for (u32 y = 0; y < height; ++y)
    {
        for (u32 x = 0; x < width; ++x)
        {
            u32 index = x + y * width;
            u32 value = (x/tileWidth) + (y/tileWidth);
            pixels[index] = (value % 2) ? odd : even;
        }
    }

    return pixels;
}

struct TriangleFace
{
    u32 i, j, k;
};

inline Vertex CreateUnitVertex(f32 x, f32 y, f32 z)
{
    Vertex result = {};
    result.position = Normalize(Vec3(x, y, z));
    result.normal = result.position;
    result.textureCoordinates = Vec2(result.position.x, result.position.y);
    return result;
}

struct MeshData
{
    Vertex *vertices;
    u32 *indices;
    u32 vertexCount;
    u32 indexCount;
};

internal MeshData CreateIcosahedronMesh(u32 tesselationLevel, MemoryArena *arena)
{
    MeshData result = {};
    // TODO: Actually calculate how many vertices and indices
    u32 maxVertices = 2048;
    u32 maxIndices = 4096;
    result.vertices = AllocateArray(arena, Vertex, maxVertices);
    result.indices = AllocateArray(arena, u32, maxIndices);

    f32 t = (1.0f + Sqrt(5.0f)) * 0.5f;

    result.vertices[0] = CreateUnitVertex(-1, t, 0);
    result.vertices[1] = CreateUnitVertex(1, t, 0);
    result.vertices[2] = CreateUnitVertex(-1, -t, 0);
    result.vertices[3] = CreateUnitVertex(1, -t, 0);

    result.vertices[4] = CreateUnitVertex(0, -1, t);
    result.vertices[5] = CreateUnitVertex(0, 1, t);
    result.vertices[6] = CreateUnitVertex(0, -1, -t);
    result.vertices[7] = CreateUnitVertex(0, 1, -t);

    result.vertices[8] = CreateUnitVertex(t, 0, -1);
    result.vertices[9] = CreateUnitVertex(t, 0, 1);
    result.vertices[10] = CreateUnitVertex(-t, 0, -1);
    result.vertices[11] = CreateUnitVertex(-t, 0, 1);

    u32 vertexCount = 12;

    u32 initialFaceCount = 20;
    TriangleFace *initialFaces =
        AllocateArray(arena, TriangleFace, initialFaceCount);

    initialFaces[0] = TriangleFace{0, 11, 5};
    initialFaces[1] = TriangleFace{0, 5, 1};
    initialFaces[2] = TriangleFace{0, 1, 7};
    initialFaces[3] = TriangleFace{0, 7, 10};
    initialFaces[4] = TriangleFace{0, 10, 11};

    initialFaces[5] = TriangleFace{1, 5, 9};
    initialFaces[6] = TriangleFace{5, 11, 4};
    initialFaces[7] = TriangleFace{11, 10, 2};
    initialFaces[8] = TriangleFace{10, 7, 6};
    initialFaces[9] = TriangleFace{7, 1, 8};

    initialFaces[10] = TriangleFace{3, 9, 4};
    initialFaces[11] = TriangleFace{3, 4, 2};
    initialFaces[12] = TriangleFace{3, 2, 6};
    initialFaces[13] = TriangleFace{3, 6, 8};
    initialFaces[14] = TriangleFace{3, 8, 9};

    initialFaces[15] = TriangleFace{4, 9, 5};
    initialFaces[16] = TriangleFace{2, 4, 11};
    initialFaces[17] = TriangleFace{6, 2, 10};
    initialFaces[18] = TriangleFace{8, 6, 7};
    initialFaces[19] = TriangleFace{9, 8, 1};

    u32 currentFaceCount = initialFaceCount;
    TriangleFace *currentFaces = initialFaces;
    for (u32 i = 0; i < tesselationLevel; ++i)
    {
        TriangleFace *newFaces =
            AllocateArray(arena, TriangleFace, currentFaceCount * 4);
        u32 newFaceCount = 0;

        for (size_t j = 0; j < currentFaceCount; ++j)
        {
            TriangleFace currentFace = currentFaces[j];

            // 3 edges in total comprised of 2 indices each
            u32 edges[6] = {currentFace.i, currentFace.j, currentFace.j,
                currentFace.k, currentFace.k, currentFace.i};

            u32 indicesAdded[3];

            for (u32 k = 0; k < 3; ++k)
            {
                // Retrieve vertices for each edge
                u32 idx0 = edges[k * 2];
                u32 idx1 = edges[k * 2 + 1];
                Vertex v0 = result.vertices[idx0];
                Vertex v1 = result.vertices[idx1];

                // Add mid point vertex
                vec3 u = v0.position + v1.position;

                Assert(vertexCount < maxVertices);
                result.vertices[vertexCount] = CreateUnitVertex(u.x, u.y, u.z);

                indicesAdded[k] = vertexCount++;
            }

            newFaces[newFaceCount++] =
                TriangleFace{currentFace.i, indicesAdded[0], indicesAdded[2]};
            newFaces[newFaceCount++] =
                TriangleFace{currentFace.j, indicesAdded[1], indicesAdded[0]};
            newFaces[newFaceCount++] =
                TriangleFace{currentFace.k, indicesAdded[2], indicesAdded[1]};
            newFaces[newFaceCount++] =
                TriangleFace{indicesAdded[0], indicesAdded[1], indicesAdded[2]};
        }

        currentFaces = newFaces;
        currentFaceCount = newFaceCount;
    }

    Assert(currentFaceCount * 3 < maxIndices);
    CopyMemory(result.indices, currentFaces, currentFaceCount * sizeof(TriangleFace));

    // Also frees the allocated faces for all tesselation levels
    //MemoryArenaFree(arena, initialFaces);

    result.vertexCount = vertexCount;
    result.indexCount = currentFaceCount * 3;

    //CalculateTangents(result);

    return result;
}

internal MeshData CreatePatchMesh(u32 gridDim, MemoryArena *arena)
{
    MeshData result = {};

    u32 vertDim = gridDim + 1;
    u32 totalVertices = vertDim * vertDim;
    Vertex *vertices = AllocateArray(arena, Vertex, totalVertices);

    u32 totalIndices = gridDim * gridDim * 2 * 3;
    u32 *indices = AllocateArray(arena, u32, totalIndices);

    for (u32 y = 0; y < vertDim; ++y)
    {
        for (u32 x = 0; x < vertDim; ++x)
        {
            float fx = x / (float)gridDim;
            float fy = y / (float)gridDim;

            Vertex *vertex = &vertices[y * vertDim + x];
            // Map to -0.5 to 0.5 range
            vertex->position = Vec3(fx - 0.5f, 0.0f, fy - 0.5f);
            vertex->normal = Vec3(0.0f, 1.0f, 0.0f);
            vertex->textureCoordinates = Vec2(fx, fy);
        }
    }

    u32 index = 0;
    for (u32 y = 0; y < gridDim; y++)
    {
        for (u32 x = 0; x < gridDim; x++)
        {
            indices[index++] = (x + vertDim * y);
            indices[index++] = ((x + 1) + vertDim * y);
            indices[index++] = (x + vertDim * (y + 1));

            indices[index++] = ((x + 1) + vertDim * y);
            indices[index++] = ((x + 1) + vertDim * (y + 1));
            indices[index++] = (x + vertDim * (y + 1));
        }
    }

    result.vertices = vertices;
    result.indices = indices;
    result.vertexCount = totalVertices;
    result.indexCount = totalIndices;

    //CalculateTangents(result);

    return result;
}

