const char *positionColorSource = R"glsl(
#ifdef VERTEX_SHADER

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec4 vertexColor;

out vec4 color;

uniform mat4 mvp;

void main()
{
    color = vertexColor;
    gl_Position = mvp * vec4(vertexPosition, 1.0);
}
#endif // VERTEX_SHADER

#ifdef FRAGMENT_SHADER
in vec4 color;
out vec4 fragmentColor;

void main()
{
    fragmentColor = color;
}
#endif // FRAGMENT_SHADER
)glsl";

const char *textShaderSource = R"glsl(
#ifdef VERTEX_SHADER

layout(location = 0) in vec2 vertexPosition;
layout(location = 1) in vec4 vertexColor;
layout(location = 4) in vec2 vertexTextureCoordinates;

out vec4 color;
out vec2 textureCoordinates;

uniform mat4 mvp;

void main()
{
    color = vertexColor;
    textureCoordinates = vertexTextureCoordinates;
    gl_Position = mvp * vec4(vertexPosition.x, vertexPosition.y, 0.0, 1.0);
}
#endif // VERTEX_SHADER

#ifdef FRAGMENT_SHADER
in vec4 color;
in vec2 textureCoordinates;

uniform sampler2D glyphSheet;

out vec4 fragmentColor;

void main()
{
    float alpha = texture(glyphSheet, textureCoordinates).r;
    fragmentColor = color;
    fragmentColor.a *= alpha;
}
#endif // FRAGMENT_SHADER
)glsl";
/*
*/
