#ifdef VERTEX_SHADER

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec3 vertexTangent;
layout(location = 3) in vec3 vertexBitangent;
layout(location = 4) in vec2 vertexTextureCoordinates;
#ifdef USE_INSTANCING
layout(location = 5) in mat4 modelMatrix;
#endif // USE_INSTANCING

out VS_OUT
{
    vec2 vertexTextureCoordinates;
    vec2 textureCoordinates;
    vec3 normal;
    vec4 fragPosInLightSpace;
    vec3 fragPosInWorldSpace;
    vec3 localPos;
    vec3 transformNormal;
#ifdef USE_NORMAL_MAPPING
    mat3 TBN;
#endif // USE_NORMAL_MAPPING
} vs_out;

uniform mat4 viewProjectionMatrix;
uniform mat4 shadowViewProjectionMatrix;

#if !defined(USE_INSTANCING)
uniform mat4 modelMatrix;
#endif // !defined(USE_INSTANCING)

// Only used for normal visualization, which I don't think even works properly
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;


uniform vec2 textureScale = vec2(1, 1);

#ifdef USE_HEIGHT_MAP
uniform sampler2D heightMap;
uniform vec2 uvScale = vec2(1, 1);
uniform vec2 uvOffset = vec2(0, 0);
#endif

int CalculateMostSignificantAxis(vec3 v)
{
    int result = 0;
    result = (abs(v[1]) > abs(v[result])) ? 1 : result;
    result = (abs(v[2]) > abs(v[result])) ? 2 : result;
    return result;
}

vec2 CalculateWorldTextureCoordinates(vec3 worldPosition, vec3 normal)
{
    vec3 p = worldPosition;

    int axis = CalculateMostSignificantAxis(normal);
    if (axis == 0)
    {
        return vec2(-p.z * sign(normal.x), p.y);
    }
    else if (axis == 1)
    {
        return vec2(-p.x, p.z * sign(normal.y));
    }
    else if (axis == 2)
    {
        return vec2(p.x * sign(normal.z), p.y);
    }
    return vec2(0);
}

void main()
{
    vec3 position = vertexPosition;
    vec3 normal = vertexNormal;
#ifdef USE_HEIGHT_MAP
    vec2 uv = vertexTextureCoordinates * uvScale + uvOffset;
    float heightSample = texture(heightMap, uv).x;
    position.y = position.y + heightSample;
    vs_out.vertexTextureCoordinates = uv;
#else
    vs_out.vertexTextureCoordinates = vertexTextureCoordinates;
#endif
    vs_out.localPos = position;

    // FIXME: Calculate the normal matrix on the CPU
    vs_out.normal = mat3(transpose(inverse(modelMatrix))) * normal;

    mat4 shadowMvp = shadowViewProjectionMatrix * modelMatrix;
    vs_out.fragPosInLightSpace = shadowMvp * vec4(position, 1.0);
    vs_out.fragPosInWorldSpace = vec3(modelMatrix * vec4(position, 1.0));

#ifdef USE_WORLD_SPACE_TEXTURE_COORDINATES
    vs_out.textureCoordinates = CalculateWorldTextureCoordinates(
        vs_out.fragPosInWorldSpace, vs_out.normal) * textureScale.x;
#else
    vs_out.textureCoordinates = vertexTextureCoordinates * textureScale;
#endif // USE_WORLD_SPACE_TEXTURE_COORDINATES

    mat3 normalMatrix = mat3(transpose(inverse(viewMatrix * modelMatrix)));
    vs_out.transformNormal = normalize(vec3(projectionMatrix * vec4(normalMatrix * normal, 0.0)));

    mat4 mvp = viewProjectionMatrix * modelMatrix;
    gl_Position = mvp * vec4(position, 1.0);

#ifdef USE_NORMAL_MAPPING
    vec3 T = normalize(vec3(modelMatrix * vec4(vertexTangent,   0.0)));
    vec3 B = normalize(vec3(modelMatrix * vec4(vertexBitangent, 0.0)));
    vec3 N = normalize(vec3(modelMatrix * vec4(vertexNormal,    0.0)));
    vs_out.TBN = mat3(T, B, N);
#endif // USE_NORMAL_MAPPING
}
#endif // VERTEX_SHADER

#ifdef GEOMETRY_SHADER
layout(triangles) in;

#ifdef DRAW_WIREFRAME
layout(line_strip, max_vertices = 4) out;
#endif

#ifdef DRAW_NORMALS
layout(line_strip, max_vertices = 6) out;
#endif

// FIXME: Don't duplicate this manually across all shader stages!
in VS_OUT
{
    vec2 vertexTextureCoordinates;
    vec2 textureCoordinates;
    vec3 normal;
    vec4 fragPosInLightSpace;
    vec3 fragPosInWorldSpace;
    vec3 localPos;
    vec3 transformNormal;
#ifdef USE_NORMAL_MAPPING
    mat3 TBN;
#endif // USE_NORMAL_MAPPING
} gs_in[];

void main()
{
#ifdef DRAW_WIREFRAME
    gl_Position = gl_in[0].gl_Position;
    EmitVertex();

    gl_Position = gl_in[1].gl_Position;
    EmitVertex();

    gl_Position = gl_in[2].gl_Position;
    EmitVertex();

    gl_Position = gl_in[0].gl_Position;
    EmitVertex();
    EndPrimitive();
#endif // DRAW_WIREFRAME

#ifdef DRAW_NORMALS
    float magnitude = 0.4;

    gl_Position = gl_in[0].gl_Position;
    EmitVertex();

    gl_Position = gl_in[0].gl_Position + vec4(gs_in[0].transformNormal, 0.0) * magnitude;
    EmitVertex();
    EndPrimitive();

    gl_Position = gl_in[1].gl_Position;
    EmitVertex();

    gl_Position = gl_in[1].gl_Position + vec4(gs_in[1].transformNormal, 0.0) * magnitude;
    EmitVertex();
    EndPrimitive();

    gl_Position = gl_in[2].gl_Position;
    EmitVertex();

    gl_Position = gl_in[2].gl_Position + vec4(gs_in[2].transformNormal, 0.0) * magnitude;
    EmitVertex();
    EndPrimitive();
#endif

}
#endif // GEOMETRY_SHADER

#ifdef FRAGMENT_SHADER

out vec4 fragmentColor;

in VS_OUT
{
    vec2 vertexTextureCoordinates;
    vec2 textureCoordinates;
    vec3 normal;
    vec4 fragPosInLightSpace;
    vec3 fragPosInWorldSpace;
    vec3 localPos;
    vec3 transformNormal;
#ifdef USE_NORMAL_MAPPING
    mat3 TBN;
#endif // USE_NORMAL_MAPPING
} fs_in;

uniform vec4 uniformColor;
uniform sampler2D albedoTexture;
uniform sampler2D shadowMap;
uniform sampler2D alphaTexture;
uniform samplerCube cubeMap;
uniform sampler2D normalMap;
uniform samplerCube irradianceCubeMap;
uniform samplerCube radianceCubeMap;
uniform sampler2D brdfLut;

#ifdef USE_ROUGHNESS_TEXTURE
uniform sampler2D roughnessTexture;
#else
uniform float roughness = 0.9;
#endif // USE_ROUGHNESS_TEXTURE

#ifdef USE_METALLIC_TEXTURE
uniform sampler2D metallicTexture;
#else
uniform float metallic = 0.0;
#endif // USE_METALLIC_TEXTURE

#ifdef USE_HEIGHT_MAP
uniform sampler2D heightMap;
uniform sampler2D terrainNormalMap;
#endif

#define PI 3.14159265359

// NOTE: Must match what is defined in game.h
#define MAX_POINT_LIGHTS 16
#define MAX_SPOT_LIGHTS 16

// NOTE: Must be std140 layout
struct PointLight
{
    vec4 position;
    vec4 color;
    vec4 attenuation;
};

// TODO: Optimize layout
struct SpotLight
{
    vec4 position;
    vec4 direction;
    vec4 color;
    vec4 attenuation[2];
};

layout(std140) uniform LightingDataUniformBuffer
{
    vec4 sunDirection;
    vec4 cameraPosition;
    vec4 sunColor;
    vec4 ambientColor;
    PointLight pointLights[MAX_POINT_LIGHTS];
    SpotLight spotLights[MAX_SPOT_LIGHTS];
    uint pointLightCount;
    uint spotLightCount;
} lightingData;

#define CALCULATE_SHADOW

float CalculateShadow()
{
    // Perform perspective divide
    vec3 projCoords = fs_in.fragPosInLightSpace.xyz / fs_in.fragPosInLightSpace.w;

    // map to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;

    float closestDepth = texture(shadowMap, projCoords.xy).r;
    float currentDepth = projCoords.z;

    float bias = 0.005;
    float shadow = currentDepth - bias > closestDepth ? 1.0 : 0.0;

    if (currentDepth > 1.0)
    {
        shadow = 0.0;
    }

    return shadow;
}

float PointLightWindowingFunction(float r, float rmax)
{
    float a = max(1 - pow(r / rmax, 4), 0);
    float result = a * a;
    return result;
}

float PointLightDistanceFunction(float r, float r0, float rmin, float rmax)
{
    float t = r0 / max(r, rmin);
    float t2 = t*t;
    return t2 * PointLightWindowingFunction(r, rmax);
}

float SpotLightDirectionFunction(vec3 D, vec3 L, float inner, float outer)
{
    float epsilon = inner - outer;
    float theta = dot(L, -D);
    float t = clamp((theta - outer) / epsilon, 0.0, 1.0);
    return t * t;
}

vec3 FresnelSchlick(float cosTheta, vec3 F0, float roughness)
{
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}

float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a = roughness * roughness;
    float a2 = a * a;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH * NdotH;

    float num = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return num / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r * r) / 8.0;

    float num = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return num / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(L, L), 0.0);
    float ggx2 = GeometrySchlickGGX(NdotV, roughness);
    float ggx1 = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

vec3 CookTorranceBrdf(vec3 surfaceNormal, vec3 lightDir, vec3 viewDir, vec3 lightColor, vec3 albedoColor,
        float surfaceRoughness, float surfaceMetallic)
{
    float NdotL = max(dot(surfaceNormal, lightDir), 0.0);
    vec3 H = normalize(viewDir + lightDir);
    vec3 radiance = lightColor;

    vec3 F0 = vec3(0.04); // default for dielectric materials
    F0 = mix(F0, albedoColor, surfaceMetallic);
    vec3 F = FresnelSchlick(max(dot(H, viewDir), 0.0), F0, surfaceRoughness);
    float NDF = DistributionGGX(surfaceNormal, H, surfaceRoughness);
    float G = GeometrySmith(surfaceNormal, viewDir, lightDir, surfaceRoughness);

    // Cook-Torrance BRDF
    float NdotV = max(dot(surfaceNormal, viewDir), 0.0);
    vec3 num = NDF * G * F;
    float denom = 4.0 * NdotV * NdotL;
    vec3 specular = num / max(denom, 0.0001);

    vec3 kS = F;
    vec3 kD = vec3(1.0) - kS;
    kD *= 1.0 - surfaceMetallic;

    return (kD * albedoColor / PI + specular) * radiance * NdotL;
}

vec3 CalculateDirectionalLight(
        vec3 lightDir, // Vector from light to the surface
        vec3 lightColor,
        vec3 viewDir,
        vec3 surfaceColor,
        vec3 surfaceNormal,
        float surfaceRoughness,
        float surfaceMetallic
        )
{
    return CookTorranceBrdf(surfaceNormal, lightDir, viewDir, lightColor,
        surfaceColor, surfaceRoughness, surfaceMetallic);
}

#if 0
vec3 CalculatePointLight(
        vec3 lightPosition,
        vec3 lightColor,
        float r0,               // Distance until attenuation occurs
        float rmin,             // Light emitter physical radius
        float rmax,             // Maximum range of the light
        vec3 viewDir,
        vec3 surfacePosition,
        vec3 surfaceNormal,
        vec3 surfaceColor,
        float shininess,
        float specularIntensity
        )
{
    vec3 lightDir = normalize(lightPosition - surfacePosition);
    float r = length(lightPosition - surfacePosition);
    vec3 H = normalize(lightDir + viewDir);

    float attenuation = PointLightDistanceFunction(r, r0, rmin, rmax);
    vec3 diffuse = max(dot(surfaceNormal, lightDir), 0.0) * lightColor * attenuation * surfaceColor;
    vec3 specular = pow(max(dot(surfaceNormal, H), 0.0), shininess) * lightColor * attenuation * specularIntensity;

    return diffuse + specular;
}

vec3 CalculateSpotLight(
        vec3 lightPosition,
        vec3 lightColor,
        vec3 lightDirection,    // Spot light forward direction
        float r0,               // Distance until attenuation occurs
        float rmin,             // Light emitter physical radius
        float rmax,             // Maximum range of the light
        float innerCosine,
        float outerCosine,
        vec3 viewDir,
        vec3 surfacePosition,
        vec3 surfaceNormal,
        vec3 surfaceColor,
        float shininess,
        float specularIntensity
        )
{
    vec3 lightDir = normalize(lightPosition - surfacePosition);
    float r = length(lightPosition - surfacePosition);
    vec3 H = normalize(lightDir + viewDir);

    float attenuation = SpotLightDirectionFunction(lightDirection,
            lightDir,
            innerCosine,
            outerCosine)
        * PointLightDistanceFunction(r, r0, rmin, rmax);

    vec3 diffuse = max(dot(surfaceNormal, lightDir), 0.0) * surfaceColor * attenuation * lightColor;
    vec3 specular = pow(max(dot(surfaceNormal, H), 0.0), shininess) * lightColor * attenuation * specularIntensity;

    return diffuse + specular;
}
#endif


vec3 CalculateIrradiance(vec3 surfaceNormal,
                         vec3 surfaceColor,
                         vec3 surfacePosition,
                         float surfaceRoughness,
                         float surfaceMetallic)
{
    vec3 cameraPosition = lightingData.cameraPosition.xyz;
    vec3 sunDirection = lightingData.sunDirection.xyz;
    vec3 sunColor = lightingData.sunColor.rgb;
    vec3 ambientColor = texture(irradianceCubeMap, surfaceNormal).rgb;

    float ambientStrength = 1.0;
    ambientColor *= ambientStrength;

    vec3 viewDir = normalize(cameraPosition - surfacePosition);
    vec3 reflectDir = reflect(-viewDir, surfaceNormal);

    const float MAX_REFLECTION_LOD = 6.0;
    vec3 prefilteredColor = textureLod(radianceCubeMap, reflectDir, surfaceRoughness * MAX_REFLECTION_LOD).rgb;
    vec2 envBrdf = texture(brdfLut, vec2(max(dot(surfaceNormal, viewDir), 0.0), surfaceRoughness)).rg;

    vec3 irradiance = vec3(0, 0, 0);
    
    // Ambient light
    vec3 F0 = vec3(0.04); // default for dielectric materials
    F0 = mix(F0, surfaceColor, surfaceMetallic);
    vec3 F = FresnelSchlick(max(dot(surfaceNormal, viewDir), 0.0), F0, surfaceRoughness);
    vec3 kS = F;
    vec3 kD = 1.0 - kS;
    kD *= 1.0 - surfaceMetallic;
    vec3 specular = prefilteredColor * (F * envBrdf.x + envBrdf.y);

    irradiance += ambientColor * surfaceColor * kD;
    irradiance += specular;

    // Directional light
    float shadow = 0.0;
#ifdef CALCULATE_SHADOW
    shadow = CalculateShadow();
#endif // CALCULATE_SHADOW

#if 1
    irradiance += CalculateDirectionalLight(sunDirection,
                                            sunColor,
                                            viewDir,
                                            surfaceColor,
                                            surfaceNormal,
                                            surfaceRoughness,
                                            surfaceMetallic) * (1.0 - shadow);
#endif

#if 0
    // Point lights
    for (uint i = 0u; i < lightingData.pointLightCount; ++i)
    {
        PointLight pointLight = lightingData.pointLights[i];
        irradiance += CalculatePointLight(pointLight.position.xyz,
                                          pointLight.color.rgb,
                                          pointLight.attenuation.x,
                                          pointLight.attenuation.y,
                                          pointLight.attenuation.z,
                                          viewDir,
                                          surfacePosition,
                                          surfaceNormal,
                                          surfaceColor,
                                          shininess,
                                          specularIntensity);
    }


    // Spot lights
    for (uint i = 0u; i < lightingData.spotLightCount; ++i)
    {
        SpotLight spotLight = lightingData.spotLights[i];
        irradiance += CalculateSpotLight(spotLight.position.xyz,
                                         spotLight.color.rgb,
                                         spotLight.direction.xyz,
                                         spotLight.attenuation[0].x,
                                         spotLight.attenuation[0].y,
                                         spotLight.attenuation[0].z,
                                         spotLight.attenuation[1].x,
                                         spotLight.attenuation[1].y,
                                         viewDir,
                                         surfacePosition,
                                         surfaceNormal,
                                         surfaceColor,
                                         shininess,
                                         specularIntensity);
    }
#endif

    return irradiance;
}

void main()
{
    float alpha = 1.0;
    vec3 baseColor = vec3(0, 0, 0);
    vec3 finalColor = vec3(0 , 0, 0);

#ifdef ALPHA_CUTOUT_TEXTURE
    if (texture(alphaTexture, fs_in.textureCoordinates).r < 0.2f)
    {
        discard;
    }
#endif

    vec3 N = normalize(fs_in.normal);
#ifdef USE_TEXTURE
    // Assuming textures are in sRGB space, convert to linear
#ifdef USE_TEXTURE_ALPHA
    vec4 albedoWithAlpha = texture(albedoTexture, fs_in.textureCoordinates).rgba;
#ifdef USE_ALPHA_BLENDING
    alpha = albedoWithAlpha.a;
#else
    if (albedoWithAlpha.a < 0.2f)
    {
        discard;
    }
#endif
    baseColor = albedoWithAlpha.rgb;
#else
    baseColor = texture(albedoTexture, fs_in.textureCoordinates).rgb;
#endif
    // TODO: Test that uses specular texture!
    //spec = specularIntensity * texture(specularTexture, fs_in.textureCoordinates).r;
#else
    // uniformColor is assumed to be in linear space
    baseColor = uniformColor.rgb;
#ifdef USE_ALPHA_BLENDING
    alpha = uniformColor.a;
#endif
#endif // USE_TEXTURE

#ifdef CALCULATE_LIGHTING
#ifdef USE_HEIGHT_MAP
    N = texture(terrainNormalMap, fs_in.vertexTextureCoordinates).rgb;
    N = normalize(N * 2.0 - 1.0);
    N = normalize(N.xzy);
    //baseColor = vec3(0.18, 0.18, 0.18);
    // TODO: TBN matrix?
#endif // USE_HEIGHT_MAP


#ifdef USE_NORMAL_MAPPING
    N = texture(normalMap, fs_in.textureCoordinates).rgb;
    N = normalize(N * 2.0 - 1.0);
    N = normalize(fs_in.TBN * N);
#endif // USE_NORMAL_MAPPING

#ifdef USE_ROUGHNESS_TEXTURE
    float roughness = texture(roughnessTexture, fs_in.textureCoordinates).r;
#endif

#ifdef USE_METALLIC_TEXTURE
    float metallic = texture(metallicTexture, fs_in.textureCoordinates).r;
#endif

    finalColor = CalculateIrradiance(N, baseColor, fs_in.fragPosInWorldSpace, roughness, metallic);
#else
    finalColor = baseColor;
#endif // CALCULATE_LIGHTING

#ifdef USE_SKY_GRADIENT
    vec3 high = pow(vec3(0.26, 0.45, 0.73), vec3(2.2));
    vec3 low = pow(vec3(0.48, 0.67, 0.86), vec3(2.2));
    vec3 sunDirection = lightingData.sunDirection.xyz;
    vec3 sunColor = lightingData.sunColor.xyz;
    float sun = max(dot(N, sunDirection), 0.0);
    float t = N.y * (2.0 - N.y);
    finalColor = mix(low, high, smoothstep(0, 1, t));
    finalColor = mix(finalColor, mix(low, sunColor, 0.2), 0.8 * sun * sun); 
#endif // USE_SKY_GRADIENT

#ifdef USE_CUBE_MAP
    //finalColor = textureLod(cubeMap, fs_in.localPos, 5).rgb;
    finalColor = texture(cubeMap, fs_in.localPos).rgb;
#endif

#ifdef PERFORM_TONE_MAPPING
    // Perform gamma correction
    vec3 hdrColor = texture(albedoTexture, fs_in.textureCoordinates).rgb;
    finalColor = pow(hdrColor, vec3(1.0/2.2));
#endif // PERFORM_TONE_MAPPING

    fragmentColor = vec4(finalColor, alpha);
}
#endif // FRAGMENT_SHADER
