#include "inventory.h"

global ItemData g_ItemData[MAX_ITEM_IDS] = {
    { Texture_PistolAmmoIcon, false, MAX_VIEW_MODELS }, // Item_None
    { Texture_PistolAmmoIcon, true, MAX_VIEW_MODELS }, // Item_PistolAmmo
    { Texture_PistolIcon, false, ViewModel_Pistol }, // Item_Pistol
    { Texture_ShotgunIcon, false, ViewModel_Shotgun }, // Item_Shotgun
    { Texture_PistolAmmoIcon, false, MAX_VIEW_MODELS },
};

inline u32 GetItemIcon(u32 itemId)
{
    Assert(itemId < ArrayCount(g_ItemData));
    return g_ItemData[itemId].icon;
}

inline b32 IsItemStackable(u32 itemId)
{
    Assert(itemId < ArrayCount(g_ItemData));
    return g_ItemData[itemId].isStackable;
}

inline u32 GetItemViewModel(u32 itemId)
{
    Assert(itemId < ArrayCount(g_ItemData));
    return g_ItemData[itemId].viewModel;
}

// TODO: Implement InventoryComponentSystem which will allow more efficient
// queries of item entities by owner.
internal void GetInventorySlots(EntityWorld *world, EntityId owner, InventorySlot *slots, u32 maxSlots)
{
    ClearToZero(slots, maxSlots * sizeof(*slots));

    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(
        world, InventoryItemComponentTypeId, entities, ArrayCount(entities));

    for (u32 entityIndex = 0; entityIndex < entityCount; entityIndex++)
    {
        EntityId entity = entities[entityIndex];

        InventoryItemComponent inventory;
        ecs_GetComponent(world, InventoryItemComponent, &entity, 1, &inventory);

        if (inventory.owner == owner)
        {
            Assert(inventory.slotIndex < maxSlots);
            slots[inventory.slotIndex].entity = entities[entityIndex];
            slots[inventory.slotIndex].itemId = inventory.itemId;
            slots[inventory.slotIndex].quantity = inventory.quantity;
        }
    }
}

internal void CreateInventoryItemEntity(EntityWorld *world, u32 itemId,
    u32 slotIndex, EntityId owner, u32 quantity,
    NetEntityId netEntityId = NULL_NET_ENTITY_ID)
{
    EntityId entityId = AllocateEntityId(world);

    ecs_AddComponent(world, InventoryItemComponentTypeId, entityId);
    ecs_AddComponent(world, NetworkReplicationComponentTypeId, entityId);

    InventoryItemComponent inventory;
    inventory.owner = owner;
    inventory.slotIndex = slotIndex;
    inventory.itemId = itemId;
    inventory.quantity = quantity;
    ecs_SetComponent(world, InventoryItemComponent, &entityId, 1, &inventory);

    if (netEntityId == NULL_NET_ENTITY_ID)
    {
        netEntityId = AllocateNetEntityId(world);
    }

    // TODO: Snapshots should not be part of the replication component
    NetworkReplicationComponent networkingComponent = {};
    networkingComponent.netEntityId = netEntityId;
    networkingComponent.priority = DEFAULT_INVENTORY_ITEM_NET_PRIORITY;
    networkingComponent.type = EntityType_InventoryItem;
    ecs_SetComponent(world, NetworkReplicationComponent, &entityId, 1, &networkingComponent);
}

internal b32 AddItemToInventory(EntityWorld *world, EntityId owner, u32 itemId, u32 quantity = 1)
{
    b32 result = false;

    InventorySlot inventory[PLAYER_TOTAL_INVENTORY_SLOT_COUNT];
    GetInventorySlots(world, owner, inventory, ArrayCount(inventory));

    for (u32 slotIndex = 0; slotIndex < ArrayCount(inventory); ++slotIndex)
    {
        if (inventory[slotIndex].entity == NULL_ENTITY_ID)
        {
            CreateInventoryItemEntity(world, itemId, slotIndex, owner, quantity);
            result = true;
            break;
        }
    }

    return result;
}

internal void DrawInventorySlot(RenderCommandQueue *renderCommandQueue,
    GameAssets *assets, TextBuffer *textBuffer, mat4 projection, vec2 position,
    f32 slotWidth, b32 mouseOver, InventorySlot inventorySlot)
{
    vec4 color =
        mouseOver ? Vec4(0.7, 0.7, 0.7, 1.0) : Vec4(0.02, 0.02, 0.02, 1.0);
    DrawColouredQuad(renderCommandQueue, assets, projection, position,
        Vec2(slotWidth), color);

    if (inventorySlot.entity != NULL_ENTITY_ID)
    {
        u32 itemIcon = GetItemIcon(inventorySlot.itemId);
        DrawTexturedQuad(renderCommandQueue, assets, projection, position,
            Vec2(slotWidth - 4.0f), itemIcon);

        if (IsItemStackable(inventorySlot.itemId))
        {
            char buffer[80];
            snprintf(buffer, sizeof(buffer), "%u", inventorySlot.quantity);
            DrawString(renderCommandQueue, assets, textBuffer, buffer,
                    position + Vec2(20, -35), projection, Vec4(1));
        }
    }
}

internal void UpdateInventoryDragState(InventoryUIState *uiState,
    GameInput *input, u32 slotIndex, InventorySlot inventorySlot,
    EntityId slotOwner)
{
    if (uiState->isDragInProgress)
    {
        if (WasReleased(input->buttonStates[KEY_MOUSE_BUTTON_LEFT]))
        {
            // Ignore event if dragging to the same slot
            if (uiState->dragStartingEntity != inventorySlot.entity)
            {
                // Generate inventory drag command
                Assert(uiState->commandQueueLength <
                       ArrayCount(uiState->commandQueue));
                InventoryCommand *command =
                    uiState->commandQueue + uiState->commandQueueLength++;

                command->action = InventoryAction_MoveItem;
                command->fromSlot = uiState->dragStartingSlot;
                command->fromEntity = uiState->dragStartingEntity;
                command->fromOwner = uiState->dragStartingOwner;
                command->toSlot = slotIndex;
                command->toEntity =
                    inventorySlot.entity; // NOTE: NULL_ENTITY_ID is valid here
                command->toOwner = slotOwner;

                // Complete drag
                uiState->isDragInProgress = false;
            }
        }
    }
    else
    {
        // Initiate drag
        if (WasPressed(input->buttonStates[KEY_MOUSE_BUTTON_LEFT]))
        {
            if (inventorySlot.entity != NULL_ENTITY_ID)
            {
                // Only start drag if slot is not empty
                uiState->dragStartingSlot = slotIndex;
                uiState->dragStartingEntity = inventorySlot.entity;
                uiState->dragStartingOwner = slotOwner;
                uiState->isDragInProgress = true;
            }
        }
    }
}

internal vec2 CalculateGridPosition(vec2 topLeft, f32 width,
    f32 padding, vec2 position)
{
    vec2 result = topLeft +
        Vec2(width, -width) * 0.5f + 
        Vec2(width + padding, 0) * (f32)position.x +
        Vec2(0, -width - padding) * (f32)position.y;

    return result;
}

internal u32 DrawInventorySlotGrid(InventoryUIState *uiState,
    RenderCommandQueue *renderCommandQueue, GameAssets *assets,
    TextBuffer *textBuffer, GameInput *input, mat4 projection,
    f32 framebufferWidth, f32 framebufferHeight, u32 rows, u32 cols,
    vec2 topLeftCorner, InventorySlot *inventory, u32 inventorySize,
    b32 overrideMouseOver = false, u32 activeSlot = 0)
{
    f32 slotWidth = INVENTORY_SLOT_WIDTH;
    f32 innerBorder = INVENTORY_SLOT_INNER_BORDER;

    vec2 mouseP =
        Vec2((f32)input->mousePosX, framebufferHeight - (f32)input->mousePosY);

    u32 mouseOverSlotIndex = inventorySize;

    for (u32 row = 0; row < rows; ++row)
    {
        for (u32 col = 0; col < cols; ++col)
        {
            vec2 position =
                CalculateGridPosition(topLeftCorner, INVENTORY_SLOT_WIDTH,
                    INVENTORY_SLOT_INNER_BORDER, Vec2((f32)col, (f32)row));

            u32 slotIndex = row * cols + col;
            Assert(slotIndex < inventorySize);
            InventorySlot inventorySlot = inventory[slotIndex];

            b32 mouseOver = false;
            if (!overrideMouseOver)
            {
                rect2 rect;
                rect.min.x = position.x - (slotWidth + innerBorder) * 0.5f;
                rect.min.y = position.y - (slotWidth + innerBorder) * 0.5f;
                rect.max.x = position.x + (slotWidth + innerBorder) * 0.5f;
                rect.max.y = position.y + (slotWidth + innerBorder) * 0.5f;
                mouseOver = RectContainsPoint(rect, mouseP);

                if (mouseOver)
                {
                    mouseOverSlotIndex = slotIndex;
                }
            }
            else if (slotIndex == activeSlot)
            {
                mouseOver = true;
            }

            DrawInventorySlot(renderCommandQueue, assets, textBuffer,
                projection, position, slotWidth, mouseOver, inventorySlot);
        }
    }

    return mouseOverSlotIndex;
}

internal void DrawContainerInventory(InventoryUIState *uiState,
    RenderCommandQueue *renderCommandQueue, GameAssets *assets,
    TextBuffer *textBuffer, GameInput *input, mat4 projection,
    f32 framebufferWidth, f32 framebufferHeight, EntityId owner,
    InventorySlot *slots, u32 slotCount)
{
    // TODO: Calculate panel size from desired grid dimensions
    vec2 panelDimensions = Vec2(360, 620);
    vec2 panelPosition = Vec2(
        framebufferWidth - panelDimensions.x - 20, framebufferHeight * 0.5f);
    DrawColouredQuad(renderCommandQueue, assets, projection,
        panelPosition, panelDimensions,
        Vec4(0.08, 0.08, 0.08, 1));

    vec2 topLeftCorner =
        panelPosition - Vec2(panelDimensions.x, -panelDimensions.y) * 0.5f +
        Vec2(INVENTORY_SLOT_OUTER_BORDER, -INVENTORY_SLOT_OUTER_BORDER);

    u32 cols = 4;
    u32 rows = slotCount / cols;

    u32 mouseOverSlotIndex = DrawInventorySlotGrid(uiState, renderCommandQueue,
        assets, textBuffer, input, projection, framebufferWidth, framebufferHeight, rows,
        cols, topLeftCorner, slots, slotCount);

    if (mouseOverSlotIndex != slotCount)
    {
        InventorySlot inventorySlot = slots[mouseOverSlotIndex];
        UpdateInventoryDragState(uiState, input, mouseOverSlotIndex, inventorySlot, owner);
    }
}

internal void DrawEquipmentBar(InventoryUIState *uiState,
    RenderCommandQueue *renderCommandQueue, GameAssets *assets,
    TextBuffer *textBuffer, GameInput *input, mat4 projection,
    f32 framebufferWidth, f32 framebufferHeight, EntityId owner,
    InventorySlot *equipmentSlots, u32 equipmentSlotCount,
    b32 overrideMouseOver = false, u32 activeEquipmentSlot = 0)
{
    vec2 panelDimensions = Vec2(528, 98);
    vec2 panelPosition = Vec2(framebufferWidth * 0.5f, 100);
    DrawColouredQuad(renderCommandQueue, assets, projection,
        panelPosition, panelDimensions,
        Vec4(0.08, 0.08, 0.08, 1));

    vec2 topLeftCorner =
        panelPosition - Vec2(panelDimensions.x, -panelDimensions.y) * 0.5f +
        Vec2(INVENTORY_SLOT_OUTER_BORDER, -INVENTORY_SLOT_OUTER_BORDER);

    u32 mouseOverSlotIndex = DrawInventorySlotGrid(uiState, renderCommandQueue,
        assets, textBuffer, input, projection, framebufferWidth,
        framebufferHeight, 1, equipmentSlotCount, topLeftCorner, equipmentSlots,
        equipmentSlotCount, overrideMouseOver, activeEquipmentSlot);

    if (mouseOverSlotIndex != equipmentSlotCount)
    {
        InventorySlot inventorySlot = equipmentSlots[mouseOverSlotIndex];

        // Map from equipment slot index to inventory-wide slot index
        u32 fullSlotIndex = PLAYER_INVENTORY_SLOT_COUNT + mouseOverSlotIndex;

        UpdateInventoryDragState(uiState, input, fullSlotIndex, inventorySlot, owner);
    }
}

internal void DrawInventory(InventoryUIState *uiState,
    RenderCommandQueue *renderCommandQueue, GameAssets *assets,
    TextBuffer *textBuffer, GameInput *input, f32 framebufferWidth,
    f32 framebufferHeight, EntityId inventoryOwner, InventorySlot *inventory,
    u32 inventorySize, EntityId containerOwner, InventorySlot *container,
    u32 containerSize)
{
    mat4 projection = Orthographic(
        0.0f, framebufferWidth, 0.0f, framebufferHeight);

    vec2 panelDimensions = Vec2(600, 400);
    DrawColouredQuad(renderCommandQueue, assets, projection,
        Vec2(framebufferWidth, framebufferHeight) * 0.5f,
        panelDimensions, Vec4(0.08, 0.08, 0.08, 1));


    f32 slotWidth = INVENTORY_SLOT_WIDTH;
    f32 outerBorder = INVENTORY_SLOT_OUTER_BORDER;
    f32 innerBorder = INVENTORY_SLOT_INNER_BORDER;
    u32 cols = 6;
    u32 rows = 4;
    Assert(cols * rows == PLAYER_INVENTORY_SLOT_COUNT);

    vec2 topLeftCorner =
        Vec2(framebufferWidth, framebufferHeight) * 0.5f -
        Vec2(panelDimensions.x, -panelDimensions.y) * 0.5f +
        Vec2(INVENTORY_SLOT_OUTER_BORDER, -INVENTORY_SLOT_OUTER_BORDER);

    u32 mouseOverSlotIndex = DrawInventorySlotGrid(uiState, renderCommandQueue,
        assets, textBuffer, input, projection, framebufferWidth,
        framebufferHeight, rows, cols, topLeftCorner, inventory, inventorySize);
    if (mouseOverSlotIndex != inventorySize)
    {
        InventorySlot inventorySlot = inventory[mouseOverSlotIndex];
        UpdateInventoryDragState(
            uiState, input, mouseOverSlotIndex, inventorySlot, inventoryOwner);
    }

    InventorySlot *equipmentSlots = inventory + PLAYER_INVENTORY_SLOT_COUNT;
    DrawEquipmentBar(uiState, renderCommandQueue, assets, textBuffer, input, projection,
        framebufferWidth, framebufferHeight, inventoryOwner, equipmentSlots,
        PLAYER_EQUIPMENT_BAR_SLOT_COUNT);

    if (containerSize > 0)
    {
        DrawContainerInventory(uiState, renderCommandQueue, assets, textBuffer,
            input, projection, framebufferWidth, framebufferHeight,
            containerOwner, container, containerSize);
    }

    if (uiState->isDragInProgress)
    {
        vec2 mouseP = Vec2(
            (f32)input->mousePosX, framebufferHeight - (f32)input->mousePosY);

        // Draw icon of the item being dragged on the cursor
        u32 itemId = 0;
        if (uiState->dragStartingOwner == inventoryOwner)
        {
            Assert(uiState->dragStartingSlot < inventorySize);
            itemId = inventory[uiState->dragStartingSlot].itemId;
        }
        else if (uiState->dragStartingOwner == containerOwner)
        {
            Assert(uiState->dragStartingSlot < containerSize);
            itemId = container[uiState->dragStartingSlot].itemId;
        }
        else
        {
            InvalidCodePath();
        }

        u32 itemIcon = GetItemIcon(itemId);
        DrawTexturedQuad(renderCommandQueue, assets, projection, mouseP,
            Vec2(slotWidth - 4.0f), itemIcon);

        // Cancel drag if mouse is released over something that is not an inventory slot
        if (WasReleased(input->buttonStates[KEY_MOUSE_BUTTON_LEFT]))
        {
            // Complete drag
            uiState->isDragInProgress = false;
        }
    }
}

internal InventorySlot GetInventorySlot(EntityWorld *world, EntityId owner, u32 slotIndex)
{
    InventorySlot result = {};

    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(
        world, InventoryItemComponentTypeId, entities, ArrayCount(entities));
    for (u32 entityIndex = 0; entityIndex < entityCount; ++entityIndex)
    {
        EntityId entity = entities[entityIndex];

        InventoryItemComponent item;
        ecs_GetComponent(world, InventoryItemComponent, &entity, 1, &item);

        if (item.owner == owner && item.slotIndex == slotIndex)
        {
            result.entity = entity;
            result.itemId = item.itemId;
        }
    }

    return result;
}

// TODO: Accelerate this
internal b32 IsInventorySlotEmpty(EntityWorld *world, EntityId owner, u32 slotIndex)
{
    b32 result = true;

    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(
        world, InventoryItemComponentTypeId, entities, ArrayCount(entities));
    for (u32 entityIndex = 0; entityIndex < entityCount; ++entityIndex)
    {
        EntityId entity = entities[entityIndex];

        InventoryItemComponent item;
        ecs_GetComponent(world, InventoryItemComponent, &entity, 1, &item);

        if (item.owner == owner && item.slotIndex == slotIndex)
        {
            result = false; // Found item in slot
        }
    }

    return result;
}

internal void ProcessInventoryCommand(
    EntityWorld *world, EntityId owner, InventoryCommand command)
{
    InventoryOwnerComponent inventoryOwnerComponent;
    ecs_GetComponent(
        world, InventoryOwnerComponent, &owner, 1, &inventoryOwnerComponent);

    switch (command.action)
    {
    case InventoryAction_CloseContainer:
    {
        inventoryOwnerComponent.openContainer = NULL_ENTITY_ID;
        ecs_SetComponent(world, InventoryOwnerComponent, &owner, 1,
            &inventoryOwnerComponent);
        break;
    }
    case InventoryAction_MoveItem:
    {
        EntityId openContainer = inventoryOwnerComponent.openContainer;
        b32 toOwnerValid =
            command.toOwner == owner || command.toOwner == openContainer;
        b32 fromOwnerValid =
            command.fromOwner == owner || command.fromOwner == openContainer;
        if (toOwnerValid && fromOwnerValid)
        {
            if (ecs_HasComponent(
                    world, InventoryItemComponentTypeId, command.fromEntity))
            {
                InventoryItemComponent fromItemComponent;
                ecs_GetComponent(world, InventoryItemComponent,
                    &command.fromEntity, 1, &fromItemComponent);

                // Check that the source slot matches what the client specified
                b32 fromItemValid =
                    (fromItemComponent.owner == command.fromOwner) &&
                    (fromItemComponent.slotIndex == command.fromSlot);

                // Check that the destination slot is empty on the server
                b32 toSlotValid = (command.toEntity == NULL_ENTITY_ID) &&
                                  (IsInventorySlotEmpty(
                                      world, command.toOwner, command.toSlot));
                // TODO: Handle swap events when destination slot is not empty

                if (fromItemValid && toSlotValid)
                {
                    // Move item
                    fromItemComponent.slotIndex = command.toSlot;
                    fromItemComponent.owner = command.toOwner;
                    ecs_SetComponent(world, InventoryItemComponent,
                        &command.fromEntity, 1, &fromItemComponent);
                }
            }
        }
        break;
    }
    default:
        InvalidCodePath();
        break;
    }
}

internal InventorySlot GetActiveEquipmentSlot(EntityWorld *world, EntityId owner)
{
    InventoryOwnerComponent inventoryOwner;
    ecs_GetComponent(world, InventoryOwnerComponent, &owner, 1, &inventoryOwner);

    Assert(inventoryOwner.activeEquipmentSlot < PLAYER_EQUIPMENT_BAR_SLOT_COUNT);
    u32 activeSlotIndex = PLAYER_INVENTORY_SLOT_COUNT + inventoryOwner.activeEquipmentSlot;
    InventorySlot activeSlot = GetInventorySlot(world, owner, activeSlotIndex);

    return activeSlot;
}

internal u32 GetItemEntitiesWithOwner(
    EntityWorld *world, EntityId owner, EntityId *entities, u32 capacity)
{
    EntityId allEntities[MAX_ENTITIES];
    u32 fullCount = ecs_GetEntitiesWithComponent(world,
        InventoryItemComponentTypeId, allEntities, ArrayCount(allEntities));

    u32 count = 0;
    for (u32 entityIndex = 0; entityIndex < fullCount; ++entityIndex)
    {
        EntityId entityId = allEntities[entityIndex];

        InventoryItemComponent item;
        ecs_GetComponent(world, InventoryItemComponent, &entityId, 1, &item);

        if (item.owner == owner)
        {
            Assert(count < capacity);
            entities[count++] = entityId;

            if (count == capacity)
            {
                break;
            }
        }
    }

    return count;
}

internal b32 ConsumeItem(
    EntityWorld *world, EntityId owner, u32 itemId, u32 quantity = 1)
{
    b32 result = false;

    InventorySlot inventory[PLAYER_TOTAL_INVENTORY_SLOT_COUNT];
    GetInventorySlots(world, owner, inventory, ArrayCount(inventory));

    for (u32 slotIndex = 0; slotIndex < ArrayCount(inventory); ++slotIndex)
    {
        InventorySlot slot = inventory[slotIndex];

        // TODO: Handle consuming from multiple slots
        if (slot.itemId == itemId)
        {
            if (slot.quantity >= quantity)
            {
                slot.quantity -= quantity;
                if (slot.quantity > 0)
                {
                    InventoryItemComponent item;
                    ecs_GetComponent(world, InventoryItemComponent, &slot.entity, 1, &item);
                    item.quantity = slot.quantity;
                    ecs_SetComponent(world, InventoryItemComponent, &slot.entity, 1, &item);
                }
                else
                {
                    RemoveEntity(world, slot.entity);
                }

                result = true;
                break;
            }
        }
    }

    return result;
}
