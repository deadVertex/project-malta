/*
PROJECT MALTA

A new game idea to explore for the next ~30 days

Coop immersive sim style game
First person
GTFO but with more depth to the gameplay
Sandbox game?

Primary game loops:
- FPS shooting
- Crafting in base to create gear to go out and kill things, get
better resources/blueprints to craft better gear
- Building crafting work benches, defenses, resource extractors
- Emergent gameplay

FPS mechanics:
 - ADS
 - Reloading
 - Clientside recoil
 - Serverside bullet spread
 - Bullet drop
 - Bullet damage
 - Semi auto/full auto
 - Shotgun/single shot
 - View model
 - Crosshair
 - Enemy hit reactions
 - Bullet impact fx
 - Bullet holes

 Crafting Mechanics
 - No direct harvesting of natural resources, all recycled
 - Metal production (collect scrap metal, smelt into ingots, craft into items)
 - Multiple types of metal

 Respawning at sleeping bag

 Environment
  - Concrete and jungle
  - Clear blue sky

Experience from Rust Legacy, sitting in base watching a rad town in the
distance as the sun sets. Checking for any players coming this way as night
falls.  Light from camp fire in the rad town, light from camp fire in my from
another room in my base.
(Base building, view distance, day night cycle, man-made environments combined with nature)

=================================
TODO
=================================
Next increment:
  - Start playing around with building system in the mean time

  - FIX LIGHTING!
    - Offline validation with Monte-Carlo path tracing (for fun and collision detection stress testing)
        : In progress but this will take a while
    - Validate against Mitsuba
    - Debug individual PBR components (fresnel, NDF, masking function, BRDF, etc)

=================================
BACKLOG
=================================
Bugs:
  - Terrain collision model visibly differs from visual representation in some places
  - Stop leaving files open when we load assets! Allocate temporary memory for them
  - (LOW) Fix server debug draw flicker, separate server debug drawing buffer which
    is cleared on server tick rather than client frame?
  - (LINUX only?) 360 rotation bug has reappeared! (Issue is caused by lerping euler angles
    when they wrap, shouldn't have this issue with quaternions, issue is much
    better with quaternions but is still present)
  - Client side lerp is occuring between first snapshot and 0,0,0


Internal Improvements:
  - Massive framerate hitch when shooting shotgun due to ray terrain interesction tests
  - Support transforming AABB trees for triangle meshes
  - Fix RayIntersectAabb returning false if starting inside AABB
  - Use overlap test instead of ray intersect for triangle mesh midphase
  - (LOW) Modifying heighmap to blend with world prefabs (buildings)
  - Clean up enemy loot drops, despawn after time period and when the container is empty
  - Working with both the TransformComponent and NetworkReplicationComponent on
    the server for the position is not great, should try ideally only read from
    the TransformComponent
  - Tag components
  - Dirty bit optimization, to only send network updates for entities who's state has changed
  - Optimize terrain collision
  - Dedicate memory arena for entity data
  - Rework lerp buffer, so that it can overwrite itself rather than relying on us to remove entries from it
  - Defining vertex layouts from game code
  - Parameterize sky gradient shader (rather redo sky shader)
  - Special shadow pass depth only shader
  - Process update render commands before draw commands
  - Linearize depth buffer for larger view distance
  - Support non-indexed meshes for instance rendering
  - Support partial vertex buffer updates
  - Allow specifying a base instance offset for instance rendering
  - Rename color inputs to albedo for PBR shaders
  - Explore idea for shader management, don't specify a specifc shader instead
        query for one that best matches a desired configuration. i.e. Color only, no
        depth, no lighting, self tone mapped.
        Material selects its own shader based on the these tags and the data
        type for each field i.e. handling whether albedo color is a single
        value or a texture. Feature tags enable/disable presence of uniform values
  - Shader variant management!
  - Use vec3 color for debug drawing
  - Use quad tree to accelerate large terrain ray casts
  - Vertex morphing function to remove terrain cracks
  - Use instancing for drawing all terrain pieces in 1 draw call
  - Grass lighting is terrible!
  - Mapping network replication policies to entity types

Building system:
  - Placing foundations
  - Placing walls
  - Snapping walls to foundations
  - Placing floors/ceilings on top of walls
  - Placing doorways
  - Placing windows

Procedural P.O.I generation (radtowns, other terrain features):
  - Modifying heighmap to blend with world prefabs (buildings)

Combat gameplay:
  - Lootable containers, more items
  - Shooting should consume ammo
  - Enemy hit boxes
  - Dropping player inventory on death
  - Bullet spread (wait for automatic weapons, apply this on the server when we
    try to shoot with recoil present)
  - Implement proper system for parenting entities and use if the view models instead of hard coding
  - Gun play juicing (muzzle flash, sounds, animations, particle effects):

Enemy AI:
  - Path finding!

Player inventory system:
  - Combining item stacks
  - Splitting item stacks
  - Automatically adding to existing item stacks
  - Item stack limit
  - QoL: Swapping items in slots with each other
  - QoL: Right click to auto transfer
  - Picking up items
  - Juice it! Audio, animations
  - Accelerate inventory item lookup by owner


Visuals:
  - Cascaded shadow maps for sun light
  - Day night cycle
  - Proper PBR sky shader
  - Grass
  - Vertex animation of trees and grass
  - Proper particle system
  - Foliage shader
  - Spot light shadows
  - Point light shadows
  - PBR shaders need work
  - Generating irradiance and radiance cube maps from sky box
  - Local light probes


Core Engine functionality:
  - Audio!
  - Asset packs!
  - Profiling infrastructure!

Collision detection
  - GJK for most things
  - GJK raycast for sweep tests

Networking
  - (HIGH) Lag compensation
  - Unified entity replication event queue
  - Store per entity network replication priority rather than hardcoding it for the player
  - Handle exceeding maximum number of clients
  - Report mis-match of protocol version to connecting client
  - Packet sequence numbers
  - Packet acks
  - Packet delay and loss measurements
  - Packet burst handling, drop excess packets from client if we have already
    received 4 packets from them this tick

- (DONE?) Fix only sampling input at 60/30 HZ (TODO: Watch battlenonsense video and decide how we want to fix this)
    One idea is to sample input at framerate and update the player rotation
    vector, then every tick, sample the rotation vector and send it off to the
    server. Keyboard and Mouse button input would still only be sampled at
    tickrate. This seems to be fine for now, although occasionally there is
    very bad stuttering when moving but that could be because we're not doing
    any buffering.
    Both CSGO,Overwatch and Quake3 seem to collect input at frame rate, then
    CSGO and OW use a fixed update rate for clientside prediction. This is then
    lerped and displayed to the player. Updating the CSGO tickrate to 128 does
    have a noticable improvement on movement and possibly view angles.
    Using source engine terminology.
    We'll have both client and server run at a tickrate of 64HZ, a client send
    rate of 64HZ and a default client update rate of 64HZ.
*/


#include <cstdint>
#include <cstdio>

#ifdef PLATFORM_LINUX
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dlfcn.h>
#endif

#ifdef PLATFORM_WINDOWS
#define WIN32_LEAN_AND_MEAN
#include <Ws2tcpip.h>
#include <Windows.h>
#include <TimeAPI.h>
#endif

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "game.h"

#include "renderer.cpp"
#include "sockets.cpp"

global u32 g_FramebufferWidth = 1920;
global u32 g_FramebufferHeight = 1080;
global GLFWwindow *g_Window;

internal void* AllocateMemory(u64 size, u64 baseAddress = 0);
internal void FreeMemory(void *p);
internal DebugReadEntireFile(ReadEntireFile);

struct GameLibrary
{
    GameClientUpdateFunction *clientUpdate;
    GameClientFixedUpdateFunction *clientFixedUpdate;
    GameServerUpdateFunction *serverUpdate;
#ifdef PLATFORM_WINDOWS
    HMODULE handle;
    FILETIME lastWriteTime;
#elif defined(PLATFORM_LINUX)
    void *handle;
    time_t lastWriteTime;
#endif
};

internal b32 LoadGameLibrary(GameLibrary *library);
internal void UnloadGameLibrary(GameLibrary *library);
internal b32 WasGameCodeModified(GameLibrary *library);

#ifdef PLATFORM_LINUX
#include "linux_platform.cpp"
#endif

#ifdef PLATFORM_WINDOWS
#include "windows_platform.cpp"
#endif

internal DebugLogMessage(LogMessage_)
{
    char buffer[256];
    va_list args;
    va_start(args, fmt);
    vsnprintf(buffer, sizeof(buffer), fmt, args);
    va_end(args);
#ifdef PLATFORM_WINDOWS
    OutputDebugString(buffer);
    OutputDebugString("\n");
#elif defined(PLATFORM_LINUX)
    puts(buffer);
#endif
}

const char *GetGlfwErrorCodeName(i32 code)
{
#define EnumToString(ENUM) case ENUM: return #ENUM
    switch (code)
    {
        EnumToString(GLFW_NO_ERROR);
        EnumToString(GLFW_NOT_INITIALIZED);
        EnumToString(GLFW_NO_CURRENT_CONTEXT);
        EnumToString(GLFW_INVALID_ENUM);
        EnumToString(GLFW_INVALID_VALUE);
        EnumToString(GLFW_OUT_OF_MEMORY);
        EnumToString(GLFW_API_UNAVAILABLE);
        EnumToString(GLFW_VERSION_UNAVAILABLE);
        EnumToString(GLFW_PLATFORM_ERROR);
        EnumToString(GLFW_FORMAT_UNAVAILABLE);
        EnumToString(GLFW_NO_WINDOW_CONTEXT);
        default:
            return "UNKNOWN";
    }
}

#define KEY_HELPER(NAME)                                                       \
    case GLFW_KEY_##NAME:                                                      \
        return KEY_##NAME;
internal i32 ConvertKey(int key)
{
    if (key >= GLFW_KEY_SPACE && key <= GLFW_KEY_GRAVE_ACCENT)
    {
        return key;
    }
    switch (key)
    {
        KEY_HELPER(BACKSPACE);
        KEY_HELPER(TAB);
        KEY_HELPER(INSERT);
        KEY_HELPER(HOME);
        KEY_HELPER(PAGE_UP);
    // Can't use KEY_HELPER( DELETE ) as windows has a #define for DELETE
    case GLFW_KEY_DELETE:
        return KEY_DELETE;
        KEY_HELPER(END);
        KEY_HELPER(PAGE_DOWN);
        KEY_HELPER(ENTER);

        KEY_HELPER(LEFT_SHIFT);
    case GLFW_KEY_LEFT_CONTROL:
        return KEY_LEFT_CTRL;
        KEY_HELPER(LEFT_ALT);
        KEY_HELPER(RIGHT_SHIFT);
    case GLFW_KEY_RIGHT_CONTROL:
        return KEY_RIGHT_CTRL;
        KEY_HELPER(RIGHT_ALT);

        KEY_HELPER(LEFT);
        KEY_HELPER(RIGHT);
        KEY_HELPER(UP);
        KEY_HELPER(DOWN);

        KEY_HELPER(ESCAPE);

        KEY_HELPER(F1);
        KEY_HELPER(F2);
        KEY_HELPER(F3);
        KEY_HELPER(F4);
        KEY_HELPER(F5);
        KEY_HELPER(F6);
        KEY_HELPER(F7);
        KEY_HELPER(F8);
        KEY_HELPER(F9);
        KEY_HELPER(F10);
        KEY_HELPER(F11);
        KEY_HELPER(F12);
    case GLFW_KEY_KP_0:
        return KEY_NUM0;
    case GLFW_KEY_KP_1:
        return KEY_NUM1;
    case GLFW_KEY_KP_2:
        return KEY_NUM2;
    case GLFW_KEY_KP_3:
        return KEY_NUM3;
    case GLFW_KEY_KP_4:
        return KEY_NUM4;
    case GLFW_KEY_KP_5:
        return KEY_NUM5;
    case GLFW_KEY_KP_6:
        return KEY_NUM6;
    case GLFW_KEY_KP_7:
        return KEY_NUM7;
    case GLFW_KEY_KP_8:
        return KEY_NUM8;
    case GLFW_KEY_KP_9:
        return KEY_NUM9;
    case GLFW_KEY_KP_DECIMAL:
        return KEY_NUM_DECIMAL;
    case GLFW_KEY_KP_DIVIDE:
        return KEY_NUM_DIVIDE;
    case GLFW_KEY_KP_MULTIPLY:
        return KEY_NUM_MULTIPLY;
    case GLFW_KEY_KP_SUBTRACT:
        return KEY_NUM_MINUS;
    case GLFW_KEY_KP_ADD:
        return KEY_NUM_PLUS;
    case GLFW_KEY_KP_ENTER:
        return KEY_NUM_ENTER;
    }
    return KEY_UNKNOWN;
}

void KeyCallback(GLFWwindow *window, int glfwKey, int scancode, int action, int mods)
{
    GameInput *input =
            (GameInput *)glfwGetWindowUserPointer(window);

    i32 key = ConvertKey(glfwKey);
    if (key != KEY_UNKNOWN)
    {
        Assert(key < MAX_KEYS);
        if (action == GLFW_PRESS)
        {
            input->buttonStates[key].isDown = true;
        }
        else if (action == GLFW_RELEASE)
        {
            input->buttonStates[key].isDown = false;
        }
        // else: ignore key repeat messages
    }
}

internal void MouseButtonCallback(
    GLFWwindow *window, int button, int action, int mods)
{
    GameInput *input =
            (GameInput *)glfwGetWindowUserPointer(window);

    if (button == GLFW_MOUSE_BUTTON_LEFT)
    {
        input->buttonStates[KEY_MOUSE_BUTTON_LEFT].isDown = (action == GLFW_PRESS);
    }
    if (button == GLFW_MOUSE_BUTTON_MIDDLE)
    {
        input->buttonStates[KEY_MOUSE_BUTTON_MIDDLE].isDown = (action == GLFW_PRESS);
    }
    if (button == GLFW_MOUSE_BUTTON_RIGHT)
    {
        input->buttonStates[KEY_MOUSE_BUTTON_RIGHT].isDown = (action == GLFW_PRESS);
    }
}

global f64 g_PrevMousePosX;
global f64 g_PrevMousePosY;

internal void CursorPositionCallback(GLFWwindow *window, f64 xPos, f64 yPos)
{
    GameInput *input = (GameInput *)glfwGetWindowUserPointer(window);

    // Unfortunately GLFW doesn't seem to provide a way to get mouse motion
    // rather than cursor position so we have to compute the relative motion
    // ourselves.
    f64 relX = xPos - g_PrevMousePosX;
    f64 relY = yPos - g_PrevMousePosY;
    g_PrevMousePosX = xPos;
    g_PrevMousePosY = yPos;

    input->mouseRelPosX = (i32)Floor((f32)relX);
    input->mouseRelPosY = (i32)Floor((f32)relY);
    input->mousePosX = (i32)Floor((f32)xPos);
    input->mousePosY = (i32)Floor((f32)yPos);
}

internal void FramebufferSizeCallback(GLFWwindow *window, int width, int height)
{
    // Don't ever set frame buffer dimensions to zero as then we cannot create
    // a valid frame buffer for it
    g_FramebufferWidth = width > 0 ? width : g_FramebufferWidth;
    g_FramebufferHeight = height > 0 ? height : g_FramebufferHeight;
}

internal void GlfwErrorCallback(int error, const char *description)
{
    LogMessage("GLFW Error (%d): %s.", error, description);
}

global b32 g_IsServerRunning;
global b32 g_IsClientRunning;
global ServerSocketState g_ServerState;
global ClientSocketState g_ClientState;
internal DebugStartServer(StartServer)
{
    if (!g_IsServerRunning)
    {
        if (InitializeSeverSocket(&g_ServerState, port))
        {
            g_IsServerRunning = true;
        }
        else
        {
            LogMessage("Failed to start server!");
            return false;
        }
    }

    return true;
}

internal DebugStopServer(StopServer)
{
    if (g_IsServerRunning)
    {
        DeinitializeServer(&g_ServerState);
        g_IsServerRunning = false;
    }
}

internal DebugStartClient(StartClient)
{
    if (!g_IsClientRunning)
    {
        if (!InitializeClientSocket(&g_ClientState, address, port))
        {
            LogMessage("Failed to start client!");
            return false;
        }
        g_IsClientRunning = true;
    }

    return true;
}

internal DebugStopClient(StopClient)
{
    if (g_IsClientRunning)
    {
        DeinitializeClient(&g_ClientState);
        g_IsClientRunning = false;
    }
}

internal ShowMouseCursor(ShowMouseCursorImpl)
{
    glfwSetInputMode(g_Window, GLFW_CURSOR,
        isVisible ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);
}

#ifdef PLATFORM_WINDOWS
int WinMain(HINSTANCE instance, HINSTANCE prevInstance, LPSTR cmdLine,
            int cmdShow)
#elif defined(PLATFORM_LINUX)
int main(int argc, char **argv)
#endif
{
    LogMessage = &LogMessage_;

    LogMessage("Compiled agist GLFW %i.%i.%i", GLFW_VERSION_MAJOR,
           GLFW_VERSION_MINOR, GLFW_VERSION_REVISION);

    i32 major, minor, revision;
    glfwGetVersion(&major, &minor, &revision);
    LogMessage("Running against GLFW %i.%i.%i", major, minor, revision);
    LogMessage("%s", glfwGetVersionString());

    glfwSetErrorCallback(GlfwErrorCallback);
    if (!glfwInit())
    {
        LogMessage("Failed to initialize GLFW!");
        return -1;
    }
    b32 useRawInput = glfwRawMouseMotionSupported();
    LogMessage("Raw input supported: %s", useRawInput ? "TRUE" : "FALSE");

    u64 frequency = glfwGetTimerFrequency();

    if (!InitializeSockets())
    {
        glfwTerminate();
        return -1;
    }

    glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#define WINDOWED_MODE
#ifdef WINDOWED_MODE
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
    GLFWwindow *window = glfwCreateWindow(
        g_FramebufferWidth, g_FramebufferHeight, "Project Malta", NULL, NULL);
#else
    GLFWmonitor *monitor = glfwGetPrimaryMonitor();
    const GLFWvidmode *mode = glfwGetVideoMode(monitor);

    glfwWindowHint(GLFW_RED_BITS, mode->redBits);
    glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
    glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
    glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
    GLFWwindow *window = glfwCreateWindow(
        WINDOW_WIDTH, WINDOW_HEIGHT, "Project Malta", monitor, NULL);
#endif
    if (window == NULL)
    {
        const char *errorDescription = NULL;
        i32 errorCode = glfwGetError(&errorDescription);
        LogMessage("GLFW failed to create window!\t%s. (%s %d)",
               errorDescription, GetGlfwErrorCodeName(errorCode), errorCode);
        DeinitializeSockets();
        glfwTerminate();
        return -1;
    }
    g_Window = window; // Set window global pointer for ShowMouseCursor

    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    if (useRawInput)
    {
        glfwSetInputMode(window, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
    }

    GameInput input = {};
    glfwSetWindowUserPointer(window, &input);
    glfwSetKeyCallback(window, KeyCallback);
    glfwSetMouseButtonCallback(window, MouseButtonCallback);
    glfwSetCursorPosCallback(window, CursorPositionCallback);
    glfwSetFramebufferSizeCallback(window, FramebufferSizeCallback);

    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);

    const char *vendor = (const char *)glGetString(GL_VENDOR);
    const char *renderer = (const char *)glGetString(GL_RENDERER);
    const char *version = (const char *)glGetString(GL_VERSION);
    const char *shadingLanguageVersion =
        (const char *)glGetString(GL_SHADING_LANGUAGE_VERSION);
    const char *extensions = (const char *)glGetString(GL_EXTENSIONS);
    LogMessage("OpenGL Renderer\n\tVendor: %s\n\tGPU: %s\n\tVersion: "
            "%s\n\tShading Language Version: %s",
            vendor, renderer, version,
            shadingLanguageVersion);

    // Initialize GLEW and check that we have a suitable OpenGL context
    GLenum glewError = glewInit();
    if (glewError != GLEW_OK)
    {
        LogMessage("Failed to initialize GLEW %s. Error: %s",
               glewGetString(GLEW_VERSION), glewGetErrorString(glewError));
        DeinitializeSockets();
        glfwTerminate();
        return -1;
    }
    LogMessage("GLEW %s initialized", glewGetString(GLEW_VERSION));

    GameLibrary gameLibrary = {};
    if (!LoadGameLibrary(&gameLibrary))
    {
        DeinitializeSockets();
        glfwTerminate();
        return -1;
    }

    RendererState rendererState = {};
    // Configure OpenGL
    InitRenderer(&rendererState);

    u64 persistentStorageSize = Megabytes(1);
    u64 transientStorageSize = Gigabytes(1);
    u64 renderCommandQueueCapacity = Megabytes(2);
    u64 totalAllocationSize = persistentStorageSize + transientStorageSize +
                              renderCommandQueueCapacity;

    // Allocate all memory from single system memory allocation
    GameMemory memory = {};
    memory.persistentStorage = AllocateMemory(totalAllocationSize, 0);
    memory.persistentStorageSize = persistentStorageSize;
    memory.transientStorage =
        (u8 *)memory.persistentStorage + persistentStorageSize;
    memory.transientStorageSize = transientStorageSize;

    memory.logMessage = &LogMessage_;
    memory.readEntireFile = &ReadEntireFile;
    memory.freeFileMemory = &FreeMemory;
    memory.showMouseCursor = &ShowMouseCursorImpl;

    GameMemory serverMemory = {};
    serverMemory.logMessage = &LogMessage_;
    serverMemory.readEntireFile = &ReadEntireFile;
    serverMemory.freeFileMemory = &FreeMemory;

    serverMemory.persistentStorageSize = persistentStorageSize;
    serverMemory.persistentStorage =
        AllocateMemory(totalAllocationSize);
    serverMemory.transientStorage =
        (u8 *)serverMemory.persistentStorage + persistentStorageSize;
    serverMemory.transientStorageSize = transientStorageSize;

    InitializeRenderCommandQueue(&memory.renderCommandQueue,
        (u8 *)memory.transientStorage + transientStorageSize,
        (u32)renderCommandQueueCapacity);

    u64 frameStart = glfwGetTimerValue();
    f32 clientAccumulator = 0.0f;
    f32 serverAccumulator = 0.0f;
    f32 dt = 1.0f / (f32)TICK_RATE;
    f32 maxFrameTime = 0.25f;

    while (!glfwWindowShouldClose(window))
    {
        u64 frameEnd = glfwGetTimerValue();
        f32 frameTime = (f32)((frameEnd - frameStart) / (f64)frequency);
        frameTime = Min(frameTime, maxFrameTime);
        frameStart = frameEnd;
        clientAccumulator += frameTime;

        memory.framebufferWidth = g_FramebufferWidth;
        memory.framebufferHeight = g_FramebufferHeight;

        memory.wasCodeReloaded = false;
        serverMemory.wasCodeReloaded = false;
        if (WasGameCodeModified(&gameLibrary))
        {
            LogMessage("Reloading game code...");
            UnloadGameLibrary(&gameLibrary);
            ClearToZero(&gameLibrary, sizeof(GameLibrary));
            b32 result = LoadGameLibrary(&gameLibrary);
            Assert(result);
            memory.wasCodeReloaded = true;
            serverMemory.wasCodeReloaded = true;
        }

        if (g_IsServerRunning)
        {
            serverAccumulator += frameTime;

            while (serverAccumulator >= dt)
            {
                Packet packets[4];
                NetworkEvent events[4];
                u32 eventCount = 0;
                u32 count = ServerReceivePackets(&g_ServerState, packets,
                    ArrayCount(packets), events, ArrayCount(events),
                    &eventCount);

                Packet packetsToSend[4];
                serverMemory.incomingPackets = packets;
                serverMemory.incomingPacketCount = count;
                serverMemory.maxOutgoingPackets = ArrayCount(packetsToSend);
                serverMemory.outgoingPackets = packetsToSend;
                serverMemory.outgoingPacketCount = 0;
                serverMemory.networkEvents = events;
                serverMemory.networkEventCount = eventCount;
                gameLibrary.serverUpdate(&serverMemory, dt);

                SendPacketsToClients(&g_ServerState, packetsToSend,
                    serverMemory.outgoingPacketCount);

                serverAccumulator -= dt;
            }
        }

        InputBeginFrame(&input);
        glfwPollEvents();

        if (WasPressed(input.buttonStates[KEY_F1]))
        {
            StartServer(18000);
        }
        if (WasPressed(input.buttonStates[KEY_F2]))
        {
            StartClient("127.0.0.1", 18000);
        }

        // Fixed client update
        while (clientAccumulator >= dt)
        {
            Packet packets[4];
            Packet packetToSend;
            u32 count = 0;
            if (g_IsClientRunning)
            {
                count = ReceivePacketsFromServer(
                    &g_ClientState, packets, ArrayCount(packets));
            }

            memory.incomingPackets = packets;
            memory.incomingPacketCount = count;
            memory.outgoingPackets = &packetToSend;
            memory.maxOutgoingPackets = 1;

            gameLibrary.clientFixedUpdate(&input, &memory, dt);

            if (g_IsClientRunning)
            {
                Assert(memory.outgoingPacketCount == 1);
                SendPacketToServer(&g_ClientState, &packetToSend);
            }
            clientAccumulator -= dt;
        }

        ClearRenderCommandQueue(&memory.renderCommandQueue);

        // Update client game code
        f32 interp = clientAccumulator / dt;
        gameLibrary.clientUpdate(&input, &memory, frameTime, interp);

        ProcessRenderCommands(&rendererState, memory.renderCommandQueue);

        glfwSwapBuffers(window);
    }

    DeinitializeSockets();
    glfwTerminate();
    return 0;
}
