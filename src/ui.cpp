internal void DrawColouredQuad(RenderCommandQueue *renderCommandQueue,
    GameAssets *assets, mat4 projection, vec2 center, vec2 dimensions,
    vec4 color)
{
    RenderCommand_DrawVertexBuffer *drawQuad = RenderCommandQueuePush(
            renderCommandQueue, RenderCommand_DrawVertexBuffer);
    SetMesh(drawQuad, assets, Mesh_Quad);
    drawQuad->shaderInput.shader = Shader_ColorShadelessNoDepth;

    mat4 model = Translate(Vec3(center, 0)) * Scale(Vec3(dimensions, 1));
    PushUniformMat4(
        &drawQuad->shaderInput, UniformValue_ViewProjectionMatrix, projection);
    PushUniformMat4(&drawQuad->shaderInput, UniformValue_ModelMatrix, model);
    PushUniformVec4(&drawQuad->shaderInput, UniformValue_Color, color);
}

internal void DrawTexturedQuad(RenderCommandQueue *renderCommandQueue,
    GameAssets *assets, mat4 projection, vec2 center, vec2 dimensions,
    u32 texture)
{
    RenderCommand_DrawVertexBuffer *drawQuad = RenderCommandQueuePush(
        renderCommandQueue, RenderCommand_DrawVertexBuffer);
    SetMesh(drawQuad, assets, Mesh_Quad);
    drawQuad->shaderInput.shader = Shader_TextureShadelessNoDepth;

    mat4 model = Translate(Vec3(center, 0)) * Scale(Vec3(dimensions, 1));
    PushUniformMat4(
        &drawQuad->shaderInput, UniformValue_ViewProjectionMatrix, projection);
    PushUniformMat4(&drawQuad->shaderInput, UniformValue_ModelMatrix, model);
    PushUniformVec2(
        &drawQuad->shaderInput, UniformValue_TextureScale, Vec2(1, 1));
    PushUniformTexture2D(&drawQuad->shaderInput, UniformValue_Texture, texture);
}

inline void AddGlyphToTextBuffer(TextBuffer *buffer, FontGlyph glyph, vec2 p, vec4 color)
{
    Assert(buffer->count + 6 <= ArrayCount(buffer->vertices));
    VertexText *vertices = buffer->vertices + buffer->count;
    vertices[0].position = p + Vec2(glyph.x0, glyph.y0);
    vertices[0].textureCoordinates = Vec2(glyph.u0, glyph.v0);
    vertices[0].color = color;
    vertices[1].position = p + Vec2(glyph.x1, glyph.y0);
    vertices[1].textureCoordinates = Vec2(glyph.u1, glyph.v0);
    vertices[1].color = color;
    vertices[2].position = p + Vec2(glyph.x1, glyph.y1);
    vertices[2].textureCoordinates = Vec2(glyph.u1, glyph.v1);
    vertices[2].color = color;


    vertices[3].position = p + Vec2(glyph.x1, glyph.y1);
    vertices[3].textureCoordinates = Vec2(glyph.u1, glyph.v1);
    vertices[3].color = color;
    vertices[4].position = p + Vec2(glyph.x0, glyph.y1);
    vertices[4].textureCoordinates = Vec2(glyph.u0, glyph.v1);
    vertices[4].color = color;
    vertices[5].position = p + Vec2(glyph.x0, glyph.y0);
    vertices[5].textureCoordinates = Vec2(glyph.u0, glyph.v0);
    vertices[5].color = color;
    buffer->count += 6;
}

internal void DrawString(RenderCommandQueue *renderCommandQueue,
    GameAssets *assets, TextBuffer *textBuffer, const char *text, vec2 position,
    mat4 projection, vec4 color)
{
    Font font = GetFont(assets, Font_InconsolataRegular32);
    f32 advance = font.glyphs[1].x1 - font.glyphs[1].x0;

    u32 start = textBuffer->count;
    f32 x = 0.0f;
    while (*text)
    {
        u8 index = (u8)(*text);
        Assert(index >= 32);
        index -= 32;
        Assert(index < font.glyphCount);
        FontGlyph glyph = font.glyphs[index];

        if (index > 0) // space
        {
            AddGlyphToTextBuffer(
                textBuffer, glyph, position + Vec2(x, 0), color);
            x += glyph.x1 - glyph.x0;
        }
        else
        {
            x += advance;
        }

        text++;
    }

    RenderCommand_DrawVertexBuffer *drawString = RenderCommandQueuePush(
            renderCommandQueue, RenderCommand_DrawVertexBuffer);
    drawString->id = VertexBuffer_Text;
    drawString->count = textBuffer->count - start;
    drawString->first = start;
    drawString->primitive = Primitive_Triangles; // TODO: Use triangle list to save 2 vertices per quad

    drawString->shaderInput.shader = Shader_Text;
    PushUniformTexture2D(&drawString->shaderInput, UniformValue_GlyphSheet, font.texture);
    PushUniformMat4(&drawString->shaderInput, UniformValue_Mvp, projection);

    // FIXME: Incoming bad times, need to process the UpdateVertexBuffer render
    // command before we do the DrawVertexBuffer commands (somehow this is working regardless!)
}
