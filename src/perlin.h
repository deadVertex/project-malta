#pragma once

f32 PerlinImproved(f32 x, f32 y, f32 z);
f32 OctavePerlin(f32 x, f32 y, f32 z, u32 octaves, f32 persistence);
