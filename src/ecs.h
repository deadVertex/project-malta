#pragma once

struct NetEntityIdBitset
{
    u64 data;
};

struct TransformComponent
{
    vec3 position;
    vec3 scale;
    quat rotation;
};

struct StaticMeshComponent
{
    b32 isVisible;
    b32 useViewModelProjectionMatrix; // TODO: Should be own component
    u32 meshes[2];
    u32 materials[2];
    u32 count;
};

struct NetworkReplicationComponent
{
    NetEntityId netEntityId; 
    LerpBuffer lerpBuffer;
    PlayerSnapshot snapshots[2];
    u8 priority;
    u8 type;
};

struct BoxCollisionComponent
{
    vec3 halfDims;
};

struct EnemyComponent
{
    f32 timeSinceLastAttack;
    f32 sineT;
};

struct HealthComponent
{
    i32 currentHealth;
    i32 maxHealth;
};

struct TerrainComponent
{
    u32 heightMap;
};

struct InventoryItemComponent
{
    EntityId owner;
    u32 slotIndex;
    u32 itemId;
    u32 quantity;
};

struct InventoryOwnerComponent
{
    EntityId openContainer;
    u32 activeEquipmentSlot;
};

// TODO: Support tag components
struct ContainerComponent
{
    u32 __ignored;
};

struct TriangleCollisionMeshComponent
{
    u32 triangleMeshId;
};

struct DeployableComponent
{
    u32 type;
};

// TODO: Make this a hash table
struct ComponentTable
{
    EntityId *keys;
    void *values;
    u32 count;
    u32 capacity;
    u32 objectSize;
};

enum
{
    TransformComponentTypeId,
    StaticMeshComponentTypeId,
    NetworkReplicationComponentTypeId,
    BoxCollisionComponentTypeId,
    EnemyComponentTypeId,
    TerrainComponentTypeId,
    InventoryItemComponentTypeId,
    InventoryOwnerComponentTypeId,
    ContainerComponentTypeId,
    HealthComponentTypeId,
    TriangleCollisionMeshComponentTypeId,
    DeployableComponentTypeId,
    MAX_COMPONENTS,
};

#define ENTITY_DELETION_QUEUE_LENGTH 64

struct EntityWorld
{
    EntityId entities[MAX_ENTITIES];
    u32 entityCount;
    EntityId nextEntityId;

    ComponentTable tables[MAX_COMPONENTS];

    EntityId deletionQueue[ENTITY_DELETION_QUEUE_LENGTH];
    u32 deletionQueueLength;

    NetEntityIdBitset netEntityIdBitset;
    NetEntityId nextNetEntityId;
};

