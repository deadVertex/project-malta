#pragma once

enum
{
    Item_None,
    Item_PistolAmmo,
    Item_Pistol,
    Item_Shotgun,
    Item_BuildingPlan,
    MAX_ITEM_IDS,
};

struct InventorySlot
{
    EntityId entity;
    u32 itemId;
    u32 quantity;
};

#define INVENTORY_SLOT_WIDTH 80.0f
#define INVENTORY_SLOT_OUTER_BORDER 8.0f
#define INVENTORY_SLOT_INNER_BORDER 6.0f

struct ItemData
{
    u32 icon;
    b32 isStackable;
    u32 viewModel;
};
