#include "debug_drawing.cpp"
#include "collision_detection.cpp"

RaycastResult RayIntersectTriangleMesh(TriangleMesh triangleMesh,
    vec3 position, quat rotation, vec3 scale, vec3 start, vec3 end,
    b32 enableDebugDrawing, b32 useTree)
{
#if 0
    if (enableDebugDrawing)
    {
        Assert(triangleMesh.root);
        DrawBox(triangleMesh.root->min, triangleMesh.root->max, Vec4(0, 1, 0, 1));
    }
#endif

    u64 meshStart = __rdtsc();

    RaycastResult result = {};
    result.tmin = -1.0f;

    if (useTree && triangleMesh.root != NULL)
    {
        if (enableDebugDrawing)
        {
#if 1
            for (u32 triangleIndex = 0; triangleIndex < triangleMesh.indexCount / 3;
                 ++triangleIndex)
            {
                u32 a = triangleMesh.indices[triangleIndex * 3];
                u32 b = triangleMesh.indices[triangleIndex * 3 + 1];
                u32 c = triangleMesh.indices[triangleIndex * 3 + 2];

                vec3 p[3];
                p[0] = triangleMesh.vertices[a];
                p[1] = triangleMesh.vertices[b];
                p[2] = triangleMesh.vertices[c];

#ifdef DEBUG_DRAW
                DrawTriangle(p[2], p[1], p[0], Vec4(0.6, 0.3, 0.1, 1));
#endif
            }
#endif
        }

        AabbTreeNode *stack[64] = {};
        u32 stackSize = 1;
        stack[0] = triangleMesh.root;

        u32 iteration = 0;
        while (stackSize > 0)
        {
            AabbTreeNode *node = stack[--stackSize];
            RaycastResult midPhaseResult =
                RayIntersectAabb(node->min, node->max, start, end);

            if (midPhaseResult.tmin > 0.0f)
            {
                if (node->isLeaf)
                {
                    // Test triangle
                    Assert(node->triangleCount == 1);
                    u32 triangleIndex = node->triangles[0];
                    u32 a = triangleMesh.indices[triangleIndex * 3];
                    u32 b = triangleMesh.indices[triangleIndex * 3 + 1];
                    u32 c = triangleMesh.indices[triangleIndex * 3 + 2];

                    vec3 p[3];
                    p[0] = triangleMesh.vertices[a];
                    p[1] = triangleMesh.vertices[b];
                    p[2] = triangleMesh.vertices[c];

#ifdef DEBUG_DRAW
                    if (enableDebugDrawing)
                    {
                        DrawTriangle(p[2], p[1], p[0], Vec4(1, 0, 1, 1));
                    }
#endif

                    RaycastResult narrowPhaseResult =
                        RayIntersectTriangle(p[2], p[1], p[0], start, end);

                    if (narrowPhaseResult.tmin > 0.0f)
                    {
                        if (result.tmin < 0.0f || narrowPhaseResult.tmin < result.tmin)
                        {
                            result = narrowPhaseResult;
                        }
                    }
                }
                else
                {
                    Assert(stackSize + 2 <= ArrayCount(stack));
                    stack[stackSize] = node->children[0];
                    stack[stackSize + 1] = node->children[1];
                    stackSize += 2;
                }
            }

#if 0
            if (enableDebugDrawing)
            {
                DrawBox(node->min, node->max,
                        midPhaseResult.tmin > 0.0f ? Vec4(0, 1, 0, 1)
                        : Vec4(0, 0, 1, 1));
            }
#endif
#if 0
            if (iteration == 41)
            {
                if (enableDebugDrawing)
                {
                    DrawBox(node->min, node->max,
                        midPhaseResult.tmin > 0.0f ? Vec4(0, 1, 0, 1)
                                                   : Vec4(0, 0, 1, 1));
                }

                break;
            }
#endif

            iteration++;
        }
    }
    else
    {
        // TODO: Cache model matrix
        mat4 model = Translate(position) * Rotate(rotation) * Scale(scale);

        u32 triangleCount = triangleMesh.indexCount / 3;
        for (u32 triangleIndex = 0; triangleIndex < triangleCount;
             ++triangleIndex)
        {
            u32 a = triangleMesh.indices[triangleIndex * 3];
            u32 b = triangleMesh.indices[triangleIndex * 3 + 1];
            u32 c = triangleMesh.indices[triangleIndex * 3 + 2];

            vec3 p[3];
            p[0] = TransformPoint(triangleMesh.vertices[a], model);
            p[1] = TransformPoint(triangleMesh.vertices[b], model);
            p[2] = TransformPoint(triangleMesh.vertices[c], model);

#ifdef DEBUG_DRAW
            if (enableDebugDrawing)
            {
                DrawTriangle(p[2], p[1], p[0], Vec4(1, 0, 1, 1));
            }
#endif

            RaycastResult localResult =
                RayIntersectTriangle(p[2], p[1], p[0], start, end);

            if (localResult.tmin > 0.0f)
            {
                if (result.tmin < 0.0f || localResult.tmin < result.tmin)
                {
                    result = localResult;
                }
            }
        }
    }

    return result;
}

struct BuildAabbTreeStackEntry
{
    u32 *triangleIndices;
    u32 triangleIndexCount;
    AabbTreeNode *parent;
    u32 depth;
};

struct BuildAabbTreeMedianBufferEntry
{
    f32 projection;
    u32 triangleIndex;
};

void BuildAabbTree(TriangleMesh *mesh, MemoryArena *treeArena, MemoryArena *tempArena)
{
    BuildAabbTreeStackEntry stack[1024] = {};
    u32 stackSize = 1;
    BuildAabbTreeStackEntry *initialEntry = &stack[0];

    u32 triangleCount = mesh->indexCount / 3;
    initialEntry->triangleIndexCount = triangleCount;
    initialEntry->triangleIndices = AllocateArray(tempArena, u32, triangleCount);
    for (u32 i = 0; i < triangleCount; ++i)
    {
        initialEntry->triangleIndices[i] = i;
    }
    initialEntry->depth = 0;

    BuildAabbTreeMedianBufferEntry *medianBuffer =
        AllocateArray(tempArena, BuildAabbTreeMedianBufferEntry, triangleCount);
    u32 medianBufferLength = 0;

    // TODO: Probably have unnecessary stack frames which have a triangleIndexCount of 0
    while (stackSize > 0)
    {
        BuildAabbTreeStackEntry current = stack[--stackSize];

        AabbTreeNode *node = AllocateStruct(treeArena, AabbTreeNode);
        ClearToZero(node, sizeof(*node));
        if (current.parent == NULL)
        {
            mesh->root = node;
        }
        else
        {
            if (current.parent->children[0] != NULL)
            {
                current.parent->children[1] = node;
            }
            else if (current.parent->children[1] == NULL)
            {
                current.parent->children[0] = node;
            }
            else
            {
                InvalidCodePath();
            }
        }

        b32 debugDraw = false; //stackSize == 1;

        // Build AABB containing all triangles for the current stack frame
        node->min = Vec3(FLT_MAX);
        node->max = Vec3(-FLT_MAX);
        for (u32 index = 0; index < current.triangleIndexCount; ++index)
        {
            u32 triangleIndex = current.triangleIndices[index];

            // Fetch vertices
            u32 i = mesh->indices[triangleIndex * 3 + 0];
            u32 j = mesh->indices[triangleIndex * 3 + 1];
            u32 k = mesh->indices[triangleIndex * 3 + 2];

            vec3 vertices[3];
            vertices[0] = mesh->vertices[i];
            vertices[1] = mesh->vertices[j];
            vertices[2] = mesh->vertices[k];

#ifdef DEBUG_DRAW
            if (debugDraw)
            {
                DrawTriangle(vertices[2], vertices[1], vertices[0], Vec4(1, 0, 1, 1), 60.0f);
            }
#endif

            for (u32 vertexIndex = 0; vertexIndex < 3; ++vertexIndex)
            {
                node->min = Min(node->min, vertices[vertexIndex]); 
                node->max = Max(node->max, vertices[vertexIndex]); 
            }
        }

        if (debugDraw)
        {
            vec4 colors[] = {
                Vec4(1, 0, 0, 1),
                Vec4(0, 1, 0, 1),
                Vec4(0, 0, 1, 1),
                Vec4(1, 1, 0, 1),
                Vec4(1, 0, 1, 1),
                Vec4(0, 1, 1, 1)
            };

            u32 colorIndex = current.depth % ArrayCount(colors);
            DrawBox(node->min, node->max, colors[colorIndex], 60.0f);
        }

        u32 maxTrianglesPerLeaf = 1;
        if (current.triangleIndexCount <= maxTrianglesPerLeaf)
        {
            // Handle leaf case
            node->isLeaf = true;
            node->triangleCount = current.triangleIndexCount;
            node->triangles =
                AllocateArray(treeArena, u32, current.triangleIndexCount);
            CopyMemory(node->triangles, current.triangleIndices,
                sizeof(u32) * current.triangleIndexCount);
        }
        else
        {
            // Handle partition case

            // Calculate longest axis
            vec3 dimensions = node->max - node->min;
            u32 axis = CalculateLargestAxis(dimensions);
            f32 partitionValue =
                (node->min.data[axis] + node->max.data[axis]) * 0.5f;

            Assert(stackSize + 2 <= ArrayCount(stack));
            BuildAabbTreeStackEntry *next = &stack[stackSize];
            stackSize += 2;

            next[0].parent = node;
            next[1].parent = node;

            next[0].depth = current.depth + 1;
            next[1].depth = current.depth + 1;

            // TODO: Use single working array for this and avoid all allocations
            next[0].triangleIndexCount = 0;
            next[0].triangleIndices =
                AllocateArray(tempArena, u32, current.triangleIndexCount);

            next[1].triangleIndexCount = 0;
            next[1].triangleIndices =
                AllocateArray(tempArena, u32, current.triangleIndexCount);

            // Partition triangles
            for (u32 index = 0; index < current.triangleIndexCount; ++index)
            {
                u32 triangleIndex = current.triangleIndices[index];

                // Fetch vertices
                u32 i = mesh->indices[triangleIndex * 3 + 0];
                u32 j = mesh->indices[triangleIndex * 3 + 1];
                u32 k = mesh->indices[triangleIndex * 3 + 2];

                vec3 vertices[3];
                vertices[0] = mesh->vertices[i];
                vertices[1] = mesh->vertices[j];
                vertices[2] = mesh->vertices[k];

                vec3 midPoint =
                    (vertices[0] + vertices[1] + vertices[2]) * (1.0f / 3.0f);

                // Calculate projection along splitting axis
                f32 projection = midPoint.data[axis];

                // Partition by spatial median on largest axis
                u32 partitionIndex = (projection >= partitionValue) ? 1 : 0;

                // Store triangle index in the corresponding next stack frame
                Assert(next[partitionIndex].triangleIndexCount <
                       current.triangleIndexCount);
                u32 elementIndex = next[partitionIndex].triangleIndexCount++;
                next[partitionIndex].triangleIndices[elementIndex] =
                    triangleIndex;

                // Store in median buffer in case we need to use that for
                // partitioning
                Assert(medianBufferLength < triangleCount);
                BuildAabbTreeMedianBufferEntry *medianEntry =
                    medianBuffer + medianBufferLength++;
                medianEntry->projection = projection;
                medianEntry->triangleIndex = triangleIndex;
            }

            if (next[0].triangleIndexCount == 0 || next[1].triangleIndexCount == 0)
            {
                // Handle case where all primitives lie on the same side of the
                // splitting plane by partitioning them perfectly in half by
                // calculating the object median.

                // Sort median buffer
                for (u32 pass = 0; pass < medianBufferLength; ++pass)
                {
                    b32 sorted = true;
                    for (u32 index = 0; index < medianBufferLength - 1; ++index)
                    {
                        BuildAabbTreeMedianBufferEntry *a = medianBuffer + index;        
                        BuildAabbTreeMedianBufferEntry *b = medianBuffer + index + 1;        

                        if (a->projection > b->projection)
                        {
                            BuildAabbTreeMedianBufferEntry temp = *b;
                            *b = *a;
                            *a = temp;
                            sorted = false;
                        }
                    }

                    if (sorted)
                    {
                        break;
                    }
                }

                // Find median point and copy into buffers
                u32 medianIndex = medianBufferLength / 2;
                for (u32 i = 0; i < medianIndex; ++i)
                {
                    next[0].triangleIndices[i] = medianBuffer[i].triangleIndex;
                }
                next[0].triangleIndexCount = medianIndex;

                for (u32 i = medianIndex; i < medianBufferLength; ++i)
                {
                    next[1].triangleIndices[i - medianIndex] = medianBuffer[i].triangleIndex;
                }
                next[1].triangleIndexCount = medianBufferLength - medianIndex;
            }

            medianBufferLength = 0;
        }
    }
}
