#include <cstdio>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "platform.h"
#include "asset.h"
#include "math_lib.h"
#include "renderer.h" // TODO: Move vertex definitions into asset.h

#include "image_processing.cpp"

DebugLogMessage(LogMessagePrintf)
{
    char buffer[256];
    va_list args;
    va_start(args, fmt);
    vsnprintf(buffer, sizeof(buffer), fmt, args);
    va_end(args);
    puts(buffer);
}

internal b32 ProcessHDR(void *fileData, u32 length, const char *outputFile)
{
    b32 result = false;
    b32 flipYAxis = true;
    i32 width, height, channels;
    stbi_set_flip_vertically_on_load(flipYAxis);

    f32 *pixels = stbi_loadf_from_memory(
        (const u8 *)fileData, length, &width, &height, &channels, STBI_rgb_alpha);
    if (pixels != NULL)
    {
        u32 bytesPerPixel = sizeof(vec4);
        u32 faceWidth = 512;
        u32 faceSize = faceWidth * faceWidth * bytesPerPixel;
        vec4 *faces[CUBE_MAP_FACE_COUNT]; // NOTE: Datatype must match bytesPerPixel

        vec4 faceIndexColors[CUBE_MAP_FACE_COUNT] = {
            Vec4(1.0f, 0.0f, 0.0f, 1.0f),
            Vec4(1.0f, 0.0f, 0.0f, 1.0f),
            Vec4(0.0f, 1.0f, 0.0f, 1.0f),
            Vec4(0.0f, 1.0f, 0.0f, 1.0f),
            Vec4(0.0f, 0.0f, 1.0f, 1.0f),
            Vec4(0.0f, 0.0f, 1.0f, 1.0f),
        };
        Assert(bytesPerPixel == sizeof(vec4));
        for (u32 faceIndex = 0; faceIndex < CUBE_MAP_FACE_COUNT; ++faceIndex)
        {
            faces[faceIndex] = (vec4 *)malloc(faceSize);

            for (u32 y = 0; y < faceWidth; ++y)
            {
                for (u32 x = 0; x < faceWidth; ++x)
                {
                    f32 fx = (f32)x / (f32)faceWidth;
                    f32 fy = (f32)y / (f32)faceWidth;

                    fy = 1.0f - fy;

                    // Map to -0.5 to 0.5 range
                    fx -= 0.5f;
                    fy -= 0.5f;

                    vec2 uv = SampleSphericalMap(Normalize(GetSampleDir(fx, fy, faceIndex)));

                    // Sample pixels
                    vec4 sample = SampleNearestRGBA32F(pixels, width, height, uv.x, uv.y);


                    faces[faceIndex][y * faceWidth + x] = sample;
                }
            }
        }
        stbi_image_free(pixels);

        {
            ImageFileHeader header = {};
            header.width = faceWidth;
            header.height = faceWidth;
            header.colorFormat = ColorFormat_RGBA32F;
            header.flags = ImageFileFlag_CubeMap;

            // Write out file
            FILE *f = fopen(outputFile, "wb");
            fwrite(&header, 1, sizeof(header), f);
            for (u32 i = 0; i < CUBE_MAP_FACE_COUNT; ++i)
            {
                fwrite(faces[i], faceSize, 1, f);
            }
            fclose(f);
        }

        for (u32 i = 0; i < CUBE_MAP_FACE_COUNT; ++i)
        {
            free(faces[i]);
        }

        result = true;
    }
    else
    {
        LogMessage("Failed to import pixel data");
    }

    return result;
}

internal b32 ProcessImage(void *fileData, u32 length, const char *outputFile)
{
    b32 result = false;

    b32 flipYAxis = true;
    i32 width, height, channels;
    stbi_set_flip_vertically_on_load(flipYAxis);

    u8 *pixels = stbi_load_from_memory(
        (const u8 *)fileData, length, &width, &height, &channels, STBI_rgb_alpha);
    if (pixels != NULL)
    {
        u32 bufferSize = width * height * 4;

        ImageFileHeader header = {};
        header.width = (u32)width;
        header.height = (u32)height;
        header.colorFormat = ColorFormat_SRGBA8;
        header.flags = 0;

        // Write out file
        FILE *f = fopen(outputFile, "wb");
        fwrite(&header, 1, sizeof(header), f);
        fwrite(pixels, bufferSize, 1, f);
        fclose(f);

        stbi_image_free(pixels);

        result = true;
    }
    else
    {
        LogMessage("Failed to import pixel data");
    }

    return result;
}

internal void CopySubImage(u8 *subImageData, const u8 *imageData,
    u32 subImageWidth, u32 subImageHeight, u32 numChannels, u32 xStart,
    u32 yStart, u32 imagePitch)
{
    u32 subImagePitch = subImageWidth * numChannels;
    for (u32 y = yStart; y < yStart + subImageHeight; ++y)
    {
        u32 start = y * imagePitch + xStart * numChannels;
        CopyMemory(subImageData + (y - yStart) * subImagePitch,
            imageData + start, subImagePitch);
    }
}

internal b32 ProcessCubeMap(void *fileData, u32 length, const char *outputFile)
{
    b32 result = false;
    i32 width, height, channels;
    stbi_set_flip_vertically_on_load(false);
    u8 *pixels = stbi_load_from_memory(
        (const u8 *)fileData, length, &width, &height, &channels, STBI_rgb_alpha);
    if (pixels != NULL)
    {
        u32 bytesPerPixel = 4;
        u32 faceWidth = width / 4;
        Assert(height / faceWidth == 3);
        u32 pitch = width * bytesPerPixel;
        u32 facePitch = faceWidth * bytesPerPixel;

        u32 faceSize = faceWidth * faceWidth * bytesPerPixel;
        u8 *faces[CUBE_MAP_FACE_COUNT];

        for (u32 i = 0; i < CUBE_MAP_FACE_COUNT; ++i)
        {
            faces[i] = (u8 *)malloc(faceSize);
        }

        // +X
        CopySubImage(faces[CUBE_MAP_FACE_X_POS], pixels, faceWidth, faceWidth,
            bytesPerPixel, faceWidth * 2, faceWidth, pitch);

        // -X
        CopySubImage(faces[CUBE_MAP_FACE_X_NEG], pixels, faceWidth, faceWidth,
            bytesPerPixel, 0, faceWidth, pitch);

        // +Y
        CopySubImage(faces[CUBE_MAP_FACE_Y_POS], pixels, faceWidth, faceWidth,
            bytesPerPixel, faceWidth, 0, pitch);

        // -Y
        CopySubImage(faces[CUBE_MAP_FACE_Y_NEG], pixels, faceWidth, faceWidth,
            bytesPerPixel, faceWidth, faceWidth * 2, pitch);

        // +Z
        CopySubImage(faces[CUBE_MAP_FACE_Z_POS], pixels, faceWidth, faceWidth,
            bytesPerPixel, faceWidth, faceWidth, pitch);

        // -Z
        CopySubImage(faces[CUBE_MAP_FACE_Z_NEG], pixels, faceWidth, faceWidth,
            bytesPerPixel, faceWidth * 3, faceWidth, pitch);

        ImageFileHeader header = {};
        header.width = faceWidth;
        header.height = faceWidth;
        header.colorFormat = ColorFormat_SRGBA8;
        header.flags = ImageFileFlag_CubeMap;

        // Write out file
        FILE *f = fopen(outputFile, "wb");
        fwrite(&header, 1, sizeof(header), f);
        for (u32 i = 0; i < CUBE_MAP_FACE_COUNT; ++i)
        {
            fwrite(faces[i], faceSize, 1, f);
        }
        fclose(f);

        for (u32 i = 0; i < CUBE_MAP_FACE_COUNT; ++i)
        {
            free(faces[i]);
        }
        stbi_image_free(pixels);

        result = true;
    }

    return result;
}

enum
{
    VertexData_Positions = Bit(0),
    VertexData_Normals = Bit(1),
    VertexData_Tangents = Bit(2),
    VertexData_Bitangents = Bit(3),
    VertexData_TextureCoordinates = Bit(4),
    VertexData_BoneWeights = Bit(5),
};

struct SceneMeshData
{
    vec3 *vertexPositions;
    vec3 *vertexNormals;
    vec3 *vertexTangents;
    vec3 *vertexBitangents;
    vec2 *vertexTextureCoordinates;
    u32 vertexCount;
    u32 vertexDataPresent;

    //mat4 *boneInvBindPoses;
    //u32 boneCount;

    //BoneWeightPair *boneWeightPairs;
    //u32 boneWeightPairCount;

    u32 *indices;
    u32 indexCount;
};

// NOTE: Only triangles are supported
internal u32 CopyIndicesToArray(u32 *indices, u32 length, aiMesh *mesh)
{
    u32 count = 0;
    for (u32 faceIdx = 0; faceIdx < mesh->mNumFaces; ++faceIdx)
    {
        aiFace face = mesh->mFaces[faceIdx];
        Assert(face.mNumIndices == 3);
        for (u32 j = 0; j < face.mNumIndices; ++j)
        {
            indices[count++] = face.mIndices[j];
        }
    }

    return count;
}

internal void AssimpLogCallback(const char *message, char *user)
{
    printf(message);
}

internal void ProcessMesh(void *fileData, u32 length, const char *hint, const char *outputFile)
{
    f32 scale = 1.0f;
    aiPropertyStore *properties = aiCreatePropertyStore();
    aiSetImportPropertyFloat(
        properties, AI_CONFIG_GLOBAL_SCALE_FACTOR_KEY, scale);
    const aiScene *scene =
        aiImportFileFromMemoryWithProperties((const char *)fileData, length,
            aiProcess_Triangulate | aiProcess_JoinIdenticalVertices |
                aiProcess_OptimizeMeshes | aiProcess_OptimizeGraph |
                aiProcess_LimitBoneWeights | aiProcess_GlobalScale |
                aiProcess_CalcTangentSpace,
            ".obj", properties);

    if (scene)
    {
        SceneMeshData *resultMeshData = (SceneMeshData *)malloc(scene->mNumMeshes * sizeof(SceneMeshData));
        u32 flags = VertexData_Positions | VertexData_Normals |
                    VertexData_Tangents | VertexData_Bitangents |
                    VertexData_TextureCoordinates | VertexData_BoneWeights;

        // Copy mesh data into our own data layout for further processing
        for (u32 meshIndex = 0; meshIndex < scene->mNumMeshes; ++meshIndex)
        {
            aiMesh *mesh = scene->mMeshes[meshIndex];
            SceneMeshData *meshData = resultMeshData + meshIndex;
            meshData->vertexDataPresent = flags;

            // Allocate array in mesh data for indices
            meshData->indexCount = mesh->mNumFaces * 3;
            meshData->indices = (u32 *)malloc(sizeof(u32) * meshData->indexCount); //AllocateArray(arena, u32, meshData->indexCount);

            // Copy indices into allocated array
            u32 count = CopyIndicesToArray(
                meshData->indices, meshData->indexCount, mesh);
            Assert(count == meshData->indexCount);

            // Allocate array for vertex positions
            meshData->vertexCount = mesh->mNumVertices;
            meshData->vertexPositions = (vec3 *)malloc(sizeof(vec3) * mesh->mNumVertices); //AllocateArray(arena, vec3, mesh->mNumVertices);

            // Copy vertex positions into allocated array
            for (u32 vertexIdx = 0; vertexIdx < mesh->mNumVertices; ++vertexIdx)
            {
                aiVector3D v = mesh->mVertices[vertexIdx];
                meshData->vertexPositions[vertexIdx] = Vec3(v.x, v.y, v.z);
            }

            if (flags & VertexData_Normals)
            {
                Assert(mesh->mNormals != NULL);
                meshData->vertexNormals = (vec3 *)malloc(sizeof(vec3) * mesh->mNumVertices); //AllocateArray(arena, vec3, mesh->mNumVertices);

                // Copy vertex normals into allocated array
                for (u32 vertexIdx = 0; vertexIdx < mesh->mNumVertices; ++vertexIdx)
                {
                    aiVector3D v = mesh->mNormals[vertexIdx];
                    meshData->vertexNormals[vertexIdx] = Vec3(v.x, v.y, v.z);
                }
            }

            if (flags & VertexData_Tangents)
            {
                Assert(mesh->mTangents != NULL);
                meshData->vertexTangents = (vec3 *)malloc(sizeof(vec3) * mesh->mNumVertices); //AllocateArray(arena, vec3, mesh->mNumVertices);

                // Copy vertex tangents into allocated array
                for (u32 vertexIdx = 0; vertexIdx < mesh->mNumVertices; ++vertexIdx)
                {
                    aiVector3D v = mesh->mTangents[vertexIdx];
                    meshData->vertexTangents[vertexIdx] = Vec3(v.x, v.y, v.z);
                }
            }

            if (flags & VertexData_Bitangents)
            {
                Assert(mesh->mBitangents != NULL);
                meshData->vertexBitangents = (vec3 *)malloc(sizeof(vec3) * mesh->mNumVertices); //AllocateArray(arena, vec3, mesh->mNumVertices);

                // Copy vertex bitangents into allocated array
                for (u32 vertexIdx = 0; vertexIdx < mesh->mNumVertices; ++vertexIdx)
                {
                    aiVector3D v = mesh->mBitangents[vertexIdx];
                    meshData->vertexBitangents[vertexIdx] = Vec3(v.x, v.y, v.z);
                }
            }

            if (flags & VertexData_TextureCoordinates)
            {
                // TODO: Support multiple sets of texture coordinates
                Assert(mesh->mTextureCoords[0] != NULL);

                meshData->vertexTextureCoordinates =
                    (vec2 *)malloc(sizeof(vec2) * mesh->mNumVertices);
                    //AllocateArray(arena, vec2, mesh->mNumVertices);

                // Copy vertex normals into allocated array
                for (u32 vertexIdx = 0; vertexIdx < mesh->mNumVertices; ++vertexIdx)
                {
                    aiVector3D v = mesh->mTextureCoords[0][vertexIdx];
                    meshData->vertexTextureCoordinates[vertexIdx] =
                        Vec2(v.x, v.y);
                }
            }

            /*
            if (flags & VertexData_BoneWeights)
            {
                // TODO: Probably don't need to take all vertex weights, just
                // filter when importing to the largest 4
                meshData->boneWeightPairCount = MAX_BONES * mesh->mNumVertices;
                meshData->boneWeightPairs = AllocateArray(
                    tempArena, BoneWeightPair, meshData->boneWeightPairCount);
                meshData->boneInvBindPoses =
                    AllocateArray(tempArena, mat4, result.nodeCount);
                meshData->boneCount = result.nodeCount;

                ClearToZero(meshData->boneWeightPairs,
                    meshData->boneWeightPairCount * sizeof(BoneWeightPair));
                // FIXME: While correct, it looks better if they are zeroed for
                // unknown bone inverse bind poses so that they are scaled to 0
                // and not drawn for (u32 i = 0; i < MAX_BONES; ++i)
                // meshData->boneInvBindPoses[i] = Identity();

                b32 isSet[MAX_BONES] = {false};
                for (u32 meshBoneIdx = 0; meshBoneIdx < mesh->mNumBones;
                     ++meshBoneIdx)
                {
                    aiBone *bone = mesh->mBones[meshBoneIdx];

                    // Convert bone->mName to index
                    u32 nodeIdx = 0;
                    for (nodeIdx = 0; nodeIdx < result.nodeCount; ++nodeIdx)
                    {
                        if (CompareStrings(
                                result.nodeNames[nodeIdx], bone->mName.data))
                            break;
                    }

                    // TODO: Probably don't need to decompose the offset matrix
                    Assert(nodeIdx != result.nodeCount);
                    aiVector3t<float> scaling;
                    aiQuaterniont<float> rotation;
                    aiVector3t<float> position;
                    bone->mOffsetMatrix.Decompose(scaling, rotation, position);
                    vec3 s = Vec3(scaling.x, scaling.y, scaling.z);
                    vec3 p = Vec3(position.x, position.y, position.z);
                    quat r =
                        Quat(rotation.x, rotation.y, rotation.z, rotation.w);
                    Assert(isSet[nodeIdx] == false);
                    meshData->boneInvBindPoses[nodeIdx] =
                        Translate(p) * Rotate(r) * Scale(s);
                    isSet[nodeIdx] = true;

                    for (u32 weightIdx = 0; weightIdx < bone->mNumWeights;
                         ++weightIdx)
                    {
                        aiVertexWeight weight = bone->mWeights[weightIdx];
                        Assert(weight.mVertexId < mesh->mNumVertices);
                        u32 idx = nodeIdx + (weight.mVertexId * MAX_BONES);
                        Assert(meshData->boneWeightPairs[idx].bone == 0);
                        Assert(meshData->boneWeightPairs[idx].weight == 0.0f);
                        meshData->boneWeightPairs[idx].bone = nodeIdx;
                        meshData->boneWeightPairs[idx].weight = weight.mWeight;
                        // LOG_DEBUG("%u,%u: bone = %u, weight = %g",
                        // weight.mVertexId, idx, nodeIdx, weight.mWeight);
                    }
                }
            }
            */
            aiReleaseImport(scene);
            aiReleasePropertyStore(properties);
        }

        // Write out first mesh to output file
        MeshFileHeader header = {};
        header.vertexLayout = VertexLayout_Vertex;
        header.vertexCount = resultMeshData[0].vertexCount;
        header.indexCount = resultMeshData[0].indexCount;
        FILE *f = fopen(outputFile, "wb");
        fwrite(&header, 1, sizeof(header), f);

        // TODO: Handle different vertex layouts
        Assert(header.vertexLayout == VertexLayout_Vertex);
        Vertex *vertices = (Vertex *)malloc(sizeof(Vertex) * header.vertexCount);
        for (u32 i = 0; i < header.vertexCount; ++i)
        {
            vertices[i].position = resultMeshData[0].vertexPositions[i];
            vertices[i].normal = resultMeshData[0].vertexNormals[i];
            vertices[i].tangent = resultMeshData[0].vertexTangents[i];
            vertices[i].bitangent = resultMeshData[0].vertexBitangents[i];
            vertices[i].textureCoordinates = resultMeshData[0].vertexTextureCoordinates[i];
        }
        fwrite(vertices, sizeof(Vertex) * header.vertexCount, 1, f);

        // Write out indices
        fwrite(resultMeshData[0].indices, sizeof(u32) * header.indexCount, 1, f);
        fclose(f);

        // FIXME: We should probably free the memory we allocated for the mesh data, maybe?
    }
    else
    {
        LogMessage("Failed to import mesh scene data");
    }
}

internal void ProcessFont(void *fileData, u32 length, const char *outputFile)
{
    u32 width = 512;
    u32 height = 512;
    u8 *bitmap = (u8 *)malloc(width * height);
    stbtt_bakedchar characterData[96];
    stbtt_BakeFontBitmap((u8 *)fileData, 0, 32.0, bitmap, width, height, 32,
        ArrayCount(characterData), characterData);

    // Convert to RGBA8 color format
    u32 *pixels = (u32 *)malloc(width * height * sizeof(u32));
    for (u32 y = 0; y < height; ++y)
    {
        for (u32 x = 0; x < width; ++x)
        {
            u8 alpha = bitmap[y * width + x];
            pixels[y * width + x] = (alpha << 24) | (alpha << 16) | (alpha << 8) | alpha;
        }
    }
    free(bitmap);

    FontGlyph glyphs[ArrayCount(characterData)] = {};
    for (u32 i = 0; i < ArrayCount(characterData); ++i)
    {
         stbtt_aligned_quad q;

         f32 x = 0.0f;
         f32 y = 0.0f;
         stbtt_GetBakedQuad(
             characterData, width, height, (char)i, &x, &y, &q, 1);

         glyphs[i].x0 = q.x0;
         glyphs[i].y0 = -q.y0;
         glyphs[i].u0 = q.s0;
         glyphs[i].v0 = q.t0;
         glyphs[i].x1 = q.x1;
         glyphs[i].y1 = -q.y1;
         glyphs[i].u1 = q.s1;
         glyphs[i].v1 = q.t1;
    }

    FontFileHeader header = {};
    header.width = width;
    header.height = height;
    header.characterCount = ArrayCount(characterData);
    FILE *f = fopen(outputFile, "wb");
    fwrite(&header, 1, sizeof(header), f);
    fwrite(glyphs, 1, sizeof(glyphs), f);
    fwrite(pixels, 1, width * height * sizeof(u32), f);
    free(pixels);
    fclose(f);
}

struct ReadEntireFileResult
{
    void *contents;
    u32 length;
};

internal ReadEntireFileResult ReadEntireFile(const char *path)
{
    ReadEntireFileResult result = {};
    FILE *f = fopen(path, "rb");
    if (f != NULL)
    {
        fseek(f, 0, SEEK_END);
        result.length = ftell(f);
        fseek(f, 0, SEEK_SET);
        result.contents = malloc(result.length);
        size_t ret = fread(result.contents, 1, result.length, f);
        Assert(ret == result.length);
        fclose(f);
    }
    else
    {
        LogMessage("Failed to open file %s", path);
    }

    return result;
}

internal void FreeFileResult(ReadEntireFileResult result)
{
    free(result.contents);
}

int main(int argc, char **argv)
{
    LogMessage = &LogMessagePrintf;

    aiLogStream logStream = {};
    logStream.callback = &AssimpLogCallback;
    aiAttachLogStream(&logStream);

    LogMessage("Building assets...");
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/dev_grid512.png");
        ProcessImage(fileResult.contents, fileResult.length, "dev_grid512.texture");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/grass_alpha.png");
        ProcessImage(fileResult.contents, fileResult.length, "grass_alpha.texture");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/rock01.obj");
        ProcessMesh(fileResult.contents, fileResult.length, ".obj", "rock01.mesh");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/pistol.obj");
        ProcessMesh(fileResult.contents, fileResult.length, ".obj", "pistol.mesh");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/target.obj");
        ProcessMesh(fileResult.contents, fileResult.length, ".obj", "target.mesh");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/warehouse.obj");
        ProcessMesh(fileResult.contents, fileResult.length, ".obj", "warehouse.mesh");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/shack01.obj");
        ProcessMesh(fileResult.contents, fileResult.length, ".obj", "shack01.mesh");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/shotgun.obj");
        ProcessMesh(fileResult.contents, fileResult.length, ".obj", "shotgun.mesh");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/tree/body.obj");
        ProcessMesh(fileResult.contents, fileResult.length, ".obj", "tree_body.mesh");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/tree/leaves.obj");
        ProcessMesh(fileResult.contents, fileResult.length, ".obj", "tree_leaves.mesh");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/grass.obj");
        ProcessMesh(fileResult.contents, fileResult.length, ".obj", "grass.mesh");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/skybox_clear.png");
        ProcessCubeMap(fileResult.contents, fileResult.length, "skybox_clear.texture");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/PavingStones055_1K_Color.jpg");
        ProcessImage(fileResult.contents, fileResult.length, "PavingStones055_1K_Color.texture");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/PavingStones055_1K_Normal.jpg");
        ProcessImage(fileResult.contents, fileResult.length, "PavingStones055_1K_Normal.texture");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/Ground003_1K_Color.jpg");
        ProcessImage(fileResult.contents, fileResult.length, "Ground003_1K_Color.texture");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile(
            "../content/tree/TexturesCom_BarkDecidious0194_1_seamless_S.jpg");
        ProcessImage(fileResult.contents, fileResult.length, "tree_bark.texture");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile(
            "../content/tree/TexturesCom_Branches0018_1_masked_M.png");
        ProcessImage(fileResult.contents, fileResult.length, "tree_leaves.texture");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile(
            "../content/icons/pistol_ammo_icon.png");
        ProcessImage(fileResult.contents, fileResult.length, "icons_pistol_ammo.texture");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile(
            "../content/icons/pistol_icon.png");
        ProcessImage(fileResult.contents, fileResult.length, "icons_pistol.texture");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile(
            "../content/icons/shotgun_icon.png");
        ProcessImage(fileResult.contents, fileResult.length, "icons_shotgun.texture");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile(
            "../content/Inconsolata-Regular.ttf");
        ProcessFont(fileResult.contents, fileResult.length, "inconsolata_regular_32.font");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/irradiance.hdr");
        ProcessHDR(fileResult.contents, fileResult.length, "irradiance.texture");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile(
            "../content/ibl_brdf_lut.png");
        ProcessImage(fileResult.contents, fileResult.length, "ibl_brdf_lut.texture");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/radiance_0_1024x768.tga");
        ProcessCubeMap(fileResult.contents, fileResult.length, "radiance_0.texture");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/radiance_1_512x384.tga");
        ProcessCubeMap(fileResult.contents, fileResult.length, "radiance_1.texture");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/radiance_2_256x192.tga");
        ProcessCubeMap(fileResult.contents, fileResult.length, "radiance_2.texture");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/radiance_3_128x96.tga");
        ProcessCubeMap(fileResult.contents, fileResult.length, "radiance_3.texture");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/radiance_4_64x48.tga");
        ProcessCubeMap(fileResult.contents, fileResult.length, "radiance_4.texture");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/radiance_5_32x24.tga");
        ProcessCubeMap(fileResult.contents, fileResult.length, "radiance_5.texture");
        FreeFileResult(fileResult);
    }
    {
        ReadEntireFileResult fileResult = ReadEntireFile("../content/radiance_6_16x12.tga");
        ProcessCubeMap(fileResult.contents, fileResult.length, "radiance_6.texture");
        FreeFileResult(fileResult);
    }
    return 0;
}
