#include "game.h"
#include "terrain.h"

#include "collision_detection_optimized.cpp"
#include "perlin.cpp"

HeightMap GenerateHeightMap(MemoryArena *arena, u32 width, u32 height)
{
    HeightMap result = {};

    result.width = width;
    result.height = height;
    result.values = AllocateArray(arena, f32, width * height);

    f32 scale = 8.0f;
    f32 minHeight = 0.4f;
    f32 maxHeight = 0.8f;
    f32 heightScale = 0.12f; //1.0f;
    for (u32 y = 0; y < height; ++y)
    {
        for (u32 x = 0; x < width; ++x)
        {
            f32 fx = (f32)x / (f32)width;
            f32 fy = (f32)y / (f32)height;

            fx *= scale;
            fy *= scale;

#if 1
            // Rigdge noise
            //f32 value = -1.0f * Abs(OctavePerlin(fx, fy, 1.0f, 2, 0.2f) - 0.5f) + 0.5f;
            f32 value = OctavePerlin(fx, fy, 1.243f, 4, 0.4f);
            value = Min(Max(value, minHeight), maxHeight) * heightScale;
            result.values[y * width + x ] = value;
#else
            result.values[y * width + x ] = (fx < 0.5f && fy < 0.5) ? 0.5f : 0.0f;
#endif
        }
    }

    return result;
}

TerrainNormalMap GenerateTerrainNormalMap(HeightMap heightMap, MemoryArena *arena)
{
    TerrainNormalMap normalMap = {};
    normalMap.values = AllocateArray(arena, vec3, heightMap.width * heightMap.height);
    normalMap.width = heightMap.width;
    normalMap.height = heightMap.height;

    for (i32 y = 0; y < (i32)heightMap.height; ++y)
    {
        for (i32 x = 0; x < (i32)heightMap.width; ++x)
        {
            i32 x0 = Max(x - 1, 0);
            i32 x1 = Min(x + 1, heightMap.width - 1);
            i32 y0 = Max(y - 1, 0);
            i32 y1 = Min(y + 1, heightMap.height - 1);

            f32 t[4];
            t[0] = heightMap.values[y * heightMap.width + x0];
            t[1] = heightMap.values[y * heightMap.width + x1];
            t[2] = heightMap.values[y0 * heightMap.width + x];
            t[3] = heightMap.values[y1 * heightMap.width + x];
            f32 dx = 0.5f * (t[1] - t[0]);
            f32 dy = 0.5f * (t[3] - t[2]);

            f32 mul = 1.0f;
            dx *= (f32)normalMap.width * mul;
            dy *= (f32)normalMap.height * mul;

            vec3 normal = Vec3(-dx, -dy, 1.0);
            normal = Normalize(normal);
            normalMap.values[y * normalMap.width + x] = normal;
        }
    }

    return normalMap;
}

u32* ConvertNormalMapToRGBA8(TerrainNormalMap normalMap, MemoryArena *arena)
{
    u32 *pixels = AllocateArray(arena, u32, normalMap.width * normalMap.height);
    for (u32 y = 0; y < normalMap.height; ++y)
    {
        for (u32 x = 0; x < normalMap.width; ++x)
        {
            vec3 normal = normalMap.values[y * normalMap.width + x];
            normal = normal * 0.5f + Vec3(0.5f);
            pixels[y * normalMap.width + x] = ToColor(Vec4(normal, 0.0f));
        }
    }

    return pixels;
}
