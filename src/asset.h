#pragma once

enum
{
    ImageFileFlag_None    = 0x0,
    ImageFileFlag_CubeMap = 0x1,
};

enum
{
    CUBE_MAP_FACE_X_POS,
    CUBE_MAP_FACE_X_NEG,
    CUBE_MAP_FACE_Y_POS,
    CUBE_MAP_FACE_Y_NEG,
    CUBE_MAP_FACE_Z_POS,
    CUBE_MAP_FACE_Z_NEG,
    CUBE_MAP_FACE_COUNT,
};

#pragma pack(push,1)
struct ImageFileHeader
{
    u32 width;
    u32 height;
    u32 colorFormat;
    u32 flags;
};

struct MeshFileHeader
{
    u32 vertexLayout;
    u32 vertexCount;
    u32 indexCount;
};

struct FontGlyph
{
    f32 x0, y0, u0, v0; // top-left
    f32 x1, y1, u1, v1; // bottom-right
};

struct FontFileHeader
{
    u32 width;
    u32 height;
    u32 characterCount;
};
#pragma pack(pop)
