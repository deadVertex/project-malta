internal void DrawLine(DebugDrawingSystem *system, vec3 start, vec3 end,
    vec4 color, f32 lifeTime = 0.0f)
{
    if (system->lineCount < ArrayCount(system->lines))
    {
        DebugLine *line = system->lines + system->lineCount++;
        line->start = start;
        line->end = end;
        line->color = color;
        line->lifeTime = lifeTime;
    }
}

internal u32 GetVertices(
    DebugDrawingSystem *system, VertexPC *vertices, u32 capacity)
{
    u32 count = 0;
    u32 maxLines = MinU32(capacity / 2, system->lineCount);
    for (u32 lineIndex = 0; lineIndex < maxLines; ++lineIndex)
    {
        DebugLine *line = system->lines + lineIndex;

        VertexPC *v0 = vertices + count++;
        VertexPC *v1 = vertices + count++;

        v0->position = line->start;
        v0->color = line->color;
        v1->position = line->end;
        v1->color = line->color;
    }

    return count;
}

internal void AgeOut(DebugDrawingSystem *system, f32 dt)
{
    u32 lineIndex = 0;
    while (lineIndex < system->lineCount)
    {
        DebugLine *line = system->lines + lineIndex;

        // Only age out debug elements with the default lifetime after they have
        // had at least 1 frame to be drawn
        if (line->lifeTime < 0.0f)
        {
            *line = system->lines[--system->lineCount];
        }
        else
        {
            line->lifeTime -= dt;
            lineIndex++;
        }
    }
}

internal void DrawPoint(DebugDrawingSystem *system, vec3 p, f32 size,
    vec4 color, f32 lifeTime = 0.0f)
{
    DrawLine(
        system, p - Vec3(size, 0, 0), p + Vec3(size, 0, 0), color, lifeTime);
    DrawLine(
        system, p - Vec3(0, size, 0), p + Vec3(0, size, 0), color, lifeTime);
    DrawLine(
        system, p - Vec3(0, 0, size), p + Vec3(0, 0, size), color, lifeTime);
}

internal void DrawBox(DebugDrawingSystem *system, vec3 min, vec3 max,
    vec4 color, f32 lifeTime = 0.0f)
{
    // clang-format off
    vec3 vertices[8] = {
        { min.x, min.y, min.z },
        { max.x, min.y, min.z },
        { max.x, min.y, max.z },
        { min.x, min.y, max.z },

        { min.x, max.y, min.z },
        { max.x, max.y, min.z },
        { max.x, max.y, max.z },
        { min.x, max.y, max.z },
    };

    u32 indices[] = {
        0, 1,
        1, 2,
        2, 3,
        3, 0,

        4, 5,
        5, 6,
        6, 7,
        7, 4,

        0, 4,
        1, 5,
        2, 6,
        3, 7
    };
    // clang-format on

    for (u32 i = 0; i < ArrayCount(indices); i += 2)
    {
        u32 index0 = indices[i];
        u32 index1 = indices[i + 1];
        vec3 start = vertices[index0];
        vec3 end = vertices[index1];
        DrawLine(system, start, end, color, lifeTime);
    }
}

internal void DrawCircle(DebugDrawingSystem *system, vec3 center, f32 radius,
    u32 segmentCount, quat rotation, vec4 color, f32 lifeTime = 0.0f)
{
    f32 increment = (2.0f * PI) / segmentCount;
    for (u32 i = 0; i < segmentCount; ++i)
    {
        f32 t0 = increment * i;
        f32 t1 = Fmod(increment * (i + 1), 2.0f * PI);

        f32 x0 = Sin(t0);
        f32 y0 = Cos(t0);

        f32 x1 = Sin(t1);
        f32 y1 = Cos(t1);

        vec3 q0 = Vec3(x0, 0, y0) * radius;
        vec3 q1 = Vec3(x1, 0, y1) * radius;

        vec3 r0 = RotateVector(q0, rotation);
        vec3 r1 = RotateVector(q1, rotation);

        vec3 p0 = center + r0;
        vec3 p1 = center + r1;

        DrawLine(system, p0, p1, color, lifeTime);
    }
}

internal void DrawSphere(DebugDrawingSystem *system, vec3 center, f32 radius,
    vec4 color, f32 lifeTime = 0.0f)
{
    quat xz = Quat();
    quat xy = Quat(Vec3(1, 0, 0), PI * 0.5f);
    quat yz = Quat(Vec3(0, 0, 1), PI * 0.5f);

    u32 segmentCount = 24;

    DrawCircle(
        system, center, radius, segmentCount, xz, color, lifeTime);
    DrawCircle(
        system, center, radius, segmentCount, xy, color, lifeTime);
    DrawCircle(
        system, center, radius, segmentCount, yz, color, lifeTime);
}

internal void DrawGrid(DebugDrawingSystem *system, f32 lifeTime = 0.0f)
{
    // Draw ground grid
    f32 gridRadius = 512.0f;
    f32 gridIncrement = 1.0f;
    vec4 gridColor = Vec4(0.2, 0.2, 0.2, 1.0);
    for (f32 x = -gridRadius; x < gridRadius; x += gridIncrement)
    {
        DrawLine(system, Vec3(x, 0.0f, -gridRadius), Vec3(x, 0.0f, gridRadius),
            gridColor, lifeTime);
    }
    for (f32 z = -gridRadius; z < gridRadius; z += gridIncrement)
    {
        DrawLine(system, Vec3(-gridRadius, 0.0f, z), Vec3(gridRadius, 0.0f, z),
            gridColor, lifeTime);
    }
}

inline void DrawLine(vec3 start, vec3 end, vec4 color, f32 lifeTime = 0.0f)
{
    if (g_DebugDrawingSystem != NULL)
    {
        DrawLine(g_DebugDrawingSystem, start, end, color, lifeTime);
    }
}

inline void DrawPoint(vec3 p, f32 size, vec4 color, f32 lifeTime = 0.0f)
{
    if (g_DebugDrawingSystem != NULL)
    {
        DrawPoint(g_DebugDrawingSystem, p, size, color, lifeTime);
    }
}

inline void DrawBox(vec3 min, vec3 max, vec4 color, f32 lifeTime = 0.0f)
{
    if (g_DebugDrawingSystem != NULL)
    {
        DrawBox(g_DebugDrawingSystem, min, max, color, lifeTime);
    }
}

inline void DrawSphere(vec3 center, f32 radius, vec4 color, f32 lifeTime = 0.0f)
{
    if (g_DebugDrawingSystem != NULL)
    {
        DrawSphere(g_DebugDrawingSystem, center, radius, color, lifeTime);
    }
}
