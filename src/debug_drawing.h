struct DebugLine
{
    vec3 start;
    vec3 end;
    vec4 color;
    f32 lifeTime;
};

struct DebugDrawingSystem
{
    DebugLine lines[0x1000];
    u32 lineCount;
};

global DebugDrawingSystem *g_DebugDrawingSystem;
