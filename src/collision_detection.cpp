struct RaycastResult
{
    f32 tmin;
    vec3 normal;
};

// Defined in collision_detection_optimized.cpp
RaycastResult RayIntersectTriangleMesh(TriangleMesh triangleMesh,
    vec3 position, quat rotation, vec3 scale, vec3 start, vec3 end,
    b32 enableDebugDrawing = false, b32 useTree = false);
void BuildAabbTree(TriangleMesh *mesh, MemoryArena *treeArena, MemoryArena *tempArena);

inline RaycastResult RayIntersectAabb(vec3 boxMin, vec3 boxMax, vec3 start, vec3 end)
{
    f32 tmin = 0.0f;
    f32 tmax = 1.0f;

    b32 negateNormal = false;
    u32 normalIdx = 0;

    vec3 dir = end - start;
    for (u32 axis = 0; axis < 3; ++axis)
    {
        f32 s = start.data[axis];
        f32 d = dir.data[axis];
        f32 min = boxMin.data[axis];
        f32 max = boxMax.data[axis];

        if (Abs(d) < EPSILON)
        {
            // Ray is parallel to axis plane
            if (s < min || s > max)
            {
                // No collision
                tmin = -1.0f;
                break;
            }
        }
        else
        {
            f32 a = (min - s) / d;
            f32 b = (max - s) / d;

            f32 t0 = Min(a, b);
            f32 t1 = Max(a, b);

            if (t0 > tmin)
            {
                negateNormal = (d >= 0.0f);
                normalIdx = axis;
                tmin = t0;
            }

            tmax = Min(tmax, t1);

            if (tmin > tmax)
            {
                // No collision
                tmin = -1.0f;
                break;
            }
        }
    }

    RaycastResult result = {};
    result.tmin = tmin;
    if (result.tmin > 0.0f)
    {
        result.normal.data[normalIdx] = negateNormal ? -1.0f : 1.0f;
    }

    return result;
}

// NOTE: This returns values outside of the 0-1 range!!!!
inline f32 RayPlaneIntersectT(vec3 normal, f32 distance, vec3 start, vec3 end)
{
    vec3 direction = end - start;
    f32 denom = Dot(normal, direction);

    // NOTE: May divide by 0 but should still give correct result.
    f32 t = (distance - Dot(normal, start)) / denom;

    return t;
}

// NOTE: This expects vertices in clockwise winding order!
internal RaycastResult RayIntersectTriangle(
    vec3 a, vec3 b, vec3 c, vec3 start, vec3 end)
{
    RaycastResult result = {};
    result.tmin = -1.0f;

    // Calculate triangle face plane
    vec3 ab = b - a;
    vec3 ca = a - c;

    vec3 normal = Cross(ab, ca);
    Assert(LengthSq(normal) > 0.0f);
    normal = Normalize(normal);
    f32 distance = Dot(normal, a);

    vec3 direction = end - start;

    // Ignore any collision if we are not raycasting in the direction opposite
    // to the normal.
    // This creates the effect of single sided triangles, double sided would be
    // more complex to implement.
    if (Dot(normal, direction) <= 0.0f)
    {
        f32 t = RayPlaneIntersectT(normal, distance, start, end);
        if (t >= 0.0f && t < 1.0f)
        {
            vec3 normalAb = Normalize(Cross(normal, ab));
            vec3 normalCa = Normalize(Cross(normal, ca));

            vec3 bc = c - b;
            vec3 normalBc = Normalize(Cross(normal, bc));

#ifdef COLLISION_DEBUG_RAY_TRIANGLE
            {
                vec3 centroidAb = (a + b) * 0.5f;
                DrawLine(centroidAb, centroidAb + normalAb * 0.5f,
                    Vec3(1, 0, 0), 30.0f);

                vec3 centroidCa = (c + a) * 0.5f;
                DrawLine(centroidCa, centroidCa + normalCa * 0.5f,
                    Vec3(1, 0, 0), 30.0f);

                vec3 centroidBc = (b + c) * 0.5f;
                DrawLine(centroidBc, centroidBc + normalBc * 0.5f,
                    Vec3(1, 0, 0), 30.0f);
            }
#endif

            vec3 p = start + direction * t;
            if (Dot(normalAb, p - a) <= 0.0f)
            {
                if (Dot(normalCa, p - a) <= 0.0f)
                {
                    if (Dot(normalBc, p - b) <= 0.0f)
                    {
                        result.tmin = t;
                        result.normal = normal;
                    }
                }
            }
        }
    }

    return result;
}

inline RaycastResult RayIntersectSphere(
    vec3 center, f32 radius, vec3 start, vec3 end)
{
    RaycastResult result = {};
    result.tmin = -1.0f;

    vec3 delta = end - start;
    vec3 dir = Normalize(delta);
    f32 length = Length(delta);

    vec3 m = start - center; // Use sphere center as origin

    f32 a = Dot(dir, dir);
    f32 b = 2.0f * Dot(m, dir);
    f32 c = Dot(m, m) - radius * radius;

    f32 d = b * b - 4.0f * a * c; // Discriminant

    f32 t = F32_MAX;
    if (d > 0.0f)
    {
        f32 denom = 2.0f * a;
        f32 t0 = (-b - Sqrt(d)) / denom;
        f32 t1 = (-b + Sqrt(d)) / denom;

        t = t0; // Pick the entry point

        if (t <= length && t >= 0.0f)
        {
            result.tmin = t / length;
            Assert(result.tmin >= 0.0f);
            Assert(result.tmin <= 1.0f);
            vec3 hitPoint = start + dir * t;
            result.normal = Normalize(hitPoint - center);
        }
    }

    return result;
}

#ifdef DEBUG_DRAW
inline void DrawTriangle(vec3 a, vec3 b, vec3 c, vec4 color, f32 lifeTime = 0.0f)
{
    DrawLine(a, b, color, lifeTime);
    DrawLine(b, c, color, lifeTime);
    DrawLine(c, a, color, lifeTime);

    // Calculate triangle face plane, copied from RayIntersectTriangle
    vec3 ab = b - a;
    vec3 ca = a - c;

    vec3 normal = Cross(ab, ca);
    normal = Normalize(normal);

    vec3 centroid = (a + b + c) * (1.0f / 3.0f);

    f32 normalLength = 1.0f;
    DrawLine(
        centroid, centroid + normal * normalLength, Vec4(1, 0, 1, 1), lifeTime);
}


internal void DrawTriangleCollisionMesh(TriangleMesh triangleMesh)
{
    u32 triangleCount = triangleMesh.indexCount / 3;
    for (u32 triangleIndex = 0; triangleIndex < triangleCount; ++triangleIndex)
    {
        u32 a = triangleMesh.indices[triangleIndex * 3];
        u32 b = triangleMesh.indices[triangleIndex * 3 + 1];
        u32 c = triangleMesh.indices[triangleIndex * 3 + 2];

        vec3 p[3];
        p[0] = triangleMesh.vertices[a];
        p[1] = triangleMesh.vertices[b];
        p[2] = triangleMesh.vertices[c];

        DrawTriangle(p[0], p[1], p[2], Vec4(1, 0, 1, 1));
    }
}

internal void PrintTree(TriangleMesh mesh)
{
        AabbTreeNode *stack[64] = {};
        u32 stackSize = 1;
        stack[0] = mesh.root;

        AabbTreeNode *next[1024] = {};
        u32 nextSize = 0;

        u32 depth = 0;
        u32 iteration = 0;
        while (stackSize > 0)
        {
            AabbTreeNode *node = stack[--stackSize];

            if (node->isLeaf)
            {
                LogMessage("%u: [LEAF]: %u", depth, node->triangles[0]);
            }
            else
            {
                LogMessage("%u: [PART]: ", depth);
                Assert(nextSize + 2 <= ArrayCount(next));
                next[nextSize] = node->children[0];
                next[nextSize + 1] = node->children[1];
                nextSize += 2;
            }

            if (stackSize == 0)
            {
                CopyMemory(stack, next, nextSize * sizeof(AabbTreeNode*));
                stackSize = nextSize;

                nextSize = 0;
                depth++;
            }
        }
}
#endif
