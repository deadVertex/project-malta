@echo off

set Defines=-DDEBUG_FIXED_ADDRESSES -DPLATFORM_WINDOWS
set ExeLibraries=..\windows-dependencies\lib\glew32.lib ..\windows-dependencies\lib\glfw3dll.lib user32.lib winmm.lib gdi32.lib opengl32.lib Ws2_32.lib
set AssetBuilderLibraries=..\windows-dependencies\lib\assimp-vc141-mt.lib

set CompilerFlags=-Od -MT -F16777216 -nologo -Gm- -GR- -EHa -W4 -WX -wd4305 -wd4127 -wd4201 -wd4189 -wd4100 -wd4996 -wd4505 -FC -Z7 -I..\src -I ..\thirdparty -I..\windows-dependencies\include %Defines%
set LinkerFlags=-opt:ref -incremental:no

REM All files are created in the build directory
IF NOT EXIST build mkdir build
pushd build

set BUILD_ASSET_BUILDER=0
set BUILD_ASSETS=0
set BUILD_PATH_TRACER=1
set RUN_PATH_TRACER=1
set BUILD_GAME_LIB=0
set BUILD_GAME_EXE=0

if "%1%"=="--build-assets" (
    set BUILD_ASSET_BUILDER=1
    set BUILD_ASSETS=1
    set BUILD_PATH_TRACER=0
    set RUN_PATH_TRACER=0
    set BUILD_GAME_LIB=0
    set BUILD_GAME_EXE=0
)

if "%1%"=="--build-path-tracer" (
    set BUILD_ASSET_BUILDER=0
    set BUILD_ASSETS=0
    set BUILD_PATH_TRACER=1
    set RUN_PATH_TRACER=1
    set BUILD_GAME_LIB=0
    set BUILD_GAME_EXE=0
)

if %BUILD_ASSET_BUILDER%==1 (
    REM Build asset builder
    ctime -begin ..\asset-builder.ctm
    cl %CompilerFlags% ..\src\asset_builder.cpp -Fe:asset-builder.exe -link %LinkerFlags% %AssetBuilderLibraries%
    ctime -end ..\asset-builder.ctm
)

if %BUILD_ASSETS%==1 (
    REM Build assets
    ctime -begin ..\assets.ctm
    asset-builder.exe
    ctime -end ..\assets.ctm
)

if %BUILD_PATH_TRACER%==1 (
    REM Build offline path tracer
    ctime -begin ..\offline_path_tracer.ctm
    cl %CompilerFlags% -O2 ..\src\offline_path_tracer.cpp -Feoffline_path_tracer.exe -link %LinkerFlags%
    ctime -end ..\offline_path_tracer.ctm
)

if %RUN_PATH_TRACER%==1 (
    REM Run offline path tracer
    offline_path_tracer.exe
)

if %BUILD_GAME_LIB%==1 (
    REM Build game_optimized
    ctime -begin ..\game_optimized.ctm
    cl %CompilerFlags% -O2 -c ..\src\game_optimized.cpp -Fogame_optimized.obj -LD
    ctime -end ..\game_optimized.ctm

    REM Copy uber_shader.glsl to build directory
    COPY ..\src\uber_shader.glsl uber_shader.glsl

    REM Build game library
    ctime -begin ..\game.ctm
    echo WATING FOR PDB > lock.tmp
    del game_*.pdb > NUL 2> NUL
    cl %CompilerFlags%  ..\src\game.cpp game_optimized.obj -LD -link -PDB:game_%random%.pdb -DLL -EXPORT:GameClientUpdate -EXPORT:GameClientFixedUpdate -EXPORT:GameServerUpdate %LinkerFlags%
    del lock.tmp
    ctime -end ..\game.ctm
)

if %BUILD_GAME_EXE%==1 (
    REM Build platform executable
    ctime -begin ..\exe.ctm
    cl %CompilerFlags% ..\src\main.cpp -Fe:project-malta.exe -link %LinkerFlags% %ExeLibraries%
    ctime -end ..\exe.ctm
)
popd
